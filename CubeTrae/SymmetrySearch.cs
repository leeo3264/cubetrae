using System;
using System.Collections.Generic;
using System.Text;

namespace CubeTrae
{
    class SymmetrySearch
    {
#if incorporate
                        case 7: // compute least even parity pos compared of 96 x 12 single moves if odd parity symmetries
                            fseq fs1= new fseq();  fs1.init( fseqLetters );
                            pos ps1= new pos();  ps1.init( fseqLetters );  ps1.resetnull();
                            foreach( ft fsi in fsread ){ fs1.add( fsi ); }  ps1.move( fs1 );
                            ps1.iniPi();
                            short edgecyclelength= ps1.pi.measure(posInfo.mm.edgecyclelength);
                            short edgecyclerestarts= ps1.pi.measure(posInfo.mm.edgerestarts);
                            short cornercyclelength= ps1.pi.measure(posInfo.mm.cornercyclelength);
                            short cornercyclerestarts= ps1.pi.measure(posInfo.mm.cornerrestarts);
                            bool ceoddparity= (edgecyclelength + edgecyclerestarts) % 2 != 0;

                            fseq minfs= new fseq();  // minfs= fs1.copy();
                            pos minpos= new pos();  minpos.init( fseqLetters );  minpos.resetmin();
                            ft minsingle= ft.ft56;
                            symposT minsp= sympos.sympos100;
                            symposT loc= sympos.sympos100;
                            byte posi;

                            if( ceoddparity ){
                            ft[] singlemoves= new ft[]{ ft.ut1, ft.ut3, ft.f1, ft.f3, ft.r1, ft.r3, ft.bp1, ft.bp3,
                                    ft.L1, ft.L3, ft.d1, ft.d3 };
                            fseq fs2= fs1.copy();
                            foreach ( ft singlemove in singlemoves ){ //" each singlemove
                            fs1= fs2.copy();  fs1.add( singlemove ); // disabled to attempt to get to match k7 code
                            ps1.resetnull();  ps1.move( fs1 );
                            /**/ loc= sympos.sympos100; // symposT.sublib.moveloc[ 0 ];
                            /**/ posi= 0;
                            while ( posi < symposT.sublib.movetour.Length ){ //" while pos in range
                                loc= loc * symposT.sublib.movetour[ posi ];
                                fs1= fs1.transform( symposT.sublib.movetour[ posi ] );
                                ps1= ps1.transform( symposT.sublib.movetour[ posi ] );
                                if( ( ((IComparable< pos >)( ps1 )).CompareTo( minpos ) == 0 // symmetry match
                                            && ((IComparable< fseq >)( fs1 )).CompareTo( minfs ) == -1 // and least move generator
                                     ) || ((IComparable< pos >)( ps1 )).CompareTo( minpos ) == -1 ){
                                    minsingle= singlemove;
                                    minpos.copy( ps1 );
                                    minfs= fs1.copy();
                                    minsp= loc;
                                }
                                posi+= 1;
                            } //" while pos in range
                            } //" each singlemove
                            } else { // evenparity
                            posi= 0;
                            while ( posi < symposT.sublib.movetour.Length ){ //" while pos in range
                                loc= loc * symposT.sublib.movetour[ posi ];
                                fs1= fs1.transform( symposT.sublib.movetour[ posi ] );
                                ps1= ps1.transform( symposT.sublib.movetour[ posi ] );
                                if ( ( ((IComparable< pos >)( ps1 )).CompareTo( minpos ) == 0 // symmetry match
                                            && ((IComparable< fseq >)( fs1 )).CompareTo( minfs ) == -1 // and least move generator
                                        ) || ((IComparable< pos >)( ps1 )).CompareTo( minpos ) == -1 ){ // or lesser position measure
                                    minpos.copy( ps1 );
                                    minfs= fs1.copy();
                                    minsp= loc;
                                }
                                posi+= 1;
                            } //" while pos in range
                            } // evenparity
                            fsread.Clear();  foreach( ft fti in minfs ){ fsread.Add( fti ); }
                            if( symmetrytransform == sympos.c_UF_c ){
                                symmetrytransform= minsp;
                                symmetrysingle= minsingle;
                            }
                            else { symmetrytransform= sympos.sympos100; }
                            break;
#endif
#if incorporate

        internal static sympos[] movetour= new sympos[]{ // movetour[ i ] takes current to next element in tour
            sympos.c_UF_c, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, sympos.g_FD_k, sympos.d_UR_b, sympos.d_UR_b, 
        sympos.d_UR_b, sympos.g_FD_k, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, sympos.k_RD_o, sympos.d_UR_b, 
        sympos.d_UR_b, sympos.d_UR_b, sympos.k_RD_o, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, sympos.o_BD_s, 
        sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, 
            
        sympos.I_c_UF_c, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, 
        sympos.g_FD_k, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, sympos.g_FD_k, sympos.d_UR_b, sympos.d_UR_b, 
        sympos.d_UR_b, sympos.k_RD_o, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, sympos.k_RD_o, sympos.d_UR_b, 
        sympos.d_UR_b, sympos.d_UR_b, sympos.o_BD_s, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, 
            
        sympos.R_c_UF_c, 
        sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, sympos.g_FD_k, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, 
        sympos.g_FD_k, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, sympos.k_RD_o, sympos.d_UR_b, sympos.d_UR_b, 
        sympos.d_UR_b, sympos.k_RD_o, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, sympos.o_BD_s, sympos.d_UR_b, 
        sympos.d_UR_b, sympos.d_UR_b, 
            
         sympos.I_c_UF_c, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, sympos.g_FD_k, 
        sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, sympos.g_FD_k, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, 
        sympos.k_RD_o, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b, sympos.k_RD_o, sympos.d_UR_b, sympos.d_UR_b, 
        sympos.d_UR_b, sympos.o_BD_s, sympos.d_UR_b, sympos.d_UR_b, sympos.d_UR_b };

        internal static sympos[] moveloc= new sympos[]{ // moveloc[ i ] current location; moveloc[ i ] * movetour[ i ] == moveloc[ i+1 ];
            sympos.sympos100, sympos.c_UF_c, sympos.d_UR_b, sympos.a_UB_a, sympos.b_UL_d, sympos.s_LD_g, sympos.t_LF_f, sympos.q_LU_e, 
        sympos.r_LB_h, sympos.n_BR_t, sympos.o_BD_s, sympos.p_BL_r, sympos.m_BU_q, sympos.j_RF_p, sympos.k_RD_o, 
        sympos.l_RB_n, sympos.i_RU_m, sympos.f_FL_l, sympos.g_FD_k, sympos.h_FR_j, sympos.e_FU_i, sympos.w_DB_w, 
        sympos.y_DR_v, sympos.u_DF_u, sympos.v_DL_x, sympos.I_v_DL_x, sympos.I_w_DB_w, sympos.I_y_DR_v, sympos.I_u_DF_u, 
        sympos.I_e_FU_i, sympos.I_f_FL_l, sympos.I_g_FD_k, sympos.I_h_FR_j, sympos.I_l_RB_n, sympos.I_i_RU_m, sympos.I_j_RF_p, 
        sympos.I_k_RD_o, sympos.I_p_BL_r, sympos.I_m_BU_q, sympos.I_n_BR_t, sympos.I_o_BD_s, sympos.I_t_LF_f, sympos.I_q_LU_e, 
        sympos.I_r_LB_h, sympos.I_s_LD_g, sympos.I_d_UR_b, sympos.I_a_UB_a, sympos.I_b_UL_d, sympos.I_c_UF_c, sympos.IR_c_UF_c, 
        sympos.IR_d_UR_b, sympos.IR_a_UB_a, sympos.IR_b_UL_d, sympos.IR_s_LD_g, sympos.IR_t_LF_f, sympos.IR_q_LU_e, sympos.IR_r_LB_h, 
        sympos.IR_n_BR_t, sympos.IR_o_BD_s, sympos.IR_p_BL_r, sympos.IR_m_BU_q, sympos.IR_j_RF_p, sympos.IR_k_RD_o, sympos.IR_l_RB_n, 
        sympos.IR_i_RU_m, sympos.IR_f_FL_l, sympos.IR_g_FD_k, sympos.IR_h_FR_j, sympos.IR_e_FU_i, sympos.IR_w_DB_w, sympos.IR_y_DR_v, 
        sympos.IR_u_DF_u, sympos.IR_v_DL_x, sympos.R_v_DL_x, sympos.R_w_DB_w, sympos.R_y_DR_v, sympos.R_u_DF_u, sympos.R_e_FU_i, 
        sympos.R_f_FL_l, sympos.R_g_FD_k, sympos.R_h_FR_j, sympos.R_l_RB_n, sympos.R_i_RU_m, sympos.R_j_RF_p, sympos.R_k_RD_o, 
        sympos.R_p_BL_r, sympos.R_m_BU_q, sympos.R_n_BR_t, sympos.R_o_BD_s, sympos.R_t_LF_f, sympos.R_q_LU_e, sympos.R_r_LB_h, 
        sympos.R_s_LD_g, sympos.R_d_UR_b, sympos.R_a_UB_a, sympos.R_b_UL_d, sympos.R_c_UF_c };
#endif
    }
}
