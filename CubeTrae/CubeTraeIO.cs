using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CubeTrae
{
    public partial class CubeTraeIO : Form
    {
        public CubeTraeIO( System.IO.TextReader st )
        {
            InitializeComponent();

            string processline;
            parse.displayind di;
            List< pm > reading;
            List< int > numsreading; List< string > strsreading;
            List< ft > moves;
            //position pos;
            StringBuilder sb= new StringBuilder();

            if( st != null ){ while( st.Peek() != -1 ){
                processline= st.ReadLine();

                singmaster.resetorderoverride();
                di= new parse.displayind();

                reading= parse.parse1( processline, out numsreading, out strsreading );
                singmaster.resetorderoverride();
                moves= parse.parse2( reading, numsreading, strsreading, out di );
                parse.parse3( ref moves, ref reading, numsreading, strsreading, ref di );
                parse.parseK( ref moves, ref reading, numsreading, strsreading, ref di );
            } }
            if (st != null) { st.Close(); }
        }

        position lastpos; // hold last reading for tests
        private void genButton_Click(object sender, EventArgs e)
        {
            List< int > numsreading;  List< string > strsreading;
            if( parse.remcomment( inTextBox.Text ).Trim().ToLower() == "i" ){ // give info message
                outTextBox.Text= Program.info;
                return;
            }
            List< pm > reading= parse.parse1( inTextBox.Text, out numsreading, out strsreading );
            singmaster.resetorderoverride();
            parse.displayind di= new parse.displayind();
            List< ft > moves= parse.parse2( reading, numsreading, strsreading, out di );
            parse.parse3( ref moves, ref reading, numsreading, strsreading, ref di );
            parse.parseK( ref moves, ref reading, numsreading, strsreading, ref di );

            StringBuilder s1= new StringBuilder();
            s1.Append( di.prefix() );
            foreach( ft fti in moves ){
                   s1.Append( ftT.EnumString( fti ) );  s1.Append( " " ) ; }
            s1.Append( di.suffix() );

            parse.parselist( ref moves );
            position pos= new position();
            StringBuilder s= new StringBuilder();

            pos.init();
            pos.resetnull();
            s.Append( di.prefix() );
            foreach( ft fti in moves ){
                pos.move( fti );
                if( !di.displayfacechaseformat ){
                    s.Append( ftT.EnumString( fti ) );  s.Append( " " ) ; }
            }
            if( di.displayfacechaseformat ){
                s.Append( parse.toFaceChase( moves ) );
            }
            s.Append( di.suffix() );
            if( di.displayfacechaseformat ){
                ReadingInterpretedTextBox.Text= s.ToString();
            } else {
                ReadingInterpretedTextBox.Text= s1.ToString(); // replaced from s to show tokens [ , :: and such
            }
            outTextBox.Text= pos.ToString();
            lastpos= pos;
        }

        private void CubeTraeIO_SizeChanged(object sender, EventArgs e)
        {
            inTextBox.SetBounds( inTextBox.Left, inTextBox.Top, this.Size.Width-35, inTextBox.Height );
            genButton.SetBounds( this.Size.Width-62, genButton.Top, genButton.Size.Width, genButton.Size.Height );
            ReadingInterpretedTextBox.SetBounds( ReadingInterpretedTextBox.Left, ReadingInterpretedTextBox.Top,
                this.Size.Width-35, ReadingInterpretedTextBox.Height );
            outTextBox.SetBounds( outTextBox.Left, outTextBox.Top, this.Size.Width-35, outTextBox.Height );
        }

        internal bool hasancestors= false;
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CubeTraeIO cio= new CubeTraeIO( null );
            cio.hasancestors= false;
            this.hasancestors= true;
            cio.Show();
        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
#if test_charform
            StringBuilder s= new StringBuilder();
            byte k= 0;
            for( int i= 32; i < 4095; i++ ){
               if( char.IsDigit( (char)( i ) ) ){
                    k+= 1;
                    s.Append( ((char)( i )) );
                    if( k == 40 ){ break; }
                }
            }
            outTextBox.Text= s.ToString();
#endif
#if test_numerallead
            StringBuilder s= new StringBuilder();
            for ( pm pmi= pm.ut; pmi < pm.pm56; pmi++ ){
                if ( parsing.prefixint[(byte)(pmi)] ){
                    s.Append( parsing.pm_string( pmi ) );  s.Append( ' ' ); // was pm_.ToString()); s.Append(' ');
                }
            }
            parsing.test();
            tokenReadingTextBox.Text= s.ToString();
#endif
#if test_tokenclass_
            StringBuilder s= new StringBuilder();
            pmT pmTa= new pmT();
            for ( pmSub pmsi= pmSub.noop; pmsi < pmSub.pmSub16; pmsi++ ){
                s.AppendLine();
                s.Append( " ----- " ) ;  s.Append( pmsi.ToString() );  s.Append( " ----- " );
                s.AppendLine();
            //for ( pm pmi= pm.ut; pmi < pm.pm56; pmi++ ){ pmTa= pmi;
            //    if ( (pmSub)( pmTa ) == pmsi ){
            //        s.Append( pmi.ToString() );  s.Append( " " ) ;
            //    }
            for( pm pmi= pm.ut; pmi < pm.pm56; pmi++ ){
                if( pmT.pm_pmSub( pmi ) == pmsi ){
                    s.Append( pmi.ToString() );  s.Append( " " ) ;
                }
            }} // \ for loop pmi \ for loop pmsi
            outTextBox.Text= s.ToString();
#endif
#if results
 ----- noop ----- posmark whitespace
 ----- singmaster ----- ut ll ff rr bp dd  ----- doubl ----- uw lw fw rw bw dw
 ----- slice ----- ee mm ss                ----- invertedslice ----- ei mi si 
 ----- whole ----- yy xx zz                ----- invertedwhole ----- yi xi zi 
 ----- modify ----- inverted widen         ----- memget ----- get 
 ----- memset ----- hold                   ----- retrieve ----- getq enumb 
 ----- command ----- command               ----- reading -----  facechase commandSym 
 ----- display ----- posmarkq buffer       ----- transform ----- transform transformq 
 ----- token ----- t1 t2 t3 t4 t5 t6 t7    ----- numeral ----- n0 n1 n2 n3 n4 n5 n6 n7 n8 n9 
#endif
            parse.test();
            drT.test();
            ftT.test();
            if( (object)( lastpos ) != (object)( null ) ){
                measure mz = new measure( lastpos.es, lastpos.cs );
            }
        }

        private void parseToolStripMenuItem_Click( object sender, EventArgs e )
        {
            List< int > numsreading;  List< string > strsreading;
            List< pm > reading= parse.parse1( inTextBox.Text, out numsreading, out strsreading );
            parse.displayind di;
            List< ft > moves= parse.parse2( reading, numsreading, strsreading, out di );
            parse.parse3( ref moves, ref reading, numsreading, strsreading, ref di );
            StringBuilder s= new StringBuilder();
            int i= 0;
            foreach ( pm pmr in reading ){
                s.Append( CubeTrae.parse.pm_string( pmr ) );
                if( parse.readAsStref[ (byte)( pmr ) ] ){
                    s.Append( " \\" );  s.Append( strsreading[ numsreading[ i ] ] );  s.Append( "/ " ); }
                else if( pmT.pm_pmSect( pmr ) == pmSect.puncSym ){ 
                    s.Append( "^" ) ;  s.Append( numsreading[ i ] );  s.Append( ' ' ); }
                else if( numsreading[ i ] == int.MaxValue ){ s.Append( " <> " ); }
                else if( numsreading[ i ] != int.MinValue ){
                    s.Append( " (" );  s.Append( numsreading[ i ] );  s.Append( ") " );
                } else { s.Append( " () " ); }
                i++;
            }
            ReadingInterpretedTextBox.Text= s.ToString();

            s.Remove( 0, s.Length ); // clear _s_
            foreach( ft fti in moves ){
                s.Append( ftT.EnumString( fti ) );  s.Append( " " ) ; }

            outTextBox.Text= s.ToString();
        }

        private void pasteListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string input= System.Windows.Forms.Clipboard.GetText() + "\r\n";
            string processline;
            StringBuilder output= new StringBuilder();
            int linestart= 0;  int linecount= 0;
            bool pasteintoformula= inTextBox.Text.Contains( "^" );
            int slashR= 0;

            List< pm > reading;
            List< int > numsreading; List< string > strsreading;
            List< ft > moves;
            position pos;
            StringBuilder s= new StringBuilder();

            singmaster.resetorderoverride();
            parse.displayind di= new parse.displayind();
            // search for first linebreak
            for( int i= 0; i < input.Length; i++ ){
            if( input[ i ] == '\n' || i == input.Length-1 ){ // line break
                if( i > 1 && input[ i-1 ] == '\r' ){ slashR= 1; } else { slashR= 0; }
                s.Remove( 0, s.Length );
                if( pasteintoformula ){
                    processline= inTextBox.Text.Replace( "^", parse.remcomment( input.Substring( linestart, i - linestart - slashR ) ) );
                } else {
                    processline= input.Substring( linestart, i - linestart - slashR );
                }
                reading= parse.parse1( processline, out numsreading, out strsreading );
                singmaster.resetorderoverride();
                moves= parse.parse2( reading, numsreading, strsreading, out di );
                parse.parse3( ref moves, ref reading, numsreading, strsreading, ref di );
                pos= new position();

                pos.init();
                pos.resetnull();
                s.Append( di.prefix() );
                foreach ( ft fti in moves ){
                    pos.move( fti );
                    s.Append( ftT.EnumString( fti ) );  s.Append(" ");
                }
                s.Append( di.suffix() );
                //ReadingInterpretedTextBox.Text= s.ToString();
                pos.shortreport= s.ToString();
                output.Append( pos.ToString() );
                output.AppendLine();

                linestart= i;
                linestart+= 1;  linecount+= 1;
            }


                //if( ps.ToString().Length < 16 ){
                //    output.Append( ps.ToString() );  output.Append( ' ' ); }
                //output.Append( fs.ToString() );  output.Append( " (" );
                //output.Append( fs.measure( fseq.mm.faceturns ).ToString() );
                //output.Append( "." );
                //output.Append( fs.measure( fseq.mm.doublefaceturns ).ToString() );
                //if( fs.optimalmark ){ output.Append( "f*)" ); } else { output.Append( "f)" ); }
            }
            System.Windows.Forms.Clipboard.SetDataObject( output.ToString() ); // perhaps if linecount == 1, set moveTextbox
            //textBoxGen.Text= "";
            outTextBox.Text= linecount.ToString() + " lines";
            //memCountButton.Text= fs.memory.Count.ToString();
            //memCountButton.Enabled= fs.memory.Count > 0;
            //memCountLabel.Enabled= fs.memory.Count > 0;
        }

        private void remCommentsTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            outTextBox.Text= parse.remcomment( inTextBox.Text );
        }

        private void copyToTopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            inTextBox.Text= ReadingInterpretedTextBox.Text;
        }

        private List< string > entered= new List< string >();
        private short enteredpoint= -1;

        private void CubeTraeIO_KeyDown(object sender, KeyEventArgs e)
        {
            if( e.KeyData == Keys.Return ){
                if( inTextBox.Text != "" && ( 
                        entered.Count == 0 || inTextBox.Text != entered[ enteredpoint ] ) ){
                    entered.Add( inTextBox.Text );  enteredpoint= (short)( entered.Count-1 );
                }
                genButton_Click( sender, e );
                inTextBox.Text= "";
                
            }
            else if( e.KeyData == Keys.Back && inTextBox.Text == "" && entered.Count == 0 ){
                inTextBox.Text= "i";
                genButton_Click( sender, e );
                inTextBox.Text= "";
            }
            else if( entered.Count > 0 && e.KeyData == Keys.Left && inTextBox.SelectionStart == 0 ){
                if( enteredpoint == 0 && inTextBox.Text == "" ){ inTextBox.Text= entered[ 0 ]; }
                else if( enteredpoint == 0 ){ inTextBox.Text= ""; }
                else if( inTextBox.Text == "" ){ inTextBox.Text= entered[ enteredpoint ]; }
                else if( inTextBox.Text == entered[ enteredpoint ] ){ enteredpoint-= 1;
                    inTextBox.Text= entered[ enteredpoint ]; }
            }
            else if( entered.Count > 0 && e.KeyData == Keys.Right && inTextBox.SelectionStart == inTextBox.Text.Length ){
                if( enteredpoint == entered.Count-1 && inTextBox.Text == "" ){ 
                    inTextBox.Text= entered[ entered.Count-1 ];
                    inTextBox.SelectionStart= inTextBox.Text.Length;
                }
                else if( enteredpoint == entered.Count-1 ){ inTextBox.Text= ""; }
                else if( inTextBox.Text == "" ){ inTextBox.Text= entered[ enteredpoint ]; }
                else if( inTextBox.Text == entered[ enteredpoint ] ){ enteredpoint+= 1;
                    inTextBox.Text= entered[ enteredpoint ];
                    inTextBox.SelectionStart= inTextBox.Text.Length;
                }
            }
            else if( entered.Count > 0 && e.KeyData == Keys.Up && inTextBox.SelectionStart == 0 ){
                if( inTextBox.Text != entered[ enteredpoint ] ){
                    entered.Add( inTextBox.Text );  enteredpoint= (short)( entered.Count-1 ); }
                inTextBox.Text= ReadingInterpretedTextBox.Text;
            }
        }

        private void inTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            CubeTraeIO_KeyDown( sender, e );
        }

        private void CubeTraeIO_FormClosing(object sender, FormClosingEventArgs e)
        {
            if( this.hasancestors ){ // if opened child forms, ask to exit potentially destroying child forms
                if( MessageBox.Show( "Do you want to exit?", "CubeTrae",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question )
                        == DialogResult.No ){
                    e.Cancel= true;
            }}
        }
    } // cubeTracIO.form
} // namespace CubeTrae