//#define report
//#undef report

using System;
using System.Collections.Generic;
using System.Text;
//using System.Runtime.Serialization.Json;

namespace CubeTrae
{
    /// <summary>
    /// Json types excluding real and integer numbers
    /// </summary>
    public class jsonv{

        public enum jsonTokenType : byte { None= 0, StartObject, EndObject, StartArray, EndArray, PropertyName,
            String, Number, True, False, Null, Comment }
        public enum jsonValueType : byte { Undefined= 12, Object, Array, String, Number, True, False, Null, Error }
        public enum jsonParseState : byte { ElementValue= 21, Error, Object, ObjectKeyString, ObjectExpectingColon, ObjectFinish,
            Array, ArrayContinue, String, Int, Token, Finish, Complete }
        public enum jsonParseExceptionErrorType : byte { unexpectedChar= 31, duplicateKeys, undefinedToken, Unsupported, EOLinstringliteral, 
            DepthLimitExceeded }

        jsonValueType jvt= jsonValueType.Undefined;
        object contents= null;

        public jsonv this[ string s ]{ get {
            if( jvt == jsonValueType.Object ){
                Dictionary< string, jsonv > contentsO= (Dictionary< string, jsonv >)( contents );
                if( contentsO.ContainsKey( s ) ){ return contentsO[ s ]; } else { return null; }
            } else return null; }
        }
        public object this[ int i ]{ get {
            if( jvt == jsonValueType.Array ){
                List< object > contentsA= (List< object >)( contents );
                if (i > 0 && i < contentsA.Count ) { return contentsA[ i ]; } else { return null; }
            } else return null; }
        }

        public Dictionary< string, jsonv >.KeyCollection keys{ get{
            if( jvt == jsonValueType.Object ){
                Dictionary< string, jsonv > contentsO= (Dictionary< string, jsonv >)( contents );
                return contentsO.Keys;
            } else return null; }
        }

        public string asString{ get{
            if( jvt == jsonValueType.String ){
                return (string)( contents );
            } else return null; }
        }
        public object[] asArray{ get{
            if( jvt == jsonValueType.Array ){
                return ((List< object >)( contents )).ToArray();
            } else return null; }
        }

        public jsonv Read( System.IO.TextReader s, short maxdepth ){
            jsonv[] buildStk= new jsonv[ maxdepth ];  buildStk[ 0 ]= new jsonv();  short ibuildStk= 0;
            string[] buildObjKeyStk= new string[ maxdepth ];  short ibuildObjKeyStk= 0;
            StringBuilder buildstring= new StringBuilder();  bool buildstringescape= false;
            //int buildint= 0;

            jsonParseState[] jsonPS= new jsonParseState[ maxdepth+1 ];
            jsonPS[ 0 ]= jsonParseState.Complete;  short ijsonPS= 1;  jsonPS[ ijsonPS ]= jsonParseState.ElementValue;

            StringBuilder rewind= new StringBuilder();  char ch;  int rewindpos= 0;
            bool unfinishedParse= true;  while( unfinishedParse ){
                if( s.Peek() == -1 ){ unfinishedParse= false;  rewind.Append( ' ' ); } 
                else { ch= (char)( s.Read() );  rewind.Append( ch ); }
            while( rewindpos < rewind.Length ){ ch= rewind[ rewindpos ];  rewindpos+= 1;

            switch( jsonPS[ ijsonPS ] ){
            case jsonParseState.ElementValue:
                if( char.IsWhiteSpace( ch ) ){ continue; }
                //if( char.IsNumber( ch ) ){ readingJsonParseState[ readingJsonParseState.Count-1 ]= jsonParseState.Int;  
                //    buildint= (byte)( char.GetNumericValue( ch ) );  continue; }
                if( char.IsLetter( ch ) ){ jsonPS[ ijsonPS ]= jsonParseState.Token;
                    buildstring.Remove( 0, buildstring.Length );  buildstring.Append( ch );
                    continue; }
                switch( ch ){
                case '\"':
                    buildstring.Remove( 0, buildstring.Length );
                    buildStk[ ibuildStk ].jvt= jsonValueType.String;
                    buildStk[ ibuildStk ].contents= null;
                    jsonPS[ ijsonPS ]= jsonParseState.String;  break;
                case '{':
                    buildstring.Remove( 0, buildstring.Length );
                    buildStk[ ibuildStk ].jvt= jsonValueType.Object;
                    buildStk[ ibuildStk ].contents= new Dictionary< string, jsonv >();
                    jsonPS[ ijsonPS ]= jsonParseState.Object;  break;
                 case '[':
                    buildStk[ ibuildStk ].jvt= jsonValueType.Array;
                    buildStk[ ibuildStk ].contents= new List< object >();
                    jsonPS[ ijsonPS ]= jsonParseState.Array;  break;
                }
                break;
            case jsonParseState.Token:
                if( char.IsLetterOrDigit( ch ) ){ buildstring.Append( ch ); }
                else { rewind.Append( ch );
                    switch( buildstring.ToString() ){
                    case "true": 
                        buildStk[ ibuildStk ].jvt= jsonValueType.True;
                        buildStk[ ibuildStk ].contents= true;  break;
                    case "false": 
                        buildStk[ibuildStk].jvt= jsonValueType.False;
                        buildStk[ ibuildStk ].contents= false;  break;
                    case "null":
                        buildStk[ ibuildStk ].jvt= jsonValueType.Null;
                        buildStk[ ibuildStk ].contents= null; break;
                    default: 
                        buildStk[ ibuildStk ].jvt= jsonValueType.Error;
                        buildStk[ ibuildStk ].contents= jsonParseExceptionErrorType.undefinedToken;  break;
                    }
                    jsonPS[ ijsonPS ]= jsonParseState.Finish;
                }
                break;
            case jsonParseState.String:
                switch( ch ){
                case '\"':
                    if( buildstringescape ){ buildstring.Append( ch );  buildstringescape= false;  continue; }
                    buildStk[ ibuildStk ].jvt= jsonValueType.String;
                    buildStk[ ibuildStk ].contents= buildstring.ToString();
                    jsonPS[ ijsonPS ]= jsonParseState.Finish;
                    continue;
                case '\\':
                    if( buildstringescape ){ buildstring.Append( ch );  buildstringescape= false;  continue; }
                    else{ buildstringescape= true;  continue; }
                default:  buildstring.Append( ch );  break;
                }
                break;
            case jsonParseState.Array:
                if( char.IsWhiteSpace( ch ) ){ continue; }
                if( ch == ']' ) { goto case jsonParseState.Finish; }
                jsonPS[ ijsonPS ]= jsonParseState.ArrayContinue;
                ijsonPS+= 1;
                jsonPS[ ijsonPS ]= jsonParseState.ElementValue;    
                ibuildStk+= 1;  buildStk[ ibuildStk ]= new jsonv();
                goto case jsonParseState.ElementValue;
            case jsonParseState.ArrayContinue:
                if( char.IsWhiteSpace( ch ) ){ continue; }
                if( ch == ']' ) { goto case jsonParseState.Finish; }
                if( ch == ',' ) {
                    ijsonPS+= 1;
                    jsonPS[ ijsonPS ]= jsonParseState.ElementValue;
                    ibuildStk+= 1;  buildStk[ ibuildStk ]= new jsonv();
                    continue;
                }
                break;

            case jsonParseState.Object:
                if( char.IsWhiteSpace( ch ) ){ continue; }
                switch( ch ){
                case '}':
                    jsonPS[ ijsonPS ]= jsonParseState.Finish;
                    continue;
                case '\"':
                    jsonPS[ ijsonPS ]= jsonParseState.ObjectKeyString;
                    continue;
                default:
                    buildStk[ ibuildStk ].jvt= jsonValueType.Error;
                    buildStk[ ibuildStk ].contents= jsonParseExceptionErrorType.unexpectedChar;
                    jsonPS[ ijsonPS ]= jsonParseState.Finish;
                    break;
                }
                break; 
            case jsonParseState.ObjectKeyString:
                switch( ch ){
                case '\"':
                    if( buildstringescape ){ buildstring.Append( ch );  buildstringescape= false;  continue; }
                    buildObjKeyStk[ ibuildObjKeyStk ]= buildstring.ToString();  ibuildObjKeyStk+= 1;
                    jsonPS[ ijsonPS ]= jsonParseState.ObjectExpectingColon;
                    continue;
                case '\\':
                    if( buildstringescape ){ buildstring.Append( ch );  buildstringescape= false;  continue; }
                    else{ buildstringescape= true;  continue; }
                default:  buildstring.Append( ch );  break;
                }
                break;
            case jsonParseState.ObjectExpectingColon:
                if( char.IsWhiteSpace( ch ) ){ continue; }
                if( ch == ':' ){
                    jsonPS[ ijsonPS ]= jsonParseState.ObjectFinish;  ijsonPS+= 1;
                    jsonPS[ ijsonPS ]= jsonParseState.ElementValue;
                    ibuildStk+= 1;  buildStk[ ibuildStk ]= new jsonv();
                    continue;
                } else {
                    jsonPS[ ijsonPS ]= jsonParseState.Error;
                    buildStk[ ibuildStk ].jvt= jsonValueType.Error;
                    buildStk[ ibuildStk ].contents= jsonParseExceptionErrorType.unexpectedChar;
                    continue;
                }
                // break; // unreachable
            case jsonParseState.ObjectFinish:
                if( char.IsWhiteSpace( ch ) ){ continue; }
                if( ch == '}' ){ goto case jsonParseState.Finish; }
                if( ch == ',' ){
                    buildstring.Remove( 0, buildstring.Length );
                    jsonPS[ ijsonPS ]= jsonParseState.Object;  continue;
                }
                break;
            case jsonParseState.Finish:
                if( jsonPS[ ijsonPS-1 ] == jsonParseState.ObjectFinish ){
                    ijsonPS-= 1;
                    ibuildStk-= 1;
                    ibuildObjKeyStk-= 1;
                    ( (Dictionary< string, jsonv > )( buildStk[ ibuildStk ].contents ))
                        .Add( buildObjKeyStk[ ibuildObjKeyStk ], buildStk[ ibuildStk+1 ] );
                    if( ch == '}' ){ continue; }else{ goto case jsonParseState.ObjectFinish; }
                } else if( jsonPS[ ijsonPS-1 ] == jsonParseState.ArrayContinue ){
                    ijsonPS-= 1;
                    ibuildStk-= 1;
                    ((List< object >)( buildStk[ ibuildStk ].contents )).Add( buildStk[ ibuildStk+1 ].contents );
                    if( ch == ']' ){ ch= ' '; } // fix?
                    goto case jsonParseState.ArrayContinue;
                }
                ijsonPS-= 1;  continue;
            case jsonParseState.Complete:
                if( char.IsWhiteSpace( ch ) ){ continue; }
                if( char.IsSeparator( ch ) ){ continue; }
                    buildStk[ ibuildStk ].jvt= jsonValueType.Error;
                    buildStk[ ibuildStk ].contents= jsonParseExceptionErrorType.unexpectedChar;
                break;
            } //\ switch jsonPS[ ijsonPS ]
            } //\ while rewindpos < rewind.Length
                rewind.Remove( 0, rewind.Length );  rewindpos= 0;
            } //\ while( incompleteParse );
            jvt= buildStk[ 0 ].jvt;
            contents= buildStk[ 0 ].contents;
            return this; // was buildStk[ 0 ]
        } //\ Read jsonInt // method
    } // jsonInt class
    public class lettersystemcatalog{
        public short serialno;
        public string name;
        public string assignments;
        public lettersystemcatalog prev;
        public lettersystemcatalog( short serialG, string nameG, string assignmentsG ){
            serialno= serialG;  name= nameG;  assignments= assignmentsG;  prev= null;
        }

        enum extractnameserialParsestate{ skip, s, s_equals, quoted };
        public void add( string assignments ){
            System.Text.StringBuilder namebuild= new StringBuilder();
            byte serial= 255;
            extractnameserialParsestate spp= extractnameserialParsestate.skip;
            // parse for nameG and assignments
            foreach( char ch in assignments ){ // extract lettersystem name and serial number
            switch( spp ){
            case extractnameserialParsestate.skip:
                if( ch == '\"' ){ // beginning of lettersystem name
                    namebuild.Remove( 0, namebuild.Length );
                    spp= extractnameserialParsestate.quoted; }
                if( ch == 's' ){
                    spp= extractnameserialParsestate.s;
                }
                continue;
            case extractnameserialParsestate.quoted: // closing quotation
                if( ch == '\"' ){ spp= extractnameserialParsestate.skip;  continue; }
                namebuild.Append( ch );
                continue;
            case extractnameserialParsestate.s:
                if( ch == '=' ){ spp= extractnameserialParsestate.s_equals; }
                continue;
            case extractnameserialParsestate.s_equals: // limitation: currently only one-character serial-number names are permitted
                if( char.IsDigit( ch )
                  && byte.TryParse( ch.ToString(), out serial ) ){
                  spp=extractnameserialParsestate.skip; }
                spp= extractnameserialParsestate.skip;
                continue;
            }
            }
            if( serial != 255 && namebuild.Length > 0 ){
                add( serial, namebuild.ToString(), assignments );
            }
        }
        public void add( short serialG, string nameG, string assignmentsG ){ // currently multiple lettersystems with the same serial will
            lettersystemcatalog cur= this;                                   // \ only permit the last lettersystem assigned to be active
            while( (object)( cur.prev ) != (object)( null ) ){ cur= cur.prev; }
            cur.prev= new lettersystemcatalog( serialG, nameG, assignmentsG );
        }
        public lettersystemcatalog retrieve( short serialG ){
            lettersystemcatalog current= this;
            while( current != null ){
                if( current.serialno == serialG ){ return current; }
                current= current.prev;
            }
            return null;
        }
    }

    public static class catalog
    {
        public static lettersystemcatalog lscatalog;
        public static SortedList< conjugatestudyentry, conjugatestudy > cstudy= 
            new SortedList< conjugatestudyentry, conjugatestudy>();
        public static jsonv associations;
        public class entrybuild{
            entry objt= new entry();
            public string type;
            public entrybuild(){
                objt.init();
            }

            public bool centerfacetwist{ set{
                objt.centerfacetwist= value; }}
            public bool cuberotation{ set{
                objt.cuberotation= value; }}
            byte efpos= 0;
            byte cfpos= 0;

            public void insert( ef efi ){
                if( objt.outofrange ){ return; }
                if( efpos >= entry.maxlen ){ objt.outofrange= true;  return; }
                objt.efs[ efpos ]= efi;  efpos+= 1;
            }

            public void insert( cf cfi ){
                if( objt.outofrange ){ return; }
                if( cfpos >= entry.maxlen ){ objt.outofrange= true;  return; }
                objt.cfs[ cfpos ]= cfi;  cfpos+= 1;
            }

            public entry ToEntry(){
                objt.type= type;
                return objt; }

            public void rotate(){
                byte count;
                if( objt.ecount > 2 ){ // e rotates if both have rotation
                    count= 0;  ef hold= objt.efs[ 0 ];
                    while( count < objt.ecount-1 ){
                        objt.efs[ count ]= objt.efs[ count + 1 ];  count+= 1;
                    } objt.efs[ objt.ecount-1 ]= hold;
                }
                else if( objt.ccount > 2 ){
                    count = 0;  cf hold= objt.cfs[ 0 ];
                    while( count < objt.ccount-1 ){
                        objt.cfs[ count ]= objt.cfs[ count + 1 ];  count+= 1;
                    } objt.cfs[ objt.ccount-1 ]= hold;
                }
            }

            public void fliptwist(){
                byte count;
                if( objt.ecount > 1 ){
                    count= 0;  while( count < objt.ecount ){
                        objt.efs[ count ]= ssT.eOBV( objt.efs[ count ] );  count+= 1;
                    }
                } else if( objt.ccount > 1 ){
                    count= 0;  while( count < objt.ccount ){
                        objt.cfs[ count ]= ssT.cCL( objt.cfs[ count ] );  count+= 1;
                    }
                }
            }
                    
        } // entrybuild

        // moves and type value
        public struct moves_ty{
            public enum augm{ L, R, Ri, S2, S3 }
            public ft[] moves;
            public string type;
            public override string ToString(){
                StringBuilder sb= new StringBuilder();
                foreach( ft fti in moves ){
                    sb.Append( ((ftT)( fti )).ToString() );  sb.Append( ' ' );
                }
                sb.Append( "// " );
                sb.Append( type );
                sb.AppendLine();
                return sb.ToString();
            }
            public moves_ty setup( moves_ty given, augm setup_type, ft addend ){
                moves_ty res= new moves_ty();  res.type= given.type;
                short locofLbrack= -1;
                short innerLbrack= -1;
                short locofdoublecolon= -1;
                List< ft > readgivenmoves= new List<ft>();
                ft Lmove= ft.ft74;  short locofLmove= -1;
                ft Rmove= ft.ft74;  short locofRmove= -1;

                short positcount= 0;
                foreach( ft fti in given.moves ){ readgivenmoves.Add( fti );
                    if( fti == ft.ps1 && locofLbrack == -1 ){ locofLbrack= positcount; }
                    else if( fti == ft.ps1 && locofLbrack != -1 && innerLbrack == -1 ){
                        innerLbrack= positcount; }
                    else if( fti == ft.ps5 && locofdoublecolon == -1 ){ locofdoublecolon= positcount; }
                    else if( fti >= ft.ut1 && fti <= ft.s3 ){
                        if( Lmove == ft.ft74 ){ Lmove= fti;  locofLmove= positcount; }
                        else{ Rmove= fti;  locofRmove= positcount; }
                    }
                    positcount+= 1;
                } // ps1, ps2, ps3, ps4, ps5, // [ ] : , :: 

                switch( setup_type ){
                case augm.L: 
                    if( locofdoublecolon == -1 && locofLmove != -1 && innerLbrack == -1 ){
                        readgivenmoves.Insert( locofLmove, ft.ps5 );
                        readgivenmoves.Insert( locofLmove, Lmove );
                        res.type= "su_L " + res.type;
                    } else if( locofdoublecolon == -1 && locofLmove != -1 && innerLbrack != -1 ){
                        readgivenmoves.Insert( innerLbrack, ft.ps5 ); // [ [ F , ... -> [ F :: [ F , ...
                        readgivenmoves.Insert( innerLbrack, Lmove );
                        res.type= "su_L " + res.type; }
                    break;
                case augm.R:
                    if( locofdoublecolon == -1 && locofLmove != -1 && locofRmove != -1 && innerLbrack != -1 ){
                        readgivenmoves.Insert( innerLbrack, ft.ps5 );
                        readgivenmoves.Insert( innerLbrack, Rmove );
                        res.type= "su_R " + res.type;
                    } else if( locofdoublecolon == -1 && locofRmove != -1 && locofRmove != -1 ){
                        readgivenmoves.Insert( locofLmove, ft.ps5 );
                        readgivenmoves.Insert( locofLmove, Rmove );
                        res.type = "su_R " + res.type;
                    }
                    break;
                case augm.Ri:
                    if( locofdoublecolon == -1 && locofLmove != -1 && locofRmove != -1 && innerLbrack != -1 ){
                        readgivenmoves.Insert( innerLbrack, ft.ps5 ); // insert ::
                        readgivenmoves.Insert( innerLbrack, ftT.invert( Rmove ) );
                        res.type= "su_R " + res.type;
                    } else if( locofdoublecolon == -1 && locofRmove != -1 && locofRmove != -1 ){
                        readgivenmoves.Insert( locofLmove, ft.ps5 );
                        readgivenmoves.Insert( locofLmove, ftT.invert( Rmove ) );
                        res.type = "su_R " + res.type;
                    }
                    break;
                case augm.S2:
                    if( locofdoublecolon == -1 && locofLmove != -1 && innerLbrack != -1 && locofLbrack != -1 ){
                        readgivenmoves.Insert( innerLbrack, ft.ps5 );
                        readgivenmoves.Insert( innerLbrack, addend );
                    } else if( locofdoublecolon == -1 && locofLmove != -1 ){ // adding '::' clause where one does not already appear, before locofLmove
                        readgivenmoves.Insert( locofLmove, ft.ps5 );
                        readgivenmoves.Insert( locofLmove, addend );
                    }
                    res.type= "su_2 " + res.type;
                    break;
                case augm.S3:
                    readgivenmoves.Insert( locofLmove, addend );
                    break;
                }
                res.moves= readgivenmoves.ToArray();
                return res;
            }
            public List< ft > toListFt(){
                List< ft > res= new List< ft >( moves.Length );
                foreach( ft fti in moves ){ res.Add( fti ); }
                return res;
            }
        }
        public struct entry: System.Collections.Generic.IComparer< CubeTrae.catalog.entry >,
                System.IComparable< CubeTrae.catalog.entry >{
            internal const byte maxlen= 5;
            internal bool outofrange;
            internal bool centerfacetwist;
            internal bool cuberotation;
            internal ef[] efs;
            internal cf[] cfs;
            internal string type;
            public void init(){ efs= new ef[ maxlen ];  cfs= new cf[ maxlen ];
                outofrange= false;
                centerfacetwist= false;
                cuberotation= false;
                for( byte i= 0; i < maxlen; i++ ){ efs[ i ]= ef.ef24;  cfs[ i ]= cf.cf24; }
            }
            int System.IComparable< CubeTrae.catalog.entry >.CompareTo( CubeTrae.catalog.entry other ){
                if( outofrange && other.outofrange ){ return 0; }
                if( outofrange != other.outofrange ){ return outofrange ? 1 : -1; }
                if( centerfacetwist != other.centerfacetwist ){ return centerfacetwist ? -1 : 1; }
                if( cuberotation != other.cuberotation ){ return cuberotation ? -1 : 1; }
                for( byte i= 0; i < maxlen; i++ ){
                    if( efs[ i ] == other.efs[ i ] ){
                        if( efs[ i ]== ef.ef24 ){ break; } }
                    else { return catalog.catalogename[ (byte)( efs[ i ] ) ].CompareTo( catalog.catalogename[ (byte)( other.efs[ i ] ) ] ); }
                }
                for( byte i= 0; i < maxlen; i++ ){
                    if( cfs[ i ] == other.cfs[ i ] ){
                        if( cfs[ i ]== cf.cf24 ){ return 0; } }
                    else { return catalog.catalogcname[ (byte)( cfs[ i ] ) ].CompareTo( catalog.catalogcname[ (byte)( other.cfs[ i ] ) ] ); }
                }
                return 0;
            }

            int System.Collections.Generic.IComparer< CubeTrae.catalog.entry >.Compare
                    ( CubeTrae.catalog.entry L, CubeTrae.catalog.entry R)
            {
                if ( L.outofrange && R.outofrange ){ return 0; }
                if ( L.outofrange != R.outofrange ){ return L.outofrange ? 1 : -1; }
                if ( L.centerfacetwist != R.centerfacetwist ){ return L.centerfacetwist ? -1 : 1; }
                if ( L.cuberotation != R.cuberotation ){ return L.cuberotation ? -1 : 1; }
                for( byte i= 0; i < maxlen; i++ ){
                    if( L.efs[ i ] == R.efs[ i ] ){
                        if( L.efs[ i ]== ef.ef24 ){ break; } } // all edge elements to this point match - continue to corner elements
                    else { return singmaster.ee_e[ L.efs[ i ] ].CompareTo( singmaster.ee_e[ R.efs[ i ] ] ); }
                }
                for( byte i= 0; i < maxlen; i++ ){
                    if( L.cfs[ i ] == R.cfs[ i ] ){
                        if( L.cfs[ i ]== cf.cf24 ){ return 0; } }
                    else { return singmaster.cc_e[ L.cfs[ i ] ].CompareTo( singmaster.cc_e[ R.cfs[ i ] ] ); }
                }
                return 0;
            }
            public override string ToString(){
                if( outofrange ){ return "(###)"; }
                System.Text.StringBuilder sb= new System.Text.StringBuilder();
                sb.Append( "( " ) ;
                if( centerfacetwist ){ sb.Append( "@:" ); }
                if( cuberotation ){ sb.Append( "@" ); }
                foreach( ef efi in efs ){
                    if( efi == ef.ef24 ){ break; }
                    sb.Append( singmaster.ee_e[ efi ] );
                }  sb.Append( " # " ) ;
                foreach( cf cfi in cfs ){
                    if( cfi == cf.cf24 ){ break; }
                    sb.Append( singmaster.cc_e[ cfi ] );
                }
                sb.Append( " )" );
                return sb.ToString();
            }
            public static entry e( string oftype, params char[] es ){
                entrybuild ret= new entrybuild();
                foreach( char ei in es ){
                    ret.insert( singmaster.ee_e[ ei ] );
                }
                return ret.ToEntry();
            }
            public static entry c( string oftype, params char[] cs ){
                entrybuild ret= new entrybuild();
                foreach( char ci in cs ){
                    ret.insert( singmaster.cc_e[ ci ] );
                }
                return ret.ToEntry();
            }
            public byte ecount{ get{
                for( byte i= maxlen - 1; i > 0; i-- ){
                    if( this.efs[ i ] != ef.ef24 ){ return (byte)( i + 1 ); } }
                if( this.efs[ 0 ] == ef.ef24 ){ return 0; } else { return 1; }
            }}
            public byte ccount{ get{
                for( byte i= maxlen - 1; i > 0; i-- ){
                    if( this.cfs[ i ] != cf.cf24 ){ return (byte)( i + 1 ); } } 
                if( this.cfs[ 0 ] == cf.cf24 ){ return 0; } else { return 1; }
            }}
        } // struct entry


        /// <summary>
        /// generate the string of face-turns including '[', ']', ':', ',', '::', and also '\''
        /// in the pattern given
        /// </summary>
        public static List< ft > format( string pattern, params ft[] args ){
            List< ft >ret= new List<ft>();
            bool accumnum= false;  // encountered '{'
            int num= 0;
            char prevch= (char)( 0 );
            foreach( char ch in pattern ){ switch( ch ){
              case '[' : ret.Add( ft.ps1 );  break; //  [ ] : , :: 
              case ']' : ret.Add( ft.ps2 );  break;
              case ',' : ret.Add( ft.ps4 );  break;
              case '@' : ret.Add( ft.posMark );  break;
              case ':' : if( prevch == ':' ){ ret[ ret.Count-1 ]= ft.ps5; }
                    else ret.Add( ft.ps3 );  break;
              case '\'' : if( ret.Count > 0 ){ ret[ ret.Count-1 ]= ftT.invert( ret[ ret.Count-1 ] ); }  break;
              case '{' : accumnum= true;  num= 0;  break;
              case '0' :  num= num*10;  break;              case '1' :  num= num*10 + 1;  break;
              case '2' :  num= num*10 + 2;  break;          case '3' :  num= num*10 + 3;  break;
              case '4' :  num= num*10 + 4;  break;          case '5' :  num= num*10 + 5;  break;
              case '6' :  num= num*10 + 6;  break;          case '7' :  num= num*10 + 7;  break;
              case '8' :  num= num*10 + 8;  break;          case '9' :  num= num*10 + 9;  break;
              case '}' : if( accumnum && num < args.Length ){
                  ret.Add( args[ num ] );  num= 0;  accumnum= false; }  break;
            } // switch
              prevch= ch;
            } // foreach
            return ret;
        } // format

        public static string reportgen( ft[] gen ){ // todo include oder overrides?
            System.Text.StringBuilder sb= new System.Text.StringBuilder();
            foreach( ft fti in gen ){
                sb.Append( ((ftT)( fti )).ToString() );  sb.Append( ' ' );
            }
            return sb.ToString();
        }

        static List< ft > fromFtArray( ft[] ftArray ){
            List< ft > res= new List< ft >( ftArray.Length );
            foreach( ft fti in ftArray ){
                res.Add( fti );
            } return res;
        }

        public static string c( System.Collections.Generic.SortedList< entry, ft[] > c, params char[] cs ){
            entrybuild ce= new entrybuild();  foreach( char ci in cs ){ ce.insert( singmaster.cc_e[ ci ] ); }
            entry cn= ce.ToEntry();
            if( c.ContainsKey( cn ) ){ return reportgen( c[ cn ] ); } else { return ""; };
        }

        public static string e( System.Collections.Generic.SortedList< entry, ft[] > c, params char[] es ){
            entrybuild ee= new entrybuild();  foreach( char ei in es ){ ee.insert( singmaster.ee_e[ ei ] ); }
            entry en= ee.ToEntry();
            if( c.ContainsKey( en ) ){ return reportgen( c[ en ] ); } else { return ""; };
        }

        public static System.Collections.Generic.SortedList< entry, moves_ty > T= 
            new System.Collections.Generic.SortedList< entry, moves_ty >();

        internal static ft[] fslice= new ft[]{ ft.e1, ft.e2, ft.e3, ft.s1, ft.s2, ft.s3,   ft.m1, ft.m2, ft.m3 };

        // single quarter turns, two for each plane
        internal static ft[] fparallel= new ft[ 20 ]{ ft.none, ft.d3, ft.d2, ft.d1, ft.r3, ft.r2, ft.r1, ft.bp3, ft.bp2, ft.bp1, ft.L3, ft.L2, ft.L1,
            ft.f3, ft.f2, ft.f1,   ft.ut3, ft.ut2, ft.ut1, ft.none };
        internal static ft[] planedoublemove= new ft[ 6 ]{ ft.ut2, ft.L2, ft.f2, ft.r2, ft.bp2, ft.d2 };
        internal static ft[] preferredorder;  internal static ft[] extendedorder;

        static ft[] genpreferredorder(){
            List< int > numsreading;  List< string > strsreading;
            List< pm > reading= parse.parse1( Program.preferredorder, out numsreading, out strsreading );
            singmaster.resetorderoverride();  parse.displayind di = new parse.displayind();
            List< ft > moves= parse.parse2( reading, numsreading, strsreading, out di );
            parse.parse3(ref moves, ref reading, numsreading, strsreading, ref di);
            parse.parseK(ref moves, ref reading, numsreading, strsreading, ref di);
            return moves.ToArray();
        }

        static ft[] genextendedorder(){
            pm readingplane;
            List< pm > readings= new List< pm >();
            List< ft > extensions= new List< ft >();
            foreach( ft fti in preferredorder ){
                readingplane= ftT.ft_pm( fti );
                if( !readings.Contains( readingplane ) ){
                    extensions.Add( ftT.doubl( fti ) );
                    readings.Add( readingplane );
                }
            }
            int i= 0;
            foreach( ft fti in preferredorder ){ extensions.Insert( i, fti );  i+= 1; }
            return extensions.ToArray();
        }

        const byte ecfEntireRange= (byte)( (byte)( ef.ef24 ) + 1 );
        internal static char[] catalogename= new char[ ecfEntireRange ];
        internal static char[] catalogcname= new char[ ecfEntireRange ];
        internal static SortedList< char, ef > catalognamee= new SortedList< char, ef >( ecfEntireRange ); // todo remove _static_ as above
        internal static SortedList< char, cf > catalognamec= new SortedList< char, cf >( ecfEntireRange );

        public static void gen3cycles(){
            preferredorder= genpreferredorder();  extendedorder= genextendedorder();
            System.Text.StringBuilder sb= new System.Text.StringBuilder();
            System.Text.StringBuilder sbi= new System.Text.StringBuilder();

            List< ft > f1= new List< ft >();
            moves_ty gen_val;
            position p1= new position();
            entry en1;

            List< ft[] > inv= new List< ft[] >();
            List< ft[] > dbl= new List< ft[] >();
            List< ft[] > dblinverse= new List< ft[] >();

            List< moves_ty > S= new List< moves_ty >();
            List< moves_ty > Scopy= new List< moves_ty >();
            pm pmi;  pm pmj;  pm pmk;

            catalogename= (char[])( singmaster.ename.Clone() );
            catalogcname= (char[])( singmaster.cname.Clone() ); // permit a change of letter system to function
            foreach( ef efi in singmaster.ee_e ){ catalognamee.Add( singmaster.ee_e[ efi ], efi ); }
            foreach( cf cfi in singmaster.cc_e ){ catalognamec.Add( singmaster.cc_e[ cfi ], cfi ); }

            // // E D G E S
            #region edges
            foreach( ft fti in preferredorder ){  pmi= ftT.ft_pm( fti );
            foreach( ft ftSj in fslice ){ pmj= ftT.ft_pm( ftT.unwiden2( ftSj ) );
                if( pmj == pmi || pmj == pmT.parallel( pmi ) ){ continue; }
            for( ft ftk= ft.ut1; ftk <= ft.d3; ftk++ ){
                pmk= ftT.ft_pm( ftk );
                if( pmi == pmk || pmi == pmT.parallel( pmk ) ){ continue; }
                if( pmj != pmk && pmj != pmT.parallel( pmk ) ){ continue; }
            
                f1= format( "[ {0} {1} {0}', {2} ]", fti, ftSj, ftk );
                inv.Add( format( "[ {2}, {0} {1} {0}' ]", fti, ftSj, ftk ).ToArray() );
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // 528 nonduplicatges // 96 items added
                en1= p1.ToEntry();  en1.type= "int slice";  gen_val.type= en1.type;
                if( !T.ContainsKey( en1 ) ){ T.Add( en1, gen_val );  S.Add( gen_val );
                } // +360 edges
            }}} //" edges for
            foreach( ft[] fta in inv ){
                f1.Clear();  foreach( ft fti in fta ){ f1.Add( fti ); }
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // 528 nonduplicatges // 96 items added
                en1= p1.ToEntry();  en1.type= "int' slice";  gen_val.type= en1.type;
                if( !T.ContainsKey( en1 ) ){ T.Add( en1, gen_val );  S.Add( gen_val ); }
            } // +216 edges total 576
            inv.Clear();

            foreach( ft fti in preferredorder ){ pmi= ftT.ft_pm( fti );
            foreach( ft ftj in preferredorder ){ pmj= ftT.ft_pm( ftj );
                if( pmi == pmj || pmT.parallel( pmi ) == pmj ){ continue; }
            foreach( ft ftk in fslice ){ pmk= ftT.ft_pm( ftT.unwiden2( ftj ) );
                if( pmk == pmi || pmk == pmT.parallel( pmi ) ){ continue; }
                f1= format( "[ {0}, {1} {2} {1}' ]", ftk, fti, ftj );
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // 528 nonduplicatges // 96 items added
                en1= p1.ToEntry();  en1.type= "slice insert";  gen_val.type= en1.type;
                if( en1.centerfacetwist ){ continue; } // reject elements where centers are rotated
                // if( en1.ecount > 3 ){ continue; } // does not occur after rejecting en1.centerfacetwist
                if( !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, gen_val );  S.Add( gen_val );
                    inv.Add( format( "[ {1} {2} {1}', {0} ]", ftk, fti, ftj ).ToArray() );
                } // +240 edges total 816
            }}}
            foreach( ft[] fta in inv ){
                f1.Clear();  foreach( ft fti in fta ){ f1.Add( fti ); }
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // 528 nonduplicatges // 96 items added
                en1= p1.ToEntry();  en1.type= "slice insert'";  gen_val.type= en1.type;
                if( !T.ContainsKey( en1 ) ){ T.Add( en1, gen_val );  S.Add( gen_val ); }
            } // +144 edges total 960
            inv.Clear();

            foreach( ft fti in preferredorder ){ pmi= ftT.ft_pm( fti );
            foreach( ft ftj in preferredorder ){ pmj= ftT.ft_pm( ftj );
                if( pmi == pmj || pmT.parallel( pmi ) == pmj ){ continue; }
            foreach( ft ftk in preferredorder ){ pmk= ftT.ft_pm( ftk );
                if( pmk == pmi || pmT.parallel( pmk ) == pmi
                    || pmk == pmj || pmT.parallel( pmk ) == pmj ){ continue; }
                f1= format( "[ [ {0} {1} {2} : {3} ] [ {4} {5} {6} ] ]",
                    fti, ftj, // {0} {1} // F' R'
                    fparallel[ (byte)( ftT.invert( fti ) ) ], ftk, // {2} {3} // B' D'
                    fparallel[ (byte)( ftj ) ], ftT.invert( ftk ), // L D
                    fparallel[ (byte)( ftT.invert( ftj ) ) ] // L'
                        );
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );

                en1= p1.ToEntry();  en1.type= "t-wrap 43";  gen_val.type= en1.type;
                if( en1.ecount == 3 && !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, gen_val ); // verified also generates its inverses
                    #if report1
                    sb.Append( p1.ToString() );  sb.AppendLine();
                    #endif
                }
            }}} // +48 edges total 1008 saves 96.0.96 f2s

            foreach( ft fti in preferredorder ){ pmi= ftT.ft_pm( fti );
            foreach( ft ftj in preferredorder ){ pmj= ftT.ft_pm( ftj );
                if( pmi == pmj || pmT.parallel( pmi ) == pmj ){ continue; }
            for( pm pmk_= pm.ut; pmk_ <= pm.dd; pmk_++ ){
                if( pmk_ == pmi || pmk_ == pmj ){ continue; }

                f1= format( "[ [ {0} {1} : {2} ][ {3} {4} : {2} ] ]", fti, ftj, planedoublemove[ (byte)( pmk_ ) ], 
                    fparallel[ (byte)( ftj ) ], fparallel[ (byte)( fti ) ] ); // CHKTHS fix setup routine for this case

                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );

                en1= p1.ToEntry();  en1.type= "t-wrap 33";  gen_val.type= en1.type;
                if( en1.ecount == 3 && !T.ContainsKey( en1 ) ){ // 336 rejections
                    T.Add( en1, gen_val ); // verified also generates its inverses
                    #if report2 
                    sb.Append( p1.ToString() );  sb.AppendLine();
                    #endif
                }
            }}} // +48 edges 1056 saves 96.96.96 f2s

            foreach( ft fti in preferredorder ){ pmi= ftT.ft_pm( fti );
            foreach( ft ftj in preferredorder ){ pmj= ftT.ft_pm( ftj );
                if( pmi == pmj || pmT.parallel( pmi ) == pmj ){ continue; }
            for( pm pmk_= pm.ut; pmk_ <= pm.dd; pmk_++ ){
                if( pmk_ == pmi || pmk_ == pmj ){ continue; }

                f1= format( "[ [ {0} {1} {2} : {3} ][ {4} {3} {4}' ] ]", fti, ftj,
                    fparallel[ (byte)( ftT.invert( fti ) ) ],
                    planedoublemove[ (byte)( pmk_ ) ],  fparallel[ (byte)( ftj ) ] );

                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );

                en1= p1.ToEntry();  en1.type= "t-wrap 42";  gen_val.type= en1.type;
                if( en1.ecount == 3 && !T.ContainsKey( en1 ) ){ // 336 rejections
                    T.Add( en1, gen_val );  // verified also generates its inverses
                    #if report
                    //sb.Append( p1.ToString() );  sb.AppendLine();
                    #endif
                }
            }}} // +48 edges 1104 saves 96.96.96 f2s
#if su_Ltwrap5
            foreach( ft fti in fq ){ pmi= ftT.ft_pm( fti );
            foreach( ft ftj in fq ){ pmj= ftT.ft_pm( ftj );
                if( pmi == pmj || pmT.parallel( pmi ) == pmj ){ continue; }
            for( pm pmk_= pm.ut; pmk_ <= pm.dd; pmk_++ ){
                if( pmk_ == pmi || pmk_ == pmj ){ continue; }

                f1= format( "[ {0} :: {0} {1} {2} {3} , {4} ]", fti, ftj,
                    fparallel[ (byte)( ftT.invert( fti ) ) ],
                    fparallel[ (byte)( ftT.invert( ftj ) ) ],
                    planedoublemove[ (byte)( pmk_ ) ] );

                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );

                en1= p1.ToEntry();  en1.type= "su_L t-wrap 5";  gen_val.type= en1.type;
                if( en1.ecount == 3 && !T.ContainsKey( en1 ) ){ // 336 rejections
                    T.Add( en1, gen_val ); // verified also generates its inverses
                    #if report 
                    sb.Append( p1.ToString() );  sb.AppendLine();
                    #endif
                }
            }}} // +48 edges 1152
#endif
            moves_ty setupmt;
            foreach( moves_ty mt in S ){
                setupmt= mt.setup( mt, moves_ty.augm.L, ft.ft74 );
                f1.Clear();  foreach( ft fti in setupmt.moves ){ f1.Add( fti ); }
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                //    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // 528 nonduplicatges // 96 items added
                en1= p1.ToEntry();  en1.type= setupmt.type;  gen_val.type= setupmt.type;
                if( !T.ContainsKey( en1 ) ){ T.Add( en1, gen_val ); }
            } // +288 edges total 1440
            foreach( moves_ty mt in S ){
                setupmt= mt.setup( mt, moves_ty.augm.R, ft.ft74 );
                f1.Clear();  foreach( ft fti in setupmt.moves ){ f1.Add( fti ); }
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                //    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // 528 nonduplicatges // 96 items added
                en1= p1.ToEntry();  en1.type= setupmt.type;  gen_val.type= setupmt.type;
                if( !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, gen_val ); }
            } // +48 edges total 1488

            foreach( ft fti in preferredorder ){ pmi= ftT.ft_pm( fti );
            foreach( ft ftj in preferredorder ){ pmj= ftT.ft_pm( ftj );
                if( pmi == pmj || pmT.parallel( pmi ) == pmj ){ continue; }
            foreach( ft ftk in preferredorder ){ pmk= ftT.ft_pm( ftk );
                if( pmi == pmk || pmT.parallel( pmi ) == pmk ){ continue; }
                if( pmj == pmk || pmT.parallel( pmj ) == pmk ){ continue; }

                    f1= format( "[ [ {0} {1} : {2} ][ {3} ] @ [ {0} {1} : {2} ][ {3} ] ]", 
                        fti, fparallel[ (byte)( fti ) ],
                        ftT.doubl( ftj ),
                        ftT.doubl( ftk ) );
                
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );

                en1= p1.ToEntry();  en1.type= "slice plane";  gen_val.type= en1.type;
                if( en1.ecount == 3 && !T.ContainsKey( en1 ) ){ // 336 rejections
                    T.Add( en1, gen_val ); // verified also generates its inverses
                    #if report 
                    //sb.Append( p1.ToString() );  sb.AppendLine();
                    #endif
                }
            }}} // +24 edges total 1512

            foreach( moves_ty mt in S ){
            foreach( ft fta in preferredorder ){
                setupmt= mt.setup( mt, moves_ty.augm.S2, fta );
                f1.Clear();  foreach( ft fti in setupmt.moves ){ f1.Add( fti ); }
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                //    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // 528 nonduplicatges // 96 items added
                en1= p1.ToEntry();  en1.type= setupmt.type;  gen_val.type= setupmt.type;
                if( !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, gen_val ); }
            }} // +248 edges total 1760 complete

            // score up the total move count for all 1760 edge 3-cycles
            short fturns= 0;  short fdblturns= 0;  short fsliceturns= 0;
            foreach( moves_ty mt in T.Values ){
                f1.Clear();  foreach( ft fti in mt.moves ){ f1.Add( fti ); }
                parse.parselist( ref f1 );
                foreach( ft fti in f1 ){
                    switch( ftT.ft_ftSub( fti ) ){
                    case ftSub.face:  case ftSub.doubl:
                        if( ftT.ft_byte( fti ) == 2 ){ fdblturns+= 1; }
                        fturns+= 1;  break;
                    case ftSub.slice:
                        if( ftT.ft_byte( fti ) == 2 ){ fdblturns+= 2; }
                        fturns+= 2;  fsliceturns+= 1;  break;
                    }
                }
            }
            string report= T.Keys.Count.ToString() + " / 1760 gives " 
                + fturns.ToString() + "." + fdblturns.ToString() + "." + fsliceturns.ToString() + " f2s" ;
            // 1760 / 1760 gives 18912.3388.3088 f2s"
            #endregion

            // // C O R N E R S
            #region corners
            // generate all 1009 possible 3-cycle corner algs by employing the Beyer-Hardwick forms
            S.Clear();  
            inv.Clear(); // at 0 already
            dbl.Clear(); // at 0 already
            dblinverse.Clear();
            //moves_ty setupmt;

            foreach( ft fti in extendedorder ){ pmi= ftT.ft_pm( fti );
            foreach( ft ftj in preferredorder ){ pmj= ftT.ft_pm( ftj );
                if( pmj != pmi && pmj != pmT.parallel( pmi ) ){
            foreach( ft ftk in preferredorder ){ pmk= ftT.ft_pm( ftk );
                if( pmk != pmj && pmk != pmi && pmk == pmT.parallel( pmi ) ){

                f1= format( "[ {0} , {1} {2} {1}' ]", fti, ftj, ftk );
                inv.Add( format( "[ {1} {2} {1}' , {0} ]", fti, ftj, ftk ).ToArray() );
                dbl.Add( format( "[ {1} {2} {1}', {0} ]", fti, ftj, ftT.doubl( ftk ) ).ToArray() ); // 144 such
                dblinverse.Add( format( "[ {0}, {1} {2} {1}' ]", fti, ftj, ftT.doubl( ftk ) ).ToArray() );

                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );
                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();
                en1= p1.ToEntry();  en1.type= "c8";  gen_val.type= en1.type;
                if( !T.ContainsKey( en1 ) && en1.ecount == 0 && en1.ccount == 3 ){ 
                    T.Add( en1, gen_val );  Scopy.Add( gen_val );  S.Add( gen_val );  } // i.e. (_BCS) // c8
            }}}}} // 288
            foreach( ft[] f1i in dbl ){
                gen_val= new moves_ty();
                gen_val.moves= f1i;
                f1= fromFtArray( f1i );
                parse.parselist( ref f1 );
                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                en1= p1.ToEntry();  en1.type= "c8.2";  gen_val.type= en1.type;
                if( en1.ecount == 0 && en1.ccount == 3 && !T.ContainsKey( en1 ) ){
                    //sb.Append( p1.ToString() );  sb.AppendLine();
                    T.Add( en1, gen_val );  S.Add( gen_val );  } // i.e. (_AGB) // c8.2
            } // +108 336 total
            foreach( ft[] f1i in inv ){
                gen_val= new moves_ty();
                gen_val.moves= f1i;
                f1= fromFtArray( f1i );
                parse.parselist( ref f1 );
                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();
                en1= p1.ToEntry();  en1.type= "c8\'";  gen_val.type= en1.type;
                if( !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, gen_val );  S.Add( gen_val );  } // i.e. (_AKB) // c8'
            } // +144 480 total
            foreach( ft[] f1i in dblinverse ){
                gen_val= new moves_ty();
                gen_val.moves= f1i;
                f1= fromFtArray( f1i );
                parse.parselist( ref f1 );
                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );

                en1= p1.ToEntry();  en1.type= "c8\'";  gen_val.type= en1.type;
                if( en1.ecount == 0  && en1.ccount == 3 && !T.ContainsKey( en1 ) ){
                    //sb.Append( p1.ToString() );  sb.AppendLine();
                    T.Add( en1, gen_val );  S.Add( gen_val );  } // i.e. (_AKB) // c8'
            } // +144 528 total

            foreach( ft[] fti in inv ){ // verified +0 from c8 form
                gen_val.moves= fti;
                gen_val.type= "c8'";

                setupmt= gen_val.setup( gen_val, moves_ty.augm.L, ft.ft74 );
                f1= setupmt.toListFt();
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                //    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // 528 nonduplicatges // 96 items added
                en1= p1.ToEntry();  en1.type= setupmt.type;
                if( !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, setupmt ); } // i.e. (_ABM) // su_L c8'
            } // +96 624 total
            foreach( ft[] fti in dbl ){ // verified +0 from dblinverse
                gen_val.moves= fti;
                gen_val.type= "c8.2";

                setupmt= gen_val.setup( gen_val, moves_ty.augm.L, ft.ft74 );
                f1= setupmt.toListFt();
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                //    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // 528 nonduplicatges // 96 items added
                en1= p1.ToEntry();  en1.type= setupmt.type;
                if( !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, setupmt ); } // i.e. (_BYS) // su_L c8.2
            } // +96 720 total

            foreach( ft fti in preferredorder ){ pmi= ftT.ft_pm( fti );
            foreach( moves_ty mt in Scopy ){
                setupmt= mt.setup( mt, moves_ty.augm.S2, fti );
                f1 = setupmt.toListFt();
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                //    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // 528 nonduplicatges // 96 items added
                en1= p1.ToEntry();  en1.type= setupmt.type;  gen_val.type= setupmt.type;
                if( !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, gen_val ); } // i.e. (_AES) // su_2 c8
            }} // +96 corners 816

            foreach( ft fti in preferredorder ){ pmi= ftT.ft_pm( fti );
            foreach( ft[] ftib in inv ){
                gen_val.moves= ftib;
                gen_val.type= "c8'";
                setupmt= gen_val.setup( gen_val, moves_ty.augm.S2, fti );
                f1= setupmt.toListFt();
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                //    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // 528 nonduplicatges // 96 items added
                en1= p1.ToEntry();  en1.type= setupmt.type;  gen_val.type= setupmt.type;
                if( !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, gen_val ); } // i.e. (_BUY) // su_2 c8
            }} // +48 corners 864

            foreach( ft fti in extendedorder ){ pmi= ftT.ft_pm( fti );
            foreach( ft[] ftib in dbl ){
                gen_val.moves= ftib;
                gen_val.type= "c8.2";
                setupmt= gen_val.setup( gen_val, moves_ty.augm.S2, fti );
                f1= setupmt.toListFt();
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                //    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // 528 nonduplicatges // 96 items added
                en1= p1.ToEntry();  en1.type= setupmt.type;  gen_val.type= setupmt.type;
                if( !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, gen_val ); } // i.e. (_AHS) // su_2 c8.2
            }} // +32 corners 896

            S.Clear();  Scopy.Clear();
            foreach ( ft fti in preferredorder ){ pmi= ftT.ft_pm( fti );
            foreach( ft ftj in preferredorder ){ pmj= ftT.ft_pm( ftj );
                if( pmj != pmi && pmj != pmT.parallel( pmi ) && ftT.ft_byte( ftj ) != 2 ){
            foreach( ft ftk in extendedorder ){
                pmk= ftT.ft_pm( ftk );
                if( ftT.ft_byte( ftk ) == 2  && pmk != pmj && pmk != pmi ){

                f1= format( "[ [ {0} {1} : {2} ] [ {1} {0} : {2} ] ]", fti, ftj, ftk ); 
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );
                p1= new position(); p1.resetnull();  p1.move( f1 );
                    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine(); // if report is active
                en1= p1.ToEntry();  en1.type= "cyclic shift";  gen_val.type= en1.type;
                if( en1.ccount == 3 && en1.ecount == 0 && !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, gen_val );  S.Add( gen_val );  } // i.e. (_CFY) // cyclic shift
            }}}}} // +48 corners 944

            foreach( moves_ty mt in S ){ // su_L cyclic shift or columns
                setupmt= mt.setup( mt, moves_ty.augm.L, ft.none );
                f1.Clear();  foreach( ft fti in setupmt.moves ){ f1.Add( fti ); }
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                //    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // ?? nonduplicatges // ?? items added
                en1= p1.ToEntry();  en1.type= setupmt.type;  gen_val.type= setupmt.type + " columns";
                if( en1.ccount == 3 && en1.ecount == 0 && !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, gen_val );  Scopy.Add( gen_val ); } // i.e. (_DFY) // su_L cyclic shift
            } // +24 corners 968

            foreach( moves_ty mt in S ){
            foreach( ft ftj in preferredorder ){
                setupmt= mt.setup( mt, moves_ty.augm.S2, ftj );
                f1.Clear();  foreach( ft fti in setupmt.moves ){ f1.Add( fti ); }
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                //    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // ?? nonduplicatges // ?? items added
                en1= p1.ToEntry();  en1.type= setupmt.type;  gen_val.type= setupmt.type + " columns";
                if( en1.ccount == 3 && en1.ecount == 0 && !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, gen_val );  Scopy.Add( gen_val ); } // i.e. (_CWO) // su_2 cyclic shift columns
            }} // +32 corners 992

            foreach( moves_ty mt in Scopy ){
            foreach( ft ftj in preferredorder ){ pmj= ftT.ft_pm( ftj );
                setupmt= mt.setup( mt, moves_ty.augm.S3, ftj );
                f1.Clear();  foreach( ft fti in setupmt.moves ){ f1.Add( fti ); }
                gen_val= new moves_ty();
                gen_val.moves= new ft[ f1.Count ];  f1.CopyTo( gen_val.moves );
                parse.parselist( ref f1 );

                p1= new position(); p1.resetnull();  p1.move( f1 );
                //    // p1.shortreportlengththreashold= 24; // employed to determine malfunctioning generator
                p1.shortreport= reportgen( gen_val.moves );
                //sb.Append( p1.ToString() );  sb.AppendLine();  // ?? nonduplicatges // ?? items added
                en1= p1.ToEntry();  en1.type= setupmt.type;  gen_val.type= setupmt.type + " per special";
                if( en1.ccount == 3 && en1.ecount == 0 && !T.ContainsKey( en1 ) ){ 
                    T.Add( en1, gen_val ); } // i.e. (_CWU) // su_L cyclic shift columns per special
            }} // +16 corners 1008 both 
            #endregion
            #if report
            System.Windows.Forms.Clipboard.SetDataObject( sb.ToString() + sbi.ToString() );
            #endif
        } // gen3cycles
    } // catalog

    public struct conjugatestudyentry: System.IComparable< conjugatestudyentry >{
        //ef[] etargets;
        //cf[] ctargets;
        int System.IComparable< conjugatestudyentry >.CompareTo( conjugatestudyentry other){
            return 0; // TODO
        }
    }

    public class conjugatestudy{
        static ft[] movetrials= new ft[]{ ft.ut1, ft.f1, ft.r1, ft.bp1, ft.L1, ft.d1,
            ft.utw1, ft.fw1, ft.rw1, ft.bpw1, ft.Lw1, ft.dw1,
            ft.m1, ft.e1, ft.s1, ft.x1, ft.y1, ft.z1 }; // moves to check against conjugate entries for additional solutions
        public static IEnumerable< ft > movetrialorderspan( ft[] movetrialsgiven ){
            foreach( ft fti in movetrialsgiven ){
                yield return fti;
                yield return ftT.invert( fti );
            }
            foreach( ft fti in movetrialsgiven ){
                yield return ftT.doubl( fti );
            }
        }

#if info
        setup types: 1: reverse order setup
        2: additional intermediate step (face 180-degree turn)
        3: (wide plane double-turn)
        4: whole-cube (WC) buffer to arm
        5: (slice middle-layer turn)
        6; WC diagonal flip thru buffer
        7: Buffer avoid, move, restore
        8: Buffer to arm direct exchange
        9: Buffer to arm flip exchange
#endif

        interface Ireadsummarizable{
            string summary();
        }

        public class esgen: conjugatestudy, Ireadsummarizable, IComparable< esgen >{ // conjugate study initial e edge algorithms
            public byte permitted; // permitted algorithm setup types
            public bool promoted; // true ~ prioritize this algorithm, usually for a new algorithm to practice
            public ef secondary;
            public ef target;
            int System.IComparable< esgen >.CompareTo( esgen other ){
                int comp= target.CompareTo( other.target ); if( comp != 0 ){ 
                    return singmaster.ee_e[ target ].CompareTo( singmaster.ee_e[ other.target ] ); }
                return singmaster.ee_e[ secondary ].CompareTo( singmaster.ee_e[ other.secondary ] );
            }

            string Ireadsummarizable.summary(){
                StringBuilder sb= new StringBuilder(2);
                sb.Append( singmaster.ee_e[ secondary ] );
                sb.Append( singmaster.ee_e[ target ] );
                return sb.ToString();
            }
            public string summary(){ return ((Ireadsummarizable)( this )).summary(); }
            public esgen(){}
            public esgen( char secondaryc, char targetc ){
                permitted= 0;
                promoted= false;
                secondary= singmaster.ee_e[ secondaryc ];
                target= singmaster.ee_e[ targetc ];
             }
            public override string ToString(){ // report for item already in repertoire
                StringBuilder sb= new StringBuilder();
                sb.Append( '[' );
                sb.Append( singmaster.ee_e[ target ] );
                sb.Append( singmaster.ee_e[ secondary ] );
                sb.Append( ']' );
                return sb.ToString();
            }
        }

        public class esgenS: esgen{
            /* -- inherited
            public byte permitted; // permitted algorithm setup types
            public bool promoted; // true ~ prioritize this algorithm, usually for a new algorithm to practice
            public ef secondary;
            public ef target;
            int System.IComparable<esgen>.CompareTo(esgen other)
            */
            public esgen from;
            public ef inttarget;
            public ef intsecondary;
            public byte reportcategory= 0;  // 1 refers to reverse-order setup

            internal ft int_target= ft.none; // first additional move from element in repertoire
            internal ft from_int= ft.none; // setup moves to determine type

            /// <summary> target-only or secondary-only edge setups </summary>
            public static IEnumerable< esgen > gen0( esgen genFrom ){ // determine all single-move single-effect setups from _oritin_
                esgenS thi= new esgenS();
                thi.from= new esgen( singmaster.ee_e[ genFrom.secondary ], singmaster.ee_e[ genFrom.target ] );

                ef secondarycandidate= genFrom.secondary;
                ef targetcandidate= genFrom.target;
                ef holdsecondaryortarget;
                ef newsecondaryortargetcandidate;

                foreach( ft fti in movetrialorderspan( movetrials ) ){ //" from_int moves
                if( ftT.ft_ftSub( fti ) == ftSub.doubl ){ continue; } // type 3 moves only effecive for double-moves
                if( ssT.move( fti, singmaster.ee_e.origin ) == singmaster.ee_e.origin ){ //" origin unchanged
                    targetcandidate= ssT.move( fti, genFrom.target );
                    secondarycandidate= ssT.move( fti, genFrom.secondary );
                if( targetcandidate != genFrom.target && secondarycandidate == genFrom.secondary ){
                    thi.target= targetcandidate;
                    thi.secondary= secondarycandidate;

                    thi.intsecondary= thi.from.secondary;
                    thi.inttarget= thi.from.target;

                    thi.int_target= fti;
                    yield return thi;
                    holdsecondaryortarget= thi.target;
                    foreach( ft ftj in movetrialorderspan( movetrials ) ){
                    if( ftT.ft_ftSub( ftj ) == ftSub.doubl ){ continue; }
                    if( ftT.ft_pm( fti ) == ftT.ft_pm( ftj )
                            || ssT.move( ftj, singmaster.ee_e.origin ) != singmaster.ee_e.origin
                            || ssT.move( ftj, secondarycandidate ) != genFrom.secondary
                        ){ continue; }
                    newsecondaryortargetcandidate= ssT.move( ftj, holdsecondaryortarget );
                    if( newsecondaryortargetcandidate != holdsecondaryortarget ){
                        thi.from_int= ftj;
                        thi.inttarget= holdsecondaryortarget;
                        thi.target= newsecondaryortargetcandidate;
                        yield return thi;
                    }}
                } else if( secondarycandidate != genFrom.secondary && targetcandidate == genFrom.target ){
                    thi.target= targetcandidate;
                    thi.secondary= secondarycandidate;

                    thi.intsecondary= thi.from.secondary;
                    thi.inttarget = thi.from.target;

                    thi.int_target= fti;
                    yield return thi;
                    holdsecondaryortarget= thi.secondary;
                    foreach( ft ftj in movetrialorderspan( movetrials ) ){
                    if( ftT.ft_ftSub( ftj ) == ftSub.doubl ){ continue; }
                    if( ftT.ft_pm( fti ) == ftT.ft_pm( ftj )
                            || ssT.move( ftj, singmaster.ee_e.origin ) != singmaster.ee_e.origin
                            || ssT.move( ftj, targetcandidate ) != genFrom.target
                        ){ continue; }
                    newsecondaryortargetcandidate= ssT.move( ftj, holdsecondaryortarget );
                    if( newsecondaryortargetcandidate != holdsecondaryortarget ){
                        thi.from_int= ftj;
                        thi.intsecondary= holdsecondaryortarget;
                        thi.secondary= newsecondaryortargetcandidate;
                        yield return thi;
                    }}
                }
                } //" oritin unchanged
                } //" from_int moves
            }

            /// <summary> reverse-order secondary then target edge setups </summary>
            public static IEnumerable< esgen > gen1( esgen genFrom ){
                esgenS thi= new esgenS();
                thi.from= new esgen( singmaster.ee_e[ genFrom.secondary ], singmaster.ee_e[ genFrom.target ] );

                ef secondarycandidate;
                ef targetcandidate;
                ef holdsecondarycandidate;

                foreach( ft fti in movetrialorderspan( movetrials ) ){ //" primary move trials
                if( ftT.ft_ftSub( fti ) == ftSub.doubl ){ continue; } 

                thi.secondary= genFrom.secondary;
                thi.target= genFrom.target;
                thi.intsecondary= genFrom.secondary;
                thi.inttarget= genFrom.target;
                thi.reportcategory= 1;

                if( ssT.move( fti, singmaster.ee_e.origin ) == singmaster.ee_e.origin
                      && ssT.move( fti, thi.target ) == thi.target ){ //" origin and target unchanged
                    holdsecondarycandidate= ssT.move( fti, genFrom.secondary );
                if( holdsecondarycandidate != genFrom.secondary ){ //" secondary changed
                    secondarycandidate= holdsecondarycandidate;
                    thi.inttarget= thi.target;
                    thi.intsecondary= holdsecondarycandidate; // intermediate not changed
                    thi.int_target= fti; // in reverse refers to int_secondary?

                    foreach( ft ftj in movetrialorderspan( movetrials ) ){ //" secondary move trials
                    if( ftT.ft_ftSub( ftj ) == ftSub.doubl ){ continue; }
                        if( ftT.ft_pm( ftj ) != ftT.ft_pm( fti )
                            && ssT.move( ftj, singmaster.ee_e.origin ) == singmaster.ee_e.origin
                            && ssT.move( ftj, thi.intsecondary ) == thi.intsecondary ){ //" origin and inttarget unchanged
                        thi.from_int= ftj;
                        targetcandidate= ssT.move( ftj, genFrom.target );
                        if( targetcandidate != genFrom.target ){ //" secondary move successful
                            thi.target= targetcandidate;
                            thi.secondary= secondarycandidate;
                            thi.from_int= ftj;
                            yield return thi;
                        } //" secondary move successful
                    } //" origin and inttarget unchanged
                    } //" secondary move trials
                } //" target changed
                } //" origin and secondary unchanged
                } //" primary move trials
            }

            /// <summary> standard order target, then secondary edge setups </summary>
            public static IEnumerable< esgen > gen2( esgen genFrom ){
                esgenS thi= new esgenS();
                thi.from= new esgen( singmaster.ee_e[ genFrom.secondary ], singmaster.ee_e[ genFrom.target ] );

                ef secondarycandidate;
                ef targetcandidate;
                ef holdtargetcandidate;

                foreach( ft fti in movetrialorderspan( movetrials ) ){ //" primary move trials
                //if( !ftT.ftSubCornermove( ftT.ft_ftSub( fti ) ) ){ continue; }

                thi.secondary= genFrom.secondary;
                thi.target= genFrom.target;
                thi.intsecondary= genFrom.secondary;
                thi.inttarget= genFrom.target;

                if( ssT.move( fti, singmaster.ee_e.origin ) == singmaster.ee_e.origin
                      && ssT.move( fti, thi.secondary ) == thi.secondary ){ //" origin and secondary unchanged
                    holdtargetcandidate= ssT.move( fti, genFrom.target );
                if( holdtargetcandidate != genFrom.target ){ //" target changed
                    targetcandidate= holdtargetcandidate;
                    thi.inttarget= targetcandidate;
                    thi.intsecondary= thi.secondary; // intermediate not changed
                    thi.int_target= fti;

                    foreach( ft ftj in movetrialorderspan( movetrials ) ){ //" secondary move trials
                    //if( !ftT.ftSubCornermove( ftT.ft_ftSub( ftj ) ) ){ continue; }
                        if( ftT.ft_pm( ftj ) != ftT.ft_pm( fti )
                            && ssT.move( ftj, singmaster.ee_e.origin ) == singmaster.ee_e.origin
                            && ssT.move( ftj, thi.inttarget ) == thi.inttarget ){ //" origin and inttarget unchanged
                        thi.from_int= ftj;
                        secondarycandidate= ssT.move( ftj, genFrom.secondary );
                        if( secondarycandidate != genFrom.secondary ){ //" secondary move successful
                            thi.target= targetcandidate;
                            thi.secondary= secondarycandidate;
                            thi.from_int= ftj;
                            yield return thi;
                        } //" secondary move successful
                    } //" origin and inttarget unchanged
                    } //" secondary move trials
                } //" target changed
                } //" origin and secondary unchanged
                } //" primary move trials
            }

            public override string ToString(){ // 
                StringBuilder sb= new StringBuilder();
                sb.Append( singmaster.ee_e[ target ] );
                sb.Append( singmaster.ee_e[ secondary ] );
                sb.Append( ' ' );

                if( inttarget != from.target || intsecondary != from.secondary ){
                if( reportcategory == 1 ){ sb.Append( "1" ); }
                if( ftT.ft_ftSub( from_int ) == ftSub.slice ){
                    if( ftT.ft_byte( from_int ) == 2 ){ sb.Append( "52 " );
                    }else{ sb.Append( "5 " ); }
                }else if( ftT.ft_ftSub( from_int ) == ftSub.doubl ){
                    if( ftT.ft_byte( from_int ) == 2 ){ sb.Append( "32 " );
                    }else{ sb.Append( "3 " ); }
                }else if( ftT.ft_byte( from_int ) == 2 ){ sb.Append( "2 " ); }
                if( sb.Length == 4 ){ sb.Append( " " ); }

                sb.Append( singmaster.ee_e[ inttarget ] );
                sb.Append( singmaster.ee_e[ intsecondary ] ); // was from.secondary
                sb.Append( ' ' ); }

                if( ftT.ft_ftSub( int_target ) == ftSub.slice ){ 
                    if( ftT.ft_byte( int_target ) == 2 ){ sb.Append( "52 " );
                    }else{ sb.Append( "5 " ); }
                }else if( ftT.ft_ftSub( int_target ) == ftSub.doubl ){
                    if( ftT.ft_byte( int_target ) == 2 ){ sb.Append( "32 " );
                    }else{ sb.Append( "3 " ); }
                }else if( ftT.ft_byte( int_target ) == 2 ){ sb.Append( "2 " ); }

                sb.Append( singmaster.ee_e[ from.target ] );
                sb.Append( singmaster.ee_e[ from.secondary ] );
                return sb.ToString();
            }
        }


        public class csgen: conjugatestudy, Ireadsummarizable, IComparable< csgen >{ // conjugate study initial c corner algorithms
            public byte permitted; // permitted algorithm setup types
            public bool promoted;
            public cf secondary;
            public cf target;
            int System.IComparable< csgen >.CompareTo( csgen other ){
                int comp= target.CompareTo( other.target ); if( comp != 0 ){
                    return singmaster.cc_e[ target ].CompareTo( singmaster.cc_e[ other.target ] ); }
                return singmaster.cc_e[ secondary ].CompareTo( singmaster.cc_e[ other.secondary ] );
            }
            public csgen(){}
            public csgen( char secondaryc, char targetc ){
                permitted= 0;
                promoted= false;
                secondary= singmaster.cc_e[ secondaryc ];
                target= singmaster.cc_e[ targetc ];
            }
            string Ireadsummarizable.summary(){
                StringBuilder sb= new StringBuilder(2);
                sb.Append( singmaster.cc_e[ secondary ] );
                sb.Append( singmaster.cc_e[ target ] );
                return sb.ToString();
            }
            public string summary(){ return ((Ireadsummarizable)( this )).summary(); }
            public override string ToString(){ // report for item already in repertoire
                StringBuilder sb= new StringBuilder();
                sb.Append( '[' );
                sb.Append( singmaster.cc_e[ target ] );
                sb.Append( singmaster.cc_e[ secondary ] );
                sb.Append( ']' );
                return sb.ToString();
            }
        }

        public class csgenS: csgen{
            /* -- inherited // 
            public byte permitted; // permitted algorithm setup types
            public bool promoted; // true ~ prioritize this algorithm, usually for a new algorithm to practice
            public cf secondary;
            public cf target;
            int System.IComparable<esgen>.CompareTo(esgen other)
            */
            public csgen from;
            public cf inttarget;
            public cf intsecondary;
            public byte reportcategory = 0;

            internal ft int_target= ft.none; // first additional move from element in repertoire
            internal ft from_int= ft.none;

            /// <summary>
            /// generage all candidates changing only the target leaving the secondary or vice versa
            /// </summary>
            public static IEnumerable< csgen > gen0( csgen genFrom ){ // determine all single-move setups from _origin_
                csgenS thi= new csgenS();
                thi.from= new csgen( singmaster.cc_e[ genFrom.secondary ], singmaster.cc_e[ genFrom.target ] );

                cf secondarycandidate= genFrom.secondary;
                cf targetcandidate= genFrom.target;
                cf holdsecondaryortarget;
                cf newsecondaryortargetcandidate;

                foreach( ft fti in movetrialorderspan( movetrials ) ){ //" from_int moves
                if( !ftT.ftSubCornermove( ftT.ft_ftSub( fti ) ) ){ continue; }
                if( ssT.move( fti, singmaster.cc_e.origin ) == singmaster.cc_e.origin ){ //" origin unchanged
                    targetcandidate= ssT.move( fti, genFrom.target );
                    secondarycandidate= ssT.move( fti, genFrom.secondary );
                if( targetcandidate != genFrom.target && secondarycandidate == genFrom.secondary ){
                    thi.target= targetcandidate;
                    thi.secondary= secondarycandidate;

                    thi.intsecondary= thi.from.secondary;
                    thi.inttarget= thi.from.target;

                    thi.int_target= fti;
                    yield return thi;
                    holdsecondaryortarget= thi.target;
                    foreach( ft ftj in movetrialorderspan( movetrials ) ){
                    if( !ftT.ftSubCornermove( ftT.ft_ftSub( ftj ) ) 
                            || ftT.ft_pm( fti ) == ftT.ft_pm( ftj )
                            || ssT.move( ftj, singmaster.cc_e.origin ) != singmaster.cc_e.origin
                            || ssT.move( ftj, secondarycandidate ) != genFrom.secondary
                        ){ continue; }
                    newsecondaryortargetcandidate= ssT.move( ftj, holdsecondaryortarget );
                    if( newsecondaryortargetcandidate != holdsecondaryortarget ){
                        thi.from_int= ftj;
                        thi.inttarget= holdsecondaryortarget;
                        thi.target= newsecondaryortargetcandidate;
                        yield return thi;
                    }}
                }
                else if( secondarycandidate != genFrom.secondary && targetcandidate == genFrom.target ){
                    thi.target= targetcandidate;
                    thi.secondary= secondarycandidate;

                    thi.intsecondary= thi.from.secondary;
                    thi.inttarget= thi.from.target;

                    thi.int_target= fti;
                    yield return thi;
                    holdsecondaryortarget= thi.secondary;
                    foreach( ft ftj in movetrialorderspan( movetrials ) ){
                    if( !ftT.ftSubCornermove( ftT.ft_ftSub( ftj ) ) 
                            || ftT.ft_pm( fti ) == ftT.ft_pm( ftj )
                            || ssT.move( ftj, singmaster.cc_e.origin ) != singmaster.cc_e.origin
                            || ssT.move( ftj, targetcandidate ) != genFrom.target
                        ){ continue; }
                    newsecondaryortargetcandidate= ssT.move( ftj, holdsecondaryortarget );
                    if( newsecondaryortargetcandidate != holdsecondaryortarget ){
                        thi.from_int= ftj;
                        thi.intsecondary= holdsecondaryortarget;
                        thi.secondary= newsecondaryortargetcandidate;
                        yield return thi;
                    }}
                }
                } //" origin unchanged
                } //" from_int moves
            }

            public static IEnumerable< csgen > gen1( csgen genFrom ){
                csgenS thi= new csgenS();
                thi.from= new csgen( singmaster.cc_e[ genFrom.secondary ], singmaster.cc_e[ genFrom.target ] );

                cf secondarycandidate;
                cf targetcandidate;
                cf holdsecondarycandidate;

                foreach( ft fti in movetrialorderspan( movetrials ) ){
                if( !ftT.ftSubCornermove( ftT.ft_ftSub( fti ) ) ){ continue; }

                thi.secondary= genFrom.secondary;
                thi.target= genFrom.target;
                thi.intsecondary= genFrom.secondary;
                thi.inttarget= genFrom.target;
                thi.reportcategory= 1;

                if( ssT.move( fti, singmaster.cc_e.origin ) == singmaster.cc_e.origin
                        && ssT.move( fti, thi.target ) == thi.target ){ //" origin and target unchanged
                    holdsecondarycandidate= ssT.move( fti, genFrom.secondary );
                if( holdsecondarycandidate != genFrom.secondary ){ //" secondary changed
                    secondarycandidate= holdsecondarycandidate;
                    thi.inttarget= thi.target;
                    thi.intsecondary= holdsecondarycandidate; // intermediate not changed
                    thi.int_target= fti;

                    foreach( ft ftj in movetrialorderspan( movetrials ) ){ //" secondary move trials
                    if( !ftT.ftSubCornermove( ftT.ft_ftSub( ftj ) ) ){ continue; }
                        if( ftT.ft_pm( ftj ) != ftT.ft_pm( fti )
                                && ssT.move( ftj, singmaster.cc_e.origin ) == singmaster.cc_e.origin
                                && ssT.move( ftj, thi.intsecondary ) == thi.intsecondary ){
                            thi.from_int= ftj;
                            targetcandidate= ssT.move( ftj, genFrom.target );
                            if( targetcandidate != genFrom.target ){ //" secondary move successful
                                thi.target= targetcandidate;
                                thi.secondary= secondarycandidate;
                                thi.from_int= ftj;
                                yield return thi;
                            } //" secondary move successful
                        } //" origin and inttarget unchanged
                        } //" secondary move trials
                    } //" target changed
                    } //" origin and secondary unchanged
                    } //" primary move trials
                }

            public static IEnumerable< csgenS > gen2( csgen genFrom ){ // determine all items reachable form _origin_
                csgenS thi= new csgenS();
                thi.secondary= genFrom.secondary;
                thi.target= genFrom.target;

                thi.from= new csgen( singmaster.cc_e[ genFrom.secondary ], singmaster.cc_e[ genFrom.target ] );

                //ft targetmove1;  ft originaltargetmove1;
                cf secondarycandidate;
                cf targetcandidate;

                foreach( ft fti in movetrialorderspan( movetrials ) ){ //" yield loop
                if( ftT.ftSubCornermove( ftT.ft_ftSub( fti ) )
                        && ssT.move( fti, singmaster.cc_e.origin ) == singmaster.cc_e.origin 
                        && ssT.move( fti, thi.secondary ) == thi.secondary ){ // origin and secondary unchanged
                    targetcandidate= ssT.move( fti, genFrom.target );
                if( targetcandidate != genFrom.target ){ // target changed
                    thi.inttarget= targetcandidate;
                    thi.intsecondary= thi.secondary;
                    thi.int_target= fti;

                    foreach( ft ftj in movetrialorderspan( movetrials ) ){ //" secondary move trials
                        if( ftT.ftSubCornermove( ftT.ft_ftSub( fti ) )
                            && ftj != fti 
                            && ssT.move( ftj, singmaster.cc_e.origin ) == singmaster.cc_e.origin
                            && ssT.move( ftj, thi.inttarget ) == thi.inttarget ){ // origin and inttarget unchanged
                        secondarycandidate= ssT.move( ftj, genFrom.secondary );
                        if( secondarycandidate != genFrom.secondary ){ // secondary move successful
                            thi.secondary= secondarycandidate;
                            thi.target= targetcandidate;
                            thi.from_int= ftj;
                            yield return thi;
                        } // secondary move successful
                    }} //" secondary move trials
                }}} //" yield loop
            }
            public override string ToString(){ // IN BN BD
                StringBuilder sb= new StringBuilder();
                sb.Append( singmaster.cc_e[ target ] );
                sb.Append( singmaster.cc_e[ secondary ] );
                sb.Append( ' ' );
                    if( inttarget != from.target || intsecondary != from.secondary ){
                if( reportcategory == 1 ){ sb.Append( "1" ); }
                if( ftT.ft_byte( this.from_int ) == 2) { sb.Append( "2 " ); }
                if( sb.Length == 4 ){ sb.Append( " " ); }

                sb.Append( singmaster.cc_e[ inttarget ] );
                sb.Append( singmaster.cc_e[ intsecondary ] );
                sb.Append( ' ' ); }
                
                if( ftT.ft_byte( this.int_target ) == 2 ){ sb.Append( "2 "); }
                sb.Append( singmaster.cc_e[ from.target ] );
                sb.Append( singmaster.cc_e[ from.secondary ] );

                return sb.ToString();
            }
        }

        internal static string report( SortedList< string, esgen > esi ){
            byte permitted= 0;
            if( (object)( esi ) == (object)( null ) ){ return "null"; }
            System.Text.StringBuilder sb= new StringBuilder();
            foreach( string et in esi.Keys ){
                if( esi[ et ].promoted ){ sb.Append( " +" ); }
                if( esi[ et ].permitted != permitted ){ permitted= esi[ et ].permitted;  sb.Append( ' ' );  sb.Append( permitted.ToString() ); }
                sb.Append( ' ' );
                sb.Append( singmaster.ee_e[ esi[ et ].secondary ] );
                sb.Append( singmaster.ee_e[ esi[ et ].target ] );
            }
            return sb.ToString();
        }

        internal static string report( esgen et ){
            byte permitted= 0;
            System.Text.StringBuilder sb= new StringBuilder();
            if( et.promoted ){ sb.Append( " +" ); }
            sb.Append( permitted.ToString() );
            sb.Append( ' ' );
            sb.Append( singmaster.ee_e[ et.secondary ] );
            sb.Append( singmaster.ee_e[ et.target ] );
            return sb.ToString();
        }

        internal static string report( SortedList< string, csgen > csi ){
            byte permitted= 0;
            if( (object)( csi ) == (object)( null ) ){ return "null"; }
            System.Text.StringBuilder sb= new StringBuilder();
            foreach( string ct in csi.Keys ){
                if( csi[ ct ].promoted ){ sb.Append( " +" ); }
                if( csi[ ct ].permitted != permitted ){ permitted = csi[ ct ].permitted;   sb.Append( ' ' ); sb.Append( permitted.ToString() ); }
                sb.Append( ' ' );
                sb.Append( singmaster.cc_e[ csi[ ct ].secondary ] );
                sb.Append( singmaster.cc_e[ csi[ ct ].target ] );
            }
            return sb.ToString();
        }
        internal static string report( csgen ct ){
            byte permitted= 0;
            System.Text.StringBuilder sb= new StringBuilder();
            if( ct.promoted ){ sb.Append( " +" ); }
            sb.Append( permitted.ToString() );
            sb.Append( ' ' );
            sb.Append( singmaster.cc_e[ ct.secondary ] );
            sb.Append( singmaster.cc_e[ ct.target ] );
            return sb.ToString();
        }

        // add elements from conjugate list
        enum clps{ space, target, complete, extraneous }; // conjugatelist parse state
        public static void insert( SortedList< conjugatestudyentry, conjugatestudy > inset, byte type, string conjugatelist ){
            clps lps;
            bool promoted= false;
            byte permitted= 0;
            esgen esini;
            csgen csini;
            switch( type ){ // 2==edge;  3==corner
            case 2:
                ef origine= singmaster.ee_e.origin;
                if( singmaster.ee_e.overrideorigin != ef.ef24 ){ origine= singmaster.ee_e.overrideorigin; }
                ef secondarye= ef.ef24;  ef targete= ef.ef24;
                esgen esin= new esgen();
                SortedList< string, esgen >esi= new SortedList< string, esgen >();
                lps= clps.space;
                foreach( char ch in conjugatelist + " " ){ switch( lps ){
                case clps.space: if( char.IsWhiteSpace( ch )){ continue; }
                    if( singmaster.ee_e.ContainsKey( ch ) ){ secondarye= singmaster.ee_e[ ch ];
                        if( secondarye == origine ){ lps= clps.extraneous;  continue; } // abort if conjugate contains origin or common buffer
                        lps= clps.target;
                    }
                    else if( ch == '+' ){ promoted= true; }
                    else if( char.IsDigit( ch ) ){ byte.TryParse( ch.ToString(), out permitted );  }
                    continue;
                case clps.target: if( char.IsWhiteSpace( ch )){ lps= clps.space; }
                    if( singmaster.ee_e.ContainsKey( ch ) ){ targete= singmaster.ee_e[ ch ];
                        if( targete == origine ){ lps= clps.extraneous;  continue; }
                        lps= clps.complete;
                    } continue;
                case clps.complete: if( char.IsWhiteSpace( ch )){
                        esin.secondary= secondarye;  esin.target= targete;
                        esin.promoted= promoted;  promoted= false;
                        esin.permitted= permitted;
                        esini= (esgen)( esin.MemberwiseClone() );
                        if( !esi.ContainsKey( esini.summary() ) ){ esi.Add( esini.summary(), esini ); }
                        lps= clps.space;  continue; }
                    lps= clps.extraneous;  continue;
                case clps.extraneous: if( !char.IsWhiteSpace( ch )){ continue; }
                    else{ lps= clps.space;  continue; }
                }}
                // add each esi element to conjugate study and prepare
                foreach( string cg in esi.Keys ){
                    esgen ei= esi[ cg ];
                    foreach( esgenS eis in esgenS.gen1( ei ) ){
                        string result= eis.ToString(); // TODO add to conjugate catalog here
                    }
                }
                break;
            case 3:
                cf originc= singmaster.cc_e.origin;
                if( singmaster.cc_e.overrideorigin != cf.cf24 ){ originc= singmaster.cc_e.overrideorigin; }
                cf secondaryc= cf.cf24;  cf targetc= cf.cf24;
                csgen csin= new csgen();
                SortedList< string, csgen > csi= new SortedList< string, csgen >();
                lps= clps.space;
                foreach( char ch in conjugatelist + " " ){ switch( lps ){
                case clps.space: if( char.IsWhiteSpace( ch )){ continue; }
                    if( singmaster.cc_e.ContainsKey( ch ) ){ secondaryc= singmaster.cc_e[ ch ];
                        if( secondaryc == originc ){ lps= clps.extraneous;  continue; } // abort if conjugate contains origin
                        lps= clps.target; }
                    else if( ch == '+' ){ promoted= true; }
                    else if( char.IsDigit( ch ) ){ byte.TryParse( ch.ToString(), out permitted );  }
                    continue;
                case clps.target: if( char.IsWhiteSpace( ch )){ lps= clps.space; }
                    if( singmaster.cc_e.ContainsKey( ch ) ){ targetc= singmaster.cc_e[ ch ];
                        if( targetc == originc ){ lps= clps.extraneous;  continue; }
                        lps= clps.complete;
                    } continue;
                case clps.complete: if( char.IsWhiteSpace( ch )){
                        csin.secondary= secondaryc;  csin.target= targetc;
                        csin.promoted= promoted;  promoted= false;
                        csin.permitted= permitted;
                        
                        csini= (csgen)( csin.MemberwiseClone() );
                        if (!csi.ContainsKey( csini.summary() )){ csi.Add( csini.summary(), csini ); }
                        lps= clps.space;  continue; }
                    lps= clps.extraneous;  continue;
                case clps.extraneous: if( !char.IsWhiteSpace( ch )){ continue; }
                    else{ lps= clps.space;  continue; }
                }}
                //csgenS csr= new csgenS( csi);
                foreach( string cg in csi.Keys ){
                    csgen ci= csi[ cg ];
                        foreach( csgenS cis in csgenS.gen1( ci ) ){ // WAS gen2 TODO place to target list
                            string result= cis.ToString();
                        }
                }
                // add each csi element to conjugate study and prepare

                break;
            }
        }
    }
} // CubeTrae namespace
