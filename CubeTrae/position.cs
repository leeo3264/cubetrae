using System.Collections.Generic;
namespace CubeTrae
{

    // <summary> cube motion planes and parse tokens, including composites, slices, and whole cube
    // inverse slice or inverse whole cube is a transposed plane where i.e. M maps to M' or y maps to y'
    // inverse slice is employed if generating a move from a repositioned plane</summary>
    public enum pm { // plane of motion section
        ut, ll, ff, rr, bp, dd, // single face turn
        uw, lw, fw, rw, bw, dw, // double layer turn
        ee, mm, ss, ei, mi, si, // slice turn and inverse slice turn (where i.e. a cl turn maps to a ccl turn)
        yy, xx, zz, yi, xi, zi, // all layers turn and inverse all layers turn
        // parsing tokens section
        // getq required if a '(' follows "g", indicating a quoted string i.e. g(_UF), otherwise it is g323 integer
        // similarly for posmark vs. posmarkq, and transform vs. transformq, but the quoted contents are restricted
        posmark, posmarkq, facechase, inverted, widen, get, getq, getname, hold, holdq, // @ @ | ' w g g g h h
        command, commandSym, buffer, transform, transformq, conjugate,  // k K o T T C
        /*token,*/ /*keyw,*/ enumb, pairimage, pairsetup, pairretrieve, whitespace, //                      // /* To */ /* Kw */ N m s _
        ps1, ps2, ps3, ps4, ps5, // ps1="[", ps2="]", ps3=":", ps4=",", ps5="::" // [ ] : , ::
        psm, k0m, // _(nonprinting) position marker for h, g, and facechase // _(nonprinting) k0 clearing command
        n0, n1, n2, n3, n4, n5, n6, n7, n8, n9,                         // 0 1 2 3 4 5 6 7 8 9 // free numerals
        pm60 /* end of range indicator */ }

    public enum pmSect{ noop, singmaster, doubl, slice, invertedslice, whole, invertedwhole, modify, mem,
        command, faceletchaseRead, display, transform, conjugate, puncSym, parameter, numeral, pmSect17 };
    public class pmT{ // wrapper for _pm_ supports (ft)( pmi ) where pmi is a particular pm
        #region box/unbox conversions
        pm pm_subject;

        public static implicit operator pmT( pm valuepm ){
            pmT pm_return= new pmT();
            pm_return.pm_subject= valuepm;
            return pm_return;
        }
        public static implicit operator pm( pmT valuepmT ){ return valuepmT.pm_subject; }
        public static explicit operator byte( pmT valuepmT ){ return (byte)( valuepmT.pm_subject ); }
        
        #endregion
        const byte pmEntireRange= (byte)( (byte)( pm.pm60 ) + 1 );
        static pmSect[] pm_pmSect_= new pmSect[ pmEntireRange ]{
            #region pm_pmSect_
            pmSect.singmaster, pmSect.singmaster, pmSect.singmaster, pmSect.singmaster, pmSect.singmaster, pmSect.singmaster, // ULFRDB
            pmSect.doubl, pmSect.doubl, pmSect.doubl, pmSect.doubl, pmSect.doubl, pmSect.doubl, // Uw Lw Fw ... Dw
            pmSect.slice, pmSect.slice, pmSect.slice, pmSect.invertedslice, pmSect.invertedslice, pmSect.invertedslice, // M E S M' E' S'
            pmSect.whole, pmSect.whole, pmSect.whole, pmSect.invertedwhole, pmSect.invertedwhole, pmSect.invertedwhole, // y x z y' x' z'
            pmSect.noop, pmSect.display, pmSect.faceletchaseRead, pmSect.modify, pmSect.modify, // @ @ | ' w
            pmSect.mem, pmSect.mem, pmSect.mem, pmSect.mem, pmSect.mem, // g g g h h
            pmSect.command, pmSect.faceletchaseRead, pmSect.display, pmSect.transform, pmSect.transform, pmSect.conjugate, // k K o T12 Txy C
            pmSect.parameter, pmSect.mem, pmSect.mem, pmSect.mem, pmSect.noop, // N m s sn _
            pmSect.puncSym, pmSect.puncSym, pmSect.puncSym, pmSect.puncSym, pmSect.puncSym, // [ ] : , ::
            pmSect.puncSym,  pmSect.puncSym, // position marker (non-displaying token) // k0clearing marker (non-printing token)
            pmSect.numeral, pmSect.numeral, pmSect.numeral, pmSect.numeral, pmSect.numeral,                     // 0 1 2 3 4
            pmSect.numeral, pmSect.numeral, pmSect.numeral, pmSect.numeral, pmSect.numeral, pmSect.pmSect17      // 5 6 7 8 9
            #endregion
        };
        public static explicit operator pmSect( pmT value ){
            return pm_pmSect_[ (byte)( value.pm_subject ) ]; } // employed in singmaster.assignLetters
        public static pmSect pm_pmSect( pm pmi ){ return pm_pmSect_[ (byte)( pmi ) ]; } // employed in parse1

        static pm[] parallel_= new pm[]{ pm.dd, pm.rr, pm.bp, pm.ll, pm.ff, pm.ut };
        public static pm parallel( pm value ){
            if (value > pm.dd) { return value; } else return parallel_[ (byte)( value ) ];
        }

        const byte pmSectEntireRange= (byte)( (byte)( pmSect.pmSect17 ) + 1 );
        static bool[] pmSect_boolarray( params pmSect[] given ){
            bool[] res= new bool[ pmSectEntireRange ];
            for( byte i = 0; i < pmSectEntireRange; i++ ){ res[ i ]= false; }
            foreach ( pmSect pmSecti in given ){ res[ (byte)( pmSecti ) ]= true; }
            return res;
        }
        static bool[] pmSectMove_= pmSect_boolarray( pmSect.singmaster, pmSect.doubl, 
            pmSect.slice, pmSect.invertedslice, pmSect.whole, pmSect.invertedwhole,
            pmSect.modify, pmSect.numeral );
        public static bool pmSectMove( pmSect pmsi ){ return pmSectMove_[ (byte)( pmsi ) ]; } // empolyed in parse2 conjugate

        #region assist tables for fromPlaneWithPos
        const byte pmPrimaryPlanesRange= (byte)( (byte)( pm.dd ) + 1 );
        static ef[] pm_facepos0= new ef[ pmPrimaryPlanesRange ]{ ef.aq_UB, ef.ed_LU, ef.ic_FU, ef.mb_RU, ef.qa_BU, ef.uk_DF };
        static ef[] pm_facepos1= new ef[ pmPrimaryPlanesRange ]{ ef.bm_UR, ef.fl_LF, ef.jp_FR, ef.nt_RB, ef.rh_BL, ef.vo_DR };
        static ef[] pm_facepos2= new ef[ pmPrimaryPlanesRange ]{ ef.ci_UF, ef.gx_LD, ef.ku_FD, ef.ov_RD, ef.sw_BD, ef.ws_DB };
        static ef[] pm_facepos3= new ef[ pmPrimaryPlanesRange ]{ ef.de_UL, ef.hr_LB, ef.lf_FL, ef.pj_RF, ef.tn_BR, ef.xg_DL };
        #endregion
        public static ef fromPlaneWithPos( pm plane, byte pos ){ // employed to display center facelet twist according to the letter system
            if ( (byte)( plane ) >= pmPrimaryPlanesRange ){ return ef.ef24; }
            switch ( ( pos + 4 ) % 4 ){
            case 0: return pm_facepos0[ (byte)( plane ) ];    case 1: return pm_facepos1[ (byte)( plane ) ];
            case 2: return pm_facepos2[ (byte)( plane ) ];    case 3: return pm_facepos3[ (byte)( plane ) ];
            default: return ef.ef24; }
        }

        public static string pmT_string( pm valuepm ){ return parse.pm_string( valuepm ); }
    } // \ class pmT

    public enum ft : byte { // face turn
        none,
        ut1, ut2, ut3, L1, L2, L3, f1, f2, f3,          // face
        r1, r2, r3, bp1, bp2, bp3, d1, d2, d3,
        utw1, utw2, utw3, Lw1, Lw2, Lw3, fw1, fw2, fw3, // wide
        rw1, rw2, rw3, bpw1, bpw2, bpw3, dw1, dw2, dw3,
        e1, e2, e3, m1, m2, m3, s1, s2, s3,             // slice
        y1, y2, y3, x1, x2, x3, z1, z2, z3,             // whole
        T0, T1, T2, // reset symmetry transform marker, invert, reflect across m axis
        Ty1, Ty2, Ty3, Tx1, Tx2, Tx3, Tz1, Tz2, Tz3,    // whole-cube reposition transforms
        ps1, ps2, ps3, ps4, ps5, k0m,                   // [ ] : , :: // k0m indicates the command k0, clear and reset buffer
        posMark, ft74 /* posmark - null move position indicator;  ft68 - end of range indicator */ };

    //byte[] ftscore= new byte[]{ // employ for counting move types TODO
    //    0,
    //    ut1, ut2, ut3, L1, L2, L3, f1, f2, f3,          // face
    //    r1, r2, r3, bp1, bp2, bp3, d1, d2, d3,
    //    utw1, utw2, utw3, Lw1, Lw2, Lw3, fw1, fw2, fw3, // wide
    //    rw1, rw2, rw3, bpw1, bpw2, bpw3, dw1, dw2, dw3,
    //    e1, e2, e3, m1, m2, m3, s1, s2, s3,             // slice
    //    y1, y2, y3, x1, x2, x3, z1, z2, z3,             // whole
    //    T0, T1, T2, // reset symmetry transform marker, invert, reflect across m axis
    //    Ty1, Ty2, Ty3, Tx1, Tx2, Tx3, Tz1, Tz2, Tz3,    // whole-cube reposition transforms
    //    ps1, ps2, ps3, ps4, ps5,                        // [ ] : , :: 
    //    posMark, ft73 /* posmark - null move p

    //};

    public enum ftSub { noop, face, doubl, slice, invertedslice, whole, invertedwhole, posmark, transform, puncSym, ftSub10 };
    public enum ftOp { noop, invert, doubl, widen, widen3, xyz_Txyz }; // reading transformations
    public class ftT{ // wrapper for _f_ supports parallel, wide, rotation number, floor rotation,
        #region box/unbox conversions
        ft f_subject;

        // bijective conversions to the enum _f_
        public static implicit operator ftT( ft valueft ){
            ftT ft_return= new ftT();
            ft_return.f_subject= valueft;
            return ft_return;
        }
        public static implicit operator ft( ftT valueftT ){ return valueftT.f_subject; }
        public static explicit operator byte( ftT valueftT ){ return (byte)( valueftT.f_subject ); }
        #endregion
        const byte ftEntireRange= (byte)( (byte)( ft.ft74 ) + 1 );
        #region invert // respond to a \' encountered after a plane name: U' E' y'
        static ft[] invert_= new ft[ ftEntireRange ]{ ft.none,
            ft.ut3, ft.ut2, ft.ut1, ft.L3, ft.L2, ft.L1, ft.f3, ft.f2, ft.f1,          // face
            ft.r3, ft.r2, ft.r1, ft.bp3, ft.bp2, ft.bp1, ft.d3, ft.d2, ft.d1,
            ft.utw3, ft.utw2, ft.utw1, ft.Lw3, ft.Lw2, ft.Lw1, ft.fw3, ft.fw2, ft.fw1, // wide
            ft.rw3, ft.rw2, ft.rw1, ft.bpw3, ft.bpw2, ft.bpw1, ft.dw3, ft.dw2, ft.dw1,
            ft.e3, ft.e2, ft.e1, ft.m3, ft.m2, ft.m1, ft.s3, ft.s2, ft.s1,             // slice
            ft.y3, ft.y2, ft.y1, ft.x3, ft.x2, ft.x1, ft.z3, ft.z2, ft.z1,             // whole
            ft.T0, ft.T1, ft.T2, // transform noop, invert, reflect across m axis
            ft.Ty3, ft.Ty2, ft.Ty1, ft.Tx3, ft.Tx2, ft.Tx1, ft.Tz3, ft.Tz2, ft.Tz1,
            ft.ps2, ft.ps1, ft.ps3, ft.ps4, ft.ps5, ft.k0m,              // [ ] : , :: // experiment: invert of ] is [
            ft.posMark, ft.ft74 };
        #endregion
        public static ft invert( ft fti ){  return invert_[ (byte)( fti ) ];  } // employed for processing k1

        #region doubl // respond to a '2' encountered after a plane name i.e. U2, Uw2, E2, y2
        static ft[] doubl_= new ft[ ftEntireRange ]{ ft.none,
            ft.ut2, ft.ut2, ft.ut2, ft.L2, ft.L2, ft.L2, ft.f2, ft.f2, ft.f2,          // face
            ft.r2, ft.r2, ft.r2, ft.bp2, ft.bp2, ft.bp2, ft.d2, ft.d2, ft.d2,
            ft.utw2, ft.utw2, ft.utw2, ft.Lw2, ft.Lw2, ft.Lw2, ft.fw2, ft.fw2, ft.fw2, // wide
            ft.rw2, ft.rw2, ft.rw2, ft.bpw2, ft.bpw2, ft.bpw2, ft.dw2, ft.dw2, ft.dw2,
            ft.e2, ft.e2, ft.e2, ft.m2, ft.m2, ft.m2, ft.s2, ft.s2, ft.s2,             // slice
            ft.y2, ft.y2, ft.y2, ft.x2, ft.x2, ft.x2, ft.z2, ft.z2, ft.z2,             // whole
            ft.T0, ft.T1, ft.T2, // transform noop, invert, reflect across m axis
            ft.Ty2, ft.Ty2, ft.Ty2, ft.Tx2, ft.Tx2, ft.Tx2, ft.Tz2, ft.Tz2, ft.Tz2,
            ft.ps1, ft.ps2, ft.ps3, ft.ps4, ft.ps5, ft.k0m, // [ ] : , ::
            ft.posMark, ft.ft74 };
        #endregion
        public static ft doubl( ft fti ){  return doubl_[ (byte)( fti ) ];  } // support [f]2 == z2

        #region widen // suport converting L2 to Lw2 with other face moves
        static ft[] widen_= new ft[ ftEntireRange ]{ ft.none,
            ft.utw1, ft.utw2, ft.utw3, ft.Lw1, ft.Lw2, ft.Lw3, ft.fw1, ft.fw2, ft.fw3, // wide
            ft.rw1, ft.rw2, ft.rw3, ft.bpw1, ft.bpw2, ft.bpw3, ft.dw1, ft.dw2, ft.dw3,
            ft.utw1, ft.utw2, ft.utw3, ft.Lw1, ft.Lw2, ft.Lw3, ft.fw1, ft.fw2, ft.fw3, // wide
            ft.rw1, ft.rw2, ft.rw3, ft.bpw1, ft.bpw2, ft.bpw3, ft.dw1, ft.dw2, ft.dw3,
            ft.e1, ft.e2, ft.e3, ft.m1, ft.m2, ft.m3, ft.s1, ft.s2, ft.s3,
            ft.y1, ft.y2, ft.y3, ft.x1, ft.x2, ft.x3, ft.z1, ft.z2, ft.z3,
            ft.T0, ft.T1, ft.T2,
            ft.Ty1, ft.Ty2, ft.Ty3, ft.Tx1, ft.Tx2, ft.Tx3, ft.Tz1, ft.Tz2, ft.Tz3,
            ft.ps1, ft.ps2, ft.ps3, ft.ps4, ft.ps5, ft.k0m, // [ ] : , ::
            ft.posMark, ft.ft74 };
        #endregion
        public static ft widen( ft fti ){  return widen_[ (byte)( fti ) ];  } // employed in position.move

        // for construcing the equivanent whole-cube motion given a 3Rw or 3Lw or similar designation
        #region widen3 // support converting 3Rw' to x'
        static ft[] widen3_= new ft[ ftEntireRange ]{ ft.none,
            ft.y1, ft.y2, ft.y3, ft.x3, ft.x2, ft.x1, ft.z1, ft.z2, ft.z3, // wide
            ft.x1, ft.x2, ft.x3, ft.z3, ft.z2, ft.z1, ft.y3, ft.y2, ft.y1,
            ft.y1, ft.y2, ft.y3, ft.x3, ft.x2, ft.x1, ft.z1, ft.z2, ft.z3, // wide
            ft.x1, ft.x2, ft.x3, ft.z3, ft.z2, ft.z1, ft.y3, ft.y2, ft.y1,
            ft.e1, ft.e2, ft.e3, ft.m1, ft.m2, ft.m3, ft.s1, ft.s2, ft.s3,
            ft.y1, ft.y2, ft.y3, ft.x1, ft.x2, ft.x3, ft.z1, ft.z2, ft.z3,
            ft.T0, ft.T1, ft.T2,
            ft.Ty1, ft.Ty2, ft.Ty3, ft.Tx1, ft.Tx2, ft.Tx3, ft.Tz1, ft.Tz2, ft.Tz3,
            ft.ps1, ft.ps2, ft.ps3, ft.ps4, ft.ps5, ft.k0m, // [ ] : , ::
            ft.posMark, ft.ft74 };
        #endregion
        public static ft widen3( ft fti ){  return widen3_[ (byte)( fti ) ];  } // employed converting "[ f ]" to "z"

        public static bool parallel( ft ft1, ft ft2 ){
            pm pm1= ftT.ft_pm( ft1 );  pm pm2= ftT.ft_pm( ft2 );
            return pm1 != pm2 && pm1 == pmT.parallel( pm2 );
        }

        // employed for deciphering wide moves into face-chase notation, give the unwide portion
        #region unwiden // support converting 2Dw' to Dw'
        static ft[] unwiden_= new ft[ ftEntireRange ]{ ft.none,
        ft.e3, ft.e2, ft.e1, ft.m1, ft.m2, ft.m3, ft.s1, ft.s2, ft.s3,          // face transitions to slice if widened
        ft.m3, ft.m2, ft.m1, ft.s3, ft.s2, ft.s1, ft.e1, ft.e2, ft.e3,
        ft.ut1, ft.ut2, ft.ut3, ft.L1, ft.L2, ft.L3, ft.f1, ft.f2, ft.f3,       // wide transition to single face
        ft.r1, ft.r2, ft.r3, ft.bp1, ft.bp2, ft.bp3, ft.d1, ft.d2, ft.d3,
        ft.e1, ft.e2, ft.e3, ft.m1, ft.m2, ft.m3, ft.s1, ft.s2, ft.s3,          // slice
        ft.y1, ft.y2, ft.y3, ft.x1, ft.x2, ft.x3, ft.z1, ft.z2, ft.z3,          // whole
        ft.T0, ft.T1, ft.T2, // reset symmetry transform marker, invert, reflect across m axis
        ft.Ty1, ft.Ty2, ft.Ty3, ft.Tx1, ft.Tx2, ft.Tx3, ft.Tz1, ft.Tz2, ft.Tz3, // whole-cube reposition transforms
        ft.ps1, ft.ps2, ft.ps3, ft.ps4, ft.ps5, ft.k0m,                         // [ ] : , :: 
        ft.posMark, ft.ft74 };
        #endregion
        public static ft unwiden( ft fti ){ return unwiden_[ (byte)( fti ) ]; } // employed deciphering face-chase forms

        // employed deciphering 2w moves, give the equivalent motion
        #region unwiden2 // support convertintg 2Dw' to U
        static ft[] unwiden2_= new ft[ ftEntireRange ]{ ft.none,
            ft.d3, ft.d2, ft.d1, ft.r3, ft.r2, ft.r1, ft.bp3, ft.bp2, ft.bp1,  // face parallel for slice
            ft.L3, ft.L2, ft.L1, ft.f3, ft.f2, ft.f1, ft.ut3, ft.ut2, ft.ut1,
            ft.d1, ft.d2, ft.d3, ft.r1, ft.r2, ft.r3, ft.bp1, ft.bp2, ft.bp3,  // wide
            ft.L1, ft.L2, ft.L3, ft.f1, ft.f2, ft.f3, ft.ut1, ft.ut2, ft.ut3,
            ft.ut1, ft.ut2, ft.ut3, ft.L3, ft.L2, ft.L1, ft.f3, ft.f2, ft.f1,  // slice
            ft.y1, ft.y2, ft.y3, ft.x1, ft.x2, ft.x3, ft.z1, ft.z2, ft.z3,     // whole
            ft.T0, ft.T1, ft.T2, // reset transform marker, invert, reflect across m axis
            ft.Ty1, ft.Ty2, ft.Ty3, ft.Tx1, ft.Tx2, ft.Tx3, ft.Tz1, ft.Tz2, ft.Tz3,
            ft.ps1, ft.ps2, ft.ps3, ft.ps4, ft.ps5, ft.k0m, // [ ] : , ::
            ft.posMark, ft.ft74 // posmark - null move position indicator;  ft68 - end of range indicator
        };
        #endregion
        public static ft unwiden2( ft fti ){ return unwiden2_[ (byte)( fti ) ]; } // employed in position.move

        // employed decipnering slice moves
        #region wide_wide3part_ // give the whole-cube rotation component
        static ft[] wide_wide3part_= new ft[ ftEntireRange ]{ ft.none,
            ft.ut1, ft.ut2, ft.ut3, ft.L1, ft.L2, ft.L3, ft.f1, ft.f2, ft.f3,  // face
            ft.r1, ft.r2, ft.r3, ft.bp1, ft.bp2, ft.bp3, ft.d1, ft.d2, ft.d3,
            ft.y1, ft.y2, ft.y3, ft.x3, ft.x2, ft.x1, ft.z1, ft.z2, ft.z3,     // wide
            ft.x1, ft.x2, ft.x3, ft.z3, ft.z2, ft.z1, ft.y3, ft.y2, ft.y1,
            ft.y3, ft.y2, ft.y1, ft.x3, ft.x2, ft.x1, ft.z1, ft.z2, ft.z3,     // slice
            ft.y1, ft.y2, ft.y3, ft.x1, ft.x2, ft.x3, ft.z1, ft.z2, ft.z3,     // whole
            ft.T0, ft.T1, ft.T2, // internal transform marker, invert, reflect across m axis
            ft.Ty1, ft.Ty2, ft.Ty3, ft.Tx1, ft.Tx2, ft.Tx3, ft.Tz1, ft.Tz2, ft.Tz3,
            ft.ps1, ft.ps2, ft.ps3, ft.ps4, ft.ps5, ft.k0m, // [ ] : , ::
            ft.posMark, ft.ft74 // posmark - null move but operates as a position indicator;  ft68 - end of range indicator
        };
        #endregion
        public static ft wide_wide3part( ft fti ){ return wide_wide3part_[ (byte)( fti ) ]; } // employed in position.move

        #region xyz_Txyz // support converting Txy'z2 to symmetry transforms Tx Ty' Tz2 after extracting xy'z2
        static ft[] xyz_Txyz_= new ft[ ftEntireRange ]{ ft.none,
            ft.ut1, ft.ut2, ft.ut3, ft.L1, ft.L2, ft.L3, ft.f1, ft.f2, ft.f3,          // face
            ft.r1, ft.r2, ft.r3, ft.bp1, ft.bp2, ft.bp3, ft.d1, ft.d2, ft.d3,
            ft.utw1, ft.utw2, ft.utw3, ft.Lw1, ft.Lw2, ft.Lw3, ft.fw1, ft.fw2, ft.fw3, // wide
            ft.rw1, ft.rw2, ft.rw3, ft.bpw1, ft.bpw2, ft.bpw3, ft.dw1, ft.dw2, ft.dw3,
            ft.e1, ft.e2, ft.e3, ft.m1, ft.m2, ft.m3, ft.s1, ft.s2, ft.s3,             // slice
            ft.Ty1, ft.Ty2, ft.Ty3, ft.Tx1, ft.Tx2, ft.Tx3, ft.Tz1, ft.Tz2, ft.Tz3,             // whole
            ft.T0, ft.T1, ft.T2, // key prefix transform command, invert, reflect across m axis
            ft.Ty3, ft.Ty2, ft.Ty1, ft.Tx3, ft.Tx2, ft.Tx1, ft.Tz3, ft.Tz2, ft.Tz1,    // xyz_Txyz of a Transform group is its inverse, to help generate the transform
            ft.ps1, ft.ps2, ft.ps3, ft.ps4, ft.ps5, ft.k0m, // [ ] : , ::
            ft.posMark, ft.ft74 };
        #endregion
        internal static ft xyz_Txyz( ft fti ){  return xyz_Txyz_[ (byte)( fti ) ];  } // employed pos.move Transform section

        // constructions supporting deciphering and combining operations during parsing -- all employed in parse2
        public void Op( ftOp ftOpi ){
            switch ( ftOpi ){
            case ftOp.invert: f_subject= invert_[ (byte)( f_subject ) ];  break;
            case ftOp.doubl: f_subject= doubl_[ (byte)( f_subject ) ];  break;
            case ftOp.widen: f_subject= widen_[ (byte)( f_subject ) ];  break;
            case ftOp.widen3: f_subject= widen3_[ (byte)( f_subject ) ];  break;
            case ftOp.xyz_Txyz: f_subject= xyz_Txyz_[ (byte)( f_subject ) ];  break;
            }
        }

        #region reflectM // support k2 command
        static ft[] reflectM_= new ft[ ftEntireRange ]{ ft.none,
            ft.ut3, ft.ut2, ft.ut1, ft.r3, ft.r2, ft.r1, ft.f3, ft.f2, ft.f1,
            ft.L3, ft.L2, ft.L1, ft.bp3, ft.bp2, ft.bp1, ft.d3, ft.d2, ft.d1,
            ft.utw3, ft.utw2, ft.utw1, ft.rw3, ft.rw2, ft.rw1, ft.fw3, ft.fw2, ft.fw1,
            ft.Lw3, ft.Lw2, ft.Lw1, ft.bpw3, ft.bpw2, ft.bpw1, ft.dw3, ft.dw2, ft.dw1,
            ft.e3, ft.e2, ft.e1, ft.m1, ft.m2, ft.m3, ft.s3, ft.s2, ft.s1,
            ft.y3, ft.y2, ft.y1, ft.x1, ft.x2, ft.x3, ft.z3, ft.z2, ft.z1,             // whole
            ft.T0, ft.T1, ft.T2, // key prefix transform command, invert, reflect across m axis
            ft.Ty1, ft.Ty2, ft.Ty3, ft.Tx1, ft.Tx2, ft.Tx3, ft.Tz1, ft.Tz2, ft.Tz3,
            ft.ps1, ft.ps2, ft.ps3, ft.ps4, ft.ps5, ft.k0m, // [ ] : , ::
            ft.posMark, ft.ft74 };
        #endregion
        internal static ft reflectM( ft fti ){ return reflectM_[ (byte)( fti ) ]; } // employed processing k2


        // give the ft category, for deciphering the move type for position.move
        static ftSub[] ft_ftSub_= new ftSub[ ftEntireRange ]{
        #region ft_ftSub
            ftSub.noop,
            ftSub.face, ftSub.face, ftSub.face,  ftSub.face, ftSub.face, ftSub.face, //UL
            ftSub.face, ftSub.face, ftSub.face,  ftSub.face, ftSub.face, ftSub.face, //FR
            ftSub.face, ftSub.face, ftSub.face,  ftSub.face, ftSub.face, ftSub.face, //DB
            ftSub.doubl, ftSub.doubl, ftSub.doubl,  ftSub.doubl, ftSub.doubl, ftSub.doubl,  // UwLw
            ftSub.doubl, ftSub.doubl, ftSub.doubl,  ftSub.doubl, ftSub.doubl, ftSub.doubl,  // FwRw
            ftSub.doubl, ftSub.doubl, ftSub.doubl,  ftSub.doubl, ftSub.doubl, ftSub.doubl,  // BwDw
            ftSub.slice, ftSub.slice, ftSub.slice,  ftSub.slice, ftSub.slice, ftSub.slice, // E M
            ftSub.slice, ftSub.slice, ftSub.slice, // S
            ftSub.whole, ftSub.whole, ftSub.whole,  ftSub.whole, ftSub.whole, ftSub.whole, // y x
            ftSub.whole, ftSub.whole, ftSub.whole, // z
            ftSub.transform, ftSub.transform, ftSub.transform,
            ftSub.transform, ftSub.transform, ftSub.transform,  ftSub.transform, ftSub.transform, ftSub.transform,
            ftSub.transform, ftSub.transform, ftSub.transform, // T z
            ftSub.puncSym, ftSub.puncSym, ftSub.puncSym, ftSub.puncSym, ftSub.puncSym, ftSub.puncSym, // [ ] : , ::
            ftSub.noop, ftSub.ftSub10 };
        #endregion
        public static ftSub ft_ftSub( ft fti ){ return ft_ftSub_[ (byte)( fti ) ]; } // employed ssT.IniMoveTables, parse3

        static bool[] ftSubCornermove_ = new bool[ (byte)( ftSub.ftSub10 ) ]{
            // noop, face, doubl, slice, invertedslice, whole, invertedwhole, posmark, transform, puncSym, ftSub10 
               false, true, false, false, false,        true, true, false, false, false };
        public static bool ftSubCornermove( ftSub fsi ){ return ftSubCornermove_[ (byte)( fsi ) ]; }

        // convert a plane reading pm.ut into the face turn motion ft.Ut1
        const byte pmEntireRange= (byte)( (byte)( pm.pm60 ) + 1 );
        static ft[] pm_f1= new ft[ ftEntireRange ]{
        #region pm_f
            ft.ut1, ft.L1, ft.f1, ft.r1, ft.bp1, ft.d1,
            ft.utw1, ft.Lw1, ft.fw1, ft.rw1, ft.bpw1, ft.dw1,
            ft.e1, ft.m1, ft.s1, ft.e3, ft.m3, ft.s3,
            ft.y1, ft.x1, ft.z1, ft.y3, ft.x3, ft.z3,
            ft.posMark, ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74,
            ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74,  ft.ft74, ft.ft74,  ft.ft74,
            ft.ps1, ft.ps2, ft.ps3, ft.ps4, ft.ps5,
            ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74,
            ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74,
            ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74, 
            ft.ft74, ft.ft74, ft.ft74, ft.ft74, ft.ft74,  ft.ft74,
            ft.ft74 };
        #endregion

        // support consolidating rotations in the same plane
        public static ft ftmult( pm pmi, int mult ){
            if( mult < -4 ){ throw new System.Exception( "unsupported" ); }
            switch( ( mult + 4 ) % 4 ){
                case 0: return ft.none;
                case 1: return ftT.pm_f1[ (byte)( pmi ) ];
                case 2: return ftT.doubl( ftT.pm_f1[ (byte)( pmi ) ] );
                case 3: return ftT.invert( ftT.pm_f1[ (byte)( pmi ) ] );
            }
            return ft.none;
        }


        // empolyed in parse.parse2 converting a plane of motion list to a sequence of moves
        public static implicit operator ftT( pm pm_subject ){
            return (ftT)( pm_f1[ (byte)( pm_subject ) ] );
        }

        static pm[] ft_pm_= new pm[ ftEntireRange ]{
        #region ft_pm
            pm.pm60,
            pm.ut, pm.ut, pm.ut,  pm.ll, pm.ll, pm.ll,  pm.ff, pm.ff, pm.ff, // face
            pm.rr, pm.rr, pm.rr,  pm.bp, pm.bp, pm.bp,  pm.dd, pm.dd, pm.dd,
            pm.dd, pm.dd, pm.dd,  pm.rr, pm.rr, pm.rr,  pm.bp, pm.bp, pm.bp, // wide
            pm.ll, pm.ll, pm.ll,  pm.ff, pm.ff, pm.ff,  pm.ut, pm.ut, pm.ut,
            pm.ee, pm.ee, pm.ee,  pm.mm, pm.mm, pm.mm,  pm.ss, pm.ss, pm.ss, // slice
            pm.yy, pm.yy, pm.yy,  pm.xx, pm.xx, pm.xx,  pm.zz, pm.zz, pm.zz, // whole
            pm.pm60, pm.pm60, pm.pm60, // reset internal transform marker, invert, reflect across m axis
            pm.yi, pm.yi, pm.yi,  pm.xi, pm.xi, pm.xi,  pm.zi, pm.zi, pm.zi,
            pm.ps1, pm.ps2, pm.ps3, pm.ps4, pm.ps5, pm.k0m, // [ ] : , ::
            pm.pm60, pm.pm60 }; // posmark - null move position indicator;  ft68 - end of range indicator
        #endregion

        // employed in move identifying the center facelet motion (usually not visible unless turning a picture cube)
        public static pm ft_pm( ft ft_subject ){
            return ft_pm_[ (byte)( ft_subject ) ];
        }

        // give the rotation component for each face turn
        static byte[] ft_byte_= new byte[ ftEntireRange ]{
        #region ft_byte
            0,
            1, 2, 3,  1, 2, 3,  1, 2, 3,  // face
            1, 2, 3,  1, 2, 3,  1, 2, 3,
            1, 2, 3,  1, 2, 3,  1, 2, 3,  // wide
            1, 2, 3,  1, 2, 3,  1, 2, 3,
            1, 2, 3,  1, 2, 3,  1, 2, 3,  // slice
            1, 2, 3,  1, 2, 3,  1, 2, 3,  // whole
            0, 0, 0, // internal transform marker, invert, reflect across m axis
            1, 2, 3,  1, 2, 3,  1, 2, 3,
            0, 0, 0, 0, 0, 0,
            0, 0 // posmark - null move position indicator;  ft68 - end of range indicator
        };
        #endregion

        // give the motion amount for each face turn -- employed in move calculating the center face turn
        public static byte ft_byte( ft ft_subject ){
            return ft_byte_[ (byte)( ft_subject ) ];
        }

        static string[] ft_str = new string[ ftEntireRange ]{
        #region ft_str
            "",
            "U", "U2", "U'",   "L", "L2", "L'",  "F", "F2", "F'",
            "R", "R2", "R'",   "B", "B2", "B'",  "D", "D2", "D'",
            "Uw", "Uw2", "Uw'",   "Lw", "Lw2", "Lw'",  "Fw", "Fw2", "Fw'",
            "Rw", "Rw2", "Rw'",   "Bw", "Bw2", "Bw'",  "Dw", "Dw2", "Dw'",
            "E", "E2", "E'",  "M", "M2", "M'",  "S", "S2", "S'",
            "y", "y2", "y'",  "x", "x2", "x'",  "z", "z2", "z'",
            "T0", "T1", "T2",
            "Ty", "Ty2", "Ty'",  "Tx", "Tx2", "Tx'",  "Tz", "Tz2", "Tz'",
            "[", "]", ":", ",", "::", "k0",
            "@", "_"
        #endregion
        };
        public override string ToString(){ // currently unemployed
            return ft_str[ (byte)( f_subject ) ];
        }
        public static string EnumString( ft fti ){
            return ft_str[ (byte)( fti ) ];
        }

        // transforming each move according to a disposition rotation
        #region x1 y1 transformation tables for each ft
        static ft[] x1= new ft[ ftEntireRange ]{
            ft.none,
            ft.bp1, ft.bp2, ft.bp3,  ft.L1, ft.L2, ft.L3,  ft.ut1, ft.ut2, ft.ut3,
            ft.r1, ft.r2, ft.r3,  ft.d1, ft.d2, ft.d3,  ft.f1, ft.f2, ft.f3,
            ft.bpw1, ft.bpw2, ft.bpw3,  ft.Lw1, ft.Lw2, ft.Lw3,  ft.utw1, ft.utw2, ft.utw3,
            ft.rw1, ft.rw2, ft.rw3,  ft.dw1, ft.dw2, ft.dw3,  ft.fw1, ft.fw2, ft.fw3,
            ft.s1, ft.s2, ft.s3, ft.m1, ft.m2, ft.m3, ft.e3, ft.e2, ft.e1,
            ft.z3, ft.z2, ft.z1,  ft.x1, ft.x2, ft.x3,  ft.y1, ft.y2, ft.y3,
            ft.T0, ft.T1, ft.T2,
            ft.Ty1, ft.Ty2, ft.Ty3,  ft.Tx1, ft.Tx2, ft.Tx3,  ft.Tz1, ft.Tz2, ft.Tz3,
            ft.ps1, ft.ps2, ft.ps3, ft.ps4, ft.ps5,  ft.k0m, // [ ] : , ::
            ft.posMark, ft.ft74 };
        static ft[] y1= new ft[ ftEntireRange ]{
            ft.none,
            ft.ut1, ft.ut2, ft.ut3,  ft.bp1, ft.bp2, ft.bp3,  ft.L1, ft.L2, ft.L3,
            ft.f1, ft.f2, ft.f3,  ft.r1, ft.r2, ft.r3,  ft.d1, ft.d2, ft.d3,
            ft.utw1, ft.utw2, ft.utw3,  ft.bpw1, ft.bpw2, ft.bpw3,  ft.Lw1, ft.Lw2, ft.Lw3,
            ft.fw1, ft.fw2, ft.fw3,  ft.rw1, ft.rw2, ft.rw3,  ft.dw1, ft.dw2, ft.dw3,
            ft.e1, ft.e2, ft.e3, ft.s3, ft.s2, ft.s1, ft.m1, ft.m2, ft.m3,
            ft.y1, ft.y2, ft.y3,  ft.z1, ft.z2, ft.z3,  ft.x3, ft.x2, ft.x1,
            ft.T0, ft.T1, ft.T2,
            ft.Ty1, ft.Ty2, ft.Ty3,  ft.Tx1, ft.Tx2, ft.Tx3,  ft.Tz1, ft.Tz2, ft.Tz3,
            ft.ps1, ft.ps2, ft.ps3, ft.ps4, ft.ps5,  ft.k0m, // [ ] : , ::
            ft.posMark, ft.ft74 };
        #endregion

        #region construct all x y z tables
        static ft[] mult( ft[] L, ft[] R ){
            if( L.Length != R.Length ){ throw new System.Exception( "undefined" ); }
            ft[] res= new ft[ L.Length ];
            for( byte i= 0; i < L.Length; i++ ){ res[ i ] = L[ (byte)( R[ i ] ) ]; }
            return res;
        }
        static ft[] square( ft[] S ){
            ft[] res= new ft[ S.Length ];
            for( byte i= 0; i < S.Length; i++ ){
                res[ i ]= S[ (byte)( S[ i ] ) ];
            }
            return res;
        }
        static ft[] x2= square( x1 );
        static ft[] x3= mult( x1, x2 );
        static ft[] y2= square( y1 );
        static ft[] y3= mult( y1, y2 );
        static ft[] z1= mult( x3, mult( y1, x1 ) );
        static ft[] z2= square( z1 );
        static ft[] z3= mult( z1, z2 );
        #endregion

        #region tests // verify the integrity of the constructed talbes
        internal static bool equal( ft[] L, ft[] R ){
            if ( L.Length != R.Length ){ return false; }
            for( byte i= 0; i < L.Length; i++ ){
                if( L[ i ] != R[ i ] ){ 
                    return false; }
            }
            return true;
        }
        internal static bool isperm( ft[] S ){
            bool[] checks= new bool[ S.Length ];
            for( byte i= 0; i < S.Length; i++ ){ checks[ i ]= false; }
            foreach( ft fti in S ){
                if( checks[ (byte)( fti ) ] ){
                    return false; }
                checks[ (byte)( fti ) ]= true;
            }
            return true;
        }

        internal static ft[] genidentity(){
            ft[] res= new ft[ ftEntireRange ];
            for( byte i= 0; i < ftEntireRange; i++ ){
                res[ i ]= (ft)( i );
            }
            return res;
        }

        internal static bool associativity( ft[] L, ft[] M, ft[] R ){
            ft[] lassoc= mult( mult( L, M ), R );
            ft[] rassoc= mult( L, mult( M, R ) );
            return equal( lassoc, rassoc );
        }
            

        internal static void test(){
            ft[] identity= genidentity();
            ft[] x4= square( x2 );
            bool testx= equal( x4, identity );
            bool testxi= equal( identity, x4 );
            ft[] y4= square( y2 );
            bool testy= equal( y4, identity );
            bool testyp= isperm( y1 );
            ft[] z4= square( z2 );
            bool testz= equal( z4, identity );
            bool testzp= isperm( z1 );
            bool a1= associativity( x1, y1, z1 );
            // brute-force associativity test
        }
        #endregion

        public static ft mult( ft L, dr R ){ // employed resolving Kxy'
          ft res= L;
          switch ( drT.gx( R ) ){
              case 0: break;
              case 1: res= x1[ (byte)( res ) ];  break;
              case 2: res= x2[ (byte)( res ) ];  break;
              case 3: res= x3[ (byte)( res ) ];  break;
          }
          switch ( drT.gy( R ) ){
              case 0: break;
              case 1: res= y1[ (byte)( res ) ];  break;
              case 2: res= y2[ (byte)( res ) ];  break;
              case 3: res= y3[ (byte)( res ) ];  break;
          }
          switch ( drT.gz( R ) ){
              case 0: break;
              case 1: res= z1[ (byte)( res ) ];  break;
              case 2: res= z2[ (byte)( res ) ];  break;
              case 3: res= z3[ (byte)( res ) ];  break;
          }
          return res;
        }

        public static ft multinv( ft L, dr R ){ // employed during _move_, convert L according to rotation R
            ft res= L;
            switch( drT.gz( R ) ){
                case 0: break;
                case 1: res= z3[ (byte)( res ) ];  break;
                case 2: res= z2[ (byte)( res ) ];  break;
                case 3: res= z1[ (byte)( res ) ];  break;
            }
            switch( drT.gy( R ) ){
                case 0: break;
                case 1: res= y3[ (byte)( res ) ];  break;
                case 2: res= y2[ (byte)( res ) ];  break;
                case 3: res= y1[ (byte)( res ) ];  break;
            }
            switch( drT.gx( R ) ){
                case 0: break;
                case 1: res= x3[ (byte)( res ) ];  break;
                case 2: res= x2[ (byte)( res ) ];  break;
                case 3: res= x1[ (byte)( res ) ];  break;
            }
            return res;
        }

        #if unemployed
        public static ft mult3w( ft L, ft R ){
            ft res= L;
            switch ( R ){
            case ft.x1: res= x1[ (byte)( res ) ];  break;
            case ft.x2: res= x2[ (byte)( res ) ];  break;
            case ft.x3: res= x3[ (byte)( res ) ];  break;
            case ft.y1: res= y1[ (byte)( res ) ];  break;
            case ft.y2: res= y2[ (byte)( res ) ];  break;
            case ft.y3: res= y3[ (byte)( res ) ];  break;
            case ft.z1: res= z1[ (byte)( res ) ];  break;
            case ft.z2: res= z2[ (byte)( res ) ];  break;
            case ft.z3: res= z3[ (byte)( res ) ];  break;
            }
            return res;
        }
        #endif
    }; // \ ftT class

    public class parse {
        const char spch= ' '; // white space character
        const char nullch= '\u0000'; // having _newtermchar_ == nullch indicates standard quotation senteniel, usually _spch_
        const char matchany= '\u0001'; // the character following the token always accumulated into the string associated with the token

        internal static void test(){ //debug trap next line
        }

        const byte alphaEntireRange= (byte)( (byte)( 'Z' ) - (byte)( '@' ) + 1 );
        // #region alpha classifications
        /// <summary>
        /// transform upper or lower-case letter to its matching plane of motion or token parse
        /// </summary>
        static CubeTrae.pm[] Upr_pm= new pm[]{ // @?B?DEF?????LMN???RSTU?????  i?^?^^^ghi?k^^^o??^^^^?wxyz
        #region Upr_pm
/* display */   /* @*/    pm.posmark, /*99*/   /* position marker, or set scramble/read orientation: @UF=BL or @RD=UF */
     /* unused */ /*A*/         pm.pm60, // \ also position number, @1 @2 ...
                /*B*/ pm.bp,
                /* C*/    pm.conjugate, // command to insert inverse of contents at origin of moves and contents at end of moves
                /*D*/ pm.dd,
                /*E*/ pm.ee,
                /*F*/ pm.ff,
                /* G*/    pm.get,    /*99*/ /*lc*/ /* recall or insert memory, g102 also g(_RF') */
                /* H*/    pm.hold,   /*99*/ /*lc*/ /* set memory, clear memory if numm item and not at beginning of line */
                /* I*/    pm.inverted, /*lc*/ // also ' 3
     /* unused */ /*J*/         pm.pm60,
                /* K*/    pm.commandSym, /*99*/ /* lc */ /* k0 ~ clear, k1 ~ invert, k2 ~ reflect across M slice */
                /*L*/ pm.ll,             // \ Kx or Ky or Kz converts moves to match result of Tx, Ty, or Tz
                /*M*/ pm.mm, // also pm.pairimage // give pair image of attached letters in comment
 /* display */  /*N  */   pm.enumb,  /*99*/ /* enter number to stack for command, i.e. U L B @29 F R N29 k31 -- currently unused
 /* display */  /*O */ pm.buffer,     /*lc*/ /* common origin point, i.e. o=DF or o=ULB or o=(A), o(CB) order C_ before B_ */
                /*P*/           pm.pm60,
     /* unused */ /*Q*/         pm.pm60,
                /*R*/ pm.rr,
                /*S*/ pm.ss, // also pm.pairretrieve/pm.pairsetup // give setup clue of attached letters in comment
                /* T*/    pm.transform,  /*99*/ /* symmetry transforms T1 ~ invert; T2 ~ reflect; */
                /*U*/ pm.ut,             // \ Tx = x' ... ~ ~ ~ ... x,  similarly for Ty2 yZ' and so forth
     /* unused */ /*V*/         pm.pm60,
                /* W*/   pm.widen, /*lc*/
                /*X*/ pm.xx, /*lc*/
                /*Y*/ pm.yy, /*lc*/
                /*Z*/ pm.zz, /*lc*/

                    /* To1 = '['; */ // [ ] : , ::
                    /* To2 = ':' colon-conjugate A B A'; */
                    /* To3 = ',' commutator A B A' B' */
                    /* To4 = '::', formally ';', conjugate with lower precidence */
                    /* To5 = ']'; */
        #endregion
        };

        /* resort */ // static string( 

        // for lower-case characters, give the plane of motion associated exclusively with the lower case character
        static CubeTrae.pm[] Lpr_pm= new pm[ alphaEntireRange ]{ // b d f k l r u 
        #region Lpr_pm
                /* @ABC */ pm.pm60, pm.pm60, pm.bw, pm.pm60,      /* DEFG */ pm.dw, pm.pm60, pm.fw, pm.pm60,
                /* HIJK */ pm.pm60, pm.pm60, pm.pm60, pm.command, /* LMNO */ pm.lw, pm.pairimage, pm.pm60, pm.pm60,
                /* PQRS */ pm.pm60, pm.pm60, pm.rw, pm.pairretrieve,      /* TUVW */ pm.pm60, pm.uw, pm.pm60, pm.pm60,
                /* XYZ */  pm.pm60, pm.pm60, pm.pm60
        #endregion
        };

        // construct boolean arrays to efficiently check characters for single-char tokens and its prefix-type
        static bool[] string_alphaboolarray( string given, char alphalead ){
            bool[] res= new bool[ alphaEntireRange ];
            for( byte i= 0; i < alphaEntireRange; i++ ){ res[ i ]= false; }
            foreach( char ch in given ){ res[ (short)( ch ) - alphalead ]= true; }
            return res;
        }

        // single character tokens classification over the alphabet characters
        const char lowercase= (char)( 'a' - 1 );  const char uppercase= (char)( 'A' - 1 );
        static bool[] lc_expected= string_alphaboolarray( "ghiowxyz", lowercase ); // lower case expected
        static bool[] lc_notexpected= string_alphaboolarray( "CET", uppercase ); // upper case expected
        static bool[] lc_differentiate= string_alphaboolarray( "ULFRDBKMS", uppercase ); // lower case differs from upper case
            // the token name in lower case has a different interpretation than the same upper-case character as a token

        // #region token classification
        // tokens expected to be followed by an integer
        const byte pmEntireRange= (byte)( (byte)( pm.pm60 ) + 1 );

        // classify the pm in _given_ in the boolean array as true, otherwise false
        static bool[] pm_pmboolarray( params pm[] given ){
            bool[] res= new bool[ pmEntireRange ];
            for( byte i= 0; i < pmEntireRange; i++ ){ res[ i ]= false; }
            foreach( pm pmi in given ){ res[ (byte)( pmi ) ]= true; }
            return res;
        }

        // token expected to be followed by
        static bool[] prefixint= pm_pmboolarray( pm.posmark, pm.get, pm.hold, pm.command, pm.transform, pm.enumb ); // integer
        static bool[] prefixtext= pm_pmboolarray( pm.facechase, pm.commandSym, pm.buffer, pm.conjugate, pm.pairimage, pm.pairretrieve ); // attached text

        // token prefixes a string if the following character matches the prefix_char.  However, if the
        // token is already a stringref, it is the terminating character; however there is a method for
        // overriding this terminating character: set newtermchar to a non_null value, in which
        // newtermchar becomes the terminating character of the string to be attached to the token.
        static char[] pmarray_char( char defaultchar, char matchany_char, char nullchar_char, string setto, params pm[] corresponding ){
            char[] res= new char[ pmEntireRange ];
            for( byte i= 0; i < pmEntireRange; i++ ){ res[ i ]= defaultchar; }
            short p= -1;
            foreach( char ch in setto ){
                p += 1;
                if( ch == matchany_char ){ res[ (byte)( corresponding[ p ] ) ]= matchany; }
                else if( ch == nullchar_char ){ res[ (byte)( corresponding[ p ] ) ]=nullch; }
                else{ res[ (byte)( corresponding[ p ] ) ]= ch; }
            }
            return res;
        }
        // token prefixes a string if the following character matches prefix_char, or if the
        // token is a stringref, it is the terminating character
        static char[] prefixSuffix_char= pmarray_char( /*defaultchar=*/spch, /*matchany_char=*/'%', /*nullchar_char=*/'@',
            "%"         + "@"         + "("   + ")"    + "\'"    + "%",       
            pm.posmark, pm.facechase, pm.get, pm.getq, pm.hold, pm.transform/*, pm.whitespace (was to '_') */ );

        // define an association from each pm to a pm; references deafultpm unless specified in _sourceTarget_
        // even positions of _sourceTarget_, starting with 0, specify the source,  odd positions of _sourceTarget
        // specify the target for the source immediately preceding in _sourceTarget_.
        static pm[] pmarray_pm( pm defaultpm, params pm[] sourceTarget ){
            pm[] res= new pm[ pmEntireRange ];
            for( byte i= 0; i < pmEntireRange; i++ ){ res[ i ]= defaultpm; }
            short p= -1;
            pm source= defaultpm;
            foreach( pm pmi in sourceTarget ){
                p += 1;
                if( ( p % 2 ) == 0 ){ source= pmi; }
                else { res[ (byte)( source ) ]= pmi; }
            }
            return res;
        }
        // if token has a separate string-reference form, specify the association here.  Tokens first match the integer form, then
        // on failing to resolve an integer, it is converted to the corresponding string-reference form.
        static pm[] strRef_form= pmarray_pm( pm.pm60, 
            pm.posmark, /* -> */ pm.posmarkq,       // @2 vs @UL=RF
            pm.get, /* -> */ pm.getq,               // g32 vs g(_ULB)
            pm.hold, /* -> */ pm.holdq,             // h32 vs h'Jperm
            pm.transform, /* -> */ pm.transformq ); // T2 vs Txy'

        // a quotation will abort on detecting the line comment quote, and revert to eol mode, 
        //unless attached to a token designated here.
        public static bool[] hardstrquote= pm_pmboolarray(); // currently no designations

        // those tokens referring to whole-cube rotations: x y z ' 2
        // useful for processing Kxy of pm.commandSym or Txy of pm.transformq
        public static bool[] fullcuberotate= pm_pmboolarray(
            pm.yy, pm.xx, pm.zz,   pm.yi, pm.xi, pm.zi,
            pm.inverted, pm.n1, pm.n2, pm.n3 );

        // all tokens parsed have an integer associated.  In the case of a string reference token, the integer references
        // the location in the string bank attached.  On reading the result of parse1, if the token is _readAsStrref_,
        // refer to the string bank location referenced for its attachment.
        public static bool[] readAsStref= pm_pmboolarray( 
            pm.posmarkq, pm.facechase, pm.getq, pm.getname, pm.holdq, pm.commandSym, pm.buffer, pm.transformq, pm.conjugate, pm.pairimage, pm.pairretrieve );
        
        static string[] pm_string_= new string[ pmEntireRange ]{ 
        #region pm_string_
            "U", "L", "F", "R", "B", "D",
            "Uw", "Lw", "Fw", "Rw", "Bw", "Dw",
            "E", "M", "S", "Ei", "Mi", "Si",
            "y", "x", "z", "yi", "xi", "zi",
            "@", "@", "|", "\'", "w", "g", "g", "g", "h", "h", // second "g" is the g of g(_ULB) // third "g" is a name retrieve 'name
            "k", "K", "o", "T", "T", "C", "N", "m", "sn", "s", "_", // sn switch on " s2 |DH DH IH IJ||
            "[", "]", ":", ",", "::", "<-", "<k0>",
            "N0", "N1", "N2", "N3", "N4", "N5", "N6", "N7", "N8", "N9",
            "?"
        #endregion
        };
                            // thinking of extending the notation form, some ideas:
                            // k3 k4 n2 k4 c:add k0 `add x // one possible form
                            // ULF c:j_perm g:j_perm // second possible form
                            // g(_UF') _.add _.subtract _.form k4 k2 k1 _.j_perm // third possible form

        static public string pm_string( pm pmi ){
            return pm_string_[ (byte)( pmi ) ];
        }

        /// <summary> character to plane of motion (U L F R B D M E S x y z) or other token -- lexical analizer for parse1 </summary>
        static public CubeTrae.pm parse0( char ch ){ // akin to lexical secion of parser
            byte charval;
            const byte atcode= (int)( '@' ); // letter zero, one before 'A'
            if ( char.IsWhiteSpace( ch ) ) { return pm.whitespace; }
            if ( char.IsDigit( ch ) ){ // 
                if ( !byte.TryParse( ch.ToString(), out charval ) ){ return pm.pm60; }
                switch ( charval ){
                    case 0: return pm.n0;  case 1: return pm.n1;  case 2: return pm.n2;  case 3: return pm.n3;
                    case 4: return pm.n4;  case 5: return pm.n5;  case 6: return pm.n6;  case 7: return pm.n7;
                    case 8: return pm.n8;  case 9: return pm.n9;  default: return pm.pm60;
            }}
            if ( char.IsLetter( ch ) ){ // A B C ...
                bool lc= char.IsLower( ch );
                char upr= char.ToUpper( ch );
                byte uprord= (byte)( (int)( upr ) - atcode );
                if ( uprord > 26 ){ return pm.pm60; }
                pm cand= Upr_pm[ uprord ];
                if ( !lc && lc_expected[ uprord ] ){ return pm.pm60; } // 'G', 'H', 'I', 'O', 'W', 'X', 'Y', 'Z'
                if ( lc && lc_notexpected[ uprord ] ){ return pm.pm60; } // 'e'
                if ( !lc_differentiate[ uprord ] ){ return Upr_pm[ uprord ]; }
                if( lc ){ return Lpr_pm[ uprord ]; } else { return Upr_pm[ uprord ]; }
            }
            if ( char.IsPunctuation( ch ) ){
                switch ( ch ){
                    case '@': return pm.posmark;
                    case '\'':  return pm.inverted;
                    case '[': return pm.ps1;
                    case ']': return pm.ps2;
                    case ':': return pm.ps3;
                    case ',': return pm.ps4;
                }}
            if ( char.IsSymbol( ch ) ){ 
                switch ( ch ){
                    case '`':  case '\u00B4':  return pm.inverted; // also may prefix a named memory retrieve i.e. 'Jperm 
                    case '|':  return pm.facechase;
                }}
            return pm.pm60;
        }

        enum m{ linebegin, firstparenthesis, open, numpos0, numpos, inquote, quotesc, whitespace, linecomment }; // reading parse state, mode
        /// <summary> convert input string into a list of plane or tokens with a corresponding integer list and index string list </summary>
        public static System.Collections.Generic.List< pm > parse1( string s, out List< int > ni, out List< string > si ){
            List< pm > reading= new List< pm >(); // tokens parsed
            List< int > numsReading= new List< int >(); // an integer associated with each token parsed
            List< string > strReading= new List< string >(); // an independant list of strings possibly referenced via reading and numsReading
            System.Text.StringBuilder inquoted= new System.Text.StringBuilder(); // string quotation in construction
            m mode= m.linebegin; // parse mode
            pm readpm; // encoding from character encountered

            int numread= -1;  bool negate= false;
            bool linecommentlead= false; // flag '/' special since "//" begins a to end-of-line comment
            // openparen, openparenpos and closeparen track for a final list in parenthesis, to support CubeExplorer reports i.e. "U L F (2f*)"
            bool openparen= false; // see if opening parenthesis found -- track if ending parenthesis last token before eol not counting eol comment
            short openparenpos= 0; // the tracking position for the open parenthesis
            bool closeparen= false; // matched closing parenthesis found
            bool separate= true; // token is preceded by whitespace, preparatory to distinguishing constructed moves 2Lw' or 3Rw2 
            bool candidatetwocharsymboltoken= false; // set on detecting the token t2 or ':', as it is possibly part of the token t4 or "::"

            char prefixchar= nullch; // simply a convenience reading for the text prefix char for the current mode
            char newtermchar= nullch; // supply the option of having a different quotation termination character

            System.Text.StringBuilder recover= new System.Text.StringBuilder(); // allow for an _unread_ ability, on aborted subprocess string quote
            short recoveripos= 0;  char ch= nullch;

            int charcount= 0;  bool finalchar= false; // determine if last character encountered
            foreach ( char ch_s in s ){ charcount+= 1;  finalchar= charcount == s.Length;
                recover.Append( ch_s ); // recover permits a reparsing if a current quotation parsing fails
                if ( finalchar && mode == m.inquote ){        // add sentenel character to assembling quotation types
                    switch ( reading[ reading.Count - 1 ] ){ // \ that may be reparsed if the quotation fails
                    case pm.posmarkq:  case pm.transformq: case pm.transform: case pm.commandSym:
                        if( ch_s != spch ){ recover.Append( spch ); } break; // break up final incomplete read orientation note "@UL=R"
                    case pm.buffer: if ( newtermchar == nullch && ch_s != spch ){ recover.Append( spch ); }
                        else{ if ( ch_s != newtermchar ){ recover.Append( newtermchar ); } }
                        break; // break up partial buffer assignment "o=U"
                    case pm.getname:  case pm.holdq:  recover.Append( spch );  break;
                }} else if ( finalchar && mode == m.numpos0 ){ // those tokens preceding either a number or token quotation
                        recover.Append( spch ); } // properly parse @F, which is in mode numpos0 if this token is at end of line
            while ( recoveripos < recover.Length ){ ch= recover[ recoveripos ];  recoveripos+= 1;
                readpm= parse0( ch );
                switch ( mode ){
                case m.linebegin: // first characters excluding whitespace in parenthesis are a comment if first non whitespace char is '('
                    if( readpm != pm.whitespace ){
                        if( ch == '(' ){ mode= m.firstparenthesis;  break; }
                        else { mode= m.open;  goto case m.open; }
                    }
                    break;
                case m.firstparenthesis: // comment contents of first parthentsis pair 
                    if ( ch == ')' ){ mode= m.open;  }  break; // first char '(' without matching ')' is a to end-of-line comment
                case m.open:
                    if ( ch == '/' ){ // to end-of-line comment or partial to end-of-line comment indicator
                        if ( linecommentlead ){ // found '//', process to end-of-line comment
                            reading.RemoveAt( reading.Count-1 ); // remove previous '/'
                            if ( closeparen ){ // set final characters in parenthesis as comment if final nonwhitespace character is ')'
                                reading.RemoveRange( openparenpos, reading.Count - openparenpos );
                                numsReading.RemoveRange( openparenpos, numsReading.Count - openparenpos );
                            } // currently possibly some contents of si may become unreferenced if they are inthe processed final parenthesis
                            mode= m.linecomment;  break; }
                        else { linecommentlead= true; } // note that next time the previous character is '/'
                    } else if( linecommentlead ){ openparen= false;  closeparen= false;  linecommentlead= false; } // single '/' disqualifies closeparen or ')' as last

                    if ( ch == '(' ){ // track if final matched parenthesis is the final character on the line excluding whitespace or to-eol comment
                        openparen= true;
                        openparenpos= (short)( reading.Count );
                    } else if ( ch == ')' && openparen ){ closeparen= true; }
                    else if ( readpm != pm.whitespace && !linecommentlead ){ closeparen= false; } // do not count end-of-line comment

                    if ( readpm == pm.whitespace ){ mode= m.whitespace;  candidatetwocharsymboltoken= false; }
                    else if ( prefixint[ (byte)( readpm ) ] ){ // reading token prefixes an integer
                        reading.Add( readpm ); // the associated number is the parsed integer following
                        negate= false;  numread= 0;
                        mode= m.numpos0; }
                    else if ( prefixtext[ (byte)( readpm ) ] || (separate && readpm == pm.inverted) ){ // reading token prefixes a quoted string
                        if( readpm == pm.inverted ){ 
                            reading.Add( pm.getname ); } else { reading.Add( readpm ); }
                        // the associated number is the entry number in the out List<string> si -- added at quote termination
                        inquoted.Remove( 0, inquoted.Length ); // clear inquoted string to start reading into it
                        mode= m.inquote; }
                    else { // not a prefix to an integer or string quotation - simply enclose token
                        if( closeparen && openparen && !linecommentlead ){ openparen= false; }
                        if( reading.Count > 0 && ( pmT.pm_pmSect( reading[ reading.Count-1 ] ) == pmSect.puncSym
                                && reading[ reading.Count-1 ] != pm.ps2
                                || ( reading[ reading.Count-1 ] == pm.pm60 && !char.IsLetterOrDigit( ch ) ) ) ){
                            // tokens except ']' are implicitly separate and any unidentified character is also implicitly separate
                            if( candidatetwocharsymboltoken && readpm == pm.ps3 ){ 
                                reading[ reading.Count - 1 ]= pm.ps5;
                                candidatetwocharsymboltoken= false;
                                break;
                            }
                            separate= true;
                        } 
                        candidatetwocharsymboltoken= readpm == pm.ps3;
                        reading.Add( readpm ); // each read token has a numsReading entry attached
                        if( separate ){ numsReading.Add( reading.Count );  separate= false; } else { numsReading.Add( -reading.Count ); }
                        // default numsreading attached is the token position number, negative flags: attached to previous token (i.e. modifier)
                    }
                    break;
                case m.numpos0:
                    if ( ch == '-' ){ negate= true;  mode= m.numpos;  break; } // permit negative numbers for prefix number types
                    if ( pmT.pm_pmSect( readpm ) == pmSect.numeral ){ negate= false;  mode= m.numpos;  goto case m.numpos; }
                    if ( reading.Count > 0 ){ // token may prefix an integer, however no numeral characters found, does token prefix a quotation?
                        prefixchar= prefixSuffix_char[ (byte)( reading[ reading.Count-1 ] ) ];
                        if( ( ( prefixchar == matchany && readpm != pm.whitespace ) || ch == prefixchar ) ){ // fix Tx misses x
                            readpm= reading[ reading.Count - 1 ];
                            reading.RemoveAt( reading.Count - 1 );
                            reading.Add( strRef_form[ (byte)( readpm ) ] ); // change to strform token type
                            inquoted.Remove( 0, inquoted.Length ); // clear inquoted
                            if( prefixchar == matchany ){ inquoted.Append( ch ); }
                            newtermchar= nullch;
                            mode= m.inquote;
                        } else if( reading[ reading.Count-1 ] == pm.get && ch == '\'' ){ // special case g'memname sugar for 'memname
                            reading[ reading.Count-1 ]= pm.getname; // must be handled here as `g`'s prefix is '(' for catalog retrieval
                            inquoted.Remove( 0, inquoted.Length );
                            mode= m.inquote;       // \ and psm marks the insertion point in the token list which is subject being 'bumped'
                        } else { // indicate that no numerals were detected for the token prefixing an integer
                            numsReading.Add( int.MinValue ); // default no-entry value
                            separate= false;
                            if( !finalchar ){ mode= m.open;  goto case m.open; } // avoid "g(" parsing to "g() ("
                        }
                    }
                    break;
                case m.numpos:
                    if ( pmT.pm_pmSect( readpm ) == pmSect.numeral ){
                        if ( numread < int.MaxValue / 10 ){ // avoids overflow, however currently omits integers within 10 of int.MaxValue or int.MinValue
                            numread= numread * 10  +  (byte)( readpm )  -  (byte)( pm.n0 ); }
                        else{ if( negate ){ numsReading.Add( int.MinValue + 1 );  negate= false; }
                        else{ numsReading.Add( int.MaxValue ); }
                        mode= m.open; } }
                    else { 
                        if ( negate ){ numsReading.Add( -numread );  negate= false; }
                        else { numsReading.Add( numread ); }
                        if( reading[ reading.Count-1 ] == pm.get 
                                || reading[ reading.Count-1 ] == pm.hold || reading[ reading.Count-1 ] == pm.conjugate
                                || reading[ reading.Count-1 ] == pm.command
                            ){
                            if( reading[ reading.Count-1 ] == pm.command && numread == 0 ){
                                reading.RemoveAt( reading.Count-1 );  reading.Add( pm.k0m ); } // opaque operation marker
                            else {
                                reading.Add( pm.psm ); // internal position marker
                                numsReading.Add( 0 );
                            }
                        } else if( reading[ reading.Count-1 ] == pm.command && numread == 0 ){
                            reading.RemoveAt( reading.Count-1 );  reading.Add( pm.k0m ); } // replace k0 with k0m preparatory to handling at parse2
                        separate= false;
                        mode= m.open;  goto case m.open; } // enclose numeral
                    break;
                case m.inquote: // hard or soft quote, as to handling end-of-line comment // within the quote.
                    #region inquote case
                    if ( ch == '\\' ){ mode= m.quotesc;  break; } // added finalchar to permit reparsing at end if detected quotation failure
                    if ( ch == newtermchar || ( newtermchar == nullch && ch == prefixSuffix_char[ (byte)( reading[ reading.Count-1 ] ) ] ) ){
                        if ( ( reading[ reading.Count-1 ] == pm.commandSym || reading[ reading.Count-1 ] == pm.transformq )
                                && !inquoted.ToString().Contains( "x" ) 
                                && !inquoted.ToString().Contains( "y" ) && !inquoted.ToString().Contains( "z" ) ){
                            recover.Append( inquoted.ToString() ); // k123 accepted; K123 not accepted; kx'y not accepted; Kx'y accepted
                            numsReading.Add( strReading.Count );
                            strReading.Add( "" );
                            separate= false;
                            mode= m.open; goto case m.open;
                        }
                        if ( reading.Count > 0 && ( reading[ reading.Count-1 ] == pm.buffer ) // abort o=U with only one character
                                && newtermchar == nullch && inquoted[ 0 ] == '=' && inquoted.Length < 3 ){
                            recover.Append( inquoted.ToString() );
                            numsReading.Add( strReading.Count );
                            strReading.Add( "" );
                            separate= false;
                            mode= m.open;  goto case m.open;
                        }
                        if ( reading[ reading.Count-1 ] == pm.posmarkq && ( inquoted.Length != 5 ) ){ // abort and reparse
                            if( ch == '/' ){ // permit '/' tofollow @UL=RF to allow @UL=RF// or @UL=RF(comment)//
                                numsReading.Add( strReading.Count );
                                strReading.Add( inquoted.ToString() );
                                separate= false;
                                mode= m.open;  goto case m.open;
                            }
                            recover.Append( inquoted.ToString() ); recover.Append( ch ); // convert to un-marked posmark with no number
                            reading.RemoveAt( reading.Count - 1 ); reading.Add( pm.posmark );
                            numsReading.Add( int.MinValue ); // replace with posmark with no numerical value
                            separate= false;
                            mode= m.open;  goto case m.open;
                        }
                        numsReading.Add( strReading.Count ); // quotation termination character encountered
                        strReading.Add( inquoted.ToString() );
                        separate= newtermchar == nullch && prefixSuffix_char[ (byte)( reading[ reading.Count - 1 ] ) ] == spch;
                        newtermchar= nullch; // reset termchar here at end of the quotation
                        if ( reading[ reading.Count-1 ] == pm.holdq || reading[ reading.Count-1 ] == pm.getname 
                                || reading[ reading.Count-1 ]== pm.getq || reading[ reading.Count-1 ] == pm.conjugate ){
                            reading.Add( pm.psm ); // internal position marker
                            numsReading.Add( 0 );
                        }
                        mode= m.open;  break; }
                    if ( reading[ reading.Count-1 ] == pm.facechase && ch == '|' // permit "||" to break a facechase quotation other than end-of-line
                            && inquoted.Length > 0 && inquoted[ inquoted.Length-1 ] == '|' ){ // "||" encountered
                        inquoted.Remove( inquoted.Length-1, 1 ); // remove final '|'
                        numsReading.Add( strReading.Count );
                        strReading.Add( inquoted.ToString() );
                        reading.Add( pm.psm );
                        numsReading.Add( 0 );
                        separate= false;
                        mode= m.open;  break; }
                    if ( reading.Count > 0 && reading[ reading.Count-1 ] == pm.pairretrieve && inquoted.Length > 0 && ch == '|' ){
                            // special break inquote for the case "s3|" or "s2|" as sugar for "s3 |" or "s2 |"
                        numsReading.Add( strReading.Count );
                        strReading.Add( inquoted.ToString() );
                        mode= m.open;
                        goto case m.open; }
                    if ( reading[ reading.Count-1 ] == pm.posmarkq && ( // only accept @ @99 or @UL=RF 
                            inquoted.Length != 2 && pmT.pm_pmSect( readpm ) != pmSect.singmaster // || char.IsLower( ch )
                            || inquoted.Length == 2 && ch != '='
                            || inquoted.Length > 4 ) ){ // break invalid form after @ and reparse as @ with no following characters
                        if( ( !char.IsLetterOrDigit( ch ) ) && inquoted.Length == 5 ){ // possibly beginning of a to-tol quote
                            // ch == '/' || ch == '(' ) && inquoted.Length == 5 ){ // possibly beginning of a to-eol quote
                            numsReading.Add( strReading.Count ); // quotation termination character encountered
                            strReading.Add( inquoted.ToString() );
                            separate= false;
                            mode= m.open;  goto case m.open;
                        } else { // undo quotation and reparse
                            recover.Append( inquoted.ToString() );  recover.Append( ch ); // convert to un-marked posmark with no number
                            reading.RemoveAt( reading.Count-1 );  reading.Add( pm.posmark );
                            numsReading.Add( int.MinValue ); // replace with posmark with no numerical value
                            mode= m.open;  break;
                        }
                    }
                    if ( reading[ reading.Count-1 ] == pm.buffer && newtermchar == nullch && inquoted.Length == 0 ){ // fix: newtermchar as '(' not included
                        if ( ch != '=' && ch != '(' ){ // break and convert invalid after o
                            numsReading.Add( strReading.Count );
                            strReading.Add( "" );
                            mode = m.open; goto case m.open;
                        } else if ( ch == '(' ){ newtermchar= ')';  break; } else { inquoted.Append( ch );  break; }
                    }
                    if ( reading[ reading.Count-1 ] == pm.buffer && inquoted.Length == 1 && inquoted[ 0 ] == '=' && ch == '(' ){
                        newtermchar= ')'; } // do not break quote on space for "o=(A B)"
                    if ( reading[ reading.Count-1 ] == pm.buffer && newtermchar == nullch
                            && ( pmT.pm_pmSect( readpm ) != pmSect.singmaster || inquoted.Length > 3 ) ){
                        numsReading.Add( strReading.Count ); // break buffer type o= include only full Singmaster names of length 3 or length 2
                        strReading.Add( inquoted.ToString() );
                        separate= false;
                        mode = m.open;  goto case m.open; }
                    if ( ( reading[ reading.Count-1 ] == pm.commandSym || reading[ reading.Count-1 ] == pm.transformq ) 
                                && !fullcuberotate[ (byte)( readpm ) ] ){
                        // determine if x y or z appear in quoted string
                        if ( inquoted.ToString().Contains( "x" ) || inquoted.ToString().Contains( "y" ) || inquoted.ToString().Contains( "z" ) ){
                            numsReading.Add( strReading.Count ); // require K ant T in quote mode to only include full-cube rotations
                            strReading.Add( inquoted.ToString() ); // yes - include string and terminate quotation
                            separate= false;
                            mode= m.open;  goto case m.open;
                        } else { // abort and reparse if K does not contain a whole-cube rotation character in the form of 'x', 'y' or 'z'
                            recover.Append( inquoted.ToString() );  recover.Append( ch ); // k123 accepted; K123 not accepted
                            numsReading.Add( strReading.Count );
                            strReading.Add( "" );
                            mode= m.open;  break; }
                    }
                    if( reading[ reading.Count-1 ] == pm.conjugate && !pmT.pmSectMove( pmT.pm_pmSect( parse0( ch ) ) ) ){ // abort conjugate not containing a move gen
                        recover.Append( inquoted.ToString() );  recover.Append( ch );
                        numsReading.Add( strReading.Count );
                        strReading.Add( "" );
                        mode= m.open;  break; }
                    if ( !hardstrquote[ (byte)( reading[ reading.Count-1 ] ) ] && ch == '/' 
                                && inquoted.Length > 0 && inquoted[ inquoted.Length-1 ] == '/' ){ // line comment in quotation
                        inquoted.Remove( inquoted.Length-1, 1 ); // remove final '/'
                        numsReading.Add( strReading.Count );
                        strReading.Add( inquoted.ToString() );
                        mode = m.linecomment; break; // close quotation and enter line-comment mode
                    } // /*all*/ else
                    #endregion
                    inquoted.Append( ch );  break;
                case m.quotesc: // enclose following character in any case
                    inquoted.Append( ch );  mode= m.inquote;  break;
                case m.whitespace: // skip whitespaces until next non-whitespace, in which case enter open mode
                    separate= true;
                    if ( readpm != pm.whitespace ){ mode= m.open;  goto case m.open; }
                    break;
                case m.linecomment: // cease processing any chacters until the end of line at the detection of
                    break;          // \ any line comment outside of a hard quotation
                } // \ switch mode
            } // \ while recoveripos < recover.length
                recover.Remove( 0, recover.Length );  recoveripos= 0;  
            } // \foreach ( char ch_s in s ){ 
            switch( mode ){ // end of line - terminate any quotations
                case m.numpos0: numsReading.Add( int.MinValue );  break;
                case m.numpos: if( negate ){ numsReading.Add( -numread ); } else { numsReading.Add( numread ); }
                    if( reading[ reading.Count-1 ] == pm.get || reading[ reading.Count-1 ] == pm.hold ){
                        reading.Add( pm.psm ); // internal position marker
                        numsReading.Add( 0 );
                    }                    
                    break;
                case m.inquote:  case m.quotesc:
                    numsReading.Add( strReading.Count );
                    strReading.Add( inquoted.ToString() );
                    if( reading[ reading.Count-1 ] == pm.psm ){ numsReading.Add( 0 ); }
                    break;
            }
            if ( closeparen ){ // final nonwhitepace character excluding end-of-line comment is a matched ')'
                reading.RemoveRange( openparenpos, reading.Count - openparenpos );
                numsReading.RemoveRange( openparenpos, numsReading.Count - openparenpos );
                // leaving strReading potentially containing unreferenced strings
            }
            ni= numsReading;  si= strReading;  return reading;
        } //\ parse( string s, outList<int> ni, out List<string> si )

        public struct displayind{
            internal dr sro; // scramble to read orientation
            internal bool sro_UF; // scramble to read orientation specified as move UF to location specified; false ~~ move specified to UF i.e. @UF=BL
            internal bool srosetnull; // permit @UF=UF override of singmaster setting
            internal bool edgeoriginsingmaster; // edge origin specified with singmaster notation, i.e. o=UB, otherwise o=(A_)
            internal bool corneroriginsingmaster; // corner origin specified with singmaster notation, i.e. o=ULB, otherwise o=(_A)
            public void init(){
                sro= dr.c_UF_;
                sro_UF= false;
                srosetnull= false;
                suffixmsg= "";
                edgeoriginsingmaster= true;
                corneroriginsingmaster= true;
                displayfacechaseformat= false;
            }
            public string prefix(){ // if there is a prefix, the following separator space is also included
                System.Text.StringBuilder sb= new System.Text.StringBuilder();
                if( srosetnull || sro != dr.c_UF_ ){ 
                ef equiv;
                pm face;
                pm obv;
                switch( sro_UF ){
                case false: 
                    equiv= drT.dr_ef( sro );
                    face= ssT.ecf_pm( equiv );
                    obv= ssT.ecf_pm( ssT.eOBV( equiv ) );
                    sb.Append( "@UF=" );  sb.Append( pmT.pmT_string( face ) );  sb.Append( pmT.pmT_string( obv ) );
                    sb.Append( ' ' );  break;
                case true: 
                    equiv= drT.dr_ef( drT.invert( sro ) );
                    face= ssT.ecf_pm( equiv );
                    obv = ssT.ecf_pm( ssT.eOBV( equiv ) );
                    sb.Append( "@" );  sb.Append( pmT.pmT_string( face ) );  sb.Append( pmT.pmT_string( obv ) );
                    sb.Append( "=UF " );  break;
                }}
                return sb.ToString();
                //if( singmaster.ee_e.overrideorigin != ef.ef24 ){
                //    sb.Append( "o=" );  sb.Append( pmT.pmT_string( ssT.ecf_pm( singmaster.ee_e.overrideorigin ) ) );
                //    sb.Append( ssT.ecf_pm( sb
            }
            public string suffixmsg;
            public string suffix(){ // if there is attribute suffix, the preceding separater is also included
                bool facechaseclose= false;
                System.Text.StringBuilder sb= new System.Text.StringBuilder();
                switch( edgeoriginsingmaster ){
                case true: if( singmaster.ee_e.overrideorigin != ef.ef24 ){
                    if( displayfacechaseformat ){ facechaseclose= true;  sb.Append( " ||" ); }
                    sb.Append( " o=" );  sb.Append( pmT.pmT_string( ssT.ecf_pm( singmaster.ee_e.overrideorigin ) ) );
                    sb.Append( pmT.pmT_string( ssT.ecf_pm( ssT.eOBV( singmaster.ee_e.overrideorigin ) ) ) );
                    sb.Append( ' ' );
                } break;
                case false: if( singmaster.ee_e.overrideorigin != ef.ef24 ){
                    if( displayfacechaseformat ){ facechaseclose= true;  sb.Append( " ||" ); }
                    sb.Append( " o=(" );  sb.Append( singmaster.ee_e[ singmaster.ee_e.overrideorigin ] );  sb.Append( "_) " );
                }  break;
                }
                switch( corneroriginsingmaster ){
                case true: if( singmaster.cc_e.overrideorigin != cf.cf24 ){
                    if( displayfacechaseformat && !facechaseclose ){ sb.Append( " ||" ); }
                    sb.Append( " o=" );  sb.Append( pmT.pmT_string( ssT.ecf_pm( singmaster.cc_e.overrideorigin ) ) );
                    sb.Append( pmT.pmT_string( ssT.ecf_pm( ssT.cCL( singmaster.cc_e.overrideorigin ) ) ) );
                    sb.Append( pmT.pmT_string( ssT.ecf_pm( ssT.cCCL( singmaster.cc_e.overrideorigin ) ) ) );
                    sb.Append( ' ' );
                } break;
                case false: if( singmaster.cc_e.overrideorigin != cf.cf24 ){
                    if( displayfacechaseformat && !facechaseclose ){ sb.Append( " ||" ); }
                    sb.Append( " o=(_" );  sb.Append( singmaster.cc_e[ singmaster.cc_e.overrideorigin ] );  sb.Append( ") " );
                } break;
                }

                System.Text.StringBuilder edgescornersoverrides= new System.Text.StringBuilder();
                // display any order overrides
                bool[] report= new bool[ (byte)( ef.ef24 ) ];
                byte j;  int insertL;
                for( byte i= 0; i < (byte)( ef.ef24 ); i++ ){ report[ i ]= false; } 
                for( byte i= 0; i < (byte)( ef.ef24 ); i++ ){
                    if( !report[ i ] && singmaster.eorderoverride[ i ] != i ){
                        if( edgescornersoverrides.Length > 0 ){
                            edgescornersoverrides.Append( "_) o(" ); }
                        edgescornersoverrides.Append( singmaster.ee_e[ singmaster.ee_e[ i ]] );
                        insertL= edgescornersoverrides.Length - 1;
                        report[ i ]= true;  j= singmaster.eorderoverride[ i ];
                        while( !report[ j ] ){
                            edgescornersoverrides.Insert( insertL, singmaster.ee_e[ singmaster.ee_e[ j ] ] );
                            report[ j ]= true;
                            j= singmaster.eorderoverride[ j ];
                    }}
                } if( edgescornersoverrides.Length > 0 ){ sb.Append( " o(" );  sb.Append( edgescornersoverrides );  sb.Append( "_) " ); }
                edgescornersoverrides.Remove( 0, edgescornersoverrides.Length );
                for( byte i= 0; i < (byte)( cf.cf24 ); i++ ){ report[ i ]= false; }
                for( byte i= 1; i < (byte)( cf.cf24 ); i++ ){
                    if( !report[ i ] && singmaster.corderoverride[ i ] != i ){
                        if( edgescornersoverrides.Length > 0 ){
                            edgescornersoverrides.Append( ") o(_" ); }
                        edgescornersoverrides.Append( singmaster.cc_e[ singmaster.cc_e[ i ]] );
                        insertL= edgescornersoverrides.Length - 1;
                        report[ i ]= true;  j= singmaster.corderoverride[ i ];
                        while( !report[ j ] ){
                            edgescornersoverrides.Insert( insertL, singmaster.cc_e[ singmaster.cc_e[ j ] ] );
                            report[ j ]= true;
                            j= singmaster.corderoverride[ j ];
                    }}
                } if( edgescornersoverrides.Length > 0 ){ sb.Append( " o(_" );  sb.Append( edgescornersoverrides );  sb.Append( ") " ); }

                if( suffixmsg.Trim() != "" ){ sb.Append( " // " );  sb.Append( suffixmsg ); }
                return sb.ToString();
            }
            public bool displayfacechaseformat;
        }

        static sy holdsy= sy.identity; // track accumulated symmetry location from transform moves T1, T2, Tx', Ty2, and so forth; T0 resets this marker
        /// <summary> convert the output of parse1 into a list of face turns </summary>
        public static List< ft > parse2( List< pm > reading, List< int > numsreading, List< string > si, out displayind di ){
            List< ft > moves= new List< ft >();
            int prefixnumpos= int.MinValue;
            int i= -1;
            ftT movecandidate= ft.none;
            pm prefixnum= pm.pm60;
            bool modifierInConstructedMove= false;
            bool widenerInConstructedMove= false;
            bool movecountInConstructedMove= false;
            List< pm > subreading= new List< pm >();
            List< ft > subreadparse;
            bool removePuncSym= false; // k1001 converts '[' to ft.ps1 test -- for this all pm.ps1 and such need to be removed -- flagged at k1001
            //ft[] movescopy;
            dr drtrace;
            sy cursy= sy.identity; // track all transforms this line; cursy ~~ current symmetry
            di= new displayind();  di.init();
            //sy commutecursy= sy.identity;
            foreach ( pm pmr in reading ){ i+= 1;
                switch( pmT.pm_pmSect( pmr ) ){
                // invertedslice, whole, invertedwhole, modify, memget, memset, retrieve, command, reading, display,
                // transform, token, numeral, pmSub16
                case pmSect.singmaster:  case pmSect.doubl:  case pmSect.slice:  case pmSect.whole:
                    if ( numsreading[ i ] > 0 ){ // preceded by a white space
                        prefixnum= pm.pm60; }
                    if( movecandidate != ft.none ){ moves.Add( movecandidate );  prefixnum= pm.pm60; }
                    movecandidate= (ftT)( pmr );  
                    modifierInConstructedMove= false;  widenerInConstructedMove= false;  movecountInConstructedMove= false;
                    break;
                case pmSect.modify: // ' or w after a movecandidate entered
                    if ( numsreading[ i ] > 0 ){ // isolated modifier, preceded by a white space and not attached -- ignore and cancel construction
                        if( movecandidate != ft.none ){ moves.Add( movecandidate );  movecandidate= ft.none; }
                        modifierInConstructedMove= false;  widenerInConstructedMove= false;  movecountInConstructedMove= false;
                        prefixnumpos= int.MinValue;
                        prefixnum= pm.pm60;  break; }
                    switch( pmr ){
                    case pm.inverted:
                        if( !modifierInConstructedMove ){ movecandidate.Op( ftOp.invert ); modifierInConstructedMove= true; } break;
                    case pm.widen: if( !widenerInConstructedMove ){ switch( prefixnum ){ // ignore extra 'w' in move construction
                            case pm.n0:  movecandidate= ft.none;  break;
                            case pm.n1:  break;
                            case pm.n2:  case pm.pm60:  movecandidate.Op( ftOp.widen );  break;
                            case pm.n3:  movecandidate.Op( ftOp.widen3 );  break; }  widenerInConstructedMove= true;
                        if( prefixnumpos > 0 ){ numsreading[ prefixnumpos ]= int.MaxValue; } // flag employment of prefixnum
                        }
                        prefixnumpos= int.MinValue;
                        prefixnum= pm.pm60;
                        break;
                    }  break;
                case pmSect.numeral:
                    if ( numsreading[ i ] > 0 ){ // numeral appears following a space, thus construction of move is complete
                        if( movecandidate != ft.none ){ moves.Add( movecandidate );  movecandidate= ft.none; }
                        modifierInConstructedMove= false;  widenerInConstructedMove= false;  movecountInConstructedMove= false;
                        prefixnumpos= i;
                        prefixnum= pmr;  break; } // require a prefixnum for wide count to follow a space, else count it as a move count
                    else if ( movecandidate == ft.none && !movecountInConstructedMove && !modifierInConstructedMove && !widenerInConstructedMove ){
                        prefixnumpos= i;
                        prefixnum= pmr;  break; } // multiple prefix numbers are ignored except for the one immediately preceding the move symbol
                    if( !movecountInConstructedMove ){ // ignore additional numerals in the constructed move
                    switch( pmr ){
                    case pm.n0:  movecandidate= ft.none;  break;
                    case pm.n1:  default:  break; // all numerals except 0, (1), 2, or 3 are ignored (counted as noop)
                    case pm.n2: movecandidate.Op( ftOp.doubl );  break;
                    case pm.n3: movecandidate.Op( ftOp.invert );  break;
                    } movecountInConstructedMove= true; }  break;
                case pmSect.transform: // T1 or Txy2z' symmtetry transform operation
                    if( movecandidate != ft.none ){ moves.Add( movecandidate );  movecandidate= ft.none; }
                    modifierInConstructedMove= false;  widenerInConstructedMove= false;  movecountInConstructedMove= false;
                    prefixnumpos= int.MinValue;
                    prefixnum= pm.pm60;
                    switch( pmr ){
                    case pm.transform: switch( numsreading[ i ] ){ // various symmetry transforms on the cube pattern
                        case 0: moves.Add( ft.T0 );  cursy= sy.identity;   break; // added: reset transform indicator to neutral
                        case 1: moves.Add( ft.T1 );  
                            cursy= syT.mult( cursy, sy.inverse );  break; // inverse transform
                        case 2: moves.Add( ft.T2 );  
                            cursy= syT.mult( cursy, sy.reflect );  break; // reflect transform
                        case -1: holdsy= sy.identity;  break;
                        case 12:  case 21: moves.Add( ft.T1 );  moves.Add( ft.T2 ); // abbreviation T12 or T21 same as T1 T2
                                cursy= syT.mult( cursy, sy.invReflect );
                            break;
                        } break;
                    case pm.transformq:
                        List< int > subni= new List< int >();
                        List< string > subsi= new List< string >();
                        subreading= parse1( si[ numsreading[ i ] ], out subni, out subsi );
                        displayind dtcon;
                        subreadparse= parse2( subreading, subni, subsi, out dtcon ); // parse transform contents
                        drtrace= dr.c_UF_;
                        foreach( ft fti in subreadparse ){ // trace through all 3w or entire-cube rotations
                            drtrace= drT.mult3w( drtrace, fti ); }
                        switch( drT.gx( drtrace ) ){ // give the optimal generator in terms of xyz in that order
                            case 0: break;
                            case 1: moves.Add( ft.Tx1 );  break;
                            case 2: moves.Add( ft.Tx2 );  break;
                            case 3: moves.Add( ft.Tx3 );  break;
                        } switch( drT.gy( drtrace ) ){
                            case 0: break;
                            case 1: moves.Add( ft.Ty1 );  break;
                            case 2: moves.Add( ft.Ty2 );  break;
                            case 3: moves.Add( ft.Ty3 );  break;
                        } switch( drT.gz( drtrace ) ){
                            case 0: break;
                            case 1: moves.Add( ft.Tz1 );  break;
                            case 2: moves.Add( ft.Tz2 );  break;
                            case 3: moves.Add( ft.Tz3 );  break;
                        }
                        cursy=  syT.mult( cursy, drtrace );
                        break;
                    } break;
                case pmSect.noop:
                    if ( movecandidate != ft.none ){ moves.Add( movecandidate ); movecandidate= ft.none; }
                    modifierInConstructedMove= false; widenerInConstructedMove= false; movecountInConstructedMove= false;
                    switch( pmr ){
                    case pm.posmark: moves.Add( ft.posMark );  break;
                    } break;
                case pmSect.display: // one case is a move type, posmark @ which is a noop
                    string contents;
                    if ( movecandidate != ft.none ){  moves.Add( movecandidate );  movecandidate= ft.none;  }
                    modifierInConstructedMove= false;  widenerInConstructedMove= false;  movecountInConstructedMove= false;
                    switch( pmr ){
                    case pm.posmark:
                        moves.Add( ft.posMark );
                        if( numsreading[ i ] > 0 ){ // TODO note location and numeral attached, attach a location specifier similar to memget

                        }
                        break;
                    case pm.posmarkq:
                        contents= si[ numsreading[ i ] ];
                        if( contents.Length == 0 ){
                            moves.Add( ft.posMark );
                        } else if ( contents.Length == 5 ){
                            if( contents.Substring( 0, 2 ) == "UF" ){ // first type
                                di.sro_UF= false;
                                di.sro= (dr)( singmaster.singmaster_e[ (byte)( parse0( contents[ 3 ] ) ), (byte)( parse0( contents[ 4 ] ) ) ] );
                                di.srosetnull= di.sro == dr.c_UF_; // permit null override of singmaster setting
                            } else if( si[ numsreading[ i ] ].Substring( 3, 2 ) == "UF" ){ // second type
                                di.sro_UF= true;
                                di.sro= drT.invert( (dr)( singmaster.singmaster_e[ (byte)( parse0( contents[ 0 ] ) ), (byte)( parse0( contents[ 1 ] ) ) ] ) );
                                di.srosetnull= di.sro == dr.c_UF_; // permit null override of singmaster setting
                            }
                        }
                        break;
                    case pm.buffer: // o=ULB and such
                        contents= si[ numsreading[ i ] ];
                        pm pm1;  pm pm2;  pm pm3;
                        if( contents.Length > 0 && contents[ 0 ] != '=' 
                                || contents.Length > 1 && contents[ 0 ] == '=' && contents[ 1 ] == '(' ){ // is of the form o(UL_RF)or o=(_R)
                            if( contents.Length > 1 && contents[ 0 ] == '=' && contents[ 1 ] == '(' ){ 
                                contents= contents.Substring( 2 ); }
                            int divisionform= contents.IndexOf( '_' );
                            string edgeform;  string cornerform;
                            if( divisionform == -1 ){ edgeform= contents;  cornerform= ""; } else {
                                edgeform= contents.Substring( 0, divisionform ).Trim();
                                cornerform= contents.Substring( divisionform + 1, contents.Length - divisionform - 1 ).Trim();
                            }
                            if( edgeform.Length == 1 && singmaster.ee_e.ContainsKey( edgeform[ 0 ] ) ){ // origin set
                                singmaster.ee_e.overrideorigin= singmaster.ee_e[ edgeform[ 0 ] ];
                                di.edgeoriginsingmaster= false;
                            } else {
                            ef prevef= ef.ef24;
                            foreach( char ch in edgeform ){ if( singmaster.ee_e.ContainsKey( ch ) ){
                                if( prevef == ef.ef24 ){ prevef= singmaster.ee_e[ ch ]; } else {
                                    singmaster.orderoverrideset( prevef, singmaster.ee_e[ ch ] );
                                    prevef= singmaster.ee_e[ ch ];
                                }
                            }}
                            }
                            if( cornerform.Length == 1 && singmaster.cc_e.ContainsKey( cornerform[ 0 ] ) ){
                                singmaster.cc_e.overrideorigin= singmaster.cc_e[ cornerform[ 0 ] ];
                                di.corneroriginsingmaster= false;
                            } else {
                            cf prevcf= cf.cf24;
                            foreach( char ch in cornerform ){ if( singmaster.cc_e.ContainsKey( ch ) ){
                                if( prevcf == cf.cf24 ){ prevcf= singmaster.cc_e[ ch ]; } else {
                                    singmaster.orderoverrideset( prevcf, singmaster.cc_e[ ch ] );
                                    prevcf= singmaster.cc_e[ ch ];
                                }
                            }}
                            }
                            // build up order-overrides from call types
                        } else if( contents.Length > 2 && contents[ 0 ] == '=' && contents[ 1 ] != '(' ) // singmaster form
                            if( contents.Length == 3 ){ // edge form
                                ef tempedgebuffer= ef.ef24;
                                pm1= parse.parse0( contents[ 1 ] );
                                pm2= parse.parse0( contents[ 2 ] );
                                tempedgebuffer= singmaster.singmaster_e[ (byte)( pm1 ), (byte)( pm2 ) ];
                                singmaster.ee_e.overrideorigin= tempedgebuffer;
                                di.edgeoriginsingmaster= true;
                            }  else if( contents.Length == 4 ){ // corner form
                                cf tempcornerbuffer= cf.cf24;
                                pm1= parse.parse0( contents[ 1 ] );
                                pm2= parse.parse0( contents[ 2 ] );
                                pm3= parse.parse0( contents[ 3 ] );
                                tempcornerbuffer= singmaster.singmaster_c[ (byte)( pm1 ), (byte)( pm2 ), (byte)( pm3 ) ];
                                singmaster.cc_e.overrideorigin= tempcornerbuffer;
                                di.corneroriginsingmaster= true;
                            }
                        break;
                    }
                    break;
                case pmSect.puncSym: // retain the generated move count for parse-3 to render against the moves encountered
                    if ( movecandidate != ft.none ){ moves.Add( movecandidate );  movecandidate= ft.none; }
                    numsreading[ i ]= moves.Count; // have token at this stage only reference the associated move just read
                    break;
                case pmSect.mem: // parse1 puts a pm.psm to follow the h or g numeral token to denote its move number
                    if( i + 1 < reading.Count && reading[ i+1 ] == pm.psm 
                        ){ numsreading[ i+1 ]= moves.Count; } // reference position in moves
                    break;
                case pmSect.conjugate: // added parse1 puts a pm.psm to mark the insertion point for a conjugate
                    if( i + 1 < reading.Count && reading[ i+1 ] == pm.psm 
                        ){ numsreading[ i+1 ]= moves.Count; } // reference position in moves
                    break;
                case pmSect.command:
                    if( i + 1 < reading.Count && reading[ i+1 ] == pm.psm 
                        ){ numsreading[ i+1 ]= moves.Count; } // reference position in moves
                    break;
                } // \ switch( pmT.pm_pmSub( pmr ) )
            } // \ foreach ( pm pmr in reading )
            if ( movecandidate != ft.none ){ moves.Add( movecandidate );  movecandidate= ft.none; }
            if( removePuncSym ){ 
                for( int j= 0; j < reading.Count; j++ ){ if( pmT.pm_pmSect( reading[ j ] ) == pmSect.puncSym ){
                    reading[ j ]= pm.pm60;
            }}}
            return moves;
        }

        // all ps1, ps2, ..., tokens in _reading_ reference an itemnumber in _moves_; removing or inserting items in _moves_ thus
        // requires all subsequent token links to be (in)decremented to the new index position in _moves_
        static void displacetokenlinks( int displacement, int readingno, List< pm > reading, ref List< int > numsreading ){
            for( int j= readingno; j < reading.Count; j++ ){
                if( pmT.pm_pmSect( reading[ j ] ) == pmSect.puncSym ){
                    if( numsreading[ j ] < 0 && numsreading[ j ] > int.MinValue ){
                        numsreading[ j ]-= displacement; }
                    else if ( numsreading[ j ] >= 0 && numsreading[ j ] < int.MaxValue ){
                        numsreading[ j ]+= displacement; }
                }
            }
        }

        // on replacing a moveset, all references to moves greater than the number of moves reset to the final move in the set 
        static void finaltoken( int length, int readingno, List< pm > reading, ref List< int > numsreading ){
            for( int j= readingno; j < reading.Count; j++ ){
                if( pmT.pm_pmSect( reading[ j ] ) == pmSect.puncSym ){
                    if( numsreading[ j ] >= length ){ numsreading[ j ]= length-1; }
                }
            }
        }


        /// <summary>
        /// Three types of symbols are interpreted within brackets, t3 (:) at positions 0, 2, 4 or 6 -- t4 (,) at positions 1 or 5 
        /// -- and t5 (::) at position 3.  All these define conjugate or commutator expansions.  t3 and t5 are identical, except that
        /// t5 binds after the commutator (,}.  [UL:FR,B] expands to U L F R L' U' B U L R' F' L' U' B' -- [RD::U,LF] expands to
        /// R D U L F U' F' L' D' R' 
        /// </summary>
        struct bracketokens{
            internal int Lbrackpos;
            internal int Lbrackreadpos;
            internal int[] pos;
            internal int[] readpos;
            internal byte t3pos; // 0, 2, 4 or 6
            internal byte t4pos; // 1 or 5
            const byte t5pos= 3;
            internal void init(){
                Lbrackpos= int.MinValue;  Lbrackreadpos= int.MinValue;
                pos= new int[ 7 ];  readpos= new int[ 7 ];
                for( byte i= 0; i < 7; i++ ){ pos[ i ]= int.MinValue;  readpos[ i ]= int.MinValue; }
                t3pos= 0;
                t4pos= 1;
            }
            internal bool advanceTokenpos( byte ttype, int loc, int readloc ){
                if( loc == Lbrackpos ){ return false; } // do not count  [ : immediately after a left bracket
                switch( ttype ){
                case 3: if( pos[ t3pos ] >= 0 ){ return false; } // duplicate ':' at same level -- ignore
                    if( t3pos > 0 && loc == pos[ t3pos - 1 ] ){ return false; } // at least one nontoken entry before this ':'
                    break;
                case 4: if( pos[ t4pos ] >= 0 || loc == pos[ t3pos ] ){ return false; }
                    if( t3pos > 3 && loc == pos[ t5pos ] ){ return false; }
                    break; // duplicate ',' at same level -- ignore
                case 5: if( pos[ t5pos ] >= 0 || loc == pos[ t3pos ] || loc == pos[ t4pos ] ){ return false; }  break; // duplicate '::' at any level -- ignore
                }

                switch( ttype ){
                case 3:
                    pos[ t3pos ]= loc;  readpos[ t3pos ]= readloc;
                    break;
                case 4:
                    pos[ t4pos ]= loc;  readpos[ t4pos ]= readloc;
                    t3pos+= 2;
                    break;
                case 5:
                    pos[ t5pos ]= loc;  readpos[ t5pos ]= readloc;
                    t3pos= 4;
                    t4pos= 5;
                    break;
                }
                return true;
            }
                                      //     t3   t4     t3         t5           t3     t4    t3
            // internal int t5pos;    //  [ U : L  ,   R : F        ::         R : D    ,   B : U ]
                                      //     0     1     2           3           4      5     6
            #region previous/next
            internal int readprevious( byte tpos, sbyte inc ){
                if( tpos == 0 ){ return Lbrackreadpos; }else{ 
                    sbyte tposloc= (sbyte)( tpos - inc );
                    while( tposloc >= 0 && pos[ tposloc ] < 0 ){
                        tposloc-= inc;
                    }
                    if( tposloc < 0 ){ return Lbrackreadpos; }else{
                        return readpos[ tposloc ];
                    }
                }
            }
            internal int readnext( byte tpos, int top, byte inc ){ // inc added: for reading ',', do not stop on ':' types, so inc= 2 in this case
                if( tpos > 6 ){ return top; }else{
                    byte tposloc= (byte)( tpos + inc );
                    while( tposloc < 7 && pos[ tposloc ] < 0 ){
                        tposloc+= inc;
                    }
                    if( tposloc > 6 ){ return top; }else{
                        return readpos[ tposloc ];
                    }
                }
            }
            #endregion
            
            // all items at or beyond insertionpoint are advanced or retracted by displacement, so adding or removing items keeps tokens aligned
            internal void readdisplace( int displacement, int insertionpoint ){
                if( Lbrackpos >= insertionpoint ){ Lbrackpos+= displacement; }
                for( byte i= 0; i < 7; i++ ){
                    if( readpos[ i ] >= insertionpoint ){ readpos[ i ]+= displacement; }
                }
            }
        } //struct bracketokens

        // process tokens and memory resolutions from move list
        public static void parse3( ref List< ft > moves, ref List< pm > reading, List< int > numsreading, List< string > si, ref displayind di ){
            bool singlecontentmodifiedbracketform; // flag if encountered [f] or [r] to convert to z or x
            List< bracketokens > bracketstack= new List< bracketokens >();
            int tokenbracketdepth= 0; // depth of process ps.ps2, ']' from g(ABC) if it appears within a written bracket, i.e. [ g(ABC), F ]
            List< ft > tokenbracketcopy= new List<ft>(); // contents of ps.ps2 .. ps.ps1 read backwards
            int tokenbracketinsertion;
            int destin; // destination counter
            int insertionamount;
            int insertionloc;
            byte lettersystemserial;
            bracketokens bracketstackTop= new bracketokens();  bracketstackTop.init();
            char letter1= '\0';  char letter2= '\0';  char letter3= '\0';
            for( int j= 0; j < reading.Count; j++ ){
                if( pmT.pm_pmSect( reading[ j ] ) == pmSect.puncSym ){ // identify the token
                    #region tokenid
                    switch( reading[ j ] ){ // t1, t2, t3, t4, t5, // [ ] : , ::
                    case pm.ps1: // [
                        if( bracketstackTop.Lbrackpos >= 0 ){ bracketstack.Add( bracketstackTop );  bracketstackTop.init(); }
                        bracketstackTop.Lbrackpos= j;  bracketstackTop.Lbrackreadpos= numsreading[ j ];
                        break;
                    case pm.ps2: if( bracketstackTop.Lbrackpos >= 0 ){ // ] all of the elements between bracketstack[ Count-1 ] are contained within the brackets
                    #region closebracket case
                        singlecontentmodifiedbracketform= false;
                        int openingbracketMovesNo= bracketstackTop.Lbrackreadpos;
                        int closingbracketMovesNo= numsreading[ j ];
                        if( closingbracketMovesNo == openingbracketMovesNo ){ } // empty movecount pass
                        else if( ( closingbracketMovesNo - openingbracketMovesNo == 1 ) // exactly one generated move contained
                                        && ( ftT.ft_ftSub( moves[ openingbracketMovesNo ] ) == ftSub.doubl ) ){ // and of type doubleplane [f] or [r]
                                for( int i= bracketstackTop.Lbrackpos + 1; i < j; i++ ){
                                    if( reading[ i ] == pm.widen ){ goto disqualifybracketcontents; } // which doubleplane is not of the form [Fw]
                                } // verify contents are of form [f] or [r]
                                moves[ openingbracketMovesNo ]= ftT.widen3( moves[ openingbracketMovesNo ] );
                                singlecontentmodifiedbracketform= true; // detected i.e. [r], so set to convert to move "x"
                        } // exactly one component
                        // process each of up to 4 ':' clauses at positions 0, 2, 4, 6
                        for( byte i= 0; i < 7; i+= 2 ){ if( bracketstackTop.pos[ i ] >= 0 ){ // expand contents between token ':'
                            insertionamount= bracketstackTop.readpos[ i ] - bracketstackTop.readprevious( i, 1 );
                            insertionloc= bracketstackTop.readnext( i, closingbracketMovesNo, 1 );
                            tokenbracketdepth= 0;
                            for( int h= bracketstackTop.readprevious( i, 1 ); h < bracketstackTop.readpos[ i ]; h++ ){
                                if( tokenbracketdepth == 0 ){
                                    if( moves[ h ] == ft.ps1 ){ tokenbracketdepth= 1;  tokenbracketcopy.Insert( 0, ft.ps1 ); }
                                    else{ moves.Insert( insertionloc, ftT.invert( moves[ h ] ) ); // fails if moves[ h ] == ps1 thru ps5 from i.e. g(ABC)
                                    }
                                } else {
                                    tokenbracketcopy.Insert( tokenbracketcopy.Count, moves[ h ] );
                                    if( moves[ h ] == ft.ps1 ){ 
                                        tokenbracketdepth+= 1; }
                                    else if( moves[ h ] == ft.ps2 ){ tokenbracketdepth-= 1;
                                        if( tokenbracketdepth == 0 ){
                                            invertinbra( ref tokenbracketcopy, 0, tokenbracketcopy.Count );
                                            tokenbracketinsertion= 0;
                                            foreach( ft fti in tokenbracketcopy ){ moves.Insert( insertionloc + tokenbracketinsertion, fti ); 
                                                tokenbracketinsertion+= 1;
                                            }
                                            tokenbracketcopy.Clear();
                                        }
                                    }
                                }
                            }
                            bracketstackTop.readdisplace( insertionamount, insertionloc );
                            for( int h= 0; h < bracketstack.Count; h++ ){
                                bracketstack[ h ].readdisplace( insertionamount, insertionloc ); }
                            displacetokenlinks( insertionamount, j, reading, ref numsreading );
                            closingbracketMovesNo= numsreading[ j ]; // likely reset by displacetokenlinks
                        }}
                        // process each of up to 2 ',' clauses at positions 1, 5
                        for( byte i= 1; i < 7; i+= 4 ){ if( bracketstackTop.pos[ i ] >= 0 ){ // expand contents between token ','
                            insertionamount= bracketstackTop.readpos[ i ] - bracketstackTop.readprevious( i, 2 );
                            insertionloc= bracketstackTop.readnext( i, closingbracketMovesNo, 2 );
                            tokenbracketdepth= 0;
                            for( int h= bracketstackTop.readprevious( i, 2 ); h < bracketstackTop.readpos[ i ]; h++ ){
                                if( tokenbracketdepth == 0 ){
                                    if( moves[ h ] == ft.ps1 ){ 
                                        tokenbracketdepth= 1;  tokenbracketcopy.Insert( 0, ft.ps1 ); }
                                    else{ moves.Insert( insertionloc, ftT.invert( moves[ h ] ) ); // fails if moves[ h ] == ps1 thru ps5 from i.e. g(ABC)
                                    }
                                } else {
                                    tokenbracketcopy.Insert( tokenbracketcopy.Count, moves[ h ] );
                                    if( moves[ h ] == ft.ps1 ){ tokenbracketdepth+= 1; }
                                    else if( moves[ h ] == ft.ps2 ){ tokenbracketdepth-= 1;
                                        if( tokenbracketdepth == 0 ){
                                            invertinbra( ref tokenbracketcopy, 0, tokenbracketcopy.Count );
                                            tokenbracketinsertion= 0;
                                            foreach( ft fti in tokenbracketcopy ){ moves.Insert( insertionloc + tokenbracketinsertion, fti );
                                                tokenbracketinsertion+= 1;
                                            }
                                            tokenbracketcopy.Clear();
                                        }
                                    }
                                }
                            }
                            tokenbracketdepth= 0;
                            for( int h= bracketstackTop.readpos[ i ]; h < bracketstackTop.readnext( i, closingbracketMovesNo, 2 ); h++ ){
                                if( tokenbracketdepth == 0 ){
                                    if( moves[ h ] == ft.ps1 ){ 
                                        tokenbracketdepth= 1;  tokenbracketcopy.Insert( 0, ft.ps1 ); }
                                    else{ moves.Insert( insertionloc + insertionamount, ftT.invert( moves[ h ] ) ); // fails if moves[ h ] == ps1 thru ps5 from i.e. g(ABC)
                                    }
                                } else {
                                    tokenbracketcopy.Insert( tokenbracketcopy.Count, moves[ h ] );
                                    if( moves[ h ] == ft.ps1 ){ tokenbracketdepth+= 1; }
                                    else if( moves[ h ] == ft.ps2 ){ tokenbracketdepth-= 1;
                                        if( tokenbracketdepth == 0 ){
                                            invertinbra( ref tokenbracketcopy, 0, tokenbracketcopy.Count );
                                            tokenbracketinsertion= 0;
                                            foreach( ft fti in tokenbracketcopy ){ 
                                                moves.Insert( insertionloc + insertionamount + tokenbracketinsertion, fti );
                                                tokenbracketinsertion+= 1;
                                            }
                                            tokenbracketcopy.Clear();
                                        }
                                    }
                                }
                            }
                            insertionamount= bracketstackTop.readnext( i, closingbracketMovesNo, 2 ) - bracketstackTop.readprevious( i, 2 );
                            bracketstackTop.readdisplace( insertionamount, insertionloc );
                            for( int h= 0; h < bracketstack.Count; h++ ){
                                bracketstack[ h ].readdisplace( insertionamount, insertionloc ); }
                            displacetokenlinks( insertionamount, j, reading, ref numsreading );
                            closingbracketMovesNo= numsreading[ j ];
                        }}
                        // process '::' clause at position 3
                        if( bracketstackTop.pos[ 3 ] >= 0 ){
                            insertionamount= bracketstackTop.readpos[ 3 ] - bracketstackTop.Lbrackreadpos;
                            insertionloc= closingbracketMovesNo;
                            tokenbracketdepth= 0;
                            for( int h= bracketstackTop.Lbrackreadpos; h < bracketstackTop.readpos[ 3 ]; h++ ){
                                if( tokenbracketdepth == 0 ){
                                    if( moves[ h ] == ft.ps1 ){ 
                                        tokenbracketdepth= 1;  tokenbracketcopy.Insert( 0, ft.ps1 ); }
                                    else{ moves.Insert( insertionloc, ftT.invert( moves[ h ] ) ); // fails if moves[ h ] == ps1 thru ps5 from i.e. g(ABC)
                                    }
                                } else {
                                    tokenbracketcopy.Insert( tokenbracketcopy.Count, moves[ h ] );
                                    if( moves[ h ] == ft.ps1 ){ tokenbracketdepth+= 1; }
                                    else if( moves[ h ] == ft.ps2 ){ tokenbracketdepth-= 1;
                                        if( tokenbracketdepth == 0 ){
                                            invertinbra( ref tokenbracketcopy, 0, tokenbracketcopy.Count );
                                            tokenbracketinsertion= 0;
                                            foreach( ft fti in tokenbracketcopy ){ moves.Insert( insertionloc + tokenbracketinsertion, fti );
                                                tokenbracketinsertion+= 1;
                                            }
                                            tokenbracketcopy.Clear();
                                        }
                                    }
                                }
                            }
                            bracketstackTop.readdisplace( insertionamount, insertionloc );
                            for( int h= 0; h < bracketstack.Count; h++ ){
                                bracketstack[ h ].readdisplace( insertionamount, insertionloc ); }
                            displacetokenlinks( insertionamount, j, reading, ref numsreading );
                            closingbracketMovesNo= numsreading[ j ];
                        }

                        disqualifybracketcontents:
                        if( j + 1 < reading.Count && !singlecontentmodifiedbracketform 
                                    && ( pmT.pm_pmSect( reading[ j + 1 ] ) == pmSect.numeral && numsreading[ j + 1 ] < 0 
                                        || reading[ j + 1 ] == pm.inverted && numsreading[ j + 1 ] < 0 ) ){ //" dqbracket
                            switch( reading[ j + 1 ] ){ // unemployed numeral immediately follows closing bracket ']'
                            case pm.n0: // omit contents of bracket
                                for( int i= openingbracketMovesNo; i < closingbracketMovesNo; i++ ){
                                    moves.RemoveAt( openingbracketMovesNo );
                                }
                                displacetokenlinks( openingbracketMovesNo - closingbracketMovesNo, j, reading, ref numsreading );
                                break;
                            case pm.n1:  break; // pass // no-op
                            case pm.n2:
                                destin= 0;
                                for( int i= openingbracketMovesNo; i < closingbracketMovesNo; i++ ){
                                    moves.Insert( closingbracketMovesNo + destin, moves[ i ] );  destin++; }
                                displacetokenlinks( closingbracketMovesNo - openingbracketMovesNo, j, reading, ref numsreading );
                                break;
                            #region laborious cases 3..9
                            case pm.n3:
                                destin= 0;
                                for( int i= openingbracketMovesNo; i < closingbracketMovesNo; i++ ){
                                    moves.Insert( closingbracketMovesNo + destin, moves[ i ] );  destin++; }
                                    // while this works, all of the tokens following require an insertion advance
                                displacetokenlinks( closingbracketMovesNo - openingbracketMovesNo, j, reading, ref numsreading );
                                goto case pm.n2;
                            case pm.n4:
                                destin= 0;
                                for( int i= openingbracketMovesNo; i < closingbracketMovesNo; i++ ){
                                    moves.Insert( closingbracketMovesNo + destin, moves[ i ] );  destin++; }
                                displacetokenlinks( closingbracketMovesNo - openingbracketMovesNo, j, reading, ref numsreading );
                                goto case pm.n3;
                            case pm.n5:
                                destin= 0;
                                for( int i= openingbracketMovesNo; i < closingbracketMovesNo; i++ ){
                                    moves.Insert( closingbracketMovesNo + destin, moves[ i ] );  destin++; }
                                displacetokenlinks( closingbracketMovesNo - openingbracketMovesNo, j, reading, ref numsreading );
                                goto case pm.n4;
                            case pm.n6:
                                destin= 0;
                                for( int i= openingbracketMovesNo; i < closingbracketMovesNo; i++ ){
                                    moves.Insert( closingbracketMovesNo + destin, moves[ i ] );  destin++; }
                                displacetokenlinks( closingbracketMovesNo - openingbracketMovesNo, j, reading, ref numsreading );
                                goto case pm.n5;
                            case pm.n7:
                                destin= 0;
                                for( int i= openingbracketMovesNo; i < closingbracketMovesNo; i++ ){
                                    moves.Insert( closingbracketMovesNo + destin, moves[ i ] );  destin++; }
                                displacetokenlinks( closingbracketMovesNo - openingbracketMovesNo, j, reading, ref numsreading );
                                goto case pm.n6;
                            case pm.n8:
                                destin= 0;
                                for( int i= openingbracketMovesNo; i < closingbracketMovesNo; i++ ){
                                    moves.Insert( closingbracketMovesNo + destin, moves[ i ] );  destin++; }
                                displacetokenlinks( closingbracketMovesNo - openingbracketMovesNo, j, reading, ref numsreading );
                                goto case pm.n7;
                            case pm.n9:
                                destin= 0;
                                for( int i= openingbracketMovesNo; i < closingbracketMovesNo; i++ ){
                                    moves.Insert( closingbracketMovesNo + destin, moves[ i ] );  destin++; }
                                displacetokenlinks( closingbracketMovesNo - openingbracketMovesNo, j, reading, ref numsreading );
                                goto case pm.n8;
                            #endregion
                            case pm.inverted: // reverse contents of bracket and handle if bracket tokens from resolving i.e. g(_ABC)
                                List< ft >movesect= new List< ft >( closingbracketMovesNo - openingbracketMovesNo );
                                //moves.CopyTo( openingbracketMovesNo, movesect, 0, closingbracketMovesNo - openingbracketMovesNo );
                                for( int i= openingbracketMovesNo; i < closingbracketMovesNo; i++ ){
                                    movesect.Add( moves[ i ] ); }
                                invertinbra( ref movesect, 0, closingbracketMovesNo - openingbracketMovesNo );
                                destin= 0;
                                for( int i= openingbracketMovesNo; i < closingbracketMovesNo; i++ ){
                                    moves[ i ]= movesect[ destin ];  destin+= 1;
                                }
                                break;
                            }
                        } //" dq bracket
                        // if numeral follows not flagged as used (where numsreading[ j + 1 ] == int.MaxValue), apply as suffix movenum so [r]2 == z2
                        if( singlecontentmodifiedbracketform && j + 1 < reading.Count 
                                    && pmT.pm_pmSect( reading[ j + 1 ] ) == pmSect.numeral
                                    && numsreading[ j + 1 ] < 0 ){ // immediately attached and not employed ( where numsreading[ i + 1 ] == int.MaxValue )
                            switch( reading[ j + 1 ] ){
                            case pm.n2:  moves[ openingbracketMovesNo ]= ftT.doubl( moves[ openingbracketMovesNo ] );   break;// double move 
                            case pm.n3:  moves[ openingbracketMovesNo ]= ftT.invert( moves[ openingbracketMovesNo ] );  break;// inverse move
                            }
                        } else if( singlecontentmodifiedbracketform && j + 1 < reading.Count && numsreading[ j + 1 ] < 0 
                                && reading[ j + 1 ]== pm.inverted ){
                            moves[ openingbracketMovesNo ]= ftT.invert( moves[ openingbracketMovesNo ] );  break; // apply immediately following ' so  [r]' == z'
                        }
                        if( bracketstack.Count > 0 ){ 
                            bracketstackTop= bracketstack[ bracketstack.Count - 1 ];
                            bracketstack.RemoveAt( bracketstack.Count - 1 ); }
                        else { bracketstackTop.init(); }
                    } // case pm.t2
                    #endregion
                    break;
                    case pm.ps3: // :
                        if( bracketstackTop.Lbrackpos >= 0 ){
                            bracketstackTop.advanceTokenpos( 3, j, numsreading[ j ] );
                        }
                        break;
                    case pm.ps4: // ,
                        if( bracketstackTop.Lbrackpos >= 0 ){
                            bracketstackTop.advanceTokenpos( 4, j, numsreading[ j ] );
                        }
                        break;
                    case pm.ps5: // ::
                        if( bracketstackTop.Lbrackpos >= 0 ){ // [ U L :: R D , F : B ]
                             bracketstackTop.advanceTokenpos( 5, j, numsreading[ j ] );
                        }
                        break;
                    case pm.k0m: // encountered a k0 at final parse reading, clear reading and reset
                        // U [LB]2 k0  R M [D] // U (1) [^1 L (3) B (-4) ]^5 N2 (-6) <k0>^5 R (8) M (9) [^7 D (11) ]^8 
                        //moves.RemoveRange( 0, numsreading[ j ] );  // BUG all of the orphaned links need to be invalidated ln 1779 of position.cs
                        //displacetokenlinks( -numsreading[ j ], j, reading, ref numsreading );
                        bracketstack.Clear();
                        break;
                    } // switch reading[ j ]
                    #endregion
                } // if ... == pmSect.token // TODO lowercase associations list, for example "v" for verbs.  deafult list is "n" for nouns
                else if( pmT.pm_pmSect( reading[ j ] ) == pmSect.mem && reading[ j ] == pm.pairimage ){
                    if( si[ numsreading[ j ]].Length < 2 ){ continue; } // fail if less than two letters attached
                    letter1= si[ numsreading[ j ]][0];  letter2= si[ numsreading[ j ]][1];
                    if( !char.IsLetter( letter1 ) || !char.IsLetter( letter2 ) ){ continue; }
                    letter1= char.ToUpper( letter1 );  letter2= char.ToUpper( letter2 );
                    di.suffixmsg= catalog.associations[ catalog.associations[ "default" ].asString ]
                        [ letter1.ToString() ][ letter2.ToString() ].asString;
                }
                else if( pmT.pm_pmSect( reading[ j ] ) == pmSect.mem && reading[ j ] == pm.pairretrieve ){
                    letter2= '\0';  letter3= '\0';
                    switch( si[ numsreading[ j ]].Length ){
                    case 0: continue;
                    case 1: letter1= si[ numsreading[ j ]][0];  break;
                    case 2: letter2= si[ numsreading[ j ]][1];  goto case 1;
                    default: letter3= si[ numsreading[ j ]][2];  goto case 2; }

                    if( char.IsDigit( letter1 ) && letter2 == '\0' ){ // setup character type
                    byte charval;   if ( !byte.TryParse( si[ numsreading[ j ]][0].ToString(), out charval ) ){ continue; }
                    switch( charval ){
                    case 2: // edge type convert to pairsetup
                        if( si[ numsreading[ j ]].Length == 1 ){
                            reading[ j ]= pm.pairsetup;
                            si[ numsreading[ j ]]= "";
                            numsreading[ j ]= 2;
                        }
                        break;
                    case 3: // corner type convert to pairsetup
                        if( si[ numsreading[ j ]].Length == 1 ){
                            reading[ j ]= pm.pairsetup;
                            si[ numsreading[ j ]]= "";
                            numsreading[ j ]= 3;
                        }
                        break;
                    }}
                    if( letter2 == '\0' && singmaster.ee_e.ContainsKey( letter1 ) 
                            && (( letter2 == '\0' ) || letter2 == '_' && letter3 == '\0' )){ // "sR" sugar for "s=R_"
                        ef result= singmaster.ee_e[ letter1 ];
                        di.suffixmsg= singmaster.ee_e[ result ] + "_ = " +
                            pmT.pmT_string( ssT.ecf_pm( result ))
                            + pmT.pmT_string( ssT.ecf_pm( ssT.eOBV( result ) ))
                            + " " + singmaster.lettersystemname;
                        continue;
                    }
                    if( letter1 == '_' && singmaster.cc_e.ContainsKey( letter2 ) ){
                        cf result= singmaster.cc_e[ letter2 ];
                        di.suffixmsg= "_" + singmaster.cc_e[ result ] + " = "
                            + pmT.pmT_string( ssT.ecf_pm( result ) )
                            + pmT.pmT_string( ssT.ecf_pm( ssT.cCL( result ) ) )
                            + pmT.pmT_string( ssT.ecf_pm( ssT.cCCL( result ) ) )
                            + " " + singmaster.lettersystemname;
                        continue;
                    }
                    if( letter1 == '=' && char.IsDigit( letter2 ) 
                            && byte.TryParse( letter2.ToString(), out lettersystemserial ) ){ // set lettersystem -- currently only 9 permitted
                        singmaster.assignLetters( lettersystemserial );
                        di.suffixmsg= singmaster.lettersystemname + " letter system";
                        continue;
                    }
                    if( letter1 == '=' && singmaster.ee_e.ContainsKey( letter2 ) && ( letter3 == '\0' || letter3 == '_' && si[ numsreading[ j ]].Length == 3 ) ){
                            // permit s=L sugar for sL_
                        ef result= singmaster.ee_e[ letter2 ];
                        di.suffixmsg= singmaster.ee_e[ result ] + "_ = " +
                                pmT.pmT_string( ssT.ecf_pm( result ))
                                + pmT.pmT_string( ssT.ecf_pm( ssT.eOBV( result ) ))
                                + " " + singmaster.lettersystemname;
                        continue; }
                    if( letter1 == '=' && letter2 == '_' && singmaster.cc_e.ContainsKey( letter3 ) && si[ numsreading[ j ]].Length == 3 ){
                        cf result= singmaster.cc_e[ letter3 ];
                        di.suffixmsg= "_" + singmaster.cc_e[ result ] + " = "
                            + pmT.pmT_string( ssT.ecf_pm( result ))
                            + pmT.pmT_string( ssT.ecf_pm( ssT.cCL( result ) ) )
                            + pmT.pmT_string( ssT.ecf_pm( ssT.cCCL( result ) ) )
                            + " " + singmaster.lettersystemname;
                        continue; }
                    if( letter1 == '=' && si[ numsreading[ j ]].Length > 1 ){ // singmaster to lettersystem name report
                        char singmaster1= si[ numsreading[ j ]][ 1 ];
                        char singmaster2= si[ numsreading[ j ]][ 2 ];
                        char singmaster3= '\0';
                        pm pm1;  pm pm2;  pm pm3;
                        if( si[ numsreading[ j ]].Length > 3 ){ singmaster3= si[ numsreading[ j ]][3]; }
                        if( singmaster3 == '\0' ){
                            pm1= parse.parse0( singmaster1 );
                            if( pmT.pm_pmSect( pm1 ) != pmSect.singmaster ){ continue; }
                            pm2= parse.parse0( singmaster2 );
                            if( pmT.pm_pmSect( pm2 ) != pmSect.singmaster ){ continue; }
                            ef result= singmaster.singmaster_e[ (byte)( pm1 ), (byte)( pm2 ) ];
                            di.suffixmsg= singmaster.ee_e[ result ] + "_ = " +
                                pmT.pmT_string( ssT.ecf_pm( result ))
                                + pmT.pmT_string( ssT.ecf_pm( ssT.eOBV( result ) ))
                                + " " + singmaster.lettersystemname;
                        } else {
                            pm1= parse.parse0( singmaster1 );
                            if( pmT.pm_pmSect( pm1 ) != pmSect.singmaster ){ continue; }
                            pm2= parse.parse0( singmaster2 );
                            if( pmT.pm_pmSect( pm2 ) != pmSect.singmaster ){ continue; }
                            pm3= parse.parse0( singmaster3 );
                            if( pmT.pm_pmSect( pm3 ) != pmSect.singmaster ){ continue; }
                            cf result= singmaster.singmaster_c[ (byte)( pm1 ), (byte)( pm2 ), (byte)( pm3 ) ];
                            di.suffixmsg= "_" + singmaster.cc_e[ result ] + " = " +
                                pmT.pmT_string( ssT.ecf_pm( result )) 
                                + pmT.pmT_string( ssT.ecf_pm( ssT.cCL( result ))) 
                                + pmT.pmT_string( ssT.ecf_pm( ssT.cCCL( result )))
                                + " " + singmaster.lettersystemname;
                        }
                        continue;
                    }

                    if( !char.IsLetter( letter1 ) || !char.IsLetter( letter2 ) ){ continue; }
                    // TODO process letter pair, lookup pairs for setup suggestion as a comment
                }
                else if( pmT.pm_pmSect( reading[ j ] ) == pmSect.mem 
                        && j+1 < reading.Count && reading[ j + 1 ] == pm.psm ){ //" save to memory item
                    #region mem
                    switch( reading[ j ] ){
                    case pm.get:  case pm.getq:  case pm.getname:
                    #region reading[j]=pm.get(q/name)
                    int count;
                    int insertionpoint= numsreading[ j + 1 ];

                    switch( reading[ j ] ){
                    case pm.get:
                    if( singmaster.sessionMemory.ContainsKey( (short)( numsreading[ j ] ) ) ){
                        count= 0;
                        foreach( ft fti in singmaster.sessionMemory[ (short)( numsreading[ j ] ) ] ){
                            moves.Insert( insertionpoint + count, singmaster.sessionMemory[ (short)( numsreading[ j ] ) ][ count ] );
                            count += 1;
                        }
                        displacetokenlinks( singmaster.sessionMemory[ (short)( numsreading[ j ] ) ].Count, j, reading, ref numsreading );
                    } break;
                    case pm.getname:
                    if( singmaster.sessionNameMemory.ContainsKey( si[ numsreading[ j ] ] ) ){
                        count= 0;
                        foreach( ft fti in singmaster.sessionNameMemory[ si[ numsreading[ j ] ] ] ){
                            moves.Insert( insertionpoint + count, singmaster.sessionNameMemory[ si[ numsreading[ j ] ] ][ count ] );
                            count += 1;
                        }
                        displacetokenlinks( singmaster.sessionNameMemory[ si[ numsreading[ j ] ] ].Count, j, reading, ref numsreading );
                    } break;
                    case pm.getq: // - - - - -     - - - - -
                    string gqs= si[ numsreading[ j ] ].Trim();
                    catalog.entrybuild gq= new catalog.entrybuild();
                    bool invert= false;
                    if( gqs.Length == 0 ){ break; } // guard against g()
                    if( gqs[ gqs.Length-1 ] == '\'' ){ invert= true;  gqs= gqs.Remove( gqs.Length-1 ).Trim(); }
                    if( gqs[ 0 ] == '_' ){
                    if( gqs.Length == 3 ){
                        gq.insert( singmaster.cc_e.origin );
                        if( invert ){
                        gq.insert( singmaster.cc_e[ gqs[ 2 ] ] );  gq.insert( singmaster.cc_e[ gqs[ 1 ] ] );
                        } else {
                        gq.insert( singmaster.cc_e[ gqs[ 1 ] ] );  gq.insert( singmaster.cc_e[ gqs[ 2 ] ] ); }
                    } else {
                        gq.insert( singmaster.cc_e[ gqs[ 1 ] ] );
                        if( invert ){
                        gq.insert( singmaster.cc_e[ gqs[ 3 ] ] );  gq.insert( singmaster.cc_e[ gqs[ 2 ] ] );
                        } else {
                        gq.insert( singmaster.cc_e[ gqs[ 2 ] ] );  gq.insert( singmaster.cc_e[ gqs[ 3 ] ] ); }
                    }}
                    else {
                    if( gqs.Length == 2 || gqs[ 2 ] == '_' ){
                        gq.insert( singmaster.ee_e.origin );
                        if( invert ){
                        gq.insert( singmaster.ee_e[ gqs[ 1 ] ] );  gq.insert( singmaster.ee_e[ gqs[ 0 ] ] );
                        } else {
                        gq.insert( singmaster.ee_e[ gqs[ 0 ] ] );  gq.insert( singmaster.ee_e[ gqs[ 1 ] ] ); }
                    } else {
                        gq.insert( singmaster.ee_e[ gqs[ 0 ] ] );
                        if( invert ){
                        gq.insert( singmaster.ee_e[ gqs[ 2 ] ] );  gq.insert( singmaster.ee_e[ gqs[ 1 ] ] );
                        } else {
                        gq.insert( singmaster.ee_e[ gqs[ 1 ] ] );  gq.insert( singmaster.ee_e[ gqs[ 2 ] ] ); }
                    }}
                    catalog.entry gqe= gq.ToEntry();
                    if( !catalog.T.ContainsKey( gqe ) ){ gq.rotate();  gqe= gq.ToEntry(); }
                    if( !catalog.T.ContainsKey( gqe ) ){ gq.rotate();  gqe= gq.ToEntry(); }
                    if( !catalog.T.ContainsKey( gqe ) ){ gq.rotate();  gq.fliptwist();  gqe= gq.ToEntry(); }
                    if( !catalog.T.ContainsKey( gqe ) ){ gq.rotate();  gqe= gq.ToEntry(); }
                    if( !catalog.T.ContainsKey( gqe ) ){ gq.rotate();  gqe= gq.ToEntry(); }
                    if( !catalog.T.ContainsKey( gqe ) ){ gq.rotate();  gq.fliptwist();  gqe= gq.ToEntry(); }
                    if( !catalog.T.ContainsKey( gqe ) ){ gq.rotate(); gqe= gq.ToEntry(); }
                    if( !catalog.T.ContainsKey( gqe ) ){ gq.rotate(); gqe= gq.ToEntry(); }
                    if( catalog.T.ContainsKey( gqe ) ){ //" keyfound
                    int countc= 0;
                    foreach( ft fti in catalog.T[ gqe ].moves ){
                        moves.Insert( insertionpoint + countc, fti );
                        countc += 1;
                    }
                    displacetokenlinks( catalog.T[ gqe ].moves.Length, j, reading, ref numsreading );
                    if( di.suffixmsg == "" ){ 
                        di.suffixmsg= catalog.T[ gqe ].type;
                    } else { di.suffixmsg= " "; }
                    } //" keyfound
                    break;
                    }
                    #endregion
                    break;
                    }
                    #endregion
                } //" save to memory item

#if example
input: s=1 s3| TD BD ED || 
reading
Count = 9
    [0]: pairretrieve
    [1]: pairretrieve
    [2]: transformq
    [3]: dd
    [4]: bp
    [5]: dd
    [6]: ee
    [7]: dd
    [8]: facechase

input: s=1 s3 |TD BD ED||
Count = 4
    [0]: pairretrieve
    [1]: pairsetup
    [2]: facechase
    [3]: psm

#endif

                else if( pmT.pm_pmSect( reading[ j ] ) == pmSect.faceletchaseRead ){//" faceletchaseread -- detected token "|"
                    if( j > 0 && reading[ j-1 ] == pm.pairsetup ){ // immediately previous token s2 or s3, in this case
                    switch( numsreading[ j-1 ] ){ // \ specifies an edge or corner conjugate setup
                    case 2: // edge type setup -- si[ j ] contains pair setup for edges
                        conjugatestudy.insert( catalog.cstudy, 2, si[ j ] );  break;
                    case 3: // corner type setup -- si[ j ] contains pair setup for corners
                        conjugatestudy.insert( catalog.cstudy, 3, si[ j ] );  break;
                    } // switch si[ j-1 ];

                    } else {//" faceletchaseRead not following a pairsetup
                    List< ft > facechasereading;
                    parseFaceChase( si[ numsreading[ j ]], out facechasereading ); // ignores output -- formatted string
                    int countcfc= 0;
                    if( j+1 < numsreading.Count ){ // guard against no followup numsreading
                        int insertionpointfc= numsreading[ j+1 ]; // BUG this is actually the strsreading position, not insertionpoint
                        if( insertionpointfc >= moves.Count ){ foreach( ft fti in facechasereading ){
                            moves.Add( fti ); }
                        } else { foreach( ft fti in facechasereading ){
                            moves.Insert( insertionpointfc, fti );
                            insertionpointfc += 1;
                            countcfc += 1;
                        }}
                        displacetokenlinks( facechasereading.Count, j, reading, ref numsreading );
                    }//" guard against no followup numsreading
                    }//" faceletchaseRead not following a pairsetup
                }//" faceletchaseread -- detected token "|"
            } // for j .. reading.Count
            // if( di.k0clearings.Count > 0 ){ moves.RemoveRange( 0, di.k0clearings[ di.k0clearings.Count-1 ] ); }
        } // parse3

        static void invertinbra( ref List< ft > mvinbra, int S, int Len ){ // start, length
            // invert: [ R F : D B ]' == [ R F : B' D' ]  // invert second element of a commutator (colon)
            // invert: [ R F , D B ]' == [ D B , R F ] // reverse the two clauses in a conjugate (comma)
            // ps1, ps2, ps3, ps4, ps5,                        // [ ] : , :: 

            ft primesep= ft.none;
            int psloc= -1;
            short bradepth= 0;
            int psoffset;
            List< ft > hold= new List<ft>();
            for( int i= S; i < S + Len; i++ ){ //" determine prime separater _primesep_, the lowest binding token in range
                switch ( mvinbra[ i ] ){
                case ft.ps5:  if( bradepth == 0 && primesep < ft.ps5 ){
                    primesep= ft.ps5;  psloc= i; }  break;
                case ft.ps4:  if( bradepth == 0 && primesep < ft.ps4 ){ 
                    primesep= ft.ps4;  psloc= i; }  break;
                case ft.ps3:  if( bradepth == 0 && primesep < ft.ps3 ){
                    primesep= ft.ps3;  psloc= i; }  break;
                case ft.ps2:  if( bradepth > 0 ){ bradepth-= 1; }  break;
                case ft.ps1:  bradepth += 1;  break;
                }
            }
            switch( primesep ){
            case ft.ps5:  case ft.ps3:  invertinbra( ref mvinbra, psloc + 1, Len - psloc - 1 );  break;
            case ft.ps4:
                hold.Clear();
                for( int i= psloc+1; i < S + Len; i++ ){ hold.Add( mvinbra[ i ] ); }
                for( int i= psloc; i < S + Len; i++ ){ mvinbra.RemoveAt( psloc ); }
                psoffset= S;
                foreach( ft fti in hold ){ mvinbra.Insert( psoffset, fti );  psoffset+= 1; }
                mvinbra.Insert( psoffset, ft.ps4 );
                break;
            case ft.none:  // invert contents
                ft[] movescopy= mvinbra.ToArray();  List< ft > moves= new List< ft >();
                bradepth= 0; // track ps1 in the case of command k1 for matching ps2
                List< ft > inbracket= new List< ft >(); // copy the contents of [ ... ] in order
                for( int j= movescopy.Length - 1; j > -1; j-- ){
                    if( movescopy[ j ] == ft.ps1 && bradepth > 0 ){ bradepth-= 1;
                        if( bradepth == 0 ){
                            invertinbra( ref inbracket, 0, inbracket.Count );
                            foreach( ft fti in inbracket ){ moves.Add( fti ); }
                            inbracket.Clear();
                        }}
                    if( bradepth == 0 ){ 
                        moves.Add( ftT.invert( movescopy[ j ] ) ); }
                    else{ inbracket.Insert( 0, movescopy[ j ] ); }
                    if( movescopy[ j ] == ft.ps2 ){ bradepth+= 1; }
                }
                for( int i= S; i < S + Len; i++ ){ mvinbra[ i ]= moves[ i - S ]; }
                break;
            }
        }

        // process move manipulation commands
        public static void parseK( ref List< ft > moves, ref List< pm > reading, List< int > numsreading, List< string > si, ref displayind di ){
            int move_i= -1;
            ftT movecandidate= ft.none;
            ft[] movescopy;  sy cursy= sy.identity;
            List< pm > subreading= new List< pm >();
            List< ft > subreadparse;
            dr drtrace;
            int consolidaterotation;
            foreach( pm pmr in reading ){ move_i+= 1;
                switch( pmT.pm_pmSect( pmr ) ){
                case pmSect.faceletchaseRead: // contents of Kxy'z2 command to convert via symmetry transform
                    if( movecandidate != ft.none ){ moves.Add( movecandidate );  movecandidate= ft.none; }
                    switch( pmr ){
                    case pm.commandSym: // process Kxy'
                        #region commandSym
                        List< int > subnik= new List< int >();
                        List< string > subsik= new List< string >();
                        subreading= parse1( si[ numsreading[ move_i ] ], out subnik, out subsik );
                        parse.displayind diconsy;
                        subreadparse= parse2( subreading, subnik, subsik, out diconsy );
                        drtrace= dr.c_UF_;
                        foreach( ft fti in subreadparse ){
                            drtrace= drT.mult3w( drtrace, fti ); }
                        if( drtrace != dr.c_UF_ ){ //" not identity
                        movescopy= moves.ToArray();
                        for( int j= 0; j < movescopy.Length; j++ ){
                            movescopy[ j ]= ftT.mult( movescopy[ j ], drtrace ); }
                        moves.Clear();
                        foreach( ft fti in movescopy ){ moves.Add( fti ); }
                        } //\" not identity
                        #endregion
                        break;
                    } //\ switch( pmr )
                    break;
                case pmSect.command: // process k1, k2, etc., various cammands to manipulate the reading built up thus far
                    #region k0, k1, ...
                    if( movecandidate != ft.none ){ moves.Add( movecandidate );  movecandidate= ft.none; }
                    movescopy= moves.ToArray();
                    int appUpBoundary= movescopy.Length;
                    if( move_i + 1 < reading.Count ){ appUpBoundary= numsreading[ move_i + 1 ]; }
                    dr currentdisp;
                    ft srcm; ft srcr;  /*ft calcm;*/  // source move; source relocated move;  source calculated move
                    switch( numsreading[ move_i ] ){ // g(RJ) k1 // [ F' :: U2 , F E2 F' ] k1
                    // k0 already replace by ft.k0m
                    case 1: moves.Clear();  // command k1 - invert
                        #region invert
                        int bradepth= 0; // track ps1 in the case of command k1 for matching ps2
                        List< ft > inbracket= new List< ft >(); // copy the contents of [ ... ] in order
                        for( int j= appUpBoundary-1; j > -1; j-- ){
                            if( movescopy[ j ] == ft.ps1 && bradepth > 0 ){ bradepth-= 1;
                                if( bradepth == 0 ){
                                    invertinbra( ref inbracket, 0, inbracket.Count );
                                    foreach( ft fti in inbracket ){ moves.Add( fti ); }
                                    inbracket.Clear();
                                }}
                            if( bradepth == 0 ){ 
                                moves.Add( ftT.invert( movescopy[ j ] ) ); }
                            else{ inbracket.Insert( 0, movescopy[ j ] ); }
                            if( movescopy[ j ] == ft.ps2 ){ bradepth+= 1; }
                        }
                        for( int j= appUpBoundary; j < movescopy.Length; j++ ){ moves.Add( movescopy[ j ] ); }
                        #endregion
                        break;
                    case 2: moves.Clear();  // command k2 - reflectM
                        #region reflectM
                        for ( int j= 0; j < appUpBoundary; j++ ){
                            moves.Add( ftT.reflectM( movescopy[ j ] ) ); }
                        for( int j= appUpBoundary; j < movescopy.Length; j++ ){ moves.Add( movescopy[ j ] ); }
                        #endregion
                        break;
                    case 3: // remove double-plane (w-type) moves, most commonly to work with Cube Explorer which does not support double-plane moves
                        #region remove double-plane (w-type) moves
                        moves.Clear();
                        currentdisp= dr.c_UF_;
                        for( int j= 0; j < appUpBoundary; j++ ){ srcm= movescopy[ j ];  srcr= ftT.mult( srcm, currentdisp );
                            switch( ftT.ft_ftSub( srcm ) ){
                            case ftSub.doubl:
                                moves.Add( ftT.unwiden2( srcr ) );
                                currentdisp= drT.mult3w( currentdisp, ftT.invert( ftT.widen3( srcr ) ) );
                                break;
                            default: moves.Add( srcr );  break;
                            }
                        }
                        switch( drT.gx( drT.invert( currentdisp )) ){
                        case 1:  moves.Add( ft.x1 );  break;
                        case 2:  moves.Add( ft.x2 );  break;
                        case 3:  moves.Add( ft.x3 );  break;
                        }
                        switch( drT.gy( drT.invert( currentdisp )) ){
                        case 1:  moves.Add( ft.y1 );  break;
                        case 2:  moves.Add( ft.y2 );  break;
                        case 3:  moves.Add( ft.y3 );  break;
                        }
                        switch( drT.gz( drT.invert( currentdisp )) ){
                        case 1:  moves.Add( ft.z1 );  break;
                        case 2:  moves.Add( ft.z2 );  break;
                        case 3:  moves.Add( ft.z3 );  break;
                        }
                        displacetokenlinks( moves.Count - appUpBoundary, move_i, reading, ref numsreading );
                        for( int j= appUpBoundary; j < movescopy.Length; j++ ){ moves.Add( movescopy[ j ] ); }
                        #endregion
                        break;
                    case 4: // remove whole-cube rotations (x, y, or z) moves; also consolidate all at the end
                        #region remove whole-cube rotations (x, y, or z) moves
                        moves.Clear();
                        currentdisp = dr.c_UF_;
                        for( int j= 0; j < appUpBoundary; j++ ){ srcm= movescopy[ j ];  srcr= ftT.mult( srcm, currentdisp );
                            switch( ftT.ft_ftSub( srcm ) ){
                            case ftSub.whole:
                                currentdisp= drT.mult3w( currentdisp, ftT.invert( srcr ) );
                                break;
                            default: moves.Add( srcr );  break;
                            }
                        }
                        switch( drT.gx( drT.invert( currentdisp )) ){
                        case 1:  moves.Add( ft.x1 );  break;
                        case 2:  moves.Add( ft.x2 );  break;
                        case 3:  moves.Add( ft.x3 );  break;
                        }
                        switch( drT.gy( drT.invert( currentdisp )) ){
                        case 1:  moves.Add( ft.y1 );  break;
                        case 2:  moves.Add( ft.y2 );  break;
                        case 3:  moves.Add( ft.y3 );  break;
                        }
                        switch( drT.gz( drT.invert( currentdisp )) ){
                        case 1:  moves.Add( ft.z1 );  break;
                        case 2:  moves.Add( ft.z2 );  break;
                        case 3:  moves.Add( ft.z3 );  break;
                        }
                        displacetokenlinks( moves.Count - appUpBoundary, move_i, reading, ref numsreading );
                        for( int j= appUpBoundary; j < movescopy.Length; j++ ){ moves.Add( movescopy[ j ] ); }
                        #endregion
                        break;
                    case 5: // remove slice turns
                        #region remove and replace slice turns
                        moves.Clear();
                        currentdisp= dr.c_UF_;
                        for( int j= 0; j < appUpBoundary; j++ ){ srcm= movescopy[ j ];
                                srcr= ftT.mult( srcm, currentdisp );
                            switch( ftT.ft_ftSub( srcm ) ){
                            case ftSub.slice: // insert two moves representing slice turn
                                moves.Add( ftT.unwiden2( srcr ) );
                                moves.Add( ftT.unwiden2( ftT.unwiden2( srcr ) ) );
                                currentdisp= drT.mult3w( currentdisp, ftT.wide_wide3part( ftT.invert( srcr ) ) );
                                break;
                            default: moves.Add( srcr );  break;
                            }
                        }
                        switch( drT.gx( drT.invert( currentdisp )) ){
                        case 1:  moves.Add( ft.x1 );  break;
                        case 2:  moves.Add( ft.x2 );  break;
                        case 3:  moves.Add( ft.x3 );  break;
                        }
                        switch( drT.gy( drT.invert( currentdisp )) ){
                        case 1:  moves.Add( ft.y1 );  break;
                        case 2:  moves.Add( ft.y2 );  break;
                        case 3:  moves.Add( ft.y3 );  break;
                        }
                        switch( drT.gz( drT.invert( currentdisp )) ){
                        case 1:  moves.Add( ft.z1 );  break;
                        case 2:  moves.Add( ft.z2 );  break;
                        case 3:  moves.Add( ft.z3 );  break;
                        }
                        displacetokenlinks( moves.Count - appUpBoundary, move_i, reading, ref numsreading );
                        for( int j= appUpBoundary; j < movescopy.Length; j++ ){ moves.Add( movescopy[ j ] ); }
                        #endregion
                        break;
                    case 6: // consolidate all consecutive simple faceturn moves and collect cube rotations
                        #region consolidate all consecutive simple faceturn moves and collect cube rotations
                        moves.Clear();
                        currentdisp= dr.c_UF_;
                        for( int j= 0; j < appUpBoundary; j++ ){ srcm= movescopy[ j ];
                                srcr= ftT.multinv( srcm, currentdisp );
                            switch( ftT.ft_ftSub( srcm ) ){
                            case ftSub.face:
                                if( j > 0 && ftT.ft_ftSub( movescopy[ j-1 ] ) == ftSub.face 
                                        && ftT.ft_pm( movescopy[ j-1 ] ) == ftT.ft_pm( srcm ) ){
                                    consolidaterotation= ftT.ft_byte( movescopy[ j-1 ] );
                                    consolidaterotation+= ftT.ft_byte( srcm );
                                    moves.RemoveAt( moves.Count - 1 );
                                    if( consolidaterotation % 4 != 0 ){
                                        moves.Add( ftT.ftmult( ftT.ft_pm( srcm ), consolidaterotation ) ); }
                                } else { moves.Add( srcm ); }
                                break;
                            case ftSub.doubl:
                                moves.Add( srcm );
                                currentdisp= drT.mult3w( ftT.widen3( srcr ), currentdisp );
                                break;
                            case ftSub.whole:
                                moves.Add( srcm );
                                currentdisp= drT.mult3w( srcr, currentdisp );
                                break;
                            case ftSub.slice:
                                moves.Add( srcm );
                                currentdisp= drT.mult3w( ftT.wide_wide3part( srcr ), currentdisp );
                                break;
                            default: moves.Add( srcm );  break;
                            }
                        }
                        // add the elements to undo the cube rotations to this point
                        if( currentdisp != dr.c_UF_ ){ moves.Add( ft.posMark ); }
                        switch( drT.gx( drT.invert( currentdisp )) ){
                        case 1:  moves.Add( ft.x1 );  break;
                        case 2:  moves.Add( ft.x2 );  break;
                        case 3:  moves.Add( ft.x3 );  break;
                        }
                        switch( drT.gy( drT.invert( currentdisp )) ){
                        case 1:  moves.Add( ft.y1 );  break;
                        case 2:  moves.Add( ft.y2 );  break;
                        case 3:  moves.Add( ft.y3 );  break;
                        }
                        switch( drT.gz( drT.invert( currentdisp )) ){
                        case 1:  moves.Add( ft.z1 );  break;
                        case 2:  moves.Add( ft.z2 );  break;
                        case 3:  moves.Add( ft.z3 );  break;
                        }
                        displacetokenlinks( moves.Count - appUpBoundary, move_i, reading, ref numsreading );
                        for( int j= appUpBoundary; j < movescopy.Length; j++ ){ moves.Add( movescopy[ j ] ); }
                        #endregion
                        break;
                    case 8: // remove all cube positition markers "@"
                        #region remove all cube position markers "@"
                        moves.Clear();
                        for( int j= 0; j < appUpBoundary; j++ ){ srcm= movescopy[ j ];
                            if( srcm != ft.posMark ){ moves.Add( srcm ); }
                        }
                        displacetokenlinks( moves.Count - appUpBoundary, move_i, reading, ref numsreading );
                        for( int j= appUpBoundary; j < movescopy.Length; j++ ){ moves.Add( movescopy[ j ] ); }
                        #endregion
                        break;
                    case 9: // remove all after final position marker, usually to undo a partial solution search
                        #region remove all after final position marker, usually to undo a partial solution search
                        for( int j= appUpBoundary-1; j >= 0; j-- ){
                            if( movescopy[ j ] == ft.posMark && j > 0 ){
                                moves.Clear();
                                for( int k= 0; k < j; k++ ){
                                    moves.Add( movescopy[ k ] );
                                }
                                break;
                            }
                        }
                        displacetokenlinks( moves.Count - appUpBoundary, move_i, reading, ref numsreading );
                        for( int j= appUpBoundary; j < movescopy.Length; j++ ){ moves.Add( movescopy[ j ] ); }
                        #endregion
                        break;
                    case 10: // command k10 - conjugate from scramble to read orientation
                        #region conjugate from scramble to read orientation
                        dr sroemploy= di.sro;
                        int ipoint= 0;  int inserts= 0;  int insertionpoint= 0;
                        if( !di.srosetnull && sroemploy == dr.c_UF_ ){ sroemploy= singmaster.sro; }

                        if( sroemploy != dr.c_UF_ ){ switch( drT.gx( sroemploy ) ){ // give the optimal generator in terms of xyz in that order
                            case 0: break;
                            case 1: moves.Insert( appUpBoundary, ft.x1 );  ipoint+= 1;  break;
                            case 2: moves.Insert( appUpBoundary, ft.x2 );  ipoint+= 1;  break;
                            case 3: moves.Insert( appUpBoundary, ft.x3 );  ipoint+= 1;  break;
                        } switch( drT.gy( sroemploy ) ){
                            case 0: break;
                            case 1: moves.Insert( appUpBoundary + ipoint, ft.y1 );  ipoint+= 1;  break;
                            case 2: moves.Insert( appUpBoundary + ipoint, ft.y2 );  ipoint+= 1;  break;
                            case 3: moves.Insert( appUpBoundary + ipoint, ft.y3 );  ipoint+= 1;  break;
                        } switch( drT.gz( sroemploy ) ){
                            case 0: break;
                            case 1: moves.Insert( appUpBoundary + ipoint, ft.z1 );  break;
                            case 2: moves.Insert( appUpBoundary + ipoint, ft.z2 );  break;
                            case 3: moves.Insert( appUpBoundary + ipoint, ft.z3 );  break;
                        }
                        // search for a k0, in which case insert immediately after that, otherwise insert at 0
                        for( int jb= appUpBoundary-1; jb > 0; jb-- ){
                            if( moves[ jb ] == ft.k0m ){ insertionpoint= jb; break; }
                        }
                        switch( drT.gx( drT.invert( sroemploy ) ) ){ // also at the beginning of the routine
                            case 0: break;
                            case 1: moves.Insert( insertionpoint, ft.x1 );  insertionpoint+= 1;  inserts+= 1;  break;
                            case 2: moves.Insert( insertionpoint, ft.x2 );  insertionpoint+= 1;  inserts+= 1;  break;
                            case 3: moves.Insert( insertionpoint, ft.x3 );  insertionpoint+= 1;  inserts+= 1;  break;
                        }
                        switch( drT.gy( drT.invert( sroemploy ) ) ){
                            case 0: break;
                            case 1: moves.Insert( insertionpoint, ft.y1 );  insertionpoint+= 1;  inserts+= 1;  break;
                            case 2: moves.Insert( insertionpoint, ft.y2 );  insertionpoint+= 1;  inserts+= 1;  break;
                            case 3: moves.Insert( insertionpoint, ft.y3 );  insertionpoint+= 1;  inserts+= 1;  break;
                        }
                        switch( drT.gz( drT.invert( sroemploy ) ) ){
                            case 0: break;
                            case 1: moves.Insert( insertionpoint, ft.z1 );  insertionpoint+= 1;  inserts+= 1;  break;
                            case 2: moves.Insert( insertionpoint, ft.z2 );  insertionpoint+= 1;  inserts+= 1;  break;
                            case 3: moves.Insert( insertionpoint, ft.z3 );  insertionpoint+= 1;  inserts+= 1;  break;
                        }
                        }
                        displacetokenlinks( inserts, insertionpoint, reading, ref numsreading );
                        displacetokenlinks( inserts, appUpBoundary, reading, ref numsreading );
                        for( int j= appUpBoundary + inserts + 1; j < movescopy.Length - inserts; j++ ){ moves.Add( movescopy[ j ] ); }
                        #endregion
                        break;
                    case 11:
                        di.displayfacechaseformat= true;
                        break;
                    case 20: // command k20 - fix read origin and buffer
                        singmaster.ee_e.origin= singmaster.ee_e.overrideorigin;
                        singmaster.cc_e.origin= singmaster.cc_e.overrideorigin;
                        singmaster.sro= di.sro;  singmaster.sro_UF= di.sro_UF;  di.srosetnull= false;
                        break;
                    case 30: // hold symmetry
                        holdsy= cursy;
                        break;
                    case 31: // invert held symmetry
                        holdsy= syT.finverse( holdsy );
                        break;
                    case 33: // multiply by held symmetry
                        moves= syT.mult( moves, holdsy );
                        cursy= syT.mult( cursy, holdsy );
                        break;
                    case 1000: // command k1000 -- test case report edges symmetry
                        System.Text.StringBuilder sb= new System.Text.StringBuilder();
                        for( byte ia= 0; ia < (byte)( ef.ef24 ); ia++ ){
                            sb.Append( singmaster.ee_e[ singmaster.ee_e[ ia ] ] );
                        }
                        sb.Append( "#" ) ;
                        for( byte ib= 0; ib < (byte)( cf.cf24 ); ib++ ){
                            sb.Append( singmaster.cc_e[ singmaster.cc_e[ ib ] ] );
                        }
                        di.suffixmsg= "// " + sb.ToString();
                        break;
                    case 1001: // command k1001 -- test case convert plane motion parse '[' 
                               // to move type '[' which is designed to be output from the
                               // from the general 3-cycle catalog
                        int added= 0;
                        for( int j= 0; j < reading.Count; j++ ){
                            if( pmT.pm_pmSect( reading[ j ] ) == pmSect.puncSym  ){
                                // reading[ j ]= pm.pm59; // this must be deferred, hence _removePuncSym
                                moves.Insert( numsreading[ j ] + added, (ftT)( reading[ j ] ) );
                                added+= 1;
                            }
                        }
                        //removePuncSym= true; // defer removing all plane motion parse '[' // issue: mixed cases not permitted
                        break;
                    default: break; // unrecognized command is simply a noop
                }
                #endregion
                    break;
                case pmSect.conjugate: // bug C forms in a [ ... ] bracket should be ignored as invalid
                    if( movecandidate != ft.none ){ moves.Add( movecandidate );  movecandidate= ft.none; }
                    List< int > subnic= new List< int >();
                    List< string > subsic= new List< string >();
                    subreading= parse1( si[ numsreading[ move_i ] ], out subnic, out subsic );
                    displayind dicon;
                    subreadparse= parse2( subreading, subnic, subsic, out dicon );

                    // search for a k0, in which case insert immediately after that, otherwise insert at 0
                    int ji= 0;
                    for( int jb= moves.Count-1; jb > 0; jb-- ){
                        if( moves[ jb ] == ft.k0m ){ ji= jb; break; }
                    }
                    foreach( ft fti in subreadparse ){ moves.Insert( ji, ftT.invert( fti ) ); }
                    displacetokenlinks( subreadparse.Count, ji, reading, ref numsreading );
                    if( move_i + 1 == numsreading.Count || numsreading[ move_i + 1 ] >= moves.Count ){
                        foreach( ft fti in subreadparse ){ moves.Add( fti ); }
                    }else{
                        foreach( ft fti in subreadparse ){ moves.Insert( numsreading[ move_i + 1 ], fti ); }
                        displacetokenlinks( subreadparse.Count, numsreading[ move_i + 1 ], reading, ref numsreading );
                    }
                    break;
                case pmSect.mem:
                    switch( pmr ){
                    case pm.hold:  case pm.holdq:
                    #region reading[ j ] == pm.hold( q )
                    List< ft > tomem= new List< ft >();
                    int memkey= numsreading[ move_i ];

                    switch( reading[ move_i ] ){
                    case pm.hold:
                        if( singmaster.sessionMemory.ContainsKey( (short)( numsreading[ move_i ] ) ) ){
                            singmaster.sessionMemory.Remove( (short)( numsreading[ move_i ] ) ); } // prepare to replace any previous memory saved to _memkey_

                        for( int memcount= 0; memcount < numsreading[ move_i + 1 ]; memcount++ ){ tomem.Add( moves[ memcount ] ); }
                        singmaster.sessionMemory.Add( (short)( numsreading[ move_i ] ), tomem );
                        break;
                    case pm.holdq:
                        if( singmaster.sessionNameMemory.ContainsKey( si[ numsreading[ move_i ] ] ) ){
                            singmaster.sessionNameMemory.Remove( si[ numsreading[ move_i ] ] ); }

                        for( int memcount= 0; memcount < numsreading[ move_i + 1 ]; memcount++ ){ tomem.Add( moves[ memcount ] ); }
                        singmaster.sessionNameMemory.Add( si[ numsreading[ move_i ] ], tomem );
                        break;
                    }
                    #endregion    
                    break;
                    }
                    break;
                case pmSect.puncSym:
                    if( pmr == pm.k0m ){
                        moves.RemoveRange( 0, numsreading[ move_i ] );
                        displacetokenlinks( -numsreading[ move_i ], move_i, reading, ref numsreading );
                    }
                    break;
                }
            }
        }

        // for inserting a reading into a formula via _paste list_, remove all comments // TODO resolve C forms should be disabled as invalid
        enum remcommentmode{ newline, initLparen, parencontentscopy, parencontentsmatch, copy }
        public static string remcomment( string input ){
            bool slashfound= false; // track for "//"
            System.Text.StringBuilder sb= new System.Text.StringBuilder();
            System.Text.StringBuilder parencontents= new System.Text.StringBuilder();
            remcommentmode mode= remcommentmode.newline;
            foreach( char ch in input ){
            switch( mode ){
            case remcommentmode.newline:
                if( char.IsWhiteSpace( ch ) ){ break; }
                if( ch == '(' ){ mode= remcommentmode.initLparen; }
                else { goto case remcommentmode.copy; }
                break;
            case remcommentmode.initLparen:
                if( ch == ')' ){ mode= remcommentmode.copy; }
                break;
            case remcommentmode.copy:
                if( ch == '/' ){ 
                    if( slashfound ){ sb.Remove( sb.Length-1, 1 );  return sb.ToString(); }
                    slashfound= true; }
                else { slashfound= false; }
                if( ch == '(' ){
                    parencontents.Remove( 0, parencontents.Length );
                    mode= remcommentmode.parencontentscopy;
                    goto case remcommentmode.parencontentscopy; }
                else { sb.Append( ch ); }
                break;
            case remcommentmode.parencontentscopy:
                if( ch == '/' ){
                    if( slashfound ){ sb.Remove( sb.Length - 1, 1 );  return sb.ToString() + parencontents.ToString(); }
                    slashfound= true; }
                else { slashfound= false; }
                parencontents.Append( ch );
                if( ch == ')' ){ mode= remcommentmode.parencontentsmatch;  break; }
                break;
            case remcommentmode.parencontentsmatch:
                if( ch == '/' ){
                    if( slashfound ){ sb.Remove( sb.Length - 1, 1 );  return sb.ToString(); }
                    slashfound= true; }
                else if( slashfound ){ 
                    sb.Append( parencontents.ToString() );
                    sb.Append( '/' );  slashfound= false;
                    mode= remcommentmode.copy;
                    goto case remcommentmode.copy;
                }
                if( char.IsWhiteSpace( ch ) || ch == '/' ){ break; }
                sb.Append( parencontents.ToString() );
                mode= remcommentmode.copy;
                goto case remcommentmode.copy;
            }}
            return sb.ToString();
        }

        public static List< ft > add( List< ft > given, params ft[] ftparams ){
            foreach( ft fti in ftparams ){ given.Add( fti ); }
            return given;
        }

        public static string ToString( List< ft > given ){
            System.Text.StringBuilder sb= new System.Text.StringBuilder();
            foreach( ft fti in given ){
                sb.Append( ((ftT)( fti )).ToString() );  sb.Append( ' ' );
            }
            return sb.ToString();
        }

        /// <summary>
        /// helper for parselist, after an insert, increase all indications greater than boundary to reflect insertions
        /// </summary>
        static void bump( int boundary, int amount, ref List< int[] > puncsymlocsinplace, ref List< int > backref,
                int[] closebracketsarray, List< ft > moves ){
            for( int j= 0; j < puncsymlocsinplace.Count; j++ ){
                for( byte i= 0; i <= 8; i++ ){ if( puncsymlocsinplace[ j ][ i ] >= boundary ){
                    puncsymlocsinplace[ j ][ i ]+= amount; }
            }}
            for( int j= 0; j < backref.Count; j++ ){
                if( backref[ j ] >= boundary && moves[ j ] != ft.ps1){ // do not change backref for ']' which refers to puncsymlocsinplace
                    backref[ j ]+= amount; }}
            for( int j= 0; j < closebracketsarray.Length; j++ ){
                if( closebracketsarray[ j ] >= boundary ){ closebracketsarray[ j ]+= amount; }}
        }

        /// <summary>
        /// for list< ft > generated from a catalog, resolve all move types referring to
        /// conjugates and commutators: '[', ',', and such within this -- this is separate and more limited
        /// than the '[', ',', and such from the plane of motions list expanded at parse3
        /// </summary>
        public static void parselist( ref List< ft > moves )
        { // test case // @ [ U : R ] @ [ L :  D  ]  @ k1001
          // moves        0 1 2 3 4 5 6 7 8 9 10 11 12
          // backref   // 0 0 2 1 4 1 6 1 8 7 10  7 12
          //               [8] [0] [7] // puncsymlocsinplace[0]
          //                        ^  // nextpos == 7 in puncsymlocsinplace[0]
          //                ^          // prevpos == -1 in puncsymlocsinplace[0]
          //                ^ // k == 0 in puncsymlocsinplace[0]
          // after ins // @ [ U : R U' ] @ [ L  :  D  ]  @ k1001
            // moves      0 1 2 3 4  > 5 6 7 8  9 10 11 12
            //            0 1 2 3 4  5 6 7 8 9 10 11 12 13
            // backref // 0 0 2 1 4  3 1 6 1 8  7 10  7 12 // before bump
            //            0 0 2 1 4  3 1 6 1 9  8 11  8 13 // after bump

          //                ^ // leftboundary == 1
          //                                      ^ // nextpos
          //                                ^ prevpos
          //                            [8][0]   [7] // puncsymlocsinplace[1]

// ----- second test case [ [ U : L ] : R ] k1001
          // moves        0 1 2 3 4 5 6 7 8
          // backref

            #region generate backref and puncsymlocsinplace
            List< int > backref= new List< int >( moves.Count ); // each move of type puncSym refers to its opeining bracket move position
            List< int > bracketstack= new List< int >(); // parsing _moves_: number of open '[' in effect at this time and position of each '['
            List< int > closebrackets= new List< int >() ; // location of each ']' in _moves_ that properly match its nearest previous '['
            int[] pslocsinitialized= new int[ 9 ]; // position [7] location of '[' and [8] location of ']'
            for( byte i= 0; i < 8; i++ ){ pslocsinitialized[ i ]= -1; }
            List< int[] > puncsymlocsinplace= new List< int[] >();
            byte pos= 255; // location within puncsymlocsinplace for registering ':', ',' and '::'
            List< byte > poshold= new List<byte>(); // account for a '[' within the confines of another '['
            const byte t3posL= 1;  const byte t3posR= 5;
            const byte t5pos= 3;  
            
            const byte rbrackpos= 7;
            int bracketstacktop= int.MinValue;
            for( int j= 0; j < moves.Count; j++ ){ 
                backref.Add( j ); // usual case refers to its location number within '
                if( ftT.ft_ftSub( moves[ j ] ) == ftSub.puncSym ){
            switch( moves[ j ] ){ case ft.ps1: // [
                backref[ j ]= puncsymlocsinplace.Count; // bracket encounter number
                puncsymlocsinplace.Add( (int[])( pslocsinitialized.Clone() ) );
                puncsymlocsinplace[ backref[ j ] ][ 8 ]= j;
                if( bracketstacktop > int.MinValue ){ bracketstack.Add( bracketstacktop ); }
                poshold.Add( pos );  pos= 255;
                bracketstacktop= j; break;
            case ft.ps2: // ] // closebracketcase
                backref[ j ]= bracketstacktop; // refrense '[' with location of matching ']'
                if( bracketstacktop > int.MinValue ){
                    closebrackets.Add( j );
                    puncsymlocsinplace[ backref[ bracketstacktop ] ][ rbrackpos ]= j;
                    if( poshold.Count > 0 ){ pos= poshold[ poshold.Count-1 ];  poshold.RemoveAt( poshold.Count-1 ); }
                    if( bracketstack.Count > 0 ){ bracketstacktop= bracketstack[ bracketstack.Count-1 ];
                        bracketstack.RemoveAt( bracketstack.Count-1 ); }
                    else { bracketstacktop= int.MinValue; }
                } // refer to opening bracket
                break;
            case ft.ps3: /* : */ if( bracketstacktop < 0 ){ break; } 
                if( pos == 255 ){ 
                    pos= 0;
                    puncsymlocsinplace[ backref[ bracketstacktop ] ][ pos ]= j;
                    backref[ j ]= bracketstacktop;
                }
                else if( pos % 2 == 1 ){ // ignore any extra appearance of ':' without first encountering ',' or '::'
                    pos+= 1;
                    puncsymlocsinplace[ backref[ bracketstacktop ] ][ pos ]= j;
                    backref[ j ]= bracketstacktop;
                }
                break;

                                      //  t1 t3   t4     t3         t5           t3     t4    t3  t2
                                      //  [ U : L  ,   R : F        ::         R : D    ,   B : U ] 
                                      //  8   0    1     2           3           4      5     6   7  

            case ft.ps4: /* , */ if( bracketstacktop < 0 ){ break; }
                switch( pos ){
                    case 255:  case 0: pos= 1;
                        puncsymlocsinplace[ backref[ bracketstacktop ] ][ pos ]= j;
                        backref[ j ]= bracketstacktop;
                        break;
                    case 1:  case 2:  case 5:  case 6:  break; // already ',' in this level
                    case 3:  case 4:
                        pos= 5;
                        puncsymlocsinplace[ backref[ bracketstacktop ] ][ 5 ]= j;
                        backref[ j ]= bracketstacktop;
                        break;
                } break;
            case ft.ps5: /* :: */
                if( pos == 255 || pos < t5pos ){
                    pos= t5pos;
                    puncsymlocsinplace[ backref[ bracketstacktop ] ][ pos ]= j;
                    backref[ j ]= bracketstacktop;
                }
                break;
            }}} // switch reading[ j ] // if ... == pmSecdt.token // for j .. reading.Count
            #endregion
            int[] closebracketsarray= closebrackets.ToArray();
            int leftboundary;
            sbyte prevpos= -2;  byte nextpos= 7; // indication in puncsymlocsinplace for previous symbol and next symbol
            for( int jp= 0; jp < closebracketsarray.Length; jp++ ){ // expand each [ ] indexing from each matching ']' encountered in order
                pslocsinitialized= puncsymlocsinplace[ backref[ backref[ closebracketsarray[ jp ] ] ] ];
                for( byte k= 0; k < 7; k+= 2 ){ // examine each ':' in place
                    if( pslocsinitialized[ k ] > -1 ){ // a ':' is in place
                        // insert previous to next internal symbol or ]
                        for( byte n= (byte)( k+1 ); n <= 7; n++ ){ if( n == 7 || pslocsinitialized[ n ] > -1 ){
                            nextpos= (byte)( n );   break; }}
                        for( sbyte n= (sbyte)( k-1 ); n >= -1; n-- ){ if( n == -1 || pslocsinitialized[ n ] > -1 ){
                            prevpos= n;  break; }}
                        leftboundary= prevpos >= 0 ? pslocsinitialized[ prevpos ] : pslocsinitialized[ 8 ];
                        for( int p= leftboundary + 1; p < pslocsinitialized[ k ]; p++ ){ // interpret ':' in context
                            moves.Insert( pslocsinitialized[ nextpos ], ftT.invert( moves[ p ] ) );
                            backref.Insert( pslocsinitialized[ nextpos ], pslocsinitialized[ k ] ); // inserted symbol refers to location of its generator
                        }
                        moves[ pslocsinitialized[ k ] ]= ft.none; // blank out ':' now that it is interpreted

                        bump( pslocsinitialized[ nextpos ], pslocsinitialized[ k ] - leftboundary - 1,
                            ref puncsymlocsinplace, ref backref, closebracketsarray, moves );
                    }
                }
                if( pslocsinitialized[ t3posL ] > -1 ){ // a ',' in place before '::'
                    leftboundary= pslocsinitialized[ 8 ];
                    nextpos= t5pos;
                    if( pslocsinitialized[ nextpos ] == -1 ){ nextpos= 7; }
                    for( int p= pslocsinitialized[ t3posL ] + 1; p < pslocsinitialized[ nextpos ]; p++ ){
                        moves.Insert( pslocsinitialized[ nextpos ], ftT.invert( moves[ p ] ) );
                        backref.Insert( pslocsinitialized[ nextpos ], pslocsinitialized[ t3posL ] );
                    } // insert the B' section
                    for( int p= leftboundary + 1; p < pslocsinitialized[ t3posL ]; p++ ){ // interpret ',' in context -- insert the A' section
                        moves.Insert( pslocsinitialized[ nextpos ], ftT.invert( moves[ p ] ) );
                        backref.Insert( pslocsinitialized[ nextpos ], pslocsinitialized[ t3posL ] ); // inserted symbol refers to location of its generator
                    } // [ A , B ] // == == // [ A B A' B' ] // insert the A' section
                    moves[ pslocsinitialized[ t3posL ] ]= ft.none; // blank out ',' now that it is interpreted

                    bump( pslocsinitialized[ nextpos ], pslocsinitialized[ nextpos ] - leftboundary - 2,
                        ref puncsymlocsinplace, ref backref, closebracketsarray, moves );
                }
                if( pslocsinitialized[ t3posR ] > -1 ){ // a ',' in place after '::'
                    leftboundary= pslocsinitialized[ t5pos ];
                    if( leftboundary == -1 ){ leftboundary= pslocsinitialized[ 8 ]; }
                    nextpos= 7;
                    for( int p= pslocsinitialized[ t3posR ] + 1; p < pslocsinitialized[ nextpos ]; p++ ){
                        moves.Insert( pslocsinitialized[ nextpos ], ftT.invert( moves[ p ] ) );
                        backref.Insert( pslocsinitialized[ nextpos ], pslocsinitialized[ t3posR ] );
                    } // insert the B' section
                    for( int p= leftboundary + 1; p < pslocsinitialized[ t3posR ]; p++ ){ // interpret ',' in context -- insert the A' section
                        moves.Insert( pslocsinitialized[ nextpos ], ftT.invert( moves[ p ] ) );
                        backref.Insert( pslocsinitialized[ nextpos ], pslocsinitialized[ t3posR ] ); // inserted symbol refers to location of its generator
                    } // [ A , B ] // == == // [ A B A' B' ] // insert the A' section
                    moves[ pslocsinitialized[ t3posR ] ]= ft.none; // blank out ',' now that it is interpreted

                    bump( pslocsinitialized[ nextpos ], pslocsinitialized[ nextpos ] - leftboundary - 2,
                        ref puncsymlocsinplace, ref backref, closebracketsarray, moves );
                }
                if( pslocsinitialized[ t5pos ] > -1 ){ // a '::' in place
                    leftboundary= pslocsinitialized[ 8 ];
                    nextpos= 7;

                    for( int p= leftboundary + 1; p < pslocsinitialized[ t5pos ]; p++ ){ // interpret ',' in context -- insert the A' section
                        moves.Insert( pslocsinitialized[ nextpos ], ftT.invert( moves[ p ] ) );
                        backref.Insert( pslocsinitialized[ nextpos ], pslocsinitialized[ t5pos ] ); // inserted symbol refers to location of its generator
                    } // [ A , B ] // == == // [ A B A' B' ] // insert the A' section
                    moves[ pslocsinitialized[ t5pos ] ]= ft.none; // blank out ',' now that it is interpreted

                    bump( pslocsinitialized[ nextpos ], pslocsinitialized[ t5pos ] - leftboundary - 1,
                        ref puncsymlocsinplace, ref backref, closebracketsarray, moves );
                }
                moves[ pslocsinitialized[ 8 ] ]= ft.none;
                moves[ pslocsinitialized[ 7 ] ]= ft.none; // blank out [ and ]
            } // expand each [ ]
        } // parselist

        #region parsefacechase
        // read in face origin position, and face destination position
            // _B 2 _V 4 _F 2 _N|_E _A _B _F 2 _N|_E _A _D 3 _H // _B and _V are face indicators
        internal enum fctype{ space, face, transition, focuschange };
        internal struct fctoken{
            internal short start; // location of first character in token
            internal short length; // length of token // was byte, but cannot imagine length exceeding 128
            internal fctype type; // token type
            internal string sub( string given ){ // return the indicated substring given this record
                return given.Substring( start, length ); }
        }
        enum fcparsingstate{ space, alpha, numeric, punctuation }; // tokenizing parsing state

        struct fcmove{
            internal short from; // reading token position of face indication from <>origin
            internal short to; // reading token position of face indication to <>destination
            internal short transition; // reading position of transition indicator
        }

        static bool isalpha( char ch ){ return ch >= 'A' && ch <= 'Z'; }
        struct fcinterpret{
            const short bshort= short.MinValue;
            internal short pos;
            internal bool edgetype; // true ~ edge trace; false ~ corner trace
            internal ef efrom;
            internal cf cfrom;
            internal ef eto;
            internal cf cto;
            internal ft move;
            internal void init(){
                pos= bshort;
                edgetype= false;
                efrom= ef.ef24;
                cfrom= cf.cf24;
                eto= ef.ef24;
                cto= cf.cf24;
                move= ft.none;
            }
            internal void parsefrom( string from ){
                if( from[0] == '_' ){
                    efrom= ef.ef24;
                    edgetype= false;
                    if( from.Length > 1 && singmaster.cc_e.ContainsKey( from[1] ) ){ // isalpha( from[1] )){
                        cfrom= singmaster.cc_e[ from[1] ];
                    } else { cfrom= cf.cf24; }
                } else if ( from.Length < 2 ){ cfrom= cf.cf24;
                } else {
                    edgetype= true;
                    if( singmaster.ee_e.ContainsKey( from[0] ) ){ // isalpha( from[0] )){
                        efrom= singmaster.ee_e[ from[0] ];
                        if( from[1] == '_' ){ cfrom= cf.cf24; }
                        else { cfrom= singmaster.cc_e[ from[1] ]; }
                    } else { cfrom= cf.cf24; }
                }
            }
            internal void parseto( string to ){
                if( to[0] == '_' ){
                    eto= ef.ef24;
                    if( to.Length > 1 && singmaster.cc_e.ContainsKey( to[ 1 ] ) ){ // isalpha( to[1] )){
                        cto= singmaster.cc_e[ to[1] ];
                    }
                } else if( singmaster.ee_e.ContainsKey( to[ 0 ] ) ){ // isalpha( to[0] )){
                    eto= singmaster.ee_e[ to[0] ];
                    if( to.Length < 2 || to[1] == '_' ){ cto= cf.cf24; }
                    else { cto= singmaster.cc_e[ to[1] ]; }
                } else { cto= cf.cf24; }

                // if edges are equivalent or any edge a null edge, revert to corner type move
                if (edgetype && (efrom == eto || efrom == ef.ef24 || eto == ef.ef24)) { edgetype= false; }
            }
            public override string ToString(){ // debug
                if ( edgetype ) { return efrom.ToString() + "->" + eto.ToString(); }
                else { return cfrom.ToString() + "->" + cto.ToString(); }
            }
            public override bool Equals( object obj ) {
                if (obj is fcinterpret) { return (fcinterpret)(obj) == this; }
                return false;
            }
            public override int GetHashCode() {
                if ( edgetype ) { return (byte)(efrom) << 8 | (byte)(eto); }
                else { return (byte)(efrom) << 8 | (byte)(eto); } }
            public static bool operator ==( fcinterpret left, fcinterpret right ) {
                if ( left.edgetype != right.edgetype ){ return false; }
                if ( left.edgetype ) { return left.efrom == right.efrom; }
                else { return left.cfrom == right.cfrom; }
            }
            public static bool operator !=( fcinterpret left, fcinterpret right ) {
                if ( left.edgetype != right.edgetype ){ return true; }
                if ( left.edgetype ) { return left.efrom != right.efrom; }
                else { return left.cfrom != right.cfrom; }
            }
        } // fcinterpret

        #region slice plane detection
        enum sp{ e, m, s, sp3 };
        static ef[] eeslice= new ef[8] { ef.lf_FL, ef.pj_RF, ef.tn_BR, ef.hr_LB,
                                       ef.fl_LF, ef.jp_FR, ef.nt_RB, ef.rh_BL }; // fix
        static ef[] emslice= new ef[8] { ef.aq_UB, ef.ic_FU, ef.uk_DF, ef.sw_BD,
                                       ef.qa_BU, ef.ci_UF, ef.ku_FD, ef.ws_DB };
        static ef[] esslice= new ef[8] { ef.de_UL, ef.mb_RU, ef.vo_DR, ef.gx_LD,
                                       ef.ed_LU, ef.bm_UR, ef.ov_RD, ef.xg_DL };
        static sp[] esliceplane= genesliceplane();
        static sp[] genesliceplane(){
            sp[] gen= new sp[ (byte)( ef.ef24 ) ];
            for( byte i= 0; i < 8; i++ ){ 
                gen[ (byte)( eeslice[ i ] ) ]= sp.e;  gen[ (byte)( emslice[ i ] ) ]= sp.m;  gen[ (byte)( esslice[ i ] ) ]= sp.s;  
            }
            return gen;
        }
        static byte[] eslicepos= geneslicepos();
        static byte[] geneslicepos(){
            byte[] gen= new byte[ (byte)( ef.ef24 ) ];
            for( byte i= 0; i < 8; i++ ){ 
                gen[ (byte)( emslice[ i ] ) ]= i;  gen[ (byte)( esslice[ i ] ) ]= i;  gen[ (byte)( eeslice[ i ] ) ]= i;
            }
            return gen;
        }
        static byte[] esliceorbit= genesliceorbit();
        static byte[] genesliceorbit(){
            byte[] gen= new byte[ (byte)( ef.ef24 ) ];
            for( byte i= 0; i < 4; i++ ){ 
                gen[ (byte)( emslice[ i ] ) ]= 0;  gen[ (byte)( esslice[ i ] ) ]= 0;  gen[ (byte)( eeslice[ i ] ) ]= 0;
            }
            for( byte i= 4; i < 8; i++ ){
                gen[ (byte)( emslice[ i ] ) ]= 1;  gen[ (byte)( esslice[ i ] ) ]= 1;  gen[ (byte)( eeslice[ i ] ) ]= 1;
            }
            return gen;
        }


        const byte smr= 9; // slice moves reach; avoid having to apply modulus function by extending and repeating array elements a little.
        static ft[] mmoves= new ft[smr] { ft.none, ft.m1, ft.m2, ft.m3, ft.none, ft.m1, ft.m2, ft.m3, ft.none };
        static ft[] smoves= new ft[smr] { ft.none, ft.s1, ft.s2, ft.s3, ft.none, ft.s1, ft.s2, ft.s3, ft.none };
        static ft[] emoves= new ft[smr] { ft.none, ft.e1, ft.e2, ft.e3, ft.none, ft.e1, ft.e2, ft.e3, ft.none };
        static ft[,] slicemoves= slicemovesini();
        static ft[,] slicemovesini(){
            ft[,] fr= new ft[ (byte)( sp.sp3 ), smr ]; // _f_ type moves generated for _r_eturn
            for( byte bi= 0; bi < smr; bi++ ){ fr[ (byte)( sp.m ), bi ]= mmoves[ bi ]; }
            for( byte bi= 0; bi < smr; bi++ ){ fr[ (byte)( sp.e ), bi ]= emoves[ bi ]; }
            for( byte bi= 0; bi < smr; bi++ ){ fr[ (byte)( sp.s ), bi ]= smoves[ bi ]; }
            return fr;
        }
        #endregion

        //internal byte posIndic; // position to indicate
        const short shortnil= short.MinValue; // FIX this was byte 255, which led to spirious errors
        // face chase detection 
        internal static cf[ , ] cchase= cchaseini();
        internal static ef[ , ] echase= echaseini();
        static cf[ , ] cchaseini(){
            cf[ , ] cch= new cf[ (byte)( ft.ft74 ), (byte)( cf.cf24 ) ];
            //none, ut1,  ut2,  ut3,  f1,  f2,  f3,  r1,  r2,  r3,  bp1,  bp2,  bp3,  L1,  L2,  L3,  d1,  d2,  d3,
            //                 utw1, utw2, utw3, fw1, fw2, fw3, rw1, rw2, rw3, bpw1, bpw2, bpw3, Lw1, Lw2, Lw3, dw1, dw2, dw3,
            //x1, x2, x3, y1, y2, y3, z1, z2, z3, m1, m2, m3, e1, e2, e3, s1, s2, s3, f54
            for( cf ci= ssT.cf0; ci < cf.cf24; ci++ ){ cch[ (byte)(ft.none), (byte)(ci) ]= cf.cf24; }
            for( ft fti= ft.ut1; fti <= ft.d3; fti++ ){
                for( cf ci= ssT.cf0; ci < cf.cf24; ci++ ){ cch[ (byte)( ftT.invert( fti )), (byte)(ci) ]= ssT.cmove[ (byte)( fti ), (byte)( ci ) ]; }
            }
            return cch;
        } // with this array, find the move that generates the indicated edge or corner cubie change
        static ef[ , ] echaseini(){
            ef[ , ] ech= new ef[ (byte)( ft.ft74 ), (byte)( ef.ef24 ) ];
            for( ef ei= ssT.ef0; ei < ef.ef24; ei++ ){ ech[ (byte)( ft.none ), (byte)( ei ) ]= ef.ef24; }
            for( ft fti= ft.ut1; fti <= ft.d3; fti++ ){
                for( ef ei= ssT.ef0; ei < ef.ef24; ei++ ){ ech[ (byte)( ftT.invert( fti )), (byte)( ei ) ]= ssT.emove[ (byte)( fti ), (byte)( ei ) ]; }
            }
            return ech;
        }
//#if test
        internal static string debugtreading( List< fctoken > treadingi, string s ){
            System.Text.StringBuilder sb= new System.Text.StringBuilder();
            byte b= 0;
            foreach( fctoken fci in treadingi ){
                sb.Append( "[" );  sb.Append( b.ToString() );  sb.Append( "]: " ) ;
                b++;
                sb.Append( fci.type.ToString() );  sb.Append( " " );
                sb.Append( fci.sub( s ) );
                sb.Append( " ;  " );
            }
            return sb.ToString();
        }
//#endif
        static string[] cornerfacechase_transition= new string[ (byte)( ft.ft74 ) + 1 ]{ // face chase format transition names
            "0", "",  "2",  "",  "",  "2",  "",  "",  "2",  "",  "",  "2",  "",  "",  "2",  "",  "",  "2",  "",
            "3", "32", "3", "3", "32", "3", "3", "32", "3", "3", "32", "3", "3", "32", "3", "3", "32", "3",
            "5", "52", "5", "5", "52", "5", "5", "52", "5", "4", "42", "4", "4", "42", "4", "4", "42", "4",
            "T0", "T1", "T2", "T4", "T42", "T4", "T4", "T42", "T4", "T4", "T42", "T4", ".", ".", ".", ".", ".", 
            "k0", "@", "`" };
        public static string toCornerfacetransition( ft f_subject ) { return cornerfacechase_transition[ (byte)( f_subject ) ]; }

        internal static string parseFaceChase( string s, out List< ft > fs ){ // determine the moves given a list of face indicators
            fctoken tcurrent= new fctoken();
            List< fctoken > treading= new List< fctoken >();
            fcparsingstate fcps= fcparsingstate.space;  // tokenize parsing state
            char ch;

            short si= 0;
            short sd= (short)( s.Length );

        #region tokenize
            for ( short sj= si; sj < sd; sj++ ){ ch= s[sj];  // tokenize input string s
            switch( fcps ){
            case fcparsingstate.space:
                if (ch <= ' ') { break; } // already in space form -- looking for next token
                if (singmaster.ee_e.ContainsKey( ch ) || ch == '_' ) { 
                    tcurrent.start= sj;
                    tcurrent.length= 1;
                    tcurrent.type= fctype.face;
                    fcps= fcparsingstate.alpha;
                } else if (ch >= '0' && ch <= '9' ) {
                    tcurrent.start= sj;
                    tcurrent.length= 1;
                    tcurrent.type= fctype.transition;
                    fcps= fcparsingstate.numeric;
                } else if (ch == '|' || ch == '\u00A1' ) { // \uA1 == inverted explnation mark
                    tcurrent.start= sj;
                    tcurrent.length= 1;
                    tcurrent.type= fctype.focuschange;
                    fcps= fcparsingstate.punctuation;
                }
                break;
            case fcparsingstate.alpha:
                if (ch <= ' ' ){ treading.Add( tcurrent );
                    fcps= fcparsingstate.space;
                } else if ( singmaster.ee_e.ContainsKey( ch ) || ch == '_' ){
                    tcurrent.length+= 1;
                } else if ( ch == '|' || ch == '\u00A1' ){ // \uA1 inverted explnation mark
                    treading.Add( tcurrent );
                    tcurrent.start= sj;
                    tcurrent.length= 1;
                    tcurrent.type= fctype.focuschange;
                    fcps= fcparsingstate.punctuation;
                } else if ( ch >= '0' && ch <= '9' ){
                    treading.Add( tcurrent );
                    tcurrent.start= sj;
                    tcurrent.length= 1;
                    tcurrent.type= fctype.transition;
                    fcps= fcparsingstate.numeric;
                }
                break;
            case fcparsingstate.numeric:
                if (ch <= ' ' ){ treading.Add( tcurrent );
                    fcps= fcparsingstate.space;
                } else if ( singmaster.ee_e.ContainsKey( ch ) || ch == '_' ){
                    treading.Add( tcurrent );
                    tcurrent.start= sj;
                    tcurrent.length= 1;
                    tcurrent.type= fctype.face;
                    fcps= fcparsingstate.alpha;
                } else if ( ch == '|' || ch == '\u00A1' ){ // \uA1 inverted explnation mark
                    treading.Add( tcurrent );
                    tcurrent.start= sj;
                    tcurrent.length= 1;
                    tcurrent.type= fctype.transition;
                    fcps= fcparsingstate.punctuation;
                } else if ( ch >= '0' && ch <= '9' ){
                    tcurrent.length+= 1;
                }
                break;
            case fcparsingstate.punctuation:
                if (ch <= ' '){ treading.Add( tcurrent );
                    fcps= fcparsingstate.space;
                } else if ( singmaster.ee_e.ContainsKey( ch ) || ch == '_' ){
                    treading.Add( tcurrent );
                    tcurrent.start= sj;
                    tcurrent.length= 1;
                    tcurrent.type= fctype.face;
                    fcps= fcparsingstate.alpha;
                } else if ( ch >= '0' && ch <= '9' ){
                    treading.Add( tcurrent );
                    tcurrent.start= sj;
                    tcurrent.length= 1;
                    tcurrent.type= fctype.transition;
                    fcps= fcparsingstate.numeric;
                } else {
                    tcurrent.length+= 1;
                }
                break;
            } // switch fcp
            } // for -- each char sj
            if( fcps != fcparsingstate.space ){ treading.Add( tcurrent ); }
            #endregion

            fcmove fccurrent= new fcmove();
            fccurrent.from= shortnil;
            fccurrent.to= shortnil;
            fccurrent.transition= shortnil;
            
            List< fcmove > fcreading= new List<fcmove>();
        #region pull transitions
            for( short ri= 0; ri < treading.Count; ri++ ){
            switch( treading[ri].type ){
            case fctype.face:
                if( fccurrent.from == shortnil ){ fccurrent.from= ri; }
                else if( fccurrent.to == shortnil ){ 
                    fccurrent.to= ri;
                    fcreading.Add( fccurrent );
                    fccurrent.from= fccurrent.to;
                    fccurrent.to= shortnil;
                    fccurrent.transition= shortnil;
                } break;
            case fctype.transition:
                if( fccurrent.transition == shortnil ){ fccurrent.transition= ri; }
                break;
            case fctype.focuschange:
                fccurrent.from= shortnil;
                fccurrent.transition= shortnil;
                fccurrent.to= shortnil;
                break;
            }
            }
            if( fccurrent.to != shortnil ){ 
                fcreading.Add( fccurrent ); }

            #endregion

            List< fcinterpret > interpret= new List<fcinterpret>( fcreading.Count );
            fcinterpret fci= new fcinterpret();
            fci.init();

            fcmove fcreadmove;

        #region generate interpretation from transitions
            for ( short bi= 0; bi < fcreading.Count; bi++ ){
                fcreadmove= fcreading[bi];
                fci.pos= bi;
                fci.parsefrom( treading[ fcreadmove.from ].sub(s) ); // old form specified form letter system
                fci.parseto( treading[ fcreadmove.to ].sub(s) ); // old version specified form letter system
                sp sliceplanefrom;  byte sliceplanefromnum;
                sp sliceplaneto;  byte sliceplanetonum;
                byte sliceorbitfrom;  byte sliceorbitto;

                if( fci.edgetype ){ // search edge type for face transition match
                if( fcreadmove.transition != shortnil && treading[ fcreadmove.transition ].sub(s).Contains( "5" ) ) { // slice move
                    sliceplanefrom= esliceplane[ (byte)( fci.efrom ) ]; // edge type indication required for slice
                    sliceplaneto= esliceplane[ (byte)( fci.eto ) ];
                    sliceplanefromnum= eslicepos[ (byte)( fci.efrom ) ];
                    sliceplanetonum= eslicepos[ (byte)( fci.eto ) ];
                    sliceorbitfrom= esliceorbit[ (byte)( fci.efrom ) ];
                    sliceorbitto= esliceorbit[ (byte)( fci.eto ) ];
                    if( sliceplanefrom == sliceplaneto && sliceorbitfrom == sliceorbitto ){ // there is some hope of a proper slice move
                        fci.move= slicemoves[ (byte)( sliceplaneto ), 4 + sliceplanetonum - sliceplanefromnum ];
                        goto processtransitioninterpretations;
                    }
                } else { for( ft f1= ft.ut1; f1 < ft.utw1; f1++ ){ // search edge type for face transition move
                    if ( echase[ (byte)( f1 ), (byte)( fci.efrom ) ] == fci.eto ){ // found face move
                        fci.move= f1;  goto processtransitioninterpretations;
                    }} // no face turn generates, however a whole-cube rotation may key off of a slice move
                    if( fcreadmove.transition != shortnil && treading[ fcreadmove.transition ].sub(s).Contains( "4" ) ) {
                        sliceplanefrom= esliceplane[ (byte)( fci.efrom ) ]; // edge type indication required for slice
                        sliceplaneto= esliceplane[ (byte)( fci.eto ) ];
                        sliceplanefromnum= eslicepos[ (byte)( fci.efrom ) ];
                        sliceplanetonum= eslicepos[ (byte)( fci.eto ) ];
                        if( sliceplanefrom == sliceplaneto ){ // there is some hope of a proper slice move
                            fci.move= slicemoves[ (byte)( sliceplaneto ), 4 + sliceplanetonum - sliceplanefromnum ];
                            goto processtransitioninterpretations;
                    }}}
                    if( fci.cfrom < cf.cf24 ){ // guard against having no corner move placed
                    for( ft f1= ft.ut1; f1 < ft.utw1; f1++ ){ // no edge move found - search in corner moves
                    if ( cchase[ (byte)( f1 ), (byte)( fci.cfrom ) ] == fci.cto ){ // found face move
                        fci.move= f1;  goto processtransitioninterpretations;
                }}}}
                else{ // fci.cornertype // search corner type for face transition match
                    if( fci.cfrom < cf.cf24 && fcreadmove.transition != shortnil ){ // guard against undeciphered cfrom
                        if( treading[ fcreadmove.transition ].sub(s).Contains( "5" ) ){ // corner type to edge type face chase
                            fci.efrom= singmaster.ee_e[ singmaster.cc_e[ fci.cfrom ] ];
                            fci.eto= singmaster.ee_e[ singmaster.cc_e[ fci.cto ] ]; // get edge with similar name

                            sliceplanefrom= esliceplane[ (byte)( fci.efrom ) ]; // edge type indication required for slice
                            sliceplaneto= esliceplane[ (byte)( fci.eto ) ];
                            sliceplanefromnum= eslicepos[ (byte)( fci.efrom ) ];
                            sliceplanetonum= eslicepos[ (byte)( fci.eto ) ];
                            sliceorbitfrom= esliceorbit[ (byte)( fci.efrom ) ];
                            sliceorbitto= esliceorbit[ (byte)( fci.eto ) ];

                            // bug sliceplane may be of a different orbit.
                            
                            if (sliceplanefrom == sliceplaneto && sliceorbitfrom == sliceorbitto ) { // there is some hope of a proper slice move
                                fci.move= slicemoves[ (byte)( sliceplaneto ), 4 + sliceplanetonum - sliceplanefromnum ];
                                fci.efrom= ef.ef24; // added to cancel effect of adding edge similar interpreataion
                                fci.eto= ef.ef24;
                                goto processtransitioninterpretations;
                            } else {
                                fci.efrom= ef.ef24; // impossible slice, remove similar name edge attempt to detect
                                fci.eto= ef.ef24;

                                if( fci.cfrom < cf.cf24 ){
                                for ( ft f1= ft.ut1; f1 < ft.utw1; f1++ ){
                                if ( cchase[ (byte)( f1 ), (byte)( fci.cfrom ) ] == fci.cto ){ // found face move
                                    fci.move= f1;  goto processtransitioninterpretations;
                                }}}
                        }} else {
                            if( fci.cfrom < cf.cf24 ){
                            for ( ft f1= ft.ut1; f1 < ft.utw1; f1++ ){
                            if ( cchase[ (byte)( f1 ), (byte)( fci.cfrom ) ] == fci.cto ){ // found face move
                                fci.move= f1;  goto processtransitioninterpretations;
                            }}}}
                    } else { // transition does not contain "5"
                    if( fci.cfrom < cf.cf24 ){
                    for ( ft f1= ft.ut1; f1 < ft.utw1; f1++ ){
                    if ( cchase[ (byte)( f1 ), (byte)( fci.cfrom ) ] == fci.cto ){ // found face move
                        fci.move= f1;  goto processtransitioninterpretations;
                    }}}
                }}

            // process transition interpretretation // perhaps todo makea fsw array converting slice moves from fci.move
                processtransitioninterpretations: if( fcreadmove.transition == shortnil ){ // pass
                } else if( treading[ fcreadmove.transition ].sub(s).Contains( "3" ) ){ // convert to wide move
                    fci.move= ftT.widen( fci.move );
                } else if( treading[ fcreadmove.transition ].sub(s).Contains( "4" ) ){ // convert to whole cube move
                    fci.move= ftT.widen3( fci.move );
                }
                interpret.Add( fci ); // if no face transition found, this will be the null move
            }
            #endregion

            // reprint face chase according to transitions
            ef edgefocus= ef.ef24;
            cf cornerfocus= cf.cf24;
            string tch; // transition
            System.Text.StringBuilder sb= new System.Text.StringBuilder();
            if ( interpret.Count > 0 ){ 
                edgefocus= interpret[0].efrom;
                cornerfocus= interpret[0].cfrom;
                sb.Append( singmaster.ee_e[ edgefocus ] );
                sb.Append( singmaster.cc_e[ cornerfocus ] );
            }
            for( short fi= 0; fi < interpret.Count; fi++ ){
                fci= interpret[ fi ];
                if( edgefocus != ef.ef24 && fci.efrom != edgefocus 
                        || cornerfocus != cf.cf24 && fci.cfrom != cornerfocus ){
                    
                    if( fi > 0 && interpret[ fi-1 ] == fci ){
                        sb.Append( '\u00A1' ); } else { sb.Append( '|' ); } // \uA1 inverted explnation mark
                    
                    sb.Append( singmaster.ee_e[ fci.efrom ] );
                    sb.Append( singmaster.cc_e[ fci.cfrom ] );
                }
                sb.Append( ' ' );

                tch= toCornerfacetransition( fci.move );
                if( tch != "" ){ sb.Append( tch );  sb.Append( ' ' ); }
                edgefocus= fci.eto;
                cornerfocus= fci.cto;
                sb.Append( singmaster.ee_e[ edgefocus ] );
                sb.Append( singmaster.cc_e[ cornerfocus ] );
            }

            fs= new List< ft >();
            // accumulate moves according to reading
            for( short fi= 0; fi < interpret.Count; fi++ ){
                if( interpret[ fi ].move != ft.none ){ fs.Add( interpret[ fi ].move ); }
            }
            return sb.ToString();
        }
        //#endif
        #endregion

        internal static short facereach( ft[] subj, cf from, short position, short count, out cf to ){
            short reachcount= 0;
            if( from == cf.cf24 ){ to= from;  return 0; }
            cf frompos= from;
            short runposition= position;
            cf topos= from;  ef topos_e;
            while( runposition < count ){
                if( ftT.ft_ftSub( subj[ runposition ] ) == ftSub.posmark ){ 
                    to= frompos;  return reachcount;
                } else if( ftT.ft_ftSub( subj[ runposition ] ) == ftSub.slice ){
                    topos_e= ssT.move( subj[ runposition ], singmaster.ee_e[ singmaster.cc_e[ frompos ] ] );
                    topos= singmaster.cc_e[ singmaster.ee_e[ topos_e ] ];
                } else {
                    topos= ssT.move( subj[ runposition ], frompos );
                }
                if( frompos == topos ){ to= topos;   return reachcount; }
                frompos= topos;
                runposition+= 1;
                reachcount+= 1;
            }
            to= topos;
            return reachcount;
        }

        internal static cf[] chaseorder;
        internal static cf[] slicechaseorder;
        internal static void inifacechase() {
            chaseorder= new cf[]{ cf.aer_ULB, cf.dif_UFL, cf.cmj_URF, cf.bqn_UBR,
                cf.lglu_FDL, cf.kpv_FRD, cf.otw_RBD, cf.shxs_BLD };

            slicechaseorder= new cf[]{ cf.raer_BUL, cf.eaer_LBU, cf.idif_FLU,
                cf.fdif_LUF, cf.jcmj_FUR, cf.mcmj_RFU, cf.nbqn_RUB, cf.qbqn_BRU,
                cf.glu_LFD, cf.uglu_DLF, cf.pkpv_RDF, cf.vkpv_DFR, cf.totw_BDR,
                cf.wotw_DRB, cf.hxs_LDB, cf.xhxs_DBL };
        }

        static cf bestfacereach( ft[] subj, short position, short count, out cf to, out short topos ){
            short bestreachcount= 0;
            short reachreading= 0;
            cf bestreach= ssT.cf0;
            cf readto;
            cf bestto= bestreach;
            bool withslice= false;
            for( int positionplus= position; positionplus < subj.Length; positionplus++ ){
                if( ftT.ft_ftSub( subj[ positionplus ] ) == ftSub.slice ){ withslice= true;  break; }
            }

            foreach( cf chasing in chaseorder ){
                reachreading= facereach( subj, chasing, position, count, out readto );
                if( reachreading > bestreachcount ){
                    bestreachcount= reachreading;
                    bestreach= chasing;
                    bestto= readto;
                }
            }
            if( withslice ){ foreach( cf chasing in slicechaseorder ){ // slice must check additional items
                reachreading= facereach( subj, chasing, position, count, out readto );
                if( reachreading > bestreachcount ){
                    bestreachcount= reachreading;
                    bestreach= chasing;
                    bestto= readto;
                }
            }}
            to= bestto;
            topos= bestreachcount;
            return bestreach;
        }

        internal class chaselist{ // provide facilities for holding the sequence of face chasings
            internal struct chaselet {
                internal bool istransition{ get{ return starttransition != cf.cf24; }}
                internal bool ratchet;
                internal cf starttransition;
                internal string transition;
                internal cf endtransition;
            }
            List< chaselet > Lchaselet;
            internal chaselist( short incount ){
                Lchaselet= new List< chaselet >( incount );
            }
            internal void Add( cf f, string transitionstring ){
                chaselet c1= Lchaselet[ Lchaselet.Count-1 ];
                if( c1.endtransition == cf.cf24 ){
                    c1.transition= transitionstring;
                    c1.endtransition= f;
                    Lchaselet.RemoveAt( Lchaselet.Count-1 );
                    Lchaselet.Add( c1 ); }
                else { c1.starttransition= cf.cf24;
                    c1.transition= transitionstring;
                    c1.endtransition= f;
                    c1.ratchet= false;
                    Lchaselet.Add( c1 );
                }
            }
            internal void AddNewSubsequence( cf f ){
                chaselet c1= new chaselet();
                c1.ratchet= ratchetform( f );
                c1.starttransition= f;
                c1.transition= "^"; // flag incomplete
                c1.endtransition= cf.cf24;
                Lchaselet.Add( c1 );
            }
            internal void RemoveAtEnd(){
                Lchaselet.RemoveAt( Lchaselet.Count-1 );
            }

            /// <summary>
            /// _true_ if _f_ matches the second-to-the-last item inverted exclamation mark ! or '\uA1' is the transition indicator
            /// </summary>
            internal cf atratchet( short i ){
                cf reading= cf.cf24;
                if( i < 1 || i >= Lchaselet.Count ){ return cf.cf24; }
                reading= Lchaselet[ i - 1 ].starttransition;
                if( reading != cf.cf24 ){ return reading; }
                if( i < 2 ){ return cf.cf24; }
                return Lchaselet[ i - 2 ].endtransition;
            }
            internal void ResetNewSubsequence( short i, cf f ){
                for( short j= (short)( Lchaselet.Count - 1 ); j >= i; j-- ){
                    Lchaselet.RemoveAt( j ); }

                chaselet c1= new chaselet();
                c1.ratchet= ratchetform( f );
                c1.starttransition= f;
                c1.transition= "^"; // flag incomplete
                c1.endtransition= cf.cf24;
                Lchaselet.Add( c1 );
            }

            internal bool ratchetform( cf f ){ // avoid this error type: ..._E _F|_A _D!_F _E _U // '\uA1'
                if ( Lchaselet.Count < 2 ){ return false; }
                if ( Lchaselet[ Lchaselet.Count-1 ].starttransition == cf.cf24 ){ 
                    return Lchaselet[ Lchaselet.Count - 2 ].endtransition == f; }
                else { return Lchaselet[ Lchaselet.Count - 1 ].starttransition == f; }
            }
            internal bool ratchetform( int i ){ // i.e. "_C _T _H _G|_A 32 _U _E _Q!_E _H|_A _B 32 _V _P" // '\uA1'
                if ( i < 2 ){ return false; }
                cf f= Lchaselet[ i ].starttransition;
                //if( f == facelet.ec48 ){ f= Lchaselet[ i ].endtransition; }
                if ( Lchaselet[ i - 1 ].starttransition == cf.cf24 ){ 
                    return Lchaselet[ i - 2 ].endtransition == f; }
                else { return Lchaselet[ i - 1 ].starttransition == f; }
            }

            internal short Count{ get{ return (short)( Lchaselet.Count ); }}
            internal chaselet this[ int index ]{ get{ return Lchaselet[ index ]; }}
            public string ToStringDebug(){ // debug display form
                System.Text.StringBuilder sb= new System.Text.StringBuilder();
                foreach( chaselet c1 in Lchaselet ){
                    if( c1.ratchet ){ sb.Append( '\u00A1' ); } // \uA1 inverted explnation
                    if( c1.starttransition != cf.cf24 && c1.endtransition != cf.cf24 ){ sb.Append( '|' ); }
                    sb.Append( c1.starttransition );  sb.Append( ">" );
                    sb.Append( c1.transition );  
                    sb.Append( ">" );
                    sb.Append( c1.endtransition );  sb.Append( "  " );
                }
                const byte showlength= 60;
                if( sb.Length > showlength ){ return "..." + sb.ToString().Substring( sb.Length - showlength ); } else {
                return sb.ToString(); }
            }
            public override string ToString(){
                System.Text.StringBuilder sb2= new System.Text.StringBuilder();
                for( short i= 0; i < Lchaselet.Count; i++ ){
                    if( Lchaselet[ i ].istransition ){
                        if( i > 0 ){ if( Lchaselet[ i ].ratchet ){ sb2.Append( '\u00A1' ); } else { sb2.Append( '|' ); } } // '\uA1'
                        sb2.Append( '_' );  sb2.Append( singmaster.cc_e[ Lchaselet[ i ].starttransition ] );
                    }
                    if( Lchaselet[ i ].transition == "" ){ sb2.Append( ' ' ); }
                    else { sb2.Append( ' ' );  sb2.Append( Lchaselet[ i ].transition );  sb2.Append( ' ' ); }
                    sb2.Append( '_' );  sb2.Append( singmaster.cc_e[ Lchaselet[ i ].endtransition ] ); // lsanchor ) );
                }
                //if( sb2.ToString().Contains( "|" ) || sb2.ToString().Contains( "\u00A1" ) ){ return sb2.ToString(); } // '\uA1'
                //else {
                return "|" + sb2.ToString();
            }
        }

        static cf bestfaceorientation( ft[] subj, short position, cf face, chaselist refchase ){
            if( ftT.ft_ftSub( subj[ position ] ) == ftSub.slice ){ return face; } // slice moves cannot be reoriented

            // pick the orientation of _face_ that matches the orientation of the nearest similar
            // face chasing already parsed to avoid confusing and unnecessary reorientations

            // first, check if match of immediately preceding cubie off the last sequence
            cf facenoflip= ssT.floorflip( face );

            if( refchase.Count > 1 
                  && ssT.floorflip( refchase[ (short)( refchase.Count - 2 ) ].starttransition ) == facenoflip ){
                return refchase[ (short)( refchase.Count - 2 ) ].starttransition;
            }

            // second check if match on start of previous sequence, if so begin on same orientation
            short startchain= (short)( refchase.Count ); // determine start of previous chain
              if ( startchain == 0 ){ return face; }
              startchain-= 1;
            while( startchain > 1 && refchase[ (short)( startchain - 1) ].starttransition != cf.cf24 ){ startchain-= 1; }
            if( ssT.floorflip( refchase[ startchain ].starttransition ) == facenoflip ){
                return refchase[ startchain ].starttransition;
            }

            // third check if match on final of ante-previous sequence, if so begin on same orientation
            if( startchain > 2 && ssT.floorflip( refchase[ (short)( startchain - 2 ) ].starttransition ) == facenoflip ){
                return refchase[ (short)( startchain - 2 ) ].starttransition; }

            return face;
        }

        // occassionally, the longest reverse face reach does not begin with any elements of _chaseorder_.
        // in that case, it is reported to finalfacereachstartpos
        static short reversefacereachpos( ft[] subj, cf fromend, short fromendpos, out cf finalfacereachstartpos ){
            cf frompos= fromend;
            short runposition= fromendpos;
            if ( runposition == 0 ){ finalfacereachstartpos= fromend;  return 0; }
            cf topos;  ef topos_e;
            do{
                runposition-= 1;
                if( ftT.ft_ftSub( subj[ runposition ] ) == ftSub.posmark ){ topos= cf.cf24;  continue; }
                if( ftT.ft_ftSub( subj[ runposition ] ) == ftSub.slice ){
                    if( frompos == cf.cf24 ){
                        finalfacereachstartpos= fromend;
                        return runposition;
                        }
                    topos_e= ssT.move( ftT.invert( subj[ runposition ] ), singmaster.ee_e[ singmaster.cc_e[ frompos ] ]);
                    topos= singmaster.cc_e[ singmaster.ee_e[ topos_e ] ];
                } else {
                    topos= ssT.move( ftT.invert( subj[ runposition ] ), frompos );
                }
                if( topos == frompos ){ finalfacereachstartpos= frompos;  runposition+= 1;  return runposition; }
                frompos= topos;
            } while ( runposition > 0 );
            finalfacereachstartpos= topos;  return runposition;
        }

        static short bestreversefacereach( ft[] subj, out cf revfacereachstartpos, out cf revfacereachendpos, short reachcount ){
            short bestreachcount= reachcount;
            cf bestfacereachstartpos= cf.cf24;
            cf bestfacereachendpos= cf.cf24;
            cf currentfacereachstartpos;
            short reachreading;
            bool withslice= false;
            foreach( ft fti in subj ){ if( ftT.ft_ftSub( fti ) == ftSub.slice ){ withslice= true;  break; }}
            foreach( cf chasing in chaseorder ){
                reachreading= reversefacereachpos( subj, chasing, reachcount, out currentfacereachstartpos );
                if( reachreading < bestreachcount ){
                    bestfacereachstartpos= currentfacereachstartpos;
                    bestfacereachendpos= chasing;
                    bestreachcount= reachreading;
                }
            }
            if( withslice ){ foreach( cf chasing in slicechaseorder ){
                reachreading= reversefacereachpos( subj, chasing, reachcount, out currentfacereachstartpos );
                if( reachreading < bestreachcount ){
                    bestfacereachstartpos= currentfacereachstartpos;
                    bestfacereachendpos= chasing;
                    bestreachcount= reachreading;
                }
            }}
            revfacereachstartpos= bestfacereachstartpos;
            revfacereachendpos= bestfacereachendpos;
            return bestreachcount;
        }

        // generate a face chase from a sequence, which experience shows is best form for committing the
        // sequence to memory.  Otherwise, face chases are usually more tedious to perform.
        public static string toFaceChase( List< ft > fms )
        {
            if (fms == null || fms.Count == 0) { return ""; } // uninitialized
            cf chase; cf endchase;
            cf ratchet; cf endchasetrial;
            short chaseorderpos = 0; short endchasepos = 0;

            position ps= new position();  ps.init();  ps.resetnull();  ps.move( fms );
            measure mz= new measure( ps.es, ps.cs );
            cf firstcornercyclestart= mz.readc( measure.rr.cornerinitial );
            cf firstcornercycleend= mz.readc( measure.rr.cornersecondary );

            chase= bestfacereach( fms.ToArray(), chaseorderpos, (short)( fms.Count ), out endchase, out endchasepos );
            if ( ssT.floorflip( chase ) == firstcornercyclestart && firstcornercyclestart != cf.cf24 ){
                chase= firstcornercyclestart; } // attempt to start matching first cornercycle in listing

            string transition;
            cf finalfacereachstartori= cf.cf24;
            cf finalfacereachendori;
            cf trialfacereachstartori= cf.cf24;
            short endingpos;
            endingpos= bestreversefacereach( fms.ToArray(), out finalfacereachstartori, out finalfacereachendori, (short)( fms.Count ) );
            if( endingpos > 0 ){ chase= bestfacereach( fms.ToArray(), chaseorderpos, endingpos, out endchase, out endchasepos ); }

            bool finishonendoffirstcornersequence= firstcornercycleend != cf.cf24
                && reversefacereachpos( fms.ToArray(), firstcornercycleend, (short)( fms.Count ), out trialfacereachstartori ) == endingpos;
            if ( finishonendoffirstcornersequence ) {
                finalfacereachstartori= trialfacereachstartori;
                finalfacereachendori= firstcornercycleend; // good end match desired of end of first cornercycle in listing
            }

            chaselist refchase= new chaselist( (short)( fms.Count ) );  refchase.AddNewSubsequence( chase );
            for( short i= 0; i < endingpos; i++ ){ //" for each move in fs before endingpos - start of ending sequence
            if( fms[ i ] == ft.posMark ){ continue; } // BUG: avoid
                refchasecontinuation: // refrense from ratchet joining to the same final joining boundary is possible at an earlier joining point
            if( ssT.moveslice( fms[ i ], chase ) == chase ){ //" no further action on this cubie sequence
                chase= bestfacereach( fms.ToArray(), i, endingpos, out endchase, out endchasepos ); // find the best cubie to continue on change of focus
                ratchet= cf.cf24;
                if( i > 2 ){ ratchet= refchase.atratchet( i );
                    if( facereach( fms.ToArray(), ratchet, i, endingpos, out endchasetrial ) == endchasepos ){
                        chase= ratchet; }
                }
                
                cf beginchase1;  cf endchase1;  cf endchase2;
                if( chase != ratchet ){ // fix: slice moves cannot stand a re-orientation
                  chase= bestfaceorientation( fms.ToArray(), i, chase, refchase ); } // see if orientation can match recent
                if( !refchase.ratchetform( chase ) ){ //" final refocus is not ratchet form
                for( short j= bestreversefacereach( fms.ToArray(), out beginchase1, out endchase1, (short)( i + endchasepos ) ); j <= i; j++ ){ //" reseat
                    ratchet= refchase.atratchet( j );
                if( ratchet != cf.cf24 ){ //" ratchet form possible
                    short reach= facereach( fms.ToArray(), ratchet, j, endingpos, out endchase2 );
                if ( j + reach == i + endchasepos && refchase[ refchase.Count-1 ].starttransition == cf.cf24 
                        || j + reach == i + endchasepos && refchase[ refchase.Count-1 ].starttransition == ratchet ) {
                if( j == i ){ // ratchet joining to the same final joining boundary found
                    chase= ratchet;
                    goto refchasejoiningfound;
                } else { //" ratchet joining to the same final joining boundary is possible at an earlier joining point
                    refchase.ResetNewSubsequence( j, ratchet );
                    for( short k= j; k < i; k++ ){
                        ratchet= ssT.move( fms[ k ], ratchet );
                        refchase.Add(ratchet, toCornerfacetransition( fms[ k ] )); // ((ftT)(fms[k])).ToTransitionString()); toCornerfacetransition(fci.move);
                    }
                    chase= ratchet;
                    goto refchasecontinuation;
                } //" ratchet joining to the same final joining boundary is not possible at an earlier joining point
                } } //" ratchet form possible
                } //" reseat
                } //" final refocus is not of ratchet form
                refchasejoiningfound:
                refchase.AddNewSubsequence( chase );
            } //" no further action on this edge face chase sequence
            // continue acting on this edge face chase sequence
                chase= ssT.moveslice( fms[ i ], chase );
                transition= toCornerfacetransition( fms[ i ] );//((ftT)( fms[ i ] )).ToTransitionString();
                refchase.Add( chase, transition );
            } //" for each move in fs before endingpos - start of ending sequence

            // _C 2 _W _Y 2 _B _F _G!_F _B _S _G _C _T _H _G _C // y-perm study sequence // (b:AB_#_AC) '\uA1'
            // _F 3 _B 3 _S _T 3 _C|_F _B _A 3 _E _F _V!_F _E // (BIC_#_AEM)
            // |_F _V 3 _U _T 3 _Q _E 3 _U 3 _V _F // (BCI_#_ADq-l_BCt+i)
            // _F _B 32 _Y!_B _F 3 _E _I|_F 3 _E _U // (AQI_#_ACp+i_BLe-m) // reaches to one before the start // special case fix?
            // _E _H _T _U!_T _H _E _U _T _Q  // (BIE_#_ALH) // BUG can begin at one from the start // requires special case to code?

            // the unfeatured default ending that should be correct each time
            // ref this code
            //   endingpos= bestreversefacereach( out finalfacereachstartori, out finalfacereachendori, (short)( fs.Count ) );
            if( endingpos < fms.Count ){
                refchase.ResetNewSubsequence( endingpos, finalfacereachstartori );
            }
            chase= finalfacereachstartori;
            for( short i= endingpos; i < fms.Count; i++ ){
                chase= ssT.moveslice( fms[ i ], chase );
                transition= toCornerfacetransition( fms[ i ] ); //((ftT)( fms[ i ] )).ToTransitionString();
                refchase.Add( chase, transition );
            }

            //refchasereturntostring:
            return refchase.ToString();
        } // toFaceChase
    } // \ CubeTrae.parsing

    public enum dr
    {
        c_UF_, d_UL_y3, a_UB_y2, b_UR_y1,         // U face
        e_LU_x3z1, f_LF_z1, g_LD_x1z1, h_LB_x2z1, // L face
        i_FU_x1y2, j_FR_x1y1, k_FD_x1, l_FL_x1y3, // F face
        m_RU_x3z3, n_RB_y2z, o_RD_x1z3, p_RF_z3,  // R face
        q_BU_x3, r_BL_x3y3, s_BD_x3y2, t_BR_x3y1, // B face
        u_DF_z2, v_DR_x2y1, w_DB_x2, x_DL_x2y3,   // D face
        dr24 } // \ dr // disposition rotation faces(6) x sides(4)

    public class drT{
        #region box/unbox conversions
        dr dr_subject;

        // bijective conversions to the enum _dr_
        public static implicit operator drT( dr valuedr ){
            drT dr_return= new drT();
            dr_return.dr_subject= valuedr;
            return dr_return;
        }
        public static implicit operator dr( drT valuedr ){ return valuedr.dr_subject; }
        public static explicit operator byte( drT valuedrT ){ return (byte)( valuedrT.dr_subject ); }
        #endregion
        const byte drEntireRange= (byte)( (byte)( dr.dr24 ) + 1 );

        #region invert x y z reflectM
        #region invert
        static dr[] invert_= new dr[ drEntireRange ]{ // ^ k1
            dr.c_UF_, dr.b_UR_y1, dr.a_UB_y2, dr.d_UL_y3,           // U
            dr.l_FL_x1y3, dr.p_RF_z3, dr.t_BR_x3y1, dr.h_LB_x2z1,   // L
            dr.i_FU_x1y2, dr.m_RU_x3z3, dr.q_BU_x3, dr.e_LU_x3z1,   // F
            dr.j_FR_x1y1, dr.n_RB_y2z, dr.r_BL_x3y3, dr.f_LF_z1,    // R
            dr.k_FD_x1, dr.o_RD_x1z3, dr.s_BD_x3y2, dr.g_LD_x1z1,   // B
            dr.u_DF_z2, dr.v_DR_x2y1, dr.w_DB_x2, dr.x_DL_x2y3,     // D
            dr.dr24 };
        #endregion
        public static dr invert( dr dri ) { return invert_[ (byte)( dri ) ]; }
        
        #region x
        static dr[] x_= new dr[ drEntireRange ]{ // ^ x
            dr.k_FD_x1, dr.g_LD_x1z1, dr.s_BD_x3y2, dr.o_RD_x1z3,
            dr.b_UR_y1, dr.j_FR_x1y1, dr.v_DR_x2y1, dr.t_BR_x3y1,
            dr.a_UB_y2, dr.n_RB_y2z, dr.w_DB_x2, dr.h_LB_x2z1,
            dr.d_UL_y3, dr.r_BL_x3y3, dr.x_DL_x2y3, dr.l_FL_x1y3,
            dr.c_UF_, dr.f_LF_z1, dr.u_DF_z2, dr.p_RF_z3,
            dr.i_FU_x1y2, dr.m_RU_x3z3, dr.q_BU_x3, dr.e_LU_x3z1,
            dr.dr24 };
        #endregion
        public static dr x( dr dri ) { return x_[ (byte)( dri ) ]; }

        #region y
        static dr[] y_= new dr[ drEntireRange ]{ // ^ y
            dr.b_UR_y1, dr.c_UF_, dr.d_UL_y3, dr.a_UB_y2,
            dr.h_LB_x2z1, dr.e_LU_x3z1, dr.f_LF_z1, dr.g_LD_x1z1,
            dr.l_FL_x1y3, dr.i_FU_x1y2, dr.j_FR_x1y1, dr.k_FD_x1,
            dr.p_RF_z3, dr.m_RU_x3z3, dr.n_RB_y2z, dr.o_RD_x1z3,
            dr.t_BR_x3y1, dr.q_BU_x3, dr.r_BL_x3y3, dr.s_BD_x3y2,
            dr.x_DL_x2y3, dr.u_DF_z2, dr.v_DR_x2y1, dr.w_DB_x2,
            dr.dr24 };
        #endregion
        public static dr y( dr dri ) { return y_[ (byte)( dri ) ]; }

        #region z
        static dr[] z_= new dr[ drEntireRange ]{ // ^ z
            dr.f_LF_z1, dr.r_BL_x3y3, dr.n_RB_y2z, dr.j_FR_x1y1,  
            dr.i_FU_x1y2, dr.u_DF_z2, dr.s_BD_x3y2, dr.a_UB_y2,
            dr.m_RU_x3z3, dr.v_DR_x2y1, dr.g_LD_x1z1, dr.d_UL_y3,
            dr.q_BU_x3, dr.w_DB_x2, dr.k_FD_x1, dr.c_UF_,
            dr.e_LU_x3z1, dr.x_DL_x2y3, dr.o_RD_x1z3, dr.b_UR_y1,
            dr.p_RF_z3, dr.t_BR_x3y1, dr.h_LB_x2z1, dr.l_FL_x1y3,
            dr.dr24 };
        #endregion
        public static dr z( dr dri ) { return z_[ (byte)( dri ) ]; }

        #region reflectM
        static dr[] reflectM_= new dr[ drEntireRange ]{ // ^ k2
            dr.c_UF_, dr.b_UR_y1, dr.a_UB_y2, dr.d_UL_y3,
            dr.m_RU_x3z3, dr.p_RF_z3, dr.o_RD_x1z3, dr.n_RB_y2z,
            dr.i_FU_x1y2, dr.l_FL_x1y3, dr.k_FD_x1, dr.j_FR_x1y1,
            dr.e_LU_x3z1, dr.h_LB_x2z1, dr.g_LD_x1z1, dr.f_LF_z1,
            dr.q_BU_x3, dr.t_BR_x3y1, dr.s_BD_x3y2, dr.r_BL_x3y3,
            dr.u_DF_z2, dr.x_DL_x2y3, dr.w_DB_x2, dr.v_DR_x2y1,
            dr.dr24 };
        #endregion
        public static dr reflectM( dr dri ){ return reflectM_[ (byte)( dri ) ]; }
        #endregion

        static dr[] mult( dr[] L, dr[] R ){
            if( L.Length != R.Length ){ throw new System.Exception( "undefined" ); }
            dr[] res= new dr[ L.Length ];
            for( byte i= 0; i < L.Length; i++ ){ res[ i ] = L[ (byte)( R[ i ] ) ]; }
            return res;
        }
        static dr[] square( dr[] S ){
            dr[] res= new dr[ S.Length ];
            for( byte i= 0; i < S.Length; i++ ){
                res[ i ]= S[ (byte)( S[ i ] ) ];
            }
            return res;
        }

        public static dr[] x2_= square( x_ );  public static dr x2( dr dri ) { return x2_[ (byte)( dri ) ]; }
        public static dr[] y2_= square( y_ );  public static dr y2( dr dri ) { return y2_[ (byte)( dri ) ]; }
        public static dr[] z2_= square( z_ );  public static dr z2( dr dri ) { return z2_[ (byte)( dri ) ]; }
        public static dr[] x3_= mult( x_, x2_ );  public static dr x3( dr dri ) { return x3_[ (byte)( dri ) ]; }
        public static dr[] y3_= mult( y_, y2_ );  public static dr y3( dr dri ) { return y3_[ (byte)( dri ) ]; }
        public static dr[] z3_= mult( z_, z2_ );  public static dr z3( dr dri ) { return z3_[ (byte)( dri ) ]; }

        #region tests
        internal static bool equal( dr[] L, dr[] R ){
            if ( L.Length != R.Length ){ return false; }
            for( byte i= 0; i < L.Length; i++ ){
                if( L[i] != R[i] ){ 
                    return false; }
            }
            return true;
        }
        internal static bool isperm( dr[] S ){
            bool[] checks= new bool[ S.Length ];
            for( byte i= 0; i < S.Length; i++ ){ checks[ i ]= false; }
            foreach( dr dri in S ){
                if( checks[ (byte)( dri ) ] ){
                    return false; }
                checks[ (byte)( dri ) ]= true;
            }
            return true;
        }

        internal static dr[] genidentity(){
            dr[] res= new dr[ drEntireRange ];
            for( byte i= 0; i < drEntireRange; i++ ){
                res[ i ]= (dr)( i );
            }
            return res;
        }

        internal static bool associativity( dr[] L, dr[] M, dr[] R ){
            dr[] lassoc= mult( mult( L, M ), R );
            dr[] rassoc= mult( L, mult( M, R ) );
            return equal( lassoc, rassoc );
        }
            

        internal static void test(){
            dr[] identity= genidentity();
            dr[] x2= square( x_ );
            dr[] x4= square( x2 );
            bool testx= equal( x4, identity );
            bool testxi= equal( identity, x4 );
            dr[] y2= square( y_ );
            dr[] y4= square( y2 );
            bool testy= equal( y4, identity );
            bool testyp= isperm( y_ );
            dr[] z2= square( z_ );
            dr[] z4= square( z2 );
            bool testz= equal( z4, identity );
            bool testzp= isperm( z_ );
            dr[] doubinverse= square( invert_ );
            bool testinverse= equal( doubinverse, identity );
            dr[] doubreflect= square( reflectM_ );
            bool testreflect= equal( doubreflect, identity );
            bool a1= associativity( x_, y_, z_ );
            // brute-force associativity test

            dr dx= dr.e_LU_x3z1;
            byte xr= gx( dx );
            byte yr= gy( dx );
            byte zr= gz( dx );

            dr lassoc;  dr rassoc; // verify associativity of mult
            for( dr dr1= dr.c_UF_; dr1 < dr.dr24; dr1++ ){
            for( dr dr2= dr.c_UF_; dr2 < dr.dr24; dr2++ ){
            for( dr dr3= dr.c_UF_; dr3 < dr.dr24; dr3++ ){
                lassoc= mult( mult( dr1, dr2 ), dr3 );
                rassoc= mult( dr1, mult( dr2, dr3 ) );
                if( lassoc != rassoc ){
                    throw new System.Exception( "associativity fails" );
                }
            }}}

            // test inverst to identity
            for( dr dr1= dr.c_UF_;  dr1 < dr.dr24; dr1++ ){
                if( mult( dr1, invert( dr1 ) ) != dr.c_UF_ || mult( invert( dr1 ), dr1 ) != dr.c_UF_ ){
                    throw new System.Exception( "inverse to identity fails" );
                }
            }

            singmaster.ee_e.origin= ef.aq_UB;
            foreach( ef efi in singmaster.ee_e ){
                ef efj= efi;
            }
            singmaster.cc_e.origin= cf.aer_ULB;
            foreach( cf cfi in singmaster.cc_e ){
                cf cfj= cfi;
            }


        }
        #endregion

        // encode generator to get transform as xxyyzz where xx refers to binary positions 4 and 5, yy refers to binary positions 2 and 3
        static byte[] gendr_ = new byte[ drEntireRange ]{
            0, 12, 8, 4,    49, 1, 17, 33,    24, 20, 16, 28,
            51, 9, 19, 3,   48, 60, 56, 52,    2, 36, 32, 44,    255 };

        public static byte gx( dr dri ) { return (byte)( (gendr_[ (byte)( dri ) ] >> 4) & 3); }
        public static byte gy( dr dri ) { return (byte)( (gendr_[ (byte)( dri ) ] >> 2) & 3); }
        public static byte gz( dr dri ) { return (byte)( gendr_[ (byte)( dri ) ] & 3 ); }

        public static dr mult( dr L, dr R ){
          dr res= L;
          switch ( gx( R ) ){
              case 0: break;
              case 1: res= x( res );  break;
              case 2: res= x2( res );  break;
              case 3: res= x3( res );  break;
          }
          switch ( gy( R ) ){
              case 0: break;
              case 1: res= y( res );  break;
              case 2: res= y2( res );  break;
              case 3: res= y3( res );  break;
          }
          switch ( gz( R ) ){
              case 0: break;
              case 1: res= z( res );  break;
              case 2: res= z2( res );  break;
              case 3: res= z3( res );  break;
          }
          return res;
        }

        public static dr mult3w( dr L, ft R ){ // employed parse2 section transformq
            dr res= L;
            switch ( R ){
            case ft.x1: res= x( res );  break;
            case ft.x2: res= x2( res );  break;
            case ft.x3: res= x3( res );  break;
            case ft.y1: res= y( res );  break;
            case ft.y2: res= y2( res );  break;
            case ft.y3: res= y3( res );  break;
            case ft.z1: res= z( res );  break;
            case ft.z2: res= z2( res );  break;
            case ft.z3: res= z3( res );  break;
            }
            return res;
        }
        public static dr mult3w( ft L, dr R ){
            dr res= dr.c_UF_;
            switch ( L ){ // this section can be cleaned up some
            case ft.x1: res= x( res );  break;
            case ft.x2: res= x2( res );  break;
            case ft.x3: res= x3( res );  break;
            case ft.y1: res= y( res );  break;
            case ft.y2: res= y2( res );  break;
            case ft.y3: res= y3( res );  break;
            case ft.z1: res= z( res );  break;
            case ft.z2: res= z2( res );  break;
            case ft.z3: res= z3( res );  break;
            }
            switch ( gx( R ) ){
              case 0: break;
              case 1: res= x( res );  break;
              case 2: res= x2( res );  break;
              case 3: res= x3( res );  break;
            }
            switch ( gy( R ) ){
              case 0: break;
              case 1: res= y( res );  break;
              case 2: res= y2( res );  break;
              case 3: res= y3( res );  break;
            }
            switch ( gz( R ) ){
              case 0: break;
              case 1: res= z( res );  break;
              case 2: res= z2( res );  break;
              case 3: res= z3( res );  break;
            }
            return res;
        }
        public static dr mult3w( ft L, dr M, ft R ){
            return mult3w( mult3w( L, M ), R ); // employed position.move
        }
        
        static ef[] dr_ef_= new ef[ drEntireRange ]{
        #region dr_ef
            ef.ci_UF, ef.de_UL, ef.aq_UB, ef.bm_UR,
            ef.ed_LU, ef.fl_LF, ef.gx_LD, ef.hr_LB,
            ef.ic_FU, ef.jp_FR, ef.ku_FD, ef.lf_FL,
            ef.mb_RU, ef.nt_RB, ef.ov_RD, ef.pj_RF,
            ef.qa_BU, ef.rh_BL, ef.sw_BD, ef.tn_BR,
            ef.uk_DF, ef.vo_DR, ef.ws_DB, ef.xg_DL,
            ef.ef24 };
        #endregion
        // employed reporting the disposition rotation of the puzzle
        public static ef dr_ef( dr dri ){ return dr_ef_[ (byte)( dri ) ]; }

    } // \ drT class

    // TODO M E S planes selector for each ef facelet
    public enum ef : byte
    { // edge facelet
        ci_UF, de_UL, aq_UB, bm_UR,  ed_LU, fl_LF, gx_LD, hr_LB,
        ic_FU, jp_FR, ku_FD, lf_FL,  mb_RU, nt_RB, ov_RD, pj_RF,
        qa_BU, rh_BL, sw_BD, tn_BR,  uk_DF, vo_DR, ws_DB, xg_DL,  ef24
    }
    public enum cf : byte
    { // corner facelet
        cmj_URF, dif_UFL, aer_ULB, bqn_UBR,     eaer_LBU, fdif_LUF, glu_LFD, hxs_LDB,
        idif_FLU, jcmj_FUR, kpv_FRD, lglu_FDL,  mcmj_RFU, nbqn_RUB, otw_RBD, pkpv_RDF,
        qbqn_BRU, raer_BUL, shxs_BLD, totw_BDR, uglu_DLF, vkpv_DFR, wotw_DRB, xhxs_DBL,  cf24
    }

    public class ssT{
        public const cf cf0= cf.cmj_URF;  public const ef ef0= ef.ci_UF;
        const byte ecfEntireRange= (byte)( (byte)( ef.ef24 ) + 1 );
        #region primary moves U (L)(F)(R)(B) D
        static ef[] efU= new ef[ ecfEntireRange ]{ 
            ef.bm_UR, ef.ci_UF, ef.de_UL, ef.aq_UB,  ef.ic_FU, ef.fl_LF, ef.gx_LD, ef.hr_LB,
            ef.mb_RU, ef.jp_FR, ef.ku_FD, ef.lf_FL,  ef.qa_BU, ef.nt_RB, ef.ov_RD, ef.pj_RF,
            ef.ed_LU, ef.rh_BL, ef.sw_BD, ef.tn_BR,  ef.uk_DF, ef.vo_DR, ef.ws_DB, ef.xg_DL,
            ef.ef24 };
        static cf[] cfU= new cf[ ecfEntireRange ]{
            cf.bqn_UBR, cf.cmj_URF, cf.dif_UFL, cf.aer_ULB,        cf.idif_FLU, cf.jcmj_FUR, cf.glu_LFD, cf.hxs_LDB,
            cf.mcmj_RFU, cf.nbqn_RUB, cf.kpv_FRD, cf.lglu_FDL,   cf.qbqn_BRU, cf.raer_BUL, cf.otw_RBD, cf.pkpv_RDF,
            cf.eaer_LBU, cf.fdif_LUF, cf.shxs_BLD, cf.totw_BDR, cf.uglu_DLF, cf.vkpv_DFR, cf.wotw_DRB, cf.xhxs_DBL,
            cf.cf24 };

        static ef[] efD= new ef[ ecfEntireRange ]{
            ef.ci_UF, ef.de_UL, ef.aq_UB, ef.bm_UR,  ef.ed_LU, ef.fl_LF, ef.sw_BD, ef.hr_LB,
            ef.ic_FU, ef.jp_FR, ef.gx_LD, ef.lf_FL,  ef.mb_RU, ef.nt_RB, ef.ku_FD, ef.pj_RF,
            ef.qa_BU, ef.rh_BL, ef.ov_RD, ef.tn_BR,  ef.xg_DL, ef.uk_DF, ef.vo_DR, ef.ws_DB,
            ef.ef24 };
        static cf[] cfD= new cf[ ecfEntireRange ]{
            cf.cmj_URF, cf.dif_UFL, cf.aer_ULB, cf.bqn_UBR,        cf.eaer_LBU, cf.fdif_LUF, cf.shxs_BLD, cf.totw_BDR,
            cf.idif_FLU, cf.jcmj_FUR, cf.glu_LFD, cf.hxs_LDB,  cf.mcmj_RFU, cf.nbqn_RUB, cf.kpv_FRD, cf.lglu_FDL,
            cf.qbqn_BRU, cf.raer_BUL, cf.otw_RBD, cf.pkpv_RDF,  cf.xhxs_DBL, cf.uglu_DLF, cf.vkpv_DFR, cf.wotw_DRB,
            cf.cf24 };
        #endregion

        // this is akin to U2 F2 U2 B2 L R B2 D2 F2 U2 L' R' D' B2 F2 L2 R2 U' (18f), except the cubes are mirror-imaged.
        #region reflection transition T2
        static ef[] efMreflect= new ef[ ecfEntireRange ]{
            ef.ci_UF, ef.bm_UR, ef.aq_UB, ef.de_UL,  ef.mb_RU, ef.pj_RF, ef.ov_RD, ef.nt_RB,
            ef.ic_FU, ef.lf_FL, ef.ku_FD, ef.jp_FR,  ef.ed_LU, ef.hr_LB, ef.gx_LD, ef.fl_LF,
            ef.qa_BU, ef.tn_BR, ef.sw_BD, ef.rh_BL,  ef.uk_DF, ef.xg_DL, ef.ws_DB, ef.vo_DR,
            ef.ef24 };

        static cf[] cfMreflect= new cf[ ecfEntireRange ]{
            cf.dif_UFL, cf.cmj_URF, cf.bqn_UBR, cf.aer_ULB,        cf.nbqn_RUB, cf.mcmj_RFU, cf.pkpv_RDF, cf.otw_RBD,
            cf.jcmj_FUR, cf.idif_FLU, cf.lglu_FDL, cf.kpv_FRD,   cf.fdif_LUF, cf.eaer_LBU, cf.hxs_LDB, cf.glu_LFD,
            cf.raer_BUL, cf.qbqn_BRU, cf.totw_BDR, cf.shxs_BLD, cf.vkpv_DFR, cf.uglu_DLF, cf.xhxs_DBL, cf.wotw_DRB,
            cf.cf24 };
        #endregion
        #region x y
        static ef[] efx= new ef[ ecfEntireRange ]{
            ef.qa_BU, ef.rh_BL, ef.sw_BD, ef.tn_BR,  ef.hr_LB, ef.ed_LU, ef.fl_LF, ef.gx_LD,
            ef.aq_UB, ef.bm_UR, ef.ci_UF, ef.de_UL,  ef.nt_RB, ef.ov_RD, ef.pj_RF, ef.mb_RU,
            ef.ws_DB, ef.xg_DL, ef.uk_DF, ef.vo_DR,  ef.ic_FU, ef.jp_FR, ef.ku_FD, ef.lf_FL,
            ef.ef24 };

        static cf[] cfx= new cf[ ecfEntireRange ]{
            cf.qbqn_BRU, cf.raer_BUL, cf.shxs_BLD, cf.totw_BDR,   cf.hxs_LDB, cf.eaer_LBU, cf.fdif_LUF, cf.glu_LFD,
            cf.aer_ULB, cf.bqn_UBR, cf.cmj_URF, cf.dif_UFL,          cf.nbqn_RUB, cf.otw_RBD, cf.pkpv_RDF, cf.mcmj_RFU,
            cf.wotw_DRB, cf.xhxs_DBL, cf.uglu_DLF, cf.vkpv_DFR,  cf.idif_FLU, cf.jcmj_FUR, cf.kpv_FRD, cf.lglu_FDL,
            cf.cf24 };

        static ef[] efy = new ef[ ecfEntireRange ]{
            ef.de_UL, ef.aq_UB, ef.bm_UR, ef.ci_UF,  ef.qa_BU, ef.rh_BL, ef.sw_BD, ef.tn_BR,
            ef.ed_LU, ef.fl_LF, ef.gx_LD, ef.hr_LB,  ef.ic_FU, ef.jp_FR, ef.ku_FD, ef.lf_FL,
            ef.mb_RU, ef.nt_RB, ef.ov_RD, ef.pj_RF,  ef.xg_DL, ef.uk_DF, ef.vo_DR, ef.ws_DB, ef.ef24 };


        static cf[] cfy = new cf[ ecfEntireRange ]{
            cf.dif_UFL, cf.aer_ULB, cf.bqn_UBR, cf.cmj_URF,          cf.qbqn_BRU, cf.raer_BUL, cf.shxs_BLD, cf.totw_BDR,
            cf.eaer_LBU, cf.fdif_LUF, cf.glu_LFD, cf.hxs_LDB,    cf.idif_FLU, cf.jcmj_FUR, cf.kpv_FRD, cf.lglu_FDL,
            cf.mcmj_RFU, cf.nbqn_RUB, cf.otw_RBD, cf.pkpv_RDF, cf.xhxs_DBL, cf.uglu_DLF, cf.vkpv_DFR, cf.wotw_DRB,
            cf.cf24 };
        #endregion

        #region generate multiples of primary moves
        static ef[] mult( ef[] L, ef[] R ){
            if( L.Length != R.Length ){ throw new System.Exception( "undefined" ); }
            ef[] res= new ef[ L.Length ];
            for( byte i= 0; i < L.Length; i++ ){ res[ i ] = L[ (byte)( R[ i ] ) ]; }
            return res;
        }
        static ef[] mult( ef[] L, ef[] M, ef[] R ){
            if( L.Length != R.Length || R.Length != M.Length ){ throw new System.Exception( "undefined" ); }
            ef[] res= new ef[ L.Length ];
            for( byte i= 0; i < L.Length; i++ ){ res[ i ]= L[ (byte)( M[ (byte)( R[ i ] ) ] ) ]; }
            return res;
        }

        static ef[] square( ef[] S ){
            ef[] res= new ef[ S.Length ];
            for( byte i= 0; i < S.Length; i++ ){
                res[ i ]= S[ (byte)( S[ i ] ) ];
            }
            return res;
        }

        static cf[] mult( cf[] L, cf[] R ){
            if( L.Length != R.Length ){ throw new System.Exception( "undefined" ); }
            cf[] res= new cf[ L.Length ];
            for( byte i= 0; i < L.Length; i++ ){ res[ i ] = L[ (byte)( R[ i ] ) ]; }
            return res;
        }
        static cf[] mult( cf[] L, cf[] M, cf[] R ){
            if( L.Length != R.Length | R.Length != M.Length ){ throw new System.Exception( "undefined" ); }
            cf[] res= new cf[ L.Length ];
            for( byte i= 0; i < L.Length; i++ ){ res[ i ] = L[ (byte)( M[ (byte)( R[ i ] ) ] ) ]; }
            return res;
        }

        static cf[] square( cf[] S ){
            cf[] res= new cf[ S.Length ];
            for( byte i= 0; i < S.Length; i++ ){
                res[ i ]= S[ (byte)( S[ i ] ) ];
            }
            return res;
        }

        const byte ftStandardSubrange= (byte)( (byte)( ft.d3 ) + 1 );
        public static ef[ , ] emove= new ef[ ftStandardSubrange, ecfEntireRange ];
        public static cf[ , ] cmove= new cf[ ftStandardSubrange, ecfEntireRange ];

        const byte ftWholeCubeTransitionOffset= (byte)( (byte)( ft.Ty1 ) );
        const byte ftWholeCubeTransitionSubrange= (byte)( (byte)( ft.Tz3 ) + 1 - ftWholeCubeTransitionOffset );
        public static ef[ , ] eTmove= new ef[ ftWholeCubeTransitionSubrange, ecfEntireRange ];
        public static cf[ , ] cTmove= new cf[ ftWholeCubeTransitionSubrange, ecfEntireRange ];

        static void insertToMoveTable( ft fti, ef[] given ){
            switch( ftT.ft_ftSub( fti ) ){
            case ftSub.face:
                for( byte i= 0; i < ecfEntireRange; i++ ){
                    emove[ (byte)( fti ), i ]= given[ i ];
                }
                break;
            case ftSub.transform:
            if( (byte)( fti ) < ftWholeCubeTransitionOffset ){ throw new System.Exception( "undefined T0 T1 or T2" ); }
                for( byte i= 0; i < ecfEntireRange; i++ ){
                    eTmove[ (byte)( fti ) - ftWholeCubeTransitionOffset, i ]= given[ i ];
                }
                break;
            }
        }
        static void insertToMoveTable( ft fti, cf[] given ){
            switch( ftT.ft_ftSub( fti ) ){
            case ftSub.face:
                for( byte i= 0; i < ecfEntireRange; i++ ){
                    cmove[ (byte)( fti ), i ]= given[ i ];
                }
                break;
            case ftSub.transform:
                if( (byte)( fti ) < ftWholeCubeTransitionOffset ){ throw new System.Exception( "undefined T0 T1 or T2" ); }
                for( byte i= 0; i < ecfEntireRange; i++ ){
                    cTmove[ (byte)( fti ) - ftWholeCubeTransitionOffset, i ]= given[ i ];
                }
                break;
            }
        }

        static ef[] extractEfromMoveTable( ft fti ){ // employed for constructing z table from x and y table via z == y' x y
            ef[] res= new ef[ ecfEntireRange ];
            switch( ftT.ft_ftSub( fti ) ){
            case ftSub.face:
                for( byte i= 0; i < ecfEntireRange; i++ ){
                    res[ i ]= emove[ (byte)( fti ), i ];
                }
                return res;
            case ftSub.transform:
                if( (byte)( fti ) >= ftWholeCubeTransitionOffset ){
                for( byte i= 0; i < ecfEntireRange; i++ ){
                    res[ i ]= eTmove[ (byte)( fti ) - ftWholeCubeTransitionOffset, i ];
                  }
                  return res;
                } else throw new System.Exception( "not defined" );
            }
            return null;
        }
        static cf[] extractCfromMoveTable( ft fti ){
            cf[] res= new cf[ ecfEntireRange ];
            switch( ftT.ft_ftSub( fti ) ){
            case ftSub.face:
                for( byte i= 0; i < ecfEntireRange; i++ ){
                    res[ i ]= cmove[ (byte)( fti ), i ];
                }
                return res;
            case ftSub.transform:
                if( (byte)( fti ) >= ftWholeCubeTransitionOffset ){
                for( byte i= 0; i < ecfEntireRange; i++ ){
                    res[ i ]= cTmove[ (byte)( fti ) - ftWholeCubeTransitionOffset, i ];
                  }
                  return res;
                } else throw new System.Exception( "not defined" );
            }
            return null;
        }

        #endregion
        /// <summary> construct the motion tables given the seeds efU cfU, efx cfx, efy, cfy </summary>

        static ef[] Maffect_= new ef[]{ ef.aq_UB, ef.ci_UF, ef.ic_FU, ef.ku_FD, ef.qa_BU, ef.sw_BD, ef.uk_DF, ef.ws_DB };
        static ef[] Eaffect_= new ef[]{ ef.fl_LF, ef.hr_LB, ef.jp_FR, ef.lf_FL, ef.nt_RB, ef.pj_RF, ef.rh_BL, ef.tn_BR };
        static ef[] Saffect_= new ef[]{ ef.bm_UR, ef.de_UL, ef.ed_LU, ef.gx_LD, ef.mb_RU, ef.ov_RD, ef.vo_DR, ef.xg_DL };

        static bool[] Maffect= new bool[ (byte)( ef.ef24 ) ];
        static bool[] Eaffect= new bool[ (byte)( ef.ef24 ) ];
        static bool[] Saffect= new bool[ (byte)( ef.ef24 ) ];
        static internal void IniMoveTables(){
            insertToMoveTable( ft.ut1, efU );  insertToMoveTable( ft.ut1, cfU );
            ef[] e2= square( efU );  cf[] c2= square( cfU );
            insertToMoveTable( ft.ut2, e2 );  insertToMoveTable( ft.ut2, c2 );
            insertToMoveTable( ft.ut3, mult( efU, e2 ));  insertToMoveTable( ft.ut3, mult( cfU, c2 ));

            insertToMoveTable( ft.Tx1, efx );  insertToMoveTable( ft.Tx1, cfx );
            e2= square( efx );  c2= square( cfx );
            insertToMoveTable( ft.Tx2, e2 );  insertToMoveTable( ft.Tx2, c2 );
            insertToMoveTable( ft.Tx3, mult( efx, e2 ));  insertToMoveTable( ft.Tx3, mult( cfx, c2 ));

            e2= mult( extractEfromMoveTable( ft.Tx3 ), efU, efx );
              c2= mult( extractCfromMoveTable( ft.Tx3 ), cfU, cfx );
            insertToMoveTable( ft.f1, e2 );  insertToMoveTable( ft.f1, c2 );
            e2= square( e2 );  c2= square( c2 );
            insertToMoveTable( ft.f2, e2 );  insertToMoveTable( ft.f2, c2 );
            insertToMoveTable( ft.f3, mult( extractEfromMoveTable( ft.f1 ), e2 ));  insertToMoveTable( ft.f3, mult( extractCfromMoveTable( ft.f1 ), c2 ));

            insertToMoveTable( ft.Ty1, efy );  insertToMoveTable( ft.Ty1, cfy );
            e2= square( efy );  c2= square( cfy );
            insertToMoveTable( ft.Ty2, e2 );  insertToMoveTable( ft.Ty2, c2 );
            insertToMoveTable( ft.Ty3, mult( efy, e2 ));  insertToMoveTable( ft.Ty3, mult( cfy, c2 ));

            e2= mult( extractEfromMoveTable( ft.Ty1 ), extractEfromMoveTable( ft.f1 ), extractEfromMoveTable( ft.Ty3 ) );
              c2= mult( extractCfromMoveTable( ft.Ty1 ), extractCfromMoveTable( ft.f1 ), extractCfromMoveTable( ft.Ty3 ) );
            insertToMoveTable( ft.L1, e2 );  insertToMoveTable( ft.L1, c2 );
            e2= square( e2 );  c2= square( c2 );
            insertToMoveTable( ft.L2, e2 );  insertToMoveTable( ft.L2, c2 );
            insertToMoveTable( ft.L3, mult( extractEfromMoveTable( ft.L1 ), e2 ));  insertToMoveTable( ft.L3, mult( extractCfromMoveTable( ft.L1 ), c2 ));

            e2= mult( extractEfromMoveTable( ft.Ty1 ), extractEfromMoveTable( ft.L1 ), extractEfromMoveTable( ft.Ty3 ) );
              c2= mult( extractCfromMoveTable( ft.Ty1 ), extractCfromMoveTable( ft.L1 ), extractCfromMoveTable( ft.Ty3 ) );
            insertToMoveTable( ft.bp1, e2 );  insertToMoveTable( ft.bp1, c2 );
            e2= square( e2 );  c2= square( c2 );
            insertToMoveTable( ft.bp2, e2 );  insertToMoveTable( ft.bp2, c2 );
            insertToMoveTable( ft.bp3, mult( extractEfromMoveTable( ft.bp1 ), e2 ));  insertToMoveTable( ft.bp3, mult( extractCfromMoveTable( ft.bp1 ), c2 ));

            e2= mult( extractEfromMoveTable( ft.Ty1 ), extractEfromMoveTable( ft.bp1 ), extractEfromMoveTable( ft.Ty3 ) );
              c2= mult( extractCfromMoveTable( ft.Ty1 ), extractCfromMoveTable( ft.bp1 ), extractCfromMoveTable( ft.Ty3 ) );
            insertToMoveTable( ft.r1, e2 );  insertToMoveTable( ft.r1, c2 );
            e2= square( e2 );  c2= square( c2 );
            insertToMoveTable( ft.r2, e2 );  insertToMoveTable( ft.r2, c2 );
            insertToMoveTable( ft.r3, mult( extractEfromMoveTable( ft.r1 ), e2 ));  insertToMoveTable( ft.r3, mult( extractCfromMoveTable( ft.r1 ), c2 ));

            e2= mult( extractEfromMoveTable( ft.Tx3 ), extractEfromMoveTable( ft.f1 ), efx );
              c2= mult( extractCfromMoveTable( ft.Tx3 ), extractCfromMoveTable( ft.f1 ), cfx );
            insertToMoveTable( ft.d1, e2 );  insertToMoveTable( ft.d1, c2 );
            e2= square( e2 );  c2= square( c2 );
            insertToMoveTable( ft.d2, e2 );  insertToMoveTable( ft.d2, c2 );
            insertToMoveTable( ft.d3, mult( extractEfromMoveTable( ft.d1 ), e2 ));  insertToMoveTable( ft.d3, mult( extractCfromMoveTable( ft.d1 ), c2 ));

            e2= mult( mult( efy, efx ), extractEfromMoveTable( ft.Ty3  )); // z1
              c2= mult( mult( cfy, cfx ), extractCfromMoveTable( ft.Ty3 ));
              insertToMoveTable( ft.Tz1, e2 );  insertToMoveTable( ft.Tz1, c2 );
            e2= square( e2 );  c2= square( c2 );
            insertToMoveTable( ft.Tz2, e2 );  insertToMoveTable( ft.Tz2, c2 );
            insertToMoveTable( ft.Tz3, mult( extractEfromMoveTable( ft.Tz1 ), e2 ) );  insertToMoveTable( ft.Tz3, mult( extractCfromMoveTable( ft.Tz1 ), c2 ) );

            for( byte i= 0; i < (byte)( ef.ef24 ); i++ ){ Maffect[ i ]= false;  Eaffect[ i ]= false;  Saffect[ i ]= false; }
            foreach( ef efi in Maffect_ ){ Maffect[ (byte)( efi ) ]= true; }
            foreach( ef efi in Eaffect_ ){ Eaffect[ (byte)( efi ) ]= true; }
            foreach( ef efi in Saffect_ ){ Saffect[ (byte)( efi ) ]= true; }
        }

        static byte[] rrr; // hold a e[] or c[] perm until a copy is completed from a transformation
        const byte ecfIncludedRange= (byte)( ef.ef24 );
        public static void applyMove( ft move, ef[] efp, cf[] cfp ){
            switch( ftT.ft_ftSub( move ) ){
          case ftSub.face:
                rrr= (byte[])( efp.Clone() );
                for ( byte i= 0; i < ecfIncludedRange; i++ ){
                    efp[ i ]= (ef)( rrr[ (byte)( emove[ (byte)( move ), i ] ) ] );
                }
                rrr= (byte[])( cfp.Clone() );
                for ( byte i= 0; i < ecfIncludedRange; i++ ){
                    cfp[ i ]= (cf)( rrr[ (byte)( cmove[ (byte)( move ), i ] ) ] );
                }
                break;
          case ftSub.transform: // transforms manipulate the cube position rather than the moves generating the position
                if( move == ft.T2 ){ // all transforms are conjugates
                    rrr= (byte[])( efp.Clone() );
                    for ( byte i= 0; i < ecfIncludedRange; i++ ){
                        efp[ i ]= (ef)( efMreflect[ (byte)( rrr[ (byte)( efMreflect[ i ] ) ] ) ] );
                    }
                    rrr= (byte[])( cfp.Clone() );
                    for ( byte i= 0; i < ecfIncludedRange; i++ ){
                        cfp[ i ]= (cf)( cfMreflect[ (byte)( rrr[ (byte)( cfMreflect[ i ] ) ] ) ] );
                    }
                } else if( (byte)( move ) >= ftWholeCubeTransitionOffset ){
                    ft antimove= ftT.xyz_Txyz( move ); // since _move_ is a Tx, Ty or Tz type, this gives the inverse transition
                    rrr= (byte[])( efp.Clone() );
                    for ( byte i= 0; i < ecfIncludedRange; i++ ){
                        efp[ i ]= eTmove[ (byte)( move ) - ftWholeCubeTransitionOffset,
                            (byte)( rrr[ (byte)( eTmove[ (byte)( antimove ) - ftWholeCubeTransitionOffset, i ] ) ] ) ];
                    }
                    rrr= (byte[])( cfp.Clone() );
                    for ( byte i= 0; i < ecfIncludedRange; i++ ){
                        cfp[ i ]= cTmove[ (byte)( move ) - ftWholeCubeTransitionOffset,
                            (byte)( rrr[ (byte)( cTmove[ (byte)( antimove ) - ftWholeCubeTransitionOffset, i ] ) ] ) ];
                    }
                } else { throw new System.Exception( "unimplemented" ); }
                break;
            } // switch ftT.ft_ftSub( move ) )
        } // \ applyMove

        const byte transitionoffset= (byte)( ft.y1 );
        public static ef move( ft fti, ef efi ){
            ft fft;
            switch( ftT.ft_ftSub(fti) ){
            case ftSub.noop: return efi;
            case ftSub.face: // U L F etc.
                return ssT.emove[ (byte)( ftT.invert( fti ) ), (byte)( efi ) ];
            case ftSub.doubl: // figure the edges that move, as slice form
                fft= ftT.unwiden( ftT.unwiden( fti ) );
                switch( fft ){
                case ft.m1: case ft.m2: case ft.m3:
                    if( Maffect[ (byte)( efi ) ] ){ // BUG fix here move( ft.utW1, ef.aq ) == ef.aq
                        return ssT.eTmove[ (byte)( ftT.invert( fft ) ) - (byte)( ft.m1 ) + (byte)( ft.x1 ) - transitionoffset, (byte)( efi ) ];
                    } else { return ssT.emove[ (byte)( ftT.invert( ftT.unwiden( fti ))), (byte)( efi ) ]; }
                case ft.e1: case ft.e2: case ft.e3:
                    if( Eaffect[ (byte)( efi ) ] ){ 
                        return ssT.eTmove[ (byte)( ftT.invert( fft ) ) - (byte)( ft.e1 ) + (byte)( ft.y1 ) - transitionoffset, (byte)( efi ) ];
                    } else { return ssT.emove[ (byte)( ftT.invert( ftT.unwiden( fti ))), (byte)( efi ) ]; }
                case ft.s1: case ft.s2: case ft.s3:
                    if( Saffect[ (byte)( efi ) ] ){ 
                        return ssT.eTmove[ (byte)( fft ) - (byte)( ft.s1 ) + (byte)( ft.z1 ) - transitionoffset, (byte)( efi ) ];
                    } else { return ssT.emove[ (byte)( ftT.invert( ftT.unwiden( fti ))), (byte)( efi ) ]; }
                }
                return ef.ef24;
            case ftSub.slice: switch( fti ){
                case ft.m1: case ft.m2: case ft.m3:
                    if( Maffect[ (byte)( efi ) ] ){ 
                        return ssT.eTmove[ (byte)( ftT.invert( fti ) ) - (byte)( ft.m1 ) + (byte)( ft.x1 ) - transitionoffset, (byte)( efi ) ];
                    } else { return efi; }
                case ft.e1: case ft.e2: case ft.e3:
                    if( Eaffect[ (byte)( efi ) ] ){ 
                        return ssT.eTmove[ (byte)( ftT.invert( fti ) ) - (byte)( ft.e1 ) + (byte)( ft.y1 ) - transitionoffset, (byte)( efi ) ];
                    } else { return efi; }
                case ft.s1: case ft.s2: case ft.s3:
                    if( Saffect[ (byte)( efi ) ] ){ 
                        return ssT.eTmove[ (byte)( fti ) - (byte)( ft.s1 ) + (byte)( ft.z1 ) - transitionoffset, (byte)( efi ) ];
                    } else { return efi; }
                }
                return ef.ef24;
            case ftSub.whole: // x y' z2 etc.
                return ssT.eTmove[ (byte)( fti ) - transitionoffset, (byte)( efi ) ];
            }
            return ef.ef24;
        }
        public static cf move( ft fti, cf cfi ){
            switch( ftT.ft_ftSub( fti ) ){
            case ftSub.face:
                return ssT.cmove[ (byte)( ftT.invert( fti ) ), (byte)( cfi ) ];
            case ftSub.doubl: // figure the corners that move, i.e. Uw moves corners same as U
                return ssT.cmove[ (byte)( ftT.invert( ftT.unwiden( fti ) ) ), (byte)( cfi ) ];
            case ftSub.slice: return cfi; // slice does not affect corners
            case ftSub.whole:
                return ssT.cTmove[ (byte)( fti ) - transitionoffset, (byte)( cfi ) ];
            }
            return cf.cf24; }
        public static cf moveslice( ft fti, cf cfi ){ // same as move( fti, cf cfi ) unless a slice move
          if( ftT.ft_ftSub( fti ) == ftSub.posmark ){ return cfi; }
          if( ftT.ft_ftSub( fti ) == ftSub.slice ){ // \ except slice moves are converted to ef equivalent name
              ef samename= singmaster.ee_e[ singmaster.cc_e[ cfi ]];
              ef movesamename= move( fti, samename );
              if( movesamename == samename ){ return cfi; }
              else { return singmaster.cc_e[ singmaster.ee_e[ movesamename ] ]; }
          } else return move( fti, cfi );
        }

        #region attached onto the same moving element (cubie)
        static ef[] eOBV_= new ef[ ecfEntireRange ]{
            ef.ic_FU, ef.ed_LU, ef.qa_BU, ef.mb_RU, ef.de_UL, ef.lf_FL, ef.xg_DL, ef.rh_BL,
            ef.ci_UF, ef.pj_RF, ef.uk_DF, ef.fl_LF, ef.bm_UR, ef.tn_BR, ef.vo_DR, ef.jp_FR,
            ef.aq_UB, ef.hr_LB, ef.ws_DB, ef.nt_RB, ef.ku_FD, ef.ov_RD, ef.sw_BD, ef.gx_LD, ef.ef24 };
        public static ef eOBV( ef value ){ return eOBV_[ (byte)( value ) ]; } 

        static cf[] cCL_= new cf[ ecfEntireRange ]{
            cf.mcmj_RFU, cf.idif_FLU, cf.eaer_LBU, cf.qbqn_BRU,
            cf.raer_BUL, cf.dif_UFL, cf.lglu_FDL, cf.xhxs_DBL,
            cf.fdif_LUF, cf.cmj_URF, cf.pkpv_RDF, cf.uglu_DLF,
            cf.jcmj_FUR, cf.bqn_UBR, cf.totw_BDR, cf.vkpv_DFR,
            cf.nbqn_RUB, cf.aer_ULB, cf.hxs_LDB, cf.wotw_DRB,
            cf.glu_LFD, cf.kpv_FRD, cf.otw_RBD, cf.shxs_BLD, cf.cf24 };
        public static cf cCL( cf value ){ return cCL_[ (byte)( value ) ]; } // employed

        static cf[] cCCL_= new cf[ ecfEntireRange ]{
            cf.jcmj_FUR, cf.fdif_LUF, cf.raer_BUL, cf.nbqn_RUB,
            cf.aer_ULB, cf.idif_FLU, cf.uglu_DLF, cf.shxs_BLD,
            cf.dif_UFL, cf.mcmj_RFU, cf.vkpv_DFR, cf.glu_LFD,
            cf.cmj_URF, cf.qbqn_BRU, cf.wotw_DRB, cf.kpv_FRD,
            cf.bqn_UBR, cf.eaer_LBU, cf.xhxs_DBL, cf.otw_RBD,
            cf.lglu_FDL, cf.pkpv_RDF, cf.totw_BDR, cf.hxs_LDB, cf.cf24 };
        public static cf cCCL( cf value ){ return cCCL_[ (byte)( value ) ]; }

        static pm[] ecf_pm_= new pm[ ecfEntireRange ]{ // principle plane for each ef or cf -- employed for position.ToString() and decodesingmaster.ini()
            pm.ut, pm.ut, pm.ut, pm.ut,  pm.ll, pm.ll, pm.ll, pm.ll,
            pm.ff, pm.ff, pm.ff, pm.ff,  pm.rr, pm.rr, pm.rr, pm.rr,
            pm.bp, pm.bp, pm.bp, pm.bp,  pm.dd, pm.dd, pm.dd, pm.dd, pm.pm60 };
        public static pm ecf_pm( ef value ){ return ecf_pm_[ (byte)( value ) ]; } // employed in decodesingmasterIni
        public static pm ecf_pm( cf value ){ return ecf_pm_[ (byte)( value ) ]; } // employed in decodesingmasterIni

        static cf[] c_floorflip_= new cf[ (byte)( cf.cf24 ) + 1 ]{ // support face chasing corner orientation checks
            cf.cmj_URF, cf.dif_UFL, cf.aer_ULB, cf.bqn_UBR,  cf.aer_ULB, cf.dif_UFL, cf.glu_LFD, cf.hxs_LDB,
            cf.dif_UFL, cf.cmj_URF, cf.kpv_FRD, cf.glu_LFD,  cf.cmj_URF, cf.bqn_UBR, cf.otw_RBD, cf.kpv_FRD,
            cf.bqn_UBR, cf.aer_ULB, cf.hxs_LDB, cf.otw_RBD,  cf.glu_LFD, cf.kpv_FRD, cf.otw_RBD, cf.hxs_LDB,  cf.cf24 };
        public static cf floorflip( cf value ){ return c_floorflip_[ (byte)( value ) ]; }

        #endregion
    } // class ssT

    /// <summary>
    /// model the cube position
    /// </summary>
    public class position{
        dr rds; // entire polyhedron _r_otation _d_i_s_position
        byte rcf0;  byte rcf1;  byte rcf2; // rotation of center of face -- packed, two per byte
        ef[] ep; // edge facelets permuation viewed from the whole-cube home or solving position
        cf[] cp; // corner facelets permutation similarly
        sy sym; // symmetry transform; reset by T0, moved on any transform command
        internal ef[] es{ get{ return (ef[])( ep.Clone() ); } }
        internal cf[] cs{ get{ return (cf[])( cp.Clone() ); } }

        /// <summary> give the centermost face cubie disposition measured from its solved state
        /// either 0, 1, 2, or 3 : for 0�, 90� clockwise, 180�, and 270� clockwise, respectively </summary>
        public byte this[ pm face ]{ get{ // two items are packed into each byte
            switch ( face ){ // pm.ut, pm.ff, pm.rr, pm.bp, pm.ll, pm.dd
            case pm.ut: return (byte)( rcf0 & 0x0F );
            case pm.ff: return (byte)( ( rcf0 & 0xF0 ) >> 4 );
            case pm.rr: return (byte)( rcf1 & 0x0F );
            case pm.bp: return (byte)( ( rcf1 & 0xF0 ) >> 4 );
            case pm.ll: return (byte)( rcf2 & 0x0F );
            case pm.dd: return (byte)( ( rcf2 & 0xF0 ) >> 4 );
            default: return 255; }
            } set {
            switch ( face ){ // pm.ut, pm.ff, pm.rr, pm.bp, pm.ll, pm.dd
            case pm.ut: rcf0= (byte)( (rcf0 & 0xF0) | (value & 0x0F) );  return;
            case pm.ff: rcf0= (byte)( (rcf0 & 0x0F) | ((value & 0x0F) << 4 ) );  return;
            case pm.rr: rcf1= (byte)( (rcf1 & 0xF0) | (value & 0x0F) );  return;
            case pm.bp: rcf1= (byte)( (rcf1 & 0x0F) | ((value & 0x0F) << 4 ) );  return;
            case pm.ll: rcf2= (byte)( (rcf2 & 0xF0) | (value & 0x0F) );  return;
            case pm.dd: rcf2= (byte)( (rcf2 & 0x0F) | ((value & 0x0F) << 4 ) );  return;
            default: return; }
        }}

        const byte ecfIncludedRange = (byte)(ef.ef24);
        public void init( /*Letters inPosLetters*/ ){
            rds= dr.c_UF_;
            ep= new ef[ ecfIncludedRange ];
            cp= new cf[ ecfIncludedRange ];
            //pw= new posWork( this, inPosLetters );
        }

        public void resetnull()
        { // initialize _sp_ and _cp_ to null (solved) state
            rds= dr.c_UF_;
            for( pm pmi= pm.ut; pmi <= pm.dd; pmi++ ){ this[ pmi ]= 0; }

            if( ep == null || cp == null ){ init( /*null*/ ); } // added to avoid 

            for ( ef spi= (ef)( 0 ); spi < ef.ef24; spi++ ) { ep[ (byte)( spi ) ]= spi; } // reset edges
            for ( cf cpi= (cf)( 0 ); cpi < cf.cf24; cpi++ ) { cp[ (byte)( cpi ) ]= cpi; } // reset corners
            //if ( (object)( pw ) != (object)( null ) ){ pw.update( this ); }
            //if( pw != null ){ 
                // for ( es spi= es.am_UB_aq; spi < es.e24; spi++ ){ pw.view_e[ (byte)( spi ) ]= spi; }
                // for ( cs cpi= cs.apq_ULB_aer; cpi < cs.c24; cpi++ ){ pw.view_c[ (byte)( cpi ) ]= cpi; }
                //pw.viewsequence= new fseq();
                //pw.viewsequence.init( pw.posWorkLn );
                //pw.viewrotdisp= dr.c_UF_c;
                // foreach( pm pmi in ftT.pmo ){ pw[ pmi ]= 0; }
                //pw.update( this );
            //}
        }

        #region extracted transitiion moves // called also on inverse to transform to neutral position
        void Tx1(){
            ssT.applyMove( ft.Tx1, ep, cp );
            byte motionamount= this[ pm.ut ];
              this[ pm.ut ]= this[ pm.ff ];
              this[ pm.ff ]= this[ pm.dd ];
              this[ pm.dd ]= this[ pm.bp ];
              this[ pm.bp ]= motionamount;
        }
        void Tx2(){
            ssT.applyMove( ft.Tx2, ep, cp );
            byte motionamount= this[ pm.ut ];
              this[ pm.ut ]= this[ pm.dd ];
              this[ pm.dd ]= motionamount;
            motionamount= this[ pm.ff ];
              this[ pm.ff ]= this[ pm.bp ];
              this[ pm.bp ]= motionamount;
        }
        void Tx3(){
            ssT.applyMove( ft.Tx3, ep, cp );
            byte motionamount= this[ pm.ut ];
              this[ pm.ut ]= this[ pm.bp ];
              this[ pm.bp ]= this[ pm.dd ];
              this[ pm.dd ]= this[ pm.ff ];
              this[ pm.ff ]= motionamount;
        }
        void Ty1(){
            ssT.applyMove( ft.Ty1, ep, cp );
            byte motionamount = this[ pm.ll ];
              this[ pm.ll ]= this[ pm.ff ];
              this[ pm.ff ]= this[ pm.rr ];
              this[ pm.rr ]= this[ pm.bp ];
              this[ pm.bp ]= motionamount;
        }
        void Ty2(){
            ssT.applyMove( ft.Ty2, ep, cp );
            byte motionamount= this[ pm.ll ];
              this[ pm.ll ]= this[ pm.rr ];
              this[ pm.rr ]= motionamount;
            motionamount= this[ pm.ff ];
              this[ pm.ff ]= this[ pm.bp ];
              this[ pm.bp ]= motionamount;
        }
        void Ty3(){
            ssT.applyMove( ft.Ty3, ep, cp );
            byte motionamount= this[ pm.ll ];
              this[ pm.ll ]= this[ pm.bp ];
              this[ pm.bp ]= this[ pm.rr ];
              this[ pm.rr ]= this[ pm.ff ];
              this[ pm.ff ]= motionamount;
        }
        void Tz1(){
            ssT.applyMove( ft.Tz1, ep, cp );
            byte motionamount= this[ pm.ut ];
              this[ pm.ut ]= this[ pm.ll ];
              this[ pm.ll ]= this[ pm.dd ];
              this[ pm.dd ]= this[ pm.rr ];
              this[ pm.rr ]= motionamount;
        }
        void Tz2(){
            ssT.applyMove( ft.Tz2, ep, cp );
            byte motionamount= this[ pm.ut ];
              this[ pm.ut ]= this[ pm.dd ];
              this[ pm.dd ]= motionamount;
            motionamount= this[ pm.ll ];
              this[ pm.ll ]= this[ pm.rr ];
              this[ pm.rr ]= motionamount;
        }
        void Tz3(){
            ssT.applyMove( ft.Tz3, ep, cp );
            byte motionamount= this[ pm.ut ];
              this[ pm.ut ]= this[ pm.rr ];
              this[ pm.rr ]= this[ pm.dd ];
              this[ pm.dd ]= this[ pm.ll ];
              this[ pm.ll ]= motionamount;
        }
        #endregion
        public void move( ft fm ){
            ft fr= ftT.multinv( fm, rds );
            pm motionPlane= ftT.ft_pm( fr );
            byte motionamount= ftT.ft_byte( fr );
            pm mp1;  pm mp2; // employed deciphering slice moves
            switch ( ftT.ft_ftSub( fr ) ){
            case ftSub.noop:  case ftSub.ftSub10:  default: break;
            case ftSub.face:
                this[ motionPlane ]= (byte)( (byte)( ( this[ (pmT)( motionPlane ) ] + motionamount ) % 4 ) );
                ssT.applyMove( fr, ep, cp );  break;
            case ftSub.doubl:
                this[ motionPlane ]= (byte)( (byte)( ( this[ (pmT)( motionPlane ) ] + motionamount ) % 4 ) );
                ssT.applyMove( ftT.unwiden2( fr ), ep, cp );
                rds= drT.mult3w( rds, ftT.wide_wide3part( fm ) );  break;
            case ftSub.slice:
                mp1= ftT.ft_pm( ftT.unwiden2( fr ) );
                mp2= ftT.ft_pm( ftT.unwiden2( ftT.unwiden2( fr ) ) ); // unwiden of a wide group gives the parallel planes
                this[ mp1 ]= (byte)( ( this[ mp1 ] + 4 + ftT.ft_byte( ftT.unwiden2( fr ) ) ) % 4 );
                this[ mp2 ]= (byte)( ( this[ mp2 ] + 4 + ftT.ft_byte( ftT.unwiden2( ftT.unwiden2( fr ) ) ) ) % 4 );
                ssT.applyMove( ftT.unwiden2( fr ), ep, cp );
                ssT.applyMove( ftT.unwiden2( ftT.unwiden2( fr )), ep, cp );
                rds= drT.mult3w( rds, ftT.wide_wide3part( fm ) );  break;
            case ftSub.whole:
                rds= drT.mult3w( rds, fm );  break;
            case ftSub.transform: switch( fm ){
                #region transform section
                case ft.T0: sym= sy.identity;  break; // reset symmetry note to neutral position
                case ft.T1: sym= syT.inverse( sym ); // remove or insert symmetry note in symmetry position report
                    switch ( drT.gx( rds ) ){ // return oritentation to non-rotated orientation
                    case 0:  default:  break;
                    case 1: Tx1();  break;
                    case 2: Tx2();  break;
                    case 3: Tx3();  break; }
                    switch ( drT.gy( rds ) ){
                    case 0:  default:  break;
                    case 1: Ty1();  break;
                    case 2: Ty2();  break;
                    case 3: Ty3();  break; }
                    switch ( drT.gz( rds ) ){
                    case 0:  default:  break;
                    case 1: Tz1();  break;
                    case 2: Tz2();  break;
                    case 3: Tz3();  break; }

                    rds= drT.invert( rds );
                    ef[] epn= new ef[ ecfIncludedRange ];
                    cf[] cpn= new cf[ ecfIncludedRange ];
                    for( byte i= 0; i < ecfIncludedRange; i++ ){
                        epn[ (byte)( ep[ i ] ) ]= (ef)( i );
                        cpn[ (byte)( cp[ i ] ) ]= (cf)( i );
                    }
                    ep= epn;
                    cp= cpn;
                    for( pm pmi= pm.ut; pmi <= pm.dd; pmi++ ){
                        this[ pmi ]= (byte)( ( 4 - this[ pmi ] ) % 4 );
                    }
                    break;
                case ft.T2: sym= syT.mult( sym, sy.reflect );
                    rds= drT.reflectM( rds );
                    ssT.applyMove( ft.T2, ep, cp );
                    this[ pm.ut ]= (byte)( ( 4 - this[ pm.ut ] ) % 4 );
                    motionamount= (byte)( ( 4 - this[ pm.ll ] ) % 4 );  
                      this[ pm.ll ]= (byte)( ( 4 - this[ pm.rr ] ) % 4 );
                      this[ pm.rr ]= motionamount;
                    this[ pm.ff ]= (byte)( ( 4 - this[ pm.ff ] ) % 4 );
                    this[ pm.dd ]= (byte)( ( 4 - this[ pm.dd ] ) % 4 );
                    this[ pm.bp ]= (byte)( ( 4 - this[ pm.bp ] ) % 4 );
                    break;
                case ft.Tx1: sym= syT.mult( sym, sy.k_FD_x1 );
                    rds= drT.mult3w( ft.x3, rds, ft.x1 );  Tx1();  break;
                case ft.Tx2: sym= syT.mult( sym, sy.w_DB_x2 );
                    rds= drT.mult3w( ft.x2, rds, ft.x2 );  Tx2();  break;
                case ft.Tx3:  sym= syT.mult( sym, sy.q_BU_x3 );
                    rds= drT.mult3w( ft.x1, rds, ft.x3 );  Tx3();  break;
                case ft.Ty1: sym= syT.mult( sym, sy.b_UR_y1 );
                    rds= drT.mult3w( ft.y3, rds, ft.y1 );  Ty1();  break;
                case ft.Ty2: sym= syT.mult( sym, sy.a_UB_y2 );
                    rds= drT.mult3w( ft.y2, rds, ft.y2 );  Ty2();  break;
                case ft.Ty3: sym= syT.mult( sym, sy.d_UL_y3 );
                    rds= drT.mult3w( ft.y1, rds, ft.y3 );  Ty3();  break;
                case ft.Tz1: sym = syT.mult(sym, sy.f_LF_z1);
                    rds= drT.mult3w( ft.z3, rds, ft.z1 );  Tz1();  break;
                case ft.Tz2: sym= syT.mult( sym, sy.u_DF_z2 );
                    rds= drT.mult3w( ft.z2, rds, ft.z2 );  Tz2();  break;
                case ft.Tz3: sym= syT.mult( sym, sy.p_RF_z3 );
                    rds= drT.mult3w( ft.z1, rds, ft.z3 );  Tz3();  break;
                #endregion
                } //\ ftSub.transform: switch( fm )
                break;
            } //\ switch
        }
        public void move( List< ft > fms ){
            foreach( ft fti in fms ){ move( fti ); }
        }


        public string shortreport; // this string is affixed to the ToString report if the usual report is short
        public byte shortreportlengththreashold= 8;
        public catalog.entry ToEntry(){
            catalog.entrybuild ret= new catalog.entrybuild();
            System.Text.StringBuilder rdr= new System.Text.StringBuilder();
            const byte pmPrimaryPlanesRange= (byte)( (byte)( pm.dd ) + 1 );
            bool[] planevisited= new bool[ pmPrimaryPlanesRange ];
            for( byte bpm= 0; bpm < pmPrimaryPlanesRange; bpm++ ){ planevisited[ bpm ]= false; }

            pm pmi;
            foreach ( ef efi in singmaster.ee_e ){
                if( efi == singmaster.ee_e.origin ){ continue; }
                pmi= ssT.ecf_pm( efi );  if( planevisited[ (byte)( pmi ) ] ){ continue; }
                planevisited[ (byte)( pmi ) ]= true;
                if( this[ pmi ] == 0 ){ continue; } else { ret.centerfacetwist= true;  break; }
            }

            if( rds != dr.c_UF_ ){ ret.cuberotation= true; }

            System.Text.StringBuilder er= new System.Text.StringBuilder();

            ef aen= (ef)( 0 ); // examined edge cubie position attached
            ef cyen= ssT.eOBV( (ef)( 0 ) ); // examined cycle edge cubie position
            ef pcyen= (ef)( 0 ); // examined previous cycle edge cubie position
            ef e0;  ef e1; // detect if there is only one or two flip-returns in cycle

            bool[] visited= new bool[ (byte)( ef.ef24 ) ];
            for( ef sp= (ef)( 0 ); sp < ef.ef24; sp++ ){ visited[ (byte)( sp ) ]= false; }
            #region report edge cycles
            foreach ( ef en in singmaster.ee_e ){
                if ( visited[ (byte)( en ) ] ){ continue; } // ignore if already marked as visited connected facelet
                else if ( ep[ (byte)( en ) ] == en ){ // inplace solved
                    aen= ssT.eOBV( en );
                    visited[ (byte)( en ) ]= true;  visited[ (byte)( aen ) ]= true;
                    continue;
                } else { //" report cycle until a repeat is found
                    e0= en;
                    e1= en;
                    aen= ssT.eOBV( en );
                    visited[ (byte)( en ) ]= true;  visited[ (byte)( aen ) ]= true;
                    er.Append( singmaster.ee_e[ en ] );  ret.insert( en );
                    pcyen= en;  cyen= ep[ (byte)( en ) ];
                    while( cyen != en && cyen != aen ){ //" trace cycle
                        er.Append( singmaster.ee_e[ cyen ] );  ret.insert(  cyen );
                          if( visited[ (byte)( cyen ) ] ){ er.Append( "~~~" );  break; } // avoid infinite loop if transition tables have a BUG
                        visited[ (byte)( cyen ) ]= true;
                        visited[ (byte)( ssT.eOBV( cyen ) ) ]= true;
                        e1= cyen;
                        pcyen= cyen;  cyen= ep[ (byte)( cyen ) ];
                    } //" trace cycle
                    if ( cyen == en ){ er.Append( '_' ); }
                    else if ( cyen == aen ){ //" report flip attach
                        er.Append( ( singmaster.ee_e[ cyen ] ).ToString().ToLower() );
                        if ( cyen != ssT.eOBV( e1 ) ){ // not an edge in place but flipped
                            if( ep[ (byte)( e0 ) ] != e1 ){ er.Append( '\'' ); } // only include \' if more than 2 in flip cycle
                            er.Append( singmaster.ee_e[ ssT.eOBV( pcyen ) ].ToString().ToLower() );
                        }
                        er.Append( '_' );
                    } //"report flip attach
                    continue;
                } //" report cycle until a repeat if found
            } //" report edge cycles
            #endregion

            System.Text.StringBuilder cr= new System.Text.StringBuilder();

            cf accn= ssT.cCCL( (cf)( 0 ) );  cf acn= ssT.cCL( (cf)( 0 ) ); // corner cubie has two other facelets attached
            cf cycn= (cf)( 0 );
            cf pcycn= (cf)( 0 );

            for ( cf sp= (cf)( 0 ); sp < cf.cf24; sp++ ){ visited[ (byte)( sp ) ]= false; }
            #region report corner cycles
            foreach ( cf cn in singmaster.cc_e ){ //" report corner cycles
                if ( visited[ (byte)( cn ) ] ){ continue; } // ignore if already marked as visited
                else if ( cp[ (byte)( cn ) ] == cn ){ // inplace solved
                    acn= ssT.cCL( cn );  accn= ssT.cCCL( cn );
                    visited[ (byte)( cn ) ]= true;
                    visited[ (byte)( acn ) ]= true;  visited[ (byte)( accn ) ]= true;
                    continue;
                } else { //" report cycle until a repeat is found
                    acn= ssT.cCL( cn );  accn= ssT.cCCL( cn );
                    visited[ (byte)( cn ) ]= true;  visited[ (byte)( acn ) ]= true;  visited[ (byte)( accn ) ]= true;

                    cr.Append( '_' );
                    cr.Append( singmaster.cc_e[ cn ] );  ret.insert( cn );
                    pcycn= cn;  cycn= cp[ (byte)( cn ) ];
                    while( cycn != cn && cycn != accn && cycn != acn ){ //" trace cycle
                        cr.Append( singmaster.cc_e[ cycn ] );  ret.insert( cycn );
                          if( visited[ (byte)( cycn ) ] ){ cr.Append( "~~~" );  break; } // avoid infinite loop if transition tables have a BUG
                        visited[ (byte)( cycn ) ]= true;
                        visited[ (byte)( ssT.cCL( cycn ) ) ]= true;  visited[ (byte)( ssT.cCCL( cycn ) ) ]= true;
                        pcycn= cycn;  cycn= cp[ (byte)( cycn ) ];
                    } //" trace cycle
                    if( cycn == cn ){ // pass
                    } else if( cycn == accn ){
                        cr.Append( ( singmaster.cc_e[ cycn ] ).ToString().ToLower() );
                        cr.Append( '+' );
                        cr.Append( ( singmaster.cc_e[ ssT.cCL( pcycn ) ] ).ToString().ToLower() );
                    } else if( cycn == acn ){
                        cr.Append( ( singmaster.cc_e[ cycn ] ).ToString().ToLower() );
                        cr.Append( '-' );
                        cr.Append( ( singmaster.cc_e[ ssT.cCCL( pcycn ) ] ).ToString().ToLower() );
                    }
                    continue;
                } //" report cycle
            } //" report corner cycles
            #endregion

            // affix sym notation if not in neutral position
            if( (sym != sy.identity) && (sym != sy.c_UF_) ){ cr.Append( " {" + ((syT)( sym )).ToString() + "}" ); }
            return ret.ToEntry();
        }
        public override string ToString(){
            System.Text.StringBuilder rdr= new System.Text.StringBuilder();
            const byte pmPrimaryPlanesRange= (byte)( (byte)( pm.dd ) + 1 );
            bool[] planevisited= new bool[ pmPrimaryPlanesRange ];
            for( byte bpm= 0; bpm < pmPrimaryPlanesRange; bpm++ ){ planevisited[ bpm ]= false; }

            pm pmi;  ef ereport= ef.ef24;
            byte reporttwist;

            sbyte accumtwist= 0;
            foreach ( ef efi in singmaster.ee_e ){
                if( efi == singmaster.ee_e.origin ){ continue; }
                pmi= ssT.ecf_pm( efi );  if( planevisited[ (byte)( pmi ) ] ){ continue; }
                planevisited[ (byte)( pmi ) ]= true;
                reporttwist= this[ pmi ];  accumtwist+= (sbyte)( reporttwist % 2 );
                if( reporttwist == 0 ){ continue; }
                ereport= pmT.fromPlaneWithPos( pmi, reporttwist );
                rdr.Append( singmaster.ee_e[ ereport ].ToString().ToLower() );
            }

            if( rds != dr.c_UF_ ){ // no report if in standard position
                if( rdr.Length > 0 ){ rdr.Append( '@' ); }
                rdr.Append( singmaster.ee_e[ drT.dr_ef( rds ) ] );
            }
            if( rdr.Length > 0 ){ rdr.Append( ":" ); }

            System.Text.StringBuilder er= new System.Text.StringBuilder();

            ef aen= (ef)( 0 ); // examined edge cubie position attached
            ef cyen= ssT.eOBV( (ef)( 0 ) ); // examined cycle edge cubie position
            ef pcyen= (ef)( 0 ); // examined previous cycle edge cubie position
            ef e0;  ef e1; // detect if there is only one or two flip-returns in cycle

            bool[] visited= new bool[ (byte)( ef.ef24 ) ];
            for( ef sp= (ef)( 0 ); sp < ef.ef24; sp++ ){ visited[ (byte)( sp ) ]= false; }
            #region report edge cycles
            foreach ( ef en in singmaster.ee_e ){
                if ( visited[ (byte)( en ) ] ){ continue; } // ignore if already marked as visited connected facelet
                else if ( ep[ (byte)( en ) ] == en ){ // inplace solved
                    aen= ssT.eOBV( en );
                    visited[ (byte)( en ) ]= true;  visited[ (byte)( aen ) ]= true;
                    continue;
                } else { //" report cycle until a repeat is found
                    e0= en;
                    e1= en;
                    aen= ssT.eOBV( en );
                    visited[ (byte)( en ) ]= true;  visited[ (byte)( aen ) ]= true;
                    er.Append( singmaster.ee_e[ en ] );
                    pcyen= en;  cyen= ep[ (byte)( en ) ];
                    while( cyen != en && cyen != aen ){ //" trace cycle
                        er.Append( singmaster.ee_e[ cyen ] );
                          if( visited[ (byte)( cyen ) ] ){ er.Append( "~~~" );  break; } // avoid infinite loop if transition tables have a BUG
                        visited[ (byte)( cyen ) ]= true;
                        visited[ (byte)( ssT.eOBV( cyen ) ) ]= true;
                        e1= cyen;
                        pcyen= cyen;  cyen= ep[ (byte)( cyen ) ];
                    } //" trace cycle
                    if ( cyen == en ){ er.Append( '_' ); }
                    else if ( cyen == aen ){ //" report flip attach
                        er.Append( ( singmaster.ee_e[ cyen ] ).ToString().ToLower() );
                        if ( cyen != ssT.eOBV( e1 ) ){ // not an edge in place but flipped
                            if( ep[ (byte)( e0 ) ] != e1 ){ er.Append( '\'' ); } // only include \' if more than 2 in flip cycle
                            er.Append( singmaster.ee_e[ ssT.eOBV( pcyen ) ].ToString().ToLower() );
                        }
                        er.Append( '_' );
                    } //"report flip attach
                    continue;
                } //" report cycle until a repeat if found
            } //" report edge cycles
            #endregion

            System.Text.StringBuilder cr= new System.Text.StringBuilder();

            cf accn= ssT.cCCL( (cf)( 0 ) );  cf acn= ssT.cCL( (cf)( 0 ) ); // corner cubie has two other facelets attached
            cf cycn= (cf)( 0 );
            cf pcycn= (cf)( 0 );

            for ( cf sp= (cf)( 0 ); sp < cf.cf24; sp++ ){ visited[ (byte)( sp ) ]= false; }
            #region report corner cycles
            foreach ( cf cn in singmaster.cc_e ){ //" report corner cycles
                if ( visited[ (byte)( cn ) ] ){ continue; } // ignore if already marked as visited
                else if ( cp[ (byte)( cn ) ] == cn ){ // inplace solved
                    acn= ssT.cCL( cn );  accn= ssT.cCCL( cn );
                    visited[ (byte)( cn ) ]= true;
                    visited[ (byte)( acn ) ]= true;  visited[ (byte)( accn ) ]= true;
                    continue;
                } else { //" report cycle until a repeat is found
                    acn= ssT.cCL( cn );  accn= ssT.cCCL( cn );
                    visited[ (byte)( cn ) ]= true;  visited[ (byte)( acn ) ]= true;  visited[ (byte)( accn ) ]= true;

                    cr.Append( '_' );
                    cr.Append( singmaster.cc_e[ cn ] );
                    pcycn= cn;  cycn= cp[ (byte)( cn ) ];
                    while( cycn != cn && cycn != accn && cycn != acn ){ //" trace cycle
                        cr.Append( singmaster.cc_e[ cycn ] );
                          if( visited[ (byte)( cycn ) ] ){ cr.Append( "~~~" );  break; } // avoid infinite loop if transition tables have a BUG
                        visited[ (byte)( cycn ) ]= true;
                        visited[ (byte)( ssT.cCL( cycn ) ) ]= true;  visited[ (byte)( ssT.cCCL( cycn ) ) ]= true;
                        pcycn= cycn;  cycn= cp[ (byte)( cycn ) ];
                    } //" trace cycle
                    if( cycn == cn ){ // pass
                    } else if( cycn == accn ){
                        cr.Append( ( singmaster.cc_e[ cycn ] ).ToString().ToLower() );
                        cr.Append( '+' );
                        cr.Append( ( singmaster.cc_e[ ssT.cCL( pcycn ) ] ).ToString().ToLower() );
                    } else if( cycn == acn ){
                        cr.Append( ( singmaster.cc_e[ cycn ] ).ToString().ToLower() );
                        cr.Append( '-' );
                        cr.Append( ( singmaster.cc_e[ ssT.cCCL( pcycn ) ] ).ToString().ToLower() );
                    }
                    continue;
                } //" report cycle
            } //" report corner cycles
            #endregion

            // affix sym notation if not in neutral position
            if( (sym != sy.identity) && (sym != sy.c_UF_) ){ cr.Append( " {" + ((syT)( sym )).ToString() + "}" ); }

            if( (object)( shortreport ) == (object)( null ) ){ shortreport= ""; } // bug trap
            if ( er.Length + cr.Length < shortreportlengththreashold && shortreport.Length > 0 ){
                if( accumtwist % 2 == 0 && er.Length + cr.Length != 0 ){ // even corner-edge parity report
                return "(" + rdr.ToString() + er.ToString() + "##" + cr.ToString() + ") " + shortreport ;
            } else { // odd corner-edge pairity report
                return "(" + rdr.ToString() + er.ToString() + "#" + cr.ToString() + ") " + shortreport ;
            }
            } else if( accumtwist % 2 == 0 ){
                return "(" + rdr.ToString() + er.ToString() + "##" + cr.ToString() + ")" ;
            } else {
                return "(" + rdr.ToString() + er.ToString() + "#" + cr.ToString() + ")" ;
            }
        } // string ToString();
    } // class position

    /// <summary> provide for efficiently obtaining statistics about a given position </summary>
    public class measure{
    // given a position, the following is a collection of information that can be obtained about it
        ef[] ep;
        cf[] cp;
        public void reset( ef[] epi, cf[] cpi ){ ep= epi; cp= cpi;  
            //edgestatsvalid= false;  cornerstatsvalid= false;
        }
        public measure( ef[] epi, cf[] cpi ){ ep= epi;  cp= cpi;
            //edgestatsvalid= false;  cornerstatsvalid= false;
        } // initialize posinfo;
        public void invalidatestats(){
            //edgestatsvalid= false;  cornerstatsvalid= false;
        }

        public ef leadcycle( ef ein ){ // return the leadcycle for all elements
            bool[] visited= new bool[ (byte)( ef.ef24 ) ];
            for (ef sp= ssT.ef0; sp < ef.ef24; sp++) { visited[ (byte)( sp ) ]= false; }
            ef en= ein; // exmanined edge cubie position
            ef aen= ssT.eOBV( ein ); // examined edge cubie position attached
            ef cyen= ssT.ef0; // examined cycle edge cubie position
            ef pcyen= ssT.ef0; // examined previous cycle edge cubie position

            while (en < ef.ef24)
            { //" report edge cycles
                if ( visited[ (byte)( en ) ] ) { en++; }
                else if ( ep[ (byte)( en ) ] == en) // TODO count in-placement
                { // ignore, null perm
                    return en;
                }
                else
                { //" report cycle until a repeat is found
                    aen= en; aen= ssT.eOBV( aen );
                    visited[ (byte)( en ) ]= true;  visited[ (byte)( aen ) ]= true;
                    pcyen= en;  cyen= ep[ (byte)( en ) ];

                    while ( cyen != en && cyen != aen )
                    { //" trace cycle
                        visited[ (byte)( cyen ) ]= true;  visited[ (byte)( ssT.eOBV( cyen ) ) ]= true;
                        pcyen= cyen;  cyen= ep[ (byte)( cyen ) ];
                    } //" trace cycle
                    if ( cyen == aen ){ //" report flip attach
                    } //" report flip attach

                    break;
                } //" report cycle
            } //" report edge cycles
            foreach( ef ei in singmaster.ee_e ){
                if( visited[ (byte)( ei ) ] ){ return ei; } }
            return ein;
        }

        public cf leadcycle( cf cin ) {
            bool[] visited= new bool[ (byte)( cf.cf24 ) ];
            for( cf sp= ssT.cf0; sp < cf.cf24; sp++ ){ visited[ (byte)( sp ) ]= false; }

            cf cn= cin; // same but for corner cubie
            cf accn= ssT.cCCL( cn );  cf acn= ssT.cCL( cn ); // corner cubie has two faces attached
            cf cycn= cin;
            cf pcycn= cin;

            for( cf cp= ssT.cf0; cp < cf.cf24; cp++) { visited[ (byte)( cp ) ]= false; }
            while( cn < cf.cf24 )
            { //" report corner cycles
                if ( visited[ (byte)( cn ) ] ) { cn++; } // ignore if already visited
                else if ( cp[ (byte)( cn ) ] == cn ){ return cn; }
                else
                { //" report cycle until a repeat is found
                    accn= ssT.cCL( cn );  acn= ssT.cCCL( cn );
                    visited[ (byte)( cn ) ]= true;  visited[ (byte)( accn ) ]= true;   visited[ (byte)( acn ) ]= true;

                    pcycn= cn; cycn= cp[ (byte)( cn ) ];
                    while ( cycn != cn && cycn != accn && cycn != acn )
                    { //" trace cycle
                        visited[ (byte)( cycn ) ]= true;
                        visited[ (byte)( ssT.cCL( cycn ) ) ]= true;
                        visited[ (byte)( ssT.cCCL( cycn ) ) ]= true;
                        
                        pcycn= cycn;  cycn= cp[ (byte)( cycn ) ];
                    } //" trace cycle
                    break;
                } //" report cycle until a repeat is found
            } //" report corner cycles
            foreach( cf ci in singmaster.cc_e ){
                if( visited[ (byte)( ci ) ] ){ return ci; } }
            return cin;
        }

        internal bool edgestatsvalid;
        #region edge statistics
        byte edgecyclelength;
        byte edgerestarts;
        byte edgefliprestarts;
        byte edgesinplace;
        ef edgecycleinitial;
        ef edgecycletarget;
        ef edgecyclesecondary;

        void measureedgestats() {
            byte edgecyclelengthcount= 0;
            byte edgerestartcount= 0;
            byte edgefliprestartcount= 0;
            byte edgesinplacecount= 0;
            edgecycleinitial= ef.ef24;
            edgecycletarget= ef.ef24;
            edgecyclesecondary= ef.ef24;

            bool[] visited= new bool[ (byte)( ef.ef24 ) ];
            for ( ef sp= ssT.ef0; sp < ef.ef24; sp++ ){ visited[ (byte)( sp ) ]= false; }

            ef lead= singmaster.ee_e.origin; // exmanined edge cubie position
            ef aen= ssT.eOBV( lead ); // examined edge cubie position attached
            ef cyen= lead; // examined cycle edge cubie position
            ef pcyen= lead; // examined previous cycle edge cubie position

            foreach ( ef en in singmaster.ee_e ){ //" report edge cycles
                if ( visited[ (byte)( en ) ] ) { continue; }
                else if ( ep[ (byte)( en ) ] == en ) // TODO count in-placement
                { // ignore, null perm
                    edgesinplacecount+= 1;
                    aen= en; aen= ssT.eOBV( aen );
                    visited[ (byte)( en ) ]= true;  visited[ (byte)( aen ) ]= true;
                    continue;
                }
                else
                { //" report cycle until a repeat is found
                    if( edgecycleinitial == ef.ef24 ){ edgecycleinitial= en;  edgecycletarget= ep[ (byte)( en ) ]; }
                    edgerestartcount += 1;
                    edgecyclelengthcount += 1;
                    /*debug*/
                    if ( edgecyclelengthcount > 32 ){
                        edgecyclelengthcount= 32;  return;  // no edge cycle can be of length 32, error
                    } // added
                    aen= en; aen= ssT.eOBV( aen );
                    visited[ (byte)( en ) ]= true;  visited[ (byte)( aen ) ]= true;
                    pcyen= en;  cyen= ep[ (byte)( en ) ];

                    while ( cyen != en && cyen != aen )
                    { //" trace cycle
                        edgecyclelengthcount += 1;
                        visited[ (byte)( cyen ) ]= true;
                        visited[ (byte)( ssT.eOBV( cyen ) ) ]= true;
                        pcyen= cyen;  cyen= ep[ (byte)( cyen ) ];
                    } //" trace cycle
                    if ( cyen == aen ){ //" report flip attach
                        edgefliprestartcount += 1;
                    } //" report flip attach
                    edgecyclesecondary= pcyen;
                    continue;
                } //" report cycle
            } //" report edge cycles
            edgecyclelength= edgecyclelengthcount;
            edgerestarts= edgerestartcount;
            edgefliprestarts= edgefliprestartcount;
            edgesinplace= edgesinplacecount;
            edgestatsvalid= true;
        }
        #endregion

        internal bool cornerstatsvalid;
        #region corner statistics
        byte cornercyclelength;
        byte cornerrestarts;
        byte cornerfliprestarts;
        byte cornersinplace;
        cf cornercycleinitial;
        cf cornercycletarget;
        cf cornercyclesecondary;

        void measurecornerstats() {
            byte cornercyclelengthcount= 0;
            byte cornerrestartcount= 0;
            byte cornerfliprestartcount= 0;
            byte cornersinplacecount= 0;
            cornercycleinitial= cf.cf24;
            cornercycletarget= cf.cf24;
            cornercyclesecondary= cf.cf24;
            bool[] visited= new bool[ (byte)( cf.cf24 ) ];
            for ( cf sp= ssT.cf0; sp < cf.cf24; sp++) { visited[ (byte)( sp ) ]= false; }

            cf lead= singmaster.cc_e.origin;
            cf accn= ssT.cCCL( lead );  cf acn= ssT.cCL( lead ); // corner cubie has two faces attached
            cf cycn= lead;
            cf pcycn= lead;

            for ( cf cp= ssT.cf0; cp < cf.cf24; cp++ ){ visited[ (byte)( cp ) ]= false; }
            foreach ( cf cn in singmaster.cc_e ){
                if ( visited[ (byte)( cn ) ] ) { continue; } // ignore if already visited
                else if ( cp[ (byte)( cn ) ] == cn )
                { // ignore, null perm
                    cornersinplacecount+= 1;
                    accn= ssT.cCL( cn ); acn= ssT.cCCL( cn );
                        //accn= ((ssT)( cn )).CL;  acn= ((ssT)( cn )).CCL;
                        //((faceletT)(cn)).attachCL; acn= ((faceletT)(cn)).attachCCL;
                    visited[ (byte)( cn ) ]= true;
                    visited[ (byte)( accn ) ]= true;
                    visited[ (byte)( acn ) ]= true;
                    continue;
                }
                else
                { //" report cycle until a repeat is found
                    if( cornercycleinitial == cf.cf24) { cornercycleinitial= cn; cornercycletarget= cp[(byte)(cn)]; }
                    cornerrestartcount += 1;
                    cornercyclelengthcount += 1;
                    /*debug*/
                    if (cornercyclelengthcount > 32)
                    {
                        cornercyclelengthcount= 32; return;
                    } // added
                    accn= ssT.cCL( cn );  acn= ssT.cCCL( cn );
                    visited[ (byte)( cn ) ]= true;  visited[ (byte)( accn ) ]= true;   visited[ (byte)( acn ) ]= true;

                    pcycn= cn; cycn= cp[ (byte)( cn ) ];
                    while ( cycn != cn && cycn != accn && cycn != acn )
                    { //" trace cycle
                        cornercyclelengthcount += 1;
                        visited[ (byte)( cycn ) ]= true;
                        visited[ (byte)( ssT.cCL( cycn ) ) ]= true;
                        visited[ (byte)( ssT.cCCL( cycn ) ) ]= true;
                        pcycn= cycn;  cycn= cp[ (byte)( cycn ) ];
                    } //" trace cycle
                    if ( cycn == acn || cycn == accn ){ cornerfliprestartcount += 1; }
                    cornercyclesecondary= pcycn;
                    continue;
                } //" report cycle until a repeat is found
            } //" report corner cycles
            cornercyclelength= cornercyclelengthcount;
            cornerrestarts= cornerrestartcount;
            cornerfliprestarts= cornerfliprestartcount;
            cornersinplace= cornersinplacecount;
            cornerstatsvalid= true;
        }
        #endregion
        // measurement
        public enum mm{ edgecyclelength, edgerestarts, edgefliprestarts, edgesinplace,
            cornercyclelength, cornerrestarts, cornerfliprestarts, cornersinplace };

        // generate edge or corner statistics at once and hold while _ep_ and _cp_ does not change
        public short measureReport( mm m ){
        switch( m ){
        case mm.edgecyclelength:
            if ( !edgestatsvalid ){ measureedgestats(); }  return edgecyclelength;
        case mm.edgerestarts:
            if ( !edgestatsvalid ){ measureedgestats(); }  return edgerestarts;
        case mm.edgefliprestarts:
            if ( !edgestatsvalid ){ measureedgestats(); }  return edgefliprestarts;
        case mm.edgesinplace:
            if ( !edgestatsvalid ){ measureedgestats(); }  return edgesinplace;

        case mm.cornercyclelength:
            if ( !cornerstatsvalid ){ measurecornerstats(); }  return cornercyclelength;
        case mm.cornerrestarts:
            if ( !cornerstatsvalid ){ measurecornerstats(); }  return cornerrestarts;
        case mm.cornerfliprestarts:
            if ( !cornerstatsvalid ){ measurecornerstats(); }  return cornerfliprestarts;
        case mm.cornersinplace:
            if ( !cornerstatsvalid ){ measurecornerstats(); }  return cornersinplace;
        } return 0; }

        public enum rr{ edgeinitial, edgetarget, edgesecondary, cornerinitial, cornertarget, cornersecondary }; // secondary is first corner cycle end

        public ef reade( rr r ){
            switch (r) {
            case rr.edgeinitial:
                if (!edgestatsvalid) { measureedgestats(); } return edgecycleinitial;
            case rr.edgetarget:
                if (!edgestatsvalid) { measureedgestats(); } return edgecycletarget;
            case rr.edgesecondary:
                if (!edgestatsvalid) { measureedgestats(); } return edgecyclesecondary;
            default: return ef.ef24;
            }
        }

        public cf readc( rr r ){
            switch( r ){
            case rr.cornerinitial:
                if (!cornerstatsvalid) { measurecornerstats(); } return cornercycleinitial;
            case rr.cornertarget:
                if (!cornerstatsvalid) { measurecornerstats(); } return cornercycletarget;
            case rr.cornersecondary:
                if (!cornerstatsvalid) { measurecornerstats(); } return cornercyclesecondary;
            default: return cf.cf24;
            }
        }
    } // measure // provide for efficiently generating stastics for a given _pos_

    public static class singmaster
    { // for now also includes letter system
        public static string lettersystemname;
        public static byte lettersystemserialnumber;
        const byte pmSingmasterRange= (byte)( (byte)( pm.dd ) + 1 );
        public static ef[ , ] singmaster_e= new ef[ pmSingmasterRange, pmSingmasterRange ];
        public static cf[ , , ] singmaster_c= new cf[ pmSingmasterRange, pmSingmasterRange, pmSingmasterRange ];
        public static dr sro= dr.c_UF_; // scramble to read orientation, activate on command k10
        public static bool sro_UF= true; // false ~ @UF=DR form -- true ~ @DR=UF form

        /// <summary> generate singmaster_e and singmaster_c decode tables </summary>
        public static void decodeSingmasterIni(){ // populate ef[,] and cf[,,]
            byte b;
            pm p1;  pm p2;  pm p3;
            const ef e0= (ef)( 0 );
            for( ef ei= e0; ei < ef.ef24; ei++ ){ b= (byte)( ei );
                p1= ssT.ecf_pm( ei );  p2= ssT.ecf_pm( ssT.eOBV( ei ) );
                singmaster_e[ (byte)( p1 ), (byte)( p2 ) ]= ei;
            }
            const cf c0= (cf)( 0 );
            for( cf ci= c0; ci < cf.cf24; ci++ ){ b= (byte)( ci );
                p1= ssT.ecf_pm( ci );  p2= ssT.ecf_pm( ssT.cCL( ci ) );  p3= ssT.ecf_pm( ssT.cCCL( ci ) );
                singmaster_c[ (byte)( p1 ), (byte)( p2 ), (byte)( p3 ) ]= ci;
                singmaster_c[ (byte)( p1 ), (byte)( p3 ), (byte)( p2 ) ]= ci;
                // also accept the reverse-oriented (counter clockwise) corner name as an alias for the (clockwise) standard name
            }
        }

        const byte ecfEntireRange= (byte)( (byte)( ef.ef24 ) + 1 ); // TODO remove _static_ clause and place to session for session letters
        const byte ecfIncludedRange= (byte)( ef.ef24 );
        internal static char[] ename= new char[ ecfEntireRange ]; // ein to char
        internal static char[] cname= new char[ ecfEntireRange ]; // cin to char

        /// <summary> all of the edge letter names assigned </summary>
        internal static SortedList< char, ef > namee= new SortedList< char, ef >( ecfEntireRange ); // TODO remove _static_ as above
        /// <summary> all of the corner letter names assigned </summary>
        internal static SortedList< char, cf > namec= new SortedList< char, cf >( ecfEntireRange );

        /// <summary> Singmaster letter name decoding parse state (mode) </summary>
        enum assign_m { space, singmaster2, singmaster3, equals, letter2, letter3, 
            origin, originequals, originsingmaster2, originsingmaster3,
            readingorientation, readingorientation2, readingorientationequals, readingorientation3, readingorientation4,
            serialnumber, serialnumberequals, namequote,
        };

        /// <summary> specify the lettering system _lsname_ </summary>
        /// <param name="lsname">letter system name</param>
        /// <param name="singmasterNameSpec">letter system assignments</param>
        public static void assignLetters( string lsname, string singmasterNameSpec ){
            System.Text.StringBuilder namebuild= new System.Text.StringBuilder();
            lettersystemname= lsname;
            assign_m sms= assign_m.space;
            pmT singp1= pm.pm60;  pmT singp2= pm.pm60;  pmT singp3= pm.pm60;
            char ch1= '\0'; char ch2= '\0'; char ch3= '\0';

            SortedList< char, ef > eNamesortLocal= new SortedList< char, ef >( ecfIncludedRange );
            SortedList< char, cf > cNamesortLocal= new SortedList< char, cf >( ecfIncludedRange );
            ef ed= ef.ef24;
            cf cd= cf.cf24;
            foreach ( char ch in singmasterNameSpec + " " ){ //" evaluate each char in specification string
                #region parse names
                switch ( sms ){
                case assign_m.space:
                    ch1= '\0';  ch2= '\0';  ch3= '\0';
                    singp1= pm.pm60;  singp2= pm.pm60;  singp3= pm.pm60;
                    if( System.Char.IsSeparator( ch ) || ch == '\n' ){ continue; }
                    if( ch == 'o' ){ // assign origin or buffer
                        sms= assign_m.origin;
                        continue;
                    }
                    if( ch == 's' ){ // assign serial number
                        sms= assign_m.serialnumber;
                        continue;
                    }
                    if( ch == '\"' ){ // assign letter-system name
                        if( namebuild.Length > 0 ){ namebuild.Remove( 0, namebuild.Length-1 ); }
                        sms= assign_m.namequote;
                        continue;
                    }
                    if( ch == '@' ){ // origin specification @UL=DR or @DR=UL
                        sms= assign_m.readingorientation;
                        continue;
                    }
                    singp1= parse.parse0( ch );  
                    if( (pmSect)( singp1 ) == pmSect.singmaster ){ 
                        sms= assign_m.singmaster2; }
                    continue;
                case assign_m.singmaster2:
                    if( System.Char.IsSeparator( ch ) || ch == '\n' ){ continue; }
                    singp2= parse.parse0( ch );
                    if( (pmSect)( singp2 ) == pmSect.singmaster ){
                        sms= assign_m.singmaster3; }
                    continue;
                case assign_m.singmaster3:
                    if( System.Char.IsSeparator( ch ) || ch == '\n') { continue; }
                    if( ch == '=' ) { sms= assign_m.equals;  continue; }
                    singp3= parse.parse0( ch );
                    if( (pmSect)( singp3 ) != pmSect.singmaster ){ singp3= pm.pm60; }
                    continue;
                case assign_m.origin:
                    if( ch == '=' ){ sms= assign_m.originequals; }
                    continue;
                case assign_m.originequals:
                    ch1 = '\0';  ch2= '\0';  ch3= '\0';
                    singp1= pm.pm60;  singp2= pm.pm60;  singp3= pm.pm60;
                    if( System.Char.IsSeparator( ch ) || ch == '\n' ){ continue; }
                    singp1= parse.parse0( ch );
                    if( (pmSect)( singp1 ) == pmSect.singmaster ){
                        sms= assign_m.originsingmaster2; }
                    continue;
                case assign_m.serialnumber:
                    if( ch == '=' ){ sms= assign_m.serialnumberequals; }
                    continue;
                case assign_m.serialnumberequals:
                    if( char.IsDigit( ch )
                        && byte.TryParse( ch.ToString(), out lettersystemserialnumber ) ){
                        sms= assign_m.space; } else { sms= assign_m.space; }
                    continue;
                case assign_m.namequote:
                    if( ch == '\"' ){
                        lettersystemname= namebuild.ToString();
                        sms= assign_m.space;
                    } else { namebuild.Append( ch ); }
                    continue;
                case assign_m.originsingmaster2:
                    if( System.Char.IsSeparator( ch ) || ch == '\n' ){ continue; }
                    singp2= parse.parse0( ch );
                    if( (pmSect)( singp2 ) == pmSect.singmaster ){
                        sms= assign_m.originsingmaster3; }
                    continue;
                case assign_m.originsingmaster3:
                    if( System.Char.IsSeparator( ch ) || ch == '\n' ){ goto assignorgin; }
                    singp3= parse.parse0( ch );
                    continue;
                case assign_m.equals:
                    if( System.Char.IsSeparator( ch ) || ch == '\n') { continue; }
                    ch1= ch; sms= assign_m.letter2; continue;
                case assign_m.letter2:
                    if( System.Char.IsSeparator( ch ) || ch == '\n') { goto assignlettername; }
                    ch2= ch; sms= assign_m.letter3; continue;
                case assign_m.letter3:
                    if ( System.Char.IsSeparator( ch ) || ch == '\n') { goto assignlettername; }
                    ch3= ch; continue;
                case assign_m.readingorientation:
                    ch1= '\0';  ch2= '\0';    singp1= pm.pm60;  singp2= pm.pm60;
                    if( System.Char.IsSeparator( ch ) || ch == '\n') { continue; }
                    singp1= parse.parse0( ch );
                    if( (pmSect)( singp1 ) == pmSect.singmaster ){ sms= assign_m.readingorientation2; }
                    continue;
                case assign_m.readingorientation2:
                    if( System.Char.IsSeparator( ch ) || ch == '\n' ){ continue; }
                    singp2= parse.parse0( ch );
                    if( (pmSect)( singp2 ) == pmSect.singmaster ){
                        ed= singmaster.singmaster_e[ (byte)( singp1 ), (byte)( singp2 ) ];
                        sms= assign_m.readingorientationequals;
                    }
                    continue;
                case assign_m.readingorientationequals:
                    if( System.Char.IsSeparator( ch ) || ch == '\n' ){ continue; }
                    if( ch == '=' ){ 
                        singp1= pm.pm60;  singp2= pm.pm60;
                        sms= assign_m.readingorientation3; }
                    continue;
                case assign_m.readingorientation3:
                    if( System.Char.IsSeparator( ch ) || ch == '\n' ){ continue; }
                    singp1= parse.parse0( ch );
                    if( (pmSect)( singp1 ) == pmSect.singmaster ){ sms= assign_m.readingorientation4; }
                    continue;
                case assign_m.readingorientation4:
                    if( System.Char.IsSeparator( ch ) || ch == '\n' ){ continue; }
                    singp2= parse.parse0( ch );
                    if( (pmSect)( singp2 ) == pmSect.singmaster ){ goto assignreadingorientation; }
                    continue;
                }
                #endregion
            assignlettername:
                if ( singp3 == pm.pm60 ){ // edge form
                    ed= singmaster.singmaster_e[ (byte)( singp1 ), (byte)( singp2 ) ];
                    singmaster.ename[ (byte)( ed ) ]= ch1;  eNamesortLocal.Add( ch1, ed );
                    if ( ch2 == '\0' ){ continue; }
                    ed= singmaster.singmaster_e[ (byte)( singp2 ), (byte)( singp1 ) ];
                    singmaster.ename[ (byte)( ed ) ]= ch2;  eNamesortLocal.Add( ch2, ed );
                    sms= assign_m.space;
                    continue;
                }
                else
                { //" corner form
                    cd= singmaster.singmaster_c[ (byte)( singp1 ), (byte)( singp2 ), (byte)( singp3 ) ];
                    singmaster.cname[ (byte)( cd ) ]= ch1; cNamesortLocal.Add( ch1, cd );
                    if ( ch2 == '\0' ){ continue; }
                    cd= singmaster.singmaster_c[ (byte)( singp2 ), (byte)( singp3 ), (byte)( singp1 ) ];
                    singmaster.cname[ (byte)( cd ) ]= ch2;  cNamesortLocal.Add( ch2, cd );
                    if ( ch3 == '\0' ) { continue; }
                    cd= singmaster.singmaster_c[ (byte)( singp3 ), (byte)( singp1 ), (byte)( singp2 )];
                    singmaster.cname[ (byte)( cd ) ]= ch3;  cNamesortLocal.Add( ch3, cd );
                    sms= assign_m.space;
                    continue;
                } //" corner form
            assignorgin:
                if( singp3 == pm.pm60 ){ // edgeform
                    ed= singmaster.singmaster_e[ (byte)( singp1 ), (byte)( singp2 ) ];
                    singmaster.ee_e.origin= ed;
                    sms= assign_m.space;
                    continue;
                }
                else
                { //" corner form
                    cd= singmaster.singmaster_c[ (byte)( singp1 ), (byte)( singp2 ), (byte)( singp3 ) ];
                    singmaster.cc_e.origin= cd;
                    sms= assign_m.space;
                    continue;
                }
            assignreadingorientation:
                if( ed == ef.ci_UF ){ // first type @UF=DR
                    sro= (dr)( singmaster.singmaster_e[ (byte)( singp1 ), (byte)( singp2 ) ] );
                    sro_UF= false; }
                else if( singmaster.singmaster_e[ (byte)( singp1 ), (byte)( singp2 ) ] == ef.ci_UF ){
                    sro= drT.invert( (dr)( ed ) );
                    sro_UF= true; }
                sms= assign_m.space;
                continue;
            } //" evaluate each char in specification string
            byte letterpos= 0;
            singmaster.namee.Clear();
            foreach ( char ch in eNamesortLocal.Keys ){
                singmaster.eorder[ letterpos ]= eNamesortLocal[ ch ];
                singmaster.namee.Add( ch, eNamesortLocal[ ch ] );
                singmaster.eorderoverride[ letterpos ]= letterpos;  letterpos++;
            } // identity ordering
            letterpos = 0;
            singmaster.namec.Clear();
            foreach ( char ch in cNamesortLocal.Keys ){
                singmaster.corder[ letterpos ]= cNamesortLocal[ ch ];
                singmaster.corderoverride[ letterpos ]= letterpos;
                singmaster.namec.Add( ch, cNamesortLocal[ ch ] );
                letterpos++;
            } // identity ordering
        }    
        public static void assignLetters( short lettersystemserial ){
            lettersystemcatalog lsc = catalog.lscatalog;
            do{
                if( lsc.serialno == lettersystemserial ){
                    singmaster.assignLetters( lsc.name, lsc.assignments );
                    break;
                }
                lsc= lsc.prev;
            } while ( lsc != null );
        }

        public static ef[] eorder= new ef[ ecfEntireRange ]; // for sorting assigned letter names to give a presentation order
        public static cf[] corder= new cf[ ecfEntireRange ];
        public static byte[] eorderoverride= new byte[ ecfEntireRange ];
        public static byte[] corderoverride= new byte[ ecfEntireRange ];
        public static void resetorderoverride(){
            ee_e.overrideorigin= ef.ef24;  cc_e.overrideorigin= cf.cf24;
            for( byte bi= 0; bi < (byte)( ef.ef24 ); bi++ ){
                eorderoverride[ bi ]= bi;  corderoverride[ bi ]= bi;
        }}
        public static void orderoverrideset( ef from, ef to ){
            byte fromb= 255;  byte tob= 255;
            for( byte bi= 0; bi < (byte)( ef.ef24 ); bi++ ){
                if( eorder[ eorderoverride[ bi ] ] == from ){ fromb= bi;  if( tob != 255 ){ break; } }
                if( eorder[ eorderoverride[ bi ] ] == to ){ tob= bi;  if( fromb != 255 ){ break; } }
            }
            byte holdfrom= eorderoverride[ fromb ];
            eorderoverride[ fromb ]= eorderoverride[ tob ];
            eorderoverride[ tob ]= holdfrom;
            if( fromb == 0 ){ // overridebuffer
                ee_e.overrideorigin= to;
            }
        }
        public static void orderoverrideset( cf from, cf to ){
            byte fromb= 255;  byte tob= 255;
            for( byte bi= 0; bi < (byte)( cf.cf24 ); bi++ ){
                if( corder[ corderoverride[ bi ] ] == from ){ fromb= bi;  if( tob != 255 ){ break; } }
                if( corder[ corderoverride[ bi ] ] == to ){ tob= bi;  if( fromb != 255 ){ break; } }
            }
            byte holdfrom= corderoverride[ fromb ];
            corderoverride[ fromb ]= corderoverride[ tob ];
            corderoverride[ tob ]= holdfrom;
            if( fromb == 0 ){ // overridebuffer
                cc_e.overrideorigin= to;
            }
        }

        /// <summary> char name to e translations; also an alterable alphabetical ordering by letter name </summary>
        public class ee:  System.Collections.Generic.IEnumerable< ef >{
            // Letters eeLetters;
            // internal ef buffer; // copy of form buffer should display anchor only if it does not equal buffer here
            internal ef origin; // TODO also include an ending list of buffer lead values overriding i.e. collect in-place flips
            internal ef overrideorigin= ef.ef24; // temporary display anchor for repositionings TODO report singmaster buffer name
            public bool isoriginoverride{ get{ return origin != ef.ef24 /* && anchor != buffer */ ; }}
            public ef this[ char ch ]{ get{
                if( singmaster.namee.ContainsKey( ch )) {
                    return singmaster.namee[ ch ]; } else { return ef.ef24; }
            }}
            public ef this[ byte b ]{ get{
                if ( b > ecfIncludedRange ){ return ef.ef24; }
                return singmaster.eorder[ singmaster.eorderoverride[ b ] ]; }}
            public char this[ ef of_e ]{ get{
                if ( (byte)( of_e ) == ecfIncludedRange ){ return '_'; }
                return singmaster.ename[ (byte)( of_e ) ]; } }
            //internal ee( Letters ineeLetters ){ eeLetters= ineeLetters; }

            public System.Collections.Generic.IEnumerator< ef > GetEnumerator() {
                e_enr einspan= new e_enr( this );
                einspan.Reset();
                return (IEnumerator< ef >)( einspan );
            }
            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
                e_enr einspan= new e_enr( this );
                einspan.Reset();
                return (IEnumerator< ef >)( einspan );
            }

            public bool ContainsKey( char Key ){
                return singmaster.namee.ContainsKey( Key );
            }
        }

        /// <summary> letter name edges enumerator </summary>
        public class e_enr: System.Collections.Generic.IEnumerator< ef >{
            ee refee;
            const byte oneBeforeOriginPos= 254;
            const byte returnPosOrigin= 255;
            byte posinorder= returnPosOrigin; // code to read from buffer
            public void Reset(){ posinorder= 254; } // code one before initializiation point
            public bool MoveNext(){
                if( posinorder == oneBeforeOriginPos && refee.origin != ef.ef24 ){ posinorder= returnPosOrigin;  return true; }
                else if( posinorder >= oneBeforeOriginPos ){ posinorder= 0; } // fix: need to check for buffer or override buffer
                else { posinorder += 1; }
                if( posinorder >= ecfIncludedRange ){ return false; }
                if( refee[ posinorder ] == refee.overrideorigin ){ posinorder += 1; }
                else if( refee.overrideorigin == ef.ef24 && refee[ posinorder ] == refee.origin ){ posinorder += 1; }
                return posinorder < ecfIncludedRange ;
            }
            public ef Current{ get{
                if( posinorder == returnPosOrigin ){
                    if( refee.overrideorigin != ef.ef24 ){ return refee.overrideorigin; } else { return refee.origin; }}
                return refee[ posinorder ];
            }}
            object System.Collections.IEnumerator.Current { get {
                if( posinorder == returnPosOrigin ){
                    if( refee.overrideorigin != ef.ef24 ){ return refee.overrideorigin; } else { return refee.origin; }}
                return refee[ posinorder ];
            }}
            void System.IDisposable.Dispose() { }
            internal e_enr( ee inee ){ refee= inee; }
        }
        public static ee ee_e = new ee();
        public static e_enr e_enri= new e_enr( ee_e )  ;

       /// <summary> char name to c translations; also an alterable alphabetical ordering by letter name </summary>
        public class cc: System.Collections.Generic.IEnumerable< cf >{
            //Letters ccLetters;
            //internal cf buffer; // copy of form buffer should display anchor only if it does not equal buffer here
            internal cf origin;
            internal cf overrideorigin= cf.cf24;
            public bool isoriginoverride { get { 
                return origin != cf.cf24 /* && anchor != buffer */ ; }}
            // public c anchor { get { return anchorbuffer; } set { anchorbuffer= value; } }
            public cf this[ char ch ]{ get{
                if ( singmaster.namec.ContainsKey( ch )) {
                    return singmaster.namec[ ch ]; } else { return cf.cf24; }
            }}
            public cf this[ byte b ]{ get{
                if ( b >= (byte)( cf.cf24 ) ){ return cf.cf24; }
                return singmaster.corder[ singmaster.corderoverride[ b ] ]; }}
            public char this[ cf of_c ]{ get{
                if ( of_c == cf.cf24 ){ return '_'; }
                return singmaster.cname[ (byte)( of_c ) ]; } }
            //internal cc( Letters inccLetters ){ ccLetters= inccLetters; }

            public System.Collections.Generic.IEnumerator< cf > GetEnumerator() {
                c_enr cinspan= new c_enr( this );
                cinspan.Reset();
                return (IEnumerator< cf >)( cinspan );
            }
            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
                c_enr einspan= new c_enr( this );
                einspan.Reset();
                return (IEnumerator< cf >)( einspan );
            }

            public bool ContainsKey( char Key ){
                return singmaster.namec.ContainsKey( Key );
            }
        }

        /// <summary> letter name corners enumerator </summary>
        public class c_enr: System.Collections.Generic.IEnumerator< cf >{
            cc refcc;
            const byte oneBeforeOriginPos= 254;
            const byte returnPosOrigin= 255;
            byte posinorder= returnPosOrigin;
            public void Reset(){ posinorder= oneBeforeOriginPos; }
            public bool MoveNext(){
                if( posinorder == oneBeforeOriginPos && refcc.origin != cf.cf24 ){ posinorder= returnPosOrigin;  return true; }
                else if ( posinorder >= oneBeforeOriginPos ){ posinorder= 0; } // fix: need to check for buffer or override buffer
                else { posinorder += 1; }
                if( posinorder >= (byte)( cf.cf24 ) ){ return false; }
                if( refcc[ posinorder ] == refcc.overrideorigin ){ posinorder += 1; }
                else if( refcc.overrideorigin == cf.cf24 && refcc[ posinorder ] == refcc.origin ){ posinorder += 1; }
                return posinorder < (byte)( cf.cf24 );
            }
            public cf Current{ get{
                if( posinorder == returnPosOrigin ){
                    if (refcc.overrideorigin != cf.cf24) { return refcc.overrideorigin; }
                    return refcc.origin; }
                return refcc[ posinorder ];
            }}
            object System.Collections.IEnumerator.Current { get {
                if( posinorder == returnPosOrigin ){
                    if (refcc.overrideorigin != cf.cf24) { return refcc.overrideorigin; }
                    return refcc.origin; }
                return refcc[ posinorder ];
            }}
            void System.IDisposable.Dispose() { }
            internal c_enr( cc incc ){ refcc= incc; }
        }
        public static cc cc_e= new cc();
        public static c_enr c_enri= new c_enr( cc_e );

        public static SortedList< short, List< ft > > sessionMemory= new SortedList< short, List< ft >>();
        public static SortedList< string, List< ft > > sessionNameMemory= new SortedList<string,List< ft >>();
    } // \ class singmaster

    public enum sy
    { // all symmetry positions disposition_rotations(24) x reflect(2) x inverse(2)
        identity, inverse, reflect, invReflect, // explicit operator aliases and section names

        c_UF_, d_UL_y3, a_UB_y2, b_UR_y1, // no inverse and no reflect
        e_LU_x3z1, f_LF_z1, g_LD_x1z1, h_LB_x2z1,
        i_FU_x1y2, j_FR_x1y1, k_FD_x1, l_FL_x1y3,
        m_RU_x3z3, n_RB_y2z, o_RD_x1z3, p_RF_z3,
        q_BU_x3, r_BL_x3y3, s_BD_x3y2, t_BR_x3y1,
        u_DF_z2, v_DR_x2y1, w_DB_x2, x_DL_x2y3,

        I_c_UF_, I_d_UL_y3, I_a_UB_y2, I_b_UR_y1, // inverse and no reflect
        I_e_LU_x3z1, I_f_LF_z1, I_g_LD_x1z1, I_h_LB_x2z1,
        I_i_FU_x1y2, I_j_FR_x1y1, I_k_FD_x1, I_l_FL_x1y3,
        I_m_RU_x3z3, I_n_RB_y2z, I_o_RD_x1z3, I_p_RF_z3,
        I_q_BU_x3, I_r_BL_x3y3, I_s_BD_x3y2, I_t_BR_x3y1,
        I_u_DF_z2, I_v_DR_x2y1, I_w_DB_x2, I_x_DL_x2y3,

        R_c_UF_, R_d_UL_y3, R_a_UB_y2, R_b_UR_y1, // no inverse and reflect
        R_e_LU_x3z1, R_f_LF_z1, R_g_LD_x1z1, R_h_LB_x2z1,
        R_i_FU_x1y2, R_j_FR_x1y1, R_k_FD_x1, R_l_FL_x1y3,
        R_m_RU_x3z3, R_n_RB_y2z, R_o_RD_x1z3, R_p_RF_z3,
        R_q_BU_x3, R_r_BL_x3y3, R_s_BD_x3y2, R_t_BR_x3y1,
        R_u_DF_z2, R_v_DR_x2y1, R_w_DB_x2, R_x_DL_x2y3,

        IR_c_UF_, IR_d_UL_y3, IR_a_UB_y2, IR_b_UR_y1, // inverse and reflect
        IR_e_LU_x3z1, IR_f_LF_z1, IR_g_LD_x1z1, IR_h_LB_x2z1,
        IR_i_FU_x1y2, IR_j_FR_x1y1, IR_k_FD_x1, IR_l_FL_x1y3,
        IR_m_RU_x3z3, IR_n_RB_y2z, IR_o_RD_x1z3, IR_p_RF_z3,
        IR_q_BU_x3, IR_r_BL_x3y3, IR_s_BD_x3y2, IR_t_BR_x3y1,
        IR_u_DF_z2, IR_v_DR_x2y1, IR_w_DB_x2, IR_x_DL_x2y3,

        sy100 // range end mark
    };

    public class syT{
        sy sy_subject= sy.identity;
        #region boxing/unboxing
        public static implicit operator syT( sy valuesy ){
            syT syTr= new syT();
            syTr.sy_subject= valuesy;
            return syTr;
        }
        public static implicit operator sy( syT valuesyT ){
            return valuesyT.sy_subject;
        }
        public static explicit operator byte( syT valuesyT ){
            return (byte)( valuesyT.sy_subject );
        }
        public void Copy( sy from ){
            sy_subject= from;
        }
        #endregion

        const byte syEntireRange= (byte)( (byte)( sy.sy100 ) + 1 );
        // reference
        #if input_inverse_reflect
        C: xx' == xx'   -- k2 --> xx'   :C
        B: y3 -- y1     -- k2 --> y'    :D
        A: y2 == y2     -- k2 --> y2    :A
        D: y1 -- y3     -- k2 --> y     :B

        Q: x'z -- xy'   -- k2 --> x'z'  :I
        T: z -- z'      -- k2 --> z     :J
        S: xz -- x'y    -- k2 --> x z'  :K
        R: x2z == x2 z  -- k2 --> x2 z' :L

        E: xy2 == xy2   -- k2 --> x y2  :E
        H: xy -- x'z'   -- k2 --> x y'  :F  
        G: x -- x'      -- k2 --> x     :G
        F: xy' -- x'z   -- k2 --> x y   :H

        I: x'z' -- xy   -- k2 --> x' z  :Q
        L: y2z == y2z   -- k2 --> y2 z' :R
        K: xz' -- x'y'  -- k2 --> x z   :S
        J: z' -- z      -- k2 --> z     :T

        M: x' -- x      -- k2 --> x'    :M
        P: x'y' -- xz'  -- k2 --> x' y  :N
        O: x'y2 == x'y2 -- k2 --> x' y2 :O 
        N: x'y -- xz    -- k2 --> x' y' :P

        U: z2 == z2     -- k2 --> z2    :U
        Y: x2y == x2y   -- k2 --> x2 y' :V
        W: x2 == x2     -- k2 --> x2    :W
        V: x2y3 == x2y' -- k2 --> x2 y  :Y
        #endif
        #if identity_reference
        static sy[] identity= new sy[ syEntireRange ]{
            sy.identity, sy.inverse, sy.reflect, sy.invReflect, // explicit operator aliases and section names

        sy.c_UF_, sy.d_UL_y3, sy.a_UB_y2, sy.b_UR_y1, // no inverse and no reflect
        sy.e_LU_x3z1, sy.f_LF_z1, sy.g_LD_x1z1, sy.h_LB_x2z1,
        sy.i_FU_x1y2, sy.j_FR_x1y1, sy.k_FD_x1, sy.l_FL_x1y3,
        sy.m_RU_x3z3, sy.n_RB_y2z, sy.o_RD_x1z3, sy.p_RF_z3,
        sy.q_BU_x3, sy.r_BL_x3y3, sy.s_BD_x3y2, sy.t_BR_x3y1,
        sy.u_DF_z2, sy.v_DR_x2y1, sy.w_DB_x2, sy.x_DL_x2y3,

        sy.I_c_UF_, sy.I_d_UL_y3, sy.I_a_UB_y2, sy.I_b_UR_y1, // inverse and no reflect
        sy.I_e_LU_x3z1, sy.I_f_LF_z1, sy.I_g_LD_x1z1, sy.I_h_LB_x2z1,
        sy.I_i_FU_x1y2, sy.I_j_FR_x1y1, sy.I_k_FD_x1, sy.I_l_FL_x1y3,
        sy.I_m_RU_x3z3, sy.I_n_RB_y2z, sy.I_o_RD_x1z3, sy.I_p_RF_z3,
        sy.I_q_BU_x3, sy.I_r_BL_x3y3, sy.I_s_BD_x3y2, sy.I_t_BR_x3y1,
        sy.I_u_DF_z2, sy.I_v_DR_x2y1, sy.I_w_DB_x2, sy.I_x_DL_x2y3,

        sy.R_c_UF_, sy.R_d_UL_y3, sy.R_a_UB_y2, sy.R_b_UR_y1, // no inverse and reflect
        sy.R_e_LU_x3z1, sy.R_f_LF_z1, sy.R_g_LD_x1z1, sy.R_h_LB_x2z1,
        sy.R_i_FU_x1y2, sy.R_j_FR_x1y1, sy.R_k_FD_x1, sy.R_l_FL_x1y3,
        sy.R_m_RU_x3z3, sy.R_n_RB_y2z, sy.R_o_RD_x1z3, sy.R_p_RF_z3,
        sy.R_q_BU_x3, sy.R_r_BL_x3y3, sy.R_s_BD_x3y2, sy.R_t_BR_x3y1,
        sy.R_u_DF_z2, sy.R_v_DR_x2y1, sy.R_w_DB_x2, sy.R_x_DL_x2y3,

        sy.IR_c_UF_, sy.IR_d_UL_y3, sy.IR_a_UB_y2, sy.IR_b_UR_y1, // inverse and reflect
        sy.IR_e_LU_x3z1, sy.IR_f_LF_z1, sy.IR_g_LD_x1z1, sy.IR_h_LB_x2z1,
        sy.IR_i_FU_x1y2, sy.IR_j_FR_x1y1, sy.IR_k_FD_x1, sy.IR_l_FL_x1y3,
        sy.IR_m_RU_x3z3, sy.IR_n_RB_y2z, sy.IR_o_RD_x1z3, sy.IR_p_RF_z3,
        sy.IR_q_BU_x3, sy.IR_r_BL_x3y3, sy.IR_s_BD_x3y2, sy.IR_t_BR_x3y1,
        sy.IR_u_DF_z2, sy.IR_v_DR_x2y1, sy.IR_w_DB_x2, sy.IR_x_DL_x2y3,

        sy.sy100 }; // range end mark
        #endif
        static sy[] inverse_= new sy[ syEntireRange ]{
        #region inverse_
            sy.inverse, sy.identity, sy.invReflect, sy.reflect, // explicit operator aliases and section names

        sy.I_c_UF_, sy.I_d_UL_y3, sy.I_a_UB_y2, sy.I_b_UR_y1, // inverse and no reflect
        sy.I_e_LU_x3z1, sy.I_f_LF_z1, sy.I_g_LD_x1z1, sy.I_h_LB_x2z1,
        sy.I_i_FU_x1y2, sy.I_j_FR_x1y1, sy.I_k_FD_x1, sy.I_l_FL_x1y3,
        sy.I_m_RU_x3z3, sy.I_n_RB_y2z, sy.I_o_RD_x1z3, sy.I_p_RF_z3,
        sy.I_q_BU_x3, sy.I_r_BL_x3y3, sy.I_s_BD_x3y2, sy.I_t_BR_x3y1,
        sy.I_u_DF_z2, sy.I_v_DR_x2y1, sy.I_w_DB_x2, sy.I_x_DL_x2y3,

        sy.c_UF_, sy.d_UL_y3, sy.a_UB_y2, sy.b_UR_y1, // no inverse and no reflect
        sy.e_LU_x3z1, sy.f_LF_z1, sy.g_LD_x1z1, sy.h_LB_x2z1,
        sy.i_FU_x1y2, sy.j_FR_x1y1, sy.k_FD_x1, sy.l_FL_x1y3,
        sy.m_RU_x3z3, sy.n_RB_y2z, sy.o_RD_x1z3, sy.p_RF_z3,
        sy.q_BU_x3, sy.r_BL_x3y3, sy.s_BD_x3y2, sy.t_BR_x3y1,
        sy.u_DF_z2, sy.v_DR_x2y1, sy.w_DB_x2, sy.x_DL_x2y3,

        sy.IR_c_UF_, sy.IR_d_UL_y3, sy.IR_a_UB_y2, sy.IR_b_UR_y1, // inverse and reflect
        sy.IR_e_LU_x3z1, sy.IR_f_LF_z1, sy.IR_g_LD_x1z1, sy.IR_h_LB_x2z1,
        sy.IR_i_FU_x1y2, sy.IR_j_FR_x1y1, sy.IR_k_FD_x1, sy.IR_l_FL_x1y3,
        sy.IR_m_RU_x3z3, sy.IR_n_RB_y2z, sy.IR_o_RD_x1z3, sy.IR_p_RF_z3,
        sy.IR_q_BU_x3, sy.IR_r_BL_x3y3, sy.IR_s_BD_x3y2, sy.IR_t_BR_x3y1,
        sy.IR_u_DF_z2, sy.IR_v_DR_x2y1, sy.IR_w_DB_x2, sy.IR_x_DL_x2y3,

        sy.R_c_UF_, sy.R_d_UL_y3, sy.R_a_UB_y2, sy.R_b_UR_y1, // no inverse and reflect
        sy.R_e_LU_x3z1, sy.R_f_LF_z1, sy.R_g_LD_x1z1, sy.R_h_LB_x2z1,
        sy.R_i_FU_x1y2, sy.R_j_FR_x1y1, sy.R_k_FD_x1, sy.R_l_FL_x1y3,
        sy.R_m_RU_x3z3, sy.R_n_RB_y2z, sy.R_o_RD_x1z3, sy.R_p_RF_z3,
        sy.R_q_BU_x3, sy.R_r_BL_x3y3, sy.R_s_BD_x3y2, sy.R_t_BR_x3y1,
        sy.R_u_DF_z2, sy.R_v_DR_x2y1, sy.R_w_DB_x2, sy.R_x_DL_x2y3,
        #endregion
        sy.sy100 };
        public static sy inverse( sy syi ){ return inverse_[ (byte)( syi ) ]; }

        static sy[] section_= new sy[ syEntireRange ]{
        #region section
        sy.identity, sy.inverse, sy.reflect, sy.invReflect, // explicit operator aliases and section names

        sy.identity, sy.identity, sy.identity, sy.identity,
        sy.identity, sy.identity, sy.identity, sy.identity,
        sy.identity, sy.identity, sy.identity, sy.identity,
        sy.identity, sy.identity, sy.identity, sy.identity,
        sy.identity, sy.identity, sy.identity, sy.identity,
        sy.identity, sy.identity, sy.identity, sy.identity,

        sy.inverse, sy.inverse, sy.inverse, sy.inverse,
        sy.inverse, sy.inverse, sy.inverse, sy.inverse,
        sy.inverse, sy.inverse, sy.inverse, sy.inverse,
        sy.inverse, sy.inverse, sy.inverse, sy.inverse,
        sy.inverse, sy.inverse, sy.inverse, sy.inverse,
        sy.inverse, sy.inverse, sy.inverse, sy.inverse,

        sy.reflect, sy.reflect, sy.reflect, sy.reflect,
        sy.reflect, sy.reflect, sy.reflect, sy.reflect,
        sy.reflect, sy.reflect, sy.reflect, sy.reflect,
        sy.reflect, sy.reflect, sy.reflect, sy.reflect,
        sy.reflect, sy.reflect, sy.reflect, sy.reflect,
        sy.reflect, sy.reflect, sy.reflect, sy.reflect,

        sy.invReflect, sy.invReflect, sy.invReflect, sy.invReflect,
        sy.invReflect, sy.invReflect, sy.invReflect, sy.invReflect,
        sy.invReflect, sy.invReflect, sy.invReflect, sy.invReflect,
        sy.invReflect, sy.invReflect, sy.invReflect, sy.invReflect,
        sy.invReflect, sy.invReflect, sy.invReflect, sy.invReflect,
        sy.invReflect, sy.invReflect, sy.invReflect, sy.invReflect,
        #endregion
        sy.sy100 };
        public static sy tosection( sy syi ){ return section_[ (byte)( syi ) ]; }

        static dr[] to_dr_ = new dr[ syEntireRange ] {
        #region to_dr_
        dr.c_UF_, dr.c_UF_, dr.c_UF_, dr.c_UF_,
        dr.c_UF_, dr.d_UL_y3, dr.a_UB_y2, dr.b_UR_y1,
        dr.e_LU_x3z1, dr.f_LF_z1, dr.g_LD_x1z1, dr.h_LB_x2z1,
        dr.i_FU_x1y2, dr.j_FR_x1y1, dr.k_FD_x1, dr.l_FL_x1y3,
        dr.m_RU_x3z3, dr.n_RB_y2z, dr.o_RD_x1z3, dr.p_RF_z3,
        dr.q_BU_x3, dr.r_BL_x3y3, dr.s_BD_x3y2, dr.t_BR_x3y1,
        dr.u_DF_z2, dr.v_DR_x2y1, dr.w_DB_x2, dr.x_DL_x2y3,

        dr.c_UF_, dr.d_UL_y3, dr.a_UB_y2, dr.b_UR_y1,
        dr.e_LU_x3z1, dr.f_LF_z1, dr.g_LD_x1z1, dr.h_LB_x2z1,
        dr.i_FU_x1y2, dr.j_FR_x1y1, dr.k_FD_x1, dr.l_FL_x1y3,
        dr.m_RU_x3z3, dr.n_RB_y2z, dr.o_RD_x1z3, dr.p_RF_z3,
        dr.q_BU_x3, dr.r_BL_x3y3, dr.s_BD_x3y2, dr.t_BR_x3y1,
        dr.u_DF_z2, dr.v_DR_x2y1, dr.w_DB_x2, dr.x_DL_x2y3,

        dr.c_UF_, dr.d_UL_y3, dr.a_UB_y2, dr.b_UR_y1,
        dr.e_LU_x3z1, dr.f_LF_z1, dr.g_LD_x1z1, dr.h_LB_x2z1,
        dr.i_FU_x1y2, dr.j_FR_x1y1, dr.k_FD_x1, dr.l_FL_x1y3,
        dr.m_RU_x3z3, dr.n_RB_y2z, dr.o_RD_x1z3, dr.p_RF_z3,
        dr.q_BU_x3, dr.r_BL_x3y3, dr.s_BD_x3y2, dr.t_BR_x3y1,
        dr.u_DF_z2, dr.v_DR_x2y1, dr.w_DB_x2, dr.x_DL_x2y3,

        dr.c_UF_, dr.d_UL_y3, dr.a_UB_y2, dr.b_UR_y1,
        dr.e_LU_x3z1, dr.f_LF_z1, dr.g_LD_x1z1, dr.h_LB_x2z1,
        dr.i_FU_x1y2, dr.j_FR_x1y1, dr.k_FD_x1, dr.l_FL_x1y3,
        dr.m_RU_x3z3, dr.n_RB_y2z, dr.o_RD_x1z3, dr.p_RF_z3,
        dr.q_BU_x3, dr.r_BL_x3y3, dr.s_BD_x3y2, dr.t_BR_x3y1,
        dr.u_DF_z2, dr.v_DR_x2y1, dr.w_DB_x2, dr.x_DL_x2y3,

        dr.dr24
        #endregion
        };
        static dr to_dr( sy syi ){ return to_dr_[ (byte)( syi ) ]; }

        const byte drEntireRange= (byte)( (byte)( dr.dr24 ) + 1 );
        #region dr to section
        static sy[] from_dr_= new sy[ drEntireRange ]{
            sy.c_UF_, sy.d_UL_y3, sy.a_UB_y2, sy.b_UR_y1,
            sy.e_LU_x3z1, sy.f_LF_z1, sy.g_LD_x1z1, sy.h_LB_x2z1,
            sy.i_FU_x1y2, sy.j_FR_x1y1, sy.k_FD_x1, sy.l_FL_x1y3,
            sy.m_RU_x3z3, sy.n_RB_y2z, sy.o_RD_x1z3, sy.p_RF_z3,
            sy.q_BU_x3, sy.r_BL_x3y3, sy.s_BD_x3y2, sy.t_BR_x3y1,
            sy.u_DF_z2, sy.v_DR_x2y1, sy.w_DB_x2, sy.x_DL_x2y3,
            sy.sy100 };
        static sy[] from_dr_I= new sy[ drEntireRange ]{
            sy.I_c_UF_, sy.I_d_UL_y3, sy.I_a_UB_y2, sy.I_b_UR_y1,
            sy.I_e_LU_x3z1, sy.I_f_LF_z1, sy.I_g_LD_x1z1, sy.I_h_LB_x2z1,
            sy.I_i_FU_x1y2, sy.I_j_FR_x1y1, sy.I_k_FD_x1, sy.I_l_FL_x1y3,
            sy.I_m_RU_x3z3, sy.I_n_RB_y2z, sy.I_o_RD_x1z3, sy.I_p_RF_z3,
            sy.I_q_BU_x3, sy.I_r_BL_x3y3, sy.I_s_BD_x3y2, sy.I_t_BR_x3y1,
            sy.I_u_DF_z2, sy.I_v_DR_x2y1, sy.I_w_DB_x2, sy.I_x_DL_x2y3,
            sy.sy100 };
        static sy[] from_dr_R= new sy[ drEntireRange ]{
            sy.R_c_UF_, sy.R_d_UL_y3, sy.R_a_UB_y2, sy.R_b_UR_y1,
            sy.R_e_LU_x3z1, sy.R_f_LF_z1, sy.R_g_LD_x1z1, sy.R_h_LB_x2z1,
            sy.R_i_FU_x1y2, sy.R_j_FR_x1y1, sy.R_k_FD_x1, sy.R_l_FL_x1y3,
            sy.R_m_RU_x3z3, sy.R_n_RB_y2z, sy.R_o_RD_x1z3, sy.R_p_RF_z3,
            sy.R_q_BU_x3, sy.R_r_BL_x3y3, sy.R_s_BD_x3y2, sy.R_t_BR_x3y1,
            sy.R_u_DF_z2, sy.R_v_DR_x2y1, sy.R_w_DB_x2, sy.R_x_DL_x2y3,
            sy.sy100 };
        static sy[] from_dr_IR= new sy[ drEntireRange ]{
            sy.IR_c_UF_, sy.IR_d_UL_y3, sy.IR_a_UB_y2, sy.IR_b_UR_y1,
            sy.IR_e_LU_x3z1, sy.IR_f_LF_z1, sy.IR_g_LD_x1z1, sy.IR_h_LB_x2z1,
            sy.IR_i_FU_x1y2, sy.IR_j_FR_x1y1, sy.IR_k_FD_x1, sy.IR_l_FL_x1y3,
            sy.IR_m_RU_x3z3, sy.IR_n_RB_y2z, sy.IR_o_RD_x1z3, sy.IR_p_RF_z3,
            sy.IR_q_BU_x3, sy.IR_r_BL_x3y3, sy.IR_s_BD_x3y2, sy.IR_t_BR_x3y1,
            sy.IR_u_DF_z2, sy.IR_v_DR_x2y1, sy.IR_w_DB_x2, sy.IR_x_DL_x2y3,
            sy.sy100 };
        #endregion
        static sy from_dr( dr dri ){ return from_dr_[ (byte)( dri ) ]; }
        static sy from_dr( dr dri, sy sectioni ){
            switch( tosection( sectioni ) ){
            case sy.identity:  default:  return from_dr_[ (byte)( dri ) ];  //break;
            case sy.inverse: return from_dr_I[ (byte)( dri ) ];  //break;
            case sy.reflect: return from_dr_R[ (byte)( dri ) ];  //break;
            case sy.invReflect: return from_dr_IR[ (byte)( dri ) ];  //break;
            }
        }

       // computed inverse symmetry transform for a non-symmetrical position
        static sy[] finverse_= new sy[ syEntireRange ]{
        #region finverse
        sy.identity, sy.inverse, sy.reflect, sy.invReflect,
        sy.c_UF_, sy.b_UR_y1, sy.a_UB_y2, sy.d_UL_y3,
        sy.l_FL_x1y3, sy.p_RF_z3, sy.t_BR_x3y1, sy.h_LB_x2z1,
        sy.i_FU_x1y2, sy.m_RU_x3z3, sy.q_BU_x3, sy.e_LU_x3z1,
        sy.j_FR_x1y1, sy.n_RB_y2z, sy.r_BL_x3y3, sy.f_LF_z1,
        sy.k_FD_x1, sy.o_RD_x1z3, sy.s_BD_x3y2, sy.g_LD_x1z1,
        sy.u_DF_z2, sy.v_DR_x2y1, sy.w_DB_x2, sy.x_DL_x2y3,

        sy.I_c_UF_, sy.I_b_UR_y1, sy.I_a_UB_y2, sy.I_d_UL_y3,
        sy.I_l_FL_x1y3, sy.I_p_RF_z3, sy.I_t_BR_x3y1, sy.I_h_LB_x2z1,
        sy.I_i_FU_x1y2, sy.I_m_RU_x3z3, sy.I_q_BU_x3, sy.I_e_LU_x3z1,
        sy.I_j_FR_x1y1, sy.I_n_RB_y2z, sy.I_r_BL_x3y3, sy.I_f_LF_z1,
        sy.I_k_FD_x1, sy.I_o_RD_x1z3, sy.I_s_BD_x3y2, sy.I_g_LD_x1z1,
        sy.I_u_DF_z2, sy.I_v_DR_x2y1, sy.I_w_DB_x2, sy.I_x_DL_x2y3,

        sy.R_c_UF_, sy.R_d_UL_y3, sy.R_a_UB_y2, sy.R_b_UR_y1,
        sy.R_j_FR_x1y1, sy.R_f_LF_z1, sy.R_r_BL_x3y3, sy.R_n_RB_y2z,
        sy.R_i_FU_x1y2, sy.R_e_LU_x3z1, sy.R_q_BU_x3, sy.R_m_RU_x3z3,
        sy.R_l_FL_x1y3, sy.R_h_LB_x2z1, sy.R_t_BR_x3y1, sy.R_p_RF_z3,
        sy.R_k_FD_x1, sy.R_g_LD_x1z1, sy.R_s_BD_x3y2, sy.R_o_RD_x1z3,
        sy.R_u_DF_z2, sy.R_x_DL_x2y3, sy.R_w_DB_x2, sy.R_v_DR_x2y1,

        sy.IR_c_UF_, sy.IR_d_UL_y3, sy.IR_a_UB_y2, sy.IR_b_UR_y1,
        sy.IR_j_FR_x1y1, sy.IR_f_LF_z1, sy.IR_r_BL_x3y3, sy.IR_n_RB_y2z,
        sy.IR_i_FU_x1y2, sy.IR_e_LU_x3z1, sy.IR_q_BU_x3, sy.IR_m_RU_x3z3,
        sy.IR_l_FL_x1y3, sy.IR_h_LB_x2z1, sy.IR_t_BR_x3y1, sy.IR_p_RF_z3,
        sy.IR_k_FD_x1, sy.IR_g_LD_x1z1, sy.IR_s_BD_x3y2, sy.IR_o_RD_x1z3,
        sy.IR_u_DF_z2, sy.IR_x_DL_x2y3, sy.IR_w_DB_x2, sy.IR_v_DR_x2y1,   sy.sy100
        };
        #endregion
        public static sy finverse( sy syi ){ return finverse_[ (byte)( syi ) ]; }


        // extract the inverse and reflect flags as sections, as these commute independently, and compute the flag product
        static sy multsect( sy L, sy R ){
            sy Ls= tosection( L );  sy Rs= tosection( R ); // sy.identity, sy.inverse, sy.reflect, or sy.inverse_and_reflect
            if( Rs == sy.identity ){ return Ls; }
            switch ( Ls ){
            case sy.identity:  default:  return Rs;
            case sy.inverse:  switch ( Rs ){
                case sy.inverse:  default:  return sy.identity;
                case sy.reflect:  return sy.invReflect;
                case sy.invReflect:  return sy.reflect; }
                //break;  // unreachable as all cases return out of method
            case sy.reflect:  switch ( Rs ){
                case sy.inverse:  return sy.invReflect;
                case sy.reflect:  default: return sy.identity;
                case sy.invReflect: return sy.inverse; }
                //break; // unreachable as all cases return out of method
            case sy.invReflect:  switch ( Rs ){
                case sy.inverse:  return sy.reflect;
                case sy.reflect:  return sy.inverse;
                case sy.invReflect:  default: return sy.identity; }
                //break; // unreachable as all cases return out of method
            }
            // return sy.sy100; // unreachable as all cases return out of method
        }
        public static sy mult( sy L, sy R ){
            sy Rsect= section_[ (byte)( R ) ];  if( Rsect == sy.reflect || Rsect == sy.invReflect ){
                return from_dr( drT.mult( drT.reflectM( to_dr( L ) ), to_dr( R ) ), multsect( L, R ) );
            } else {
                return from_dr( drT.mult( to_dr( L ), to_dr( R ) ), multsect( L, R ) );
            }
        }
        public static sy mult( sy L, dr R ){
            return from_dr( drT.mult( to_dr( L ), R ), tosection( L ) );
        }

        string sectionToString(){
            switch ( syT.section_[ (byte)( sy_subject ) ] ){
            case sy.identity: default: return "";
            case sy.inverse: return "I_";
            case sy.reflect: return "R_";
            case sy.invReflect: return "IR_";
            }
        }

        string genToString(){
            System.Text.StringBuilder sb= new System.Text.StringBuilder();
            dr to_dr= to_dr_[ (byte)( sy_subject ) ];
            switch ( drT.gx( to_dr ) ){
                case 0:  default: break;
                case 1: sb.Append( "x" );  break;
                case 2: sb.Append( "x2" );  break;
                case 3: sb.Append( "x\'" );  break;
            }
            switch ( drT.gy( to_dr ) ){
                case 0:  default: break;
                case 1: sb.Append( "y" );  break;
                case 2: sb.Append( "y2" );  break;
                case 3: sb.Append( "y\'" );  break;
            }
            switch ( drT.gz( to_dr ) ){
                case 0:  default: break;
                case 1: sb.Append( "z" );  break;
                case 2: sb.Append( "z2" );  break;
                case 3: sb.Append( "z\'" );  break;
            }
            if ( sb.Length > 0 ){ return "_" + sb.ToString(); }
            return "";
        }

        public override string ToString(){
            sy section= sy.identity;
            if( to_dr_[ (byte)( sy_subject ) ] == dr.c_UF_ ){
                section= tosection( sy_subject );
                if( section != sy.identity ){ return section.ToString(); }
                return "";
            }
            return sectionToString()
                + singmaster.ee_e[ drT.dr_ef( to_dr_[ (byte)( sy_subject ) ] ) ]
                + genToString() ;
        }

        public static List< ft > mult( List< ft > given, sy S ){
            ft[] movescopy;
            int k;
            switch( tosection( S ) ){
            case sy.identity:  default:
                movescopy= given.ToArray();
                break;
            case sy.inverse:
                movescopy= new ft[ given.Count ];
                k= 0;
                for( int j= given.Count - 1; j > -1; j-- ){
                    movescopy[ k ]= ftT.invert( given[ j ] );
                    k+= 1;
                }  break;
            case sy.reflect:
                movescopy= new ft[ given.Count ];
                for( int j= 0; j < given.Count; j++ ){
                    movescopy[ j ]= ftT.reflectM( given[ j ] );
                }  break;
            case sy.invReflect:
                movescopy= new ft[ given.Count ];
                k= 0;
                for( int j= given.Count - 1; j > -1; j-- ){
                    movescopy[ k ]= ftT.reflectM( ftT.invert( given[ j ] ) );
                    k+= 1;
                }  break;
            }
            List< ft >toret= new List<ft>( given.Count );
            for( int i= 0; i < given.Count; i++ ){
                toret.Add( ftT.mult( movescopy[ i ], syT.to_dr( S ) ) );
            }
            return toret;
        }
    } // syT
} // namespace CubeTrae
