using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CubeTrae
{
    static class Program
    {
        public const string prevbuild= "05ed419e206aa2c187034c63a0600440009dda2b";
        //  "76b80a0830cc10b5d68fa51be332f2af60d5bf1a";
        //  "eb86bb7aa2dee0c3a9a51bb65315b774ab45d2b1";
        // "c9ab34f63769d6019438dae9a126a62cfac96083";
        
        public const string info= "\"BLD as a\" codename \"CubeTrae\" build over 2019.09.17 " + prevbuild;

        const string speffz=
            " \"speffz\" " // lettersystem name, speffz -- most common letter system
            + "ULB=AER UBR=BQN URF=CMJ UFL=DIF " // corner names
            + "LFD=GLU LDB=HXS FRD=KPV RBD=OTW "

            + "UB=AQ UR=BM UF=CI UL=DE LF=FL LD=GX " // edge names
            + "LB=HR FR=JP FD=KU RB=NT RD=OV BD=SW " // V o=UB o=UBR set preferred edge and corner buffer
            + "o=UB o=UBR @UF=DR s=0"; // buffers; reading orientation from scramble orientation; serial number

        /// <summary>
        /// catalog generation places emphasis on the _preferredorder_ turns, this for the author's left-handed preference -- for a 
        /// right-handed prefrence, "R" and "R'" can be placed first.
        /// </summary>
        public const string defaultpreferredorder= "L L' U' U D D' F F' R R' B B'";
        public static string preferredorder;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );

            singmaster.decodeSingmasterIni();  ssT.IniMoveTables();
            catalog.lscatalog= new lettersystemcatalog( 0, "speffz", speffz );

            System.IO.FileStream fs= null;
            System.IO.StreamReader sr= null;
            jsonv lettersin;
            try{ // bring in additional letter systems and override preferred order, if desired
                fs= System.IO.File.Open( "letters.json", System.IO.FileMode.Open, System.IO.FileAccess.Read );
                sr= new System.IO.StreamReader( fs, true );
                lettersin= new jsonv().Read( sr, 5 );
                foreach( object li in lettersin.asArray ){
                    if( li is List< object > ){
                        System.Text.StringBuilder sb= new System.Text.StringBuilder();
                        foreach( string lis in (List< object >)( li ) ){
                            sb.Append( lis ); sb.Append( " " );
                        }
                        catalog.lscatalog.add( sb.ToString() );
                    } else if( li is Dictionary< string, jsonv > ){
                        if( ((Dictionary< string, jsonv >)( li )).ContainsKey( "order" ) ){
                            preferredorder= ((Dictionary< string, jsonv >)( li ))[ "order" ].asString;
                        }
                    }
                }
            }
                
            catch( System.IO.FileNotFoundException ){ 
                preferredorder= defaultpreferredorder;
                sr= null; }
            finally{ if( (object)( fs ) != (object)( null ) ){ fs.Close(); }  fs= null;  sr= null; }
            singmaster.assignLetters( 0 );

            catalog.gen3cycles();
            parse.inifacechase();


            try{ // load in memory image list, activated on little-"m" command, i.e. mRS returns // rose
                fs= System.IO.File.Open( "mems.json", System.IO.FileMode.Open, System.IO.FileAccess.Read );
                sr= new System.IO.StreamReader( fs, true );
                catalog.associations= new jsonv().Read( sr, 5 );
            }
            catch( System.IO.FileNotFoundException ){ sr= null; }
            finally
            {
                fs.Close();
            }

            try {
                fs= System.IO.File.Open( "ini.txt", System.IO.FileMode.Open, System.IO.FileAccess.Read );
                sr= new System.IO.StreamReader( fs, true );
            }
            catch( System.IO.FileNotFoundException ){ sr= null; }
            finally
            {
                Application.Run( new CubeTraeIO( sr ) );
                fs.Close();
            }
        }
    }
}