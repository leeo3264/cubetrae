namespace CubeTrae
{
    partial class CubeTraeIO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.inTextBox = new System.Windows.Forms.TextBox();
            this.genButton = new System.Windows.Forms.Button();
            this.genBtnMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.parseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remCommentsTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToTopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReadingInterpretedTextBox = new System.Windows.Forms.TextBox();
            this.outTextBox = new System.Windows.Forms.TextBox();
            this.genBtnMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // inTextBox
            // 
            this.inTextBox.Location = new System.Drawing.Point(12, 6);
            this.inTextBox.Name = "inTextBox";
            this.inTextBox.Size = new System.Drawing.Size(341, 20);
            this.inTextBox.TabIndex = 0;
            this.inTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inTextBox_KeyDown);
            // 
            // genButton
            // 
            this.genButton.ContextMenuStrip = this.genBtnMenuStrip;
            this.genButton.Location = new System.Drawing.Point(313, 32);
            this.genButton.Name = "genButton";
            this.genButton.Size = new System.Drawing.Size(40, 23);
            this.genButton.TabIndex = 1;
            this.genButton.Text = "gen";
            this.genButton.UseVisualStyleBackColor = true;
            this.genButton.Click += new System.EventHandler(this.genButton_Click);
            // 
            // genBtnMenuStrip
            // 
            this.genBtnMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem1,
            this.parseToolStripMenuItem,
            this.testToolStripMenuItem,
            this.pasteListToolStripMenuItem,
            this.remCommentsTestToolStripMenuItem,
            this.copyToTopToolStripMenuItem});
            this.genBtnMenuStrip.Name = "genBtnMenuStrip";
            this.genBtnMenuStrip.Size = new System.Drawing.Size(210, 136);
            // 
            // newToolStripMenuItem1
            // 
            this.newToolStripMenuItem1.Name = "newToolStripMenuItem1";
            this.newToolStripMenuItem1.Size = new System.Drawing.Size(209, 22);
            this.newToolStripMenuItem1.Text = "New";
            this.newToolStripMenuItem1.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // parseToolStripMenuItem
            // 
            this.parseToolStripMenuItem.Name = "parseToolStripMenuItem";
            this.parseToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.parseToolStripMenuItem.Text = "Parse";
            this.parseToolStripMenuItem.Click += new System.EventHandler(this.parseToolStripMenuItem_Click);
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.testToolStripMenuItem.Text = "Test";
            this.testToolStripMenuItem.Click += new System.EventHandler(this.testToolStripMenuItem_Click);
            // 
            // pasteListToolStripMenuItem
            // 
            this.pasteListToolStripMenuItem.Name = "pasteListToolStripMenuItem";
            this.pasteListToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.pasteListToolStripMenuItem.Text = "Paste List";
            this.pasteListToolStripMenuItem.Click += new System.EventHandler(this.pasteListToolStripMenuItem_Click);
            // 
            // remCommentsTestToolStripMenuItem
            // 
            this.remCommentsTestToolStripMenuItem.Name = "remCommentsTestToolStripMenuItem";
            this.remCommentsTestToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.remCommentsTestToolStripMenuItem.Text = "Rem Comments Test";
            this.remCommentsTestToolStripMenuItem.Click += new System.EventHandler(this.remCommentsTestToolStripMenuItem_Click);
            // 
            // copyToTopToolStripMenuItem
            // 
            this.copyToTopToolStripMenuItem.Name = "copyToTopToolStripMenuItem";
            this.copyToTopToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.copyToTopToolStripMenuItem.Text = "Copy to Top";
            this.copyToTopToolStripMenuItem.Click += new System.EventHandler(this.copyToTopToolStripMenuItem_Click);
            // 
            // ReadingInterpretedTextBox
            // 
            this.ReadingInterpretedTextBox.Location = new System.Drawing.Point(12, 61);
            this.ReadingInterpretedTextBox.Name = "ReadingInterpretedTextBox";
            this.ReadingInterpretedTextBox.Size = new System.Drawing.Size(341, 20);
            this.ReadingInterpretedTextBox.TabIndex = 2;
            // 
            // outTextBox
            // 
            this.outTextBox.Location = new System.Drawing.Point(12, 87);
            this.outTextBox.Name = "outTextBox";
            this.outTextBox.Size = new System.Drawing.Size(341, 20);
            this.outTextBox.TabIndex = 4;
            // 
            // CubeTraeIO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(367, 114);
            this.Controls.Add(this.outTextBox);
            this.Controls.Add(this.ReadingInterpretedTextBox);
            this.Controls.Add(this.genButton);
            this.Controls.Add(this.inTextBox);
            this.Name = "CubeTraeIO";
            this.Text = "CubeTrae";
            this.SizeChanged += new System.EventHandler(this.CubeTraeIO_SizeChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CubeTraeIO_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CubeTraeIO_KeyDown);
            this.genBtnMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox inTextBox;
        private System.Windows.Forms.Button genButton;
        private System.Windows.Forms.TextBox ReadingInterpretedTextBox;
        private System.Windows.Forms.TextBox outTextBox;
        private System.Windows.Forms.ContextMenuStrip genBtnMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remCommentsTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToTopToolStripMenuItem;
    }
}

